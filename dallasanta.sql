﻿# Host: localhost  (Version: 5.6.17)
# Date: 2015-08-03 10:30:22
# Generator: MySQL-Front 5.3  (Build 4.214)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "area_restrita_usuario"
#

CREATE TABLE `area_restrita_usuario` (
  `email` varchar(150) NOT NULL,
  `senha` varchar(34) NOT NULL DEFAULT '',
  `nome` varchar(80) NOT NULL,
  `sobrenome` varchar(80) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `estado` varchar(75) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `descricao` text,
  `token_recuperar_senha` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "area_restrita_usuario"
#

INSERT INTO `area_restrita_usuario` VALUES ('italo@reweb.com.br','$1$QG5.Zu3.$ZZs6xeuxGF7UK.VYdf7xq/','italo','brunoro',NULL,'(33) 3333.33333',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('sandrogriebeler@gmail.com','$1$eW4.Pm..$OWg6HShGUG.lRsdNMFbSd1','Sandro','Griebeler','avatar-sandro-griebeler-1438024192.png','(51) 3561.0460','(51) 9961.9493','Rio Grande do Sul','Estância Velha','Centro','Rua Anita Garibaldi, 478',NULL,'teste',NULL),('sandrogriebeler@hotmail.com','$1$cM0./x/.$q4YTTtMzmb2ajNvSjY7Xk.','Sandro Augusto','Griebeler','avatar-sandro-augusto-griebeler-1436892275.jpg','(51) 9961.3779',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "area_restrita_usuario_favorito"
#

CREATE TABLE `area_restrita_usuario_favorito` (
  `email` varchar(150) NOT NULL DEFAULT '',
  `id_imovel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email`,`id_imovel`),
  CONSTRAINT `fk_usuario_email` FOREIGN KEY (`email`) REFERENCES `area_restrita_usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "area_restrita_usuario_favorito"
#

INSERT INTO `area_restrita_usuario_favorito` VALUES ('sandrogriebeler@gmail.com',343),('sandrogriebeler@gmail.com',346),('sandrogriebeler@gmail.com',354),('sandrogriebeler@gmail.com',412);

#
# Structure for table "banner"
#

CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `titulo` varchar(255) NOT NULL DEFAULT '',
  `subtitulo` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `descricao` text,
  `status` tinyint(4) DEFAULT '1',
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`tipo_id`),
  KEY `index3` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "banner"
#

INSERT INTO `banner` VALUES (1,1,'lux-office','LUX OFFICE','','http://www.dallasanta.com.br/',0,'',1,'2015-07-10 15:21:19'),(2,1,'banner-2','Banner 2','','http://www.dallasanta.com.br/',1,'',1,'2015-07-10 16:06:25'),(3,1,'titulo-teste','titulo teste','subtitulo teste','',2,'&lt;p&gt;lorem ipsum&lt;/p&gt;',1,'2015-07-29 10:44:37');

#
# Structure for table "bannertipo"
#

CREATE TABLE `bannertipo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "bannertipo"
#

INSERT INTO `bannertipo` VALUES (1,'home-conceitual','Home Conceitual',1,'2015-07-10 15:19:46');

#
# Structure for table "blog_category"
#

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "blog_category"
#


#
# Structure for table "blog"
#

CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `arquivo_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text,
  `date` datetime DEFAULT NULL,
  `date_register` datetime DEFAULT NULL,
  `video` text,
  `image` varchar(255) DEFAULT NULL,
  `destaque` char(1) DEFAULT '1',
  `status` char(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_blog_blog_category1_idx` (`category_id`),
  KEY `fk_blog_blog_arquivos1_idx` (`arquivo_id`),
  CONSTRAINT `fk_blog_blog_category1` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "blog"
#


#
# Structure for table "cliente"
#

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `url_site` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Data for table "cliente"
#

INSERT INTO `cliente` VALUES (1,'Auxiliadora Predial','img_predial.png',NULL),(2,'Imobiliária Ducati','img_ducati.png',NULL),(3,'Luagge Imóveis','img_luagge.png',NULL),(4,'Foxter Cia. Imobiliária','img_foxter.png',NULL),(5,'Guarida Imóveis','img_guarida.png',NULL),(6,'Teste','88566c49cc494dd2a6a9964bcc91e74d.jpg','http://teste.com');

#
# Structure for table "conteudo"
#

CREATE TABLE `conteudo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `descricao` text,
  `ordem` tinyint(2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for table "conteudo"
#

INSERT INTO `conteudo` VALUES (4,4,'a-dallasanta','A Dallasanta','','','&lt;h4&gt;SOBRE A EMPRESA&lt;/h4&gt;\r\n&lt;p&gt;A Dallasanta &amp;eacute; uma empresa focada na comercializa&amp;ccedil;&amp;atilde;o de im&amp;oacute;veis pr&amp;oacute;prios para loca&amp;ccedil;&amp;atilde;o e aquisi&amp;ccedil;&amp;atilde;o de ativos para renda. Atuamos no segmento de Im&amp;oacute;veis Comerciais, desde a Incorpora&amp;ccedil;&amp;atilde;o, Constru&amp;ccedil;&amp;atilde;o, Loca&amp;ccedil;&amp;atilde;o e Administra&amp;ccedil;&amp;atilde;o de Empreendimentos pr&amp;oacute;prios. Aliamos a expertise no segmento, com a filosofia empresarial de prestar servi&amp;ccedil;o com qualidade, atendimento personalizado, &amp;eacute;tica profissional e comprometimento com os clientes, buscando atender suas expectativas imobili&amp;aacute;rias.&amp;nbsp;&lt;/p&gt;\r\n&lt;h4&gt;OBJETIVO&lt;/h4&gt;\r\n&lt;p&gt;&quot;Atendimento personalizado e qualificado na busca e administra&amp;ccedil;&amp;atilde;o de im&amp;oacute;veis comerciais, gerando resultado financeiro e relacionamento com clientes.&quot;&amp;nbsp;&lt;/p&gt;\r\n&lt;h4&gt;DIFERENCIAIS&lt;/h4&gt;\r\n&lt;ul&gt;\r\n&lt;li&gt;A Dallasanta &amp;eacute; incorporadora e construtora na maioria de seus empreendimentos. Isso proporciona uma tranquilidade ao futuro inquilino ocupante, uma vez que todos os relacionamentos com o cliente ser&amp;atilde;o feitos diretamente com o propriet&amp;aacute;rio do im&amp;oacute;vel;&lt;/li&gt;\r\n&lt;li&gt;Estrutura integrada de servi&amp;ccedil;os, arquitetura, engenharia, comercializa&amp;ccedil;&amp;atilde;o de im&amp;oacute;veis para loca&amp;ccedil;&amp;atilde;o, administra&amp;ccedil;&amp;atilde;o de ativos im&amp;oacute;veis pr&amp;oacute;prios e assessoria jur&amp;iacute;dica especializada;&lt;/li&gt;\r\n&lt;li&gt;Uma das maiores carteiras de im&amp;oacute;veis comerciais pr&amp;oacute;prios para loca&amp;ccedil;&amp;atilde;o em Porto Alegre;&lt;/li&gt;\r\n&lt;li&gt;Acompanhamento total em todas as visitas aos im&amp;oacute;veis;&lt;/li&gt;\r\n&lt;li&gt;Equipe especializada e com Know-How no segmento de im&amp;oacute;veis comerciais;&lt;/li&gt;\r\n&lt;li&gt;Atuamos com foco no Core Business de nossos clientes, desenvolvendo oportunidades de neg&amp;oacute;cios imobili&amp;aacute;rios para amplia&amp;ccedil;&amp;atilde;o ou expans&amp;atilde;o de sua empresa, de acordo com a necessidade;&lt;/li&gt;\r\n&lt;li&gt;Estrutura&amp;ccedil;&amp;atilde;o e desenvolvimento de opera&amp;ccedil;&amp;otilde;es de loca&amp;ccedil;&amp;atilde;o Built-to-suit.&lt;/li&gt;\r\n&lt;/ul&gt;',0,1,'2015-07-29 15:49:04','https://www.youtube.com/watch?v=QvYmG-4FQg8&index=11&list=RD5lCddFcFmNg');

#
# Structure for table "conteudotipo"
#

CREATE TABLE `conteudotipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` tinyint(2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`pai_id`),
  KEY `index3` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for table "conteudotipo"
#

INSERT INTO `conteudotipo` VALUES (4,NULL,'a-dallasanta','A Dallasanta',NULL,1,'2015-07-29 15:47:24');

#
# Structure for table "core_acl_acos"
#

CREATE TABLE `core_acl_acos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1012 DEFAULT CHARSET=utf8;

#
# Data for table "core_acl_acos"
#

INSERT INTO `core_acl_acos` VALUES (707,NULL,'Admin_acl'),(710,NULL,'Admin_banner'),(711,NULL,'Admin_bannertype'),(714,NULL,'Admin_config'),(715,NULL,'Admin_content'),(716,NULL,'Admin_contenttype'),(717,NULL,'Admin_dashboard'),(720,NULL,'Admin_help'),(721,NULL,'Admin_login'),(722,NULL,'Admin_newsletter'),(728,NULL,'Admin_page'),(729,NULL,'Admin_phpinfo'),(744,NULL,'Admin_staticblock'),(750,NULL,'Admin_url'),(751,NULL,'Admin_user'),(752,NULL,'Admin_usergroup'),(754,NULL,'Error404'),(755,NULL,'Home'),(756,NULL,'Sitemap'),(757,NULL,'Swfupload'),(758,707,'index'),(759,707,'save'),(760,707,'refact'),(769,710,'index'),(770,710,'edit'),(771,710,'save'),(772,710,'delete'),(773,711,'index'),(774,711,'edit'),(775,711,'save'),(776,711,'delete'),(785,714,'index'),(786,714,'save'),(787,715,'index'),(788,715,'edit'),(789,715,'save'),(790,715,'delete'),(791,716,'index'),(792,716,'edit'),(793,716,'save'),(794,716,'delete'),(795,717,'index'),(803,720,'index'),(804,721,'index'),(805,721,'logon'),(806,721,'logout'),(807,722,'index'),(808,722,'edit'),(809,722,'save'),(810,722,'delete'),(831,728,'index'),(832,728,'edit'),(833,728,'save'),(834,728,'delete'),(835,729,'index'),(892,744,'index'),(893,744,'edit'),(894,744,'save'),(895,744,'delete'),(917,750,'index'),(918,750,'edit'),(919,750,'save'),(920,750,'delete'),(921,751,'index'),(922,751,'edit'),(923,751,'save'),(924,751,'delete'),(925,752,'index'),(926,752,'edit'),(927,752,'save'),(928,752,'delete'),(930,754,'index'),(931,755,'index'),(932,756,'index'),(933,757,'index'),(934,757,'save_param'),(935,757,'delete_file'),(936,757,'get_detail'),(937,757,'edit'),(938,757,'save_edit'),(939,757,'order'),(940,757,'upload'),(941,757,'thumbnail'),(943,NULL,'Parceiros'),(944,NULL,'Sobre'),(947,943,'index'),(948,944,'index'),(950,NULL,'Empreendimentos'),(951,950,'index'),(952,950,'detalhes'),(955,NULL,'Area_do_cliente'),(956,955,'index'),(957,955,'meus_dados'),(958,955,'cadastro'),(960,955,'meus_favoritos'),(961,NULL,'Fale_conosco'),(962,961,'index'),(963,961,'assessoria_imprensa'),(964,961,'manutencao'),(965,NULL,'Imoveis'),(966,965,'index'),(967,965,'venda'),(968,965,'aluguel'),(969,965,'detalhes'),(970,965,'comparar'),(971,965,'busca'),(972,955,'login'),(973,955,'logout'),(974,955,'create_session'),(976,955,'salvar'),(977,NULL,'Contact'),(978,955,'favorito'),(982,NULL,'Documentos'),(983,982,'index'),(984,NULL,'Venda_seu_imovel'),(985,984,'index'),(987,961,'trabalhe_conosco'),(988,961,'oferecer_imovel'),(989,961,'alugar_imovel'),(990,961,'fornecedor'),(991,961,'segunda_via'),(992,965,'ordem'),(993,NULL,'Admin_parceiro'),(994,993,'index'),(995,993,'edit'),(996,993,'save'),(997,993,'delete'),(998,NULL,'Admin_cliente'),(999,998,'index'),(1000,998,'edit'),(1001,998,'save'),(1002,998,'delete'),(1003,955,'recuperar_senha'),(1006,955,'view_default'),(1007,977,'send'),(1008,977,'newsletter'),(1009,965,'download_fotos'),(1011,977,'index');

#
# Structure for table "core_config"
#

CREATE TABLE `core_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `option` text,
  `value` text,
  `group` text,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `input_type` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

#
# Data for table "core_config"
#

INSERT INTO `core_config` VALUES (1,'meta_tag_title','Dallasanta','Meta_Tags','Title',NULL,'text',1),(2,'meta_tag_description','Dallasanta','Meta_Tags','Description',NULL,'textarea',1),(3,'meta_tag_keywords','Dallasanta','Meta_Tags','Keywords',NULL,'text',1),(4,'meta_tag_spam','Dallasanta','Meta_Tags','Spam',NULL,'textarea',1),(5,'meta_tag_author','Reweb Informática Ltda','Meta_Tags','Autor',NULL,'text',1),(6,'meta_tag_copryright','Copyright Reweb Informática Ltda','Meta_Tags','Copyright',NULL,'text',1),(7,'facebook_og_type','Website','Facebook','og_type',NULL,'text',1),(8,'facebook_og_image','','Facebook','og_image',NULL,'image',1),(9,'facebook_og_site_name','','Facebook','og_site_name',NULL,'text',1),(13,'meta_tag_backend_title','Gerenciador de conteúdos','Meta_Tags_Backend','Title',NULL,'text',1),(14,'meta_tag_backend_description','Gerenciador de conteúdos','Meta_Tags_Backend','Description',NULL,'textarea',1),(16,'meta_tag_backend_keywords','Keywords','Meta_Tags_Backend','Keywords',NULL,'text',1),(17,'meta_tag_backend_spam','Spam','Meta_Tags_Backend','Spam',NULL,'textarea',1),(18,'meta_tag_backend_author','Reweb Informática Ltda','Meta_Tags_Backend','Autor',NULL,'text',1),(19,'meta_tag_backend_copryright','Copyright Reweb Informática Ltda','Meta_Tags_Backend','copyright',NULL,'text',1),(20,'general_client','Dallasanta','Geral','Nome do Cliente',NULL,'text',1),(21,'general_logo','c81d554b36f394da216a207e2a73c088.png','Geral','Logo',NULL,'image',1),(22,'general_url','http://www.reweb.com.br','Geral','URL',NULL,'text',1),(23,'facebook_fb_admins','Fb admins','Facebook','fb_admins',NULL,'text',1),(24,'facebook_app_id','','Facebook','APP ID',NULL,'text',1),(25,'facebook_token','','Facebook','Token',NULL,'text',1),(26,'social_facebook','','Redes_Sociais','Facebook',NULL,'text',1),(27,'social_twitter','','Redes_Sociais','Twitter',NULL,'text',1),(28,'social_googleplus','','Redes_Sociais','Google plus',NULL,'text',1),(29,'social_pinterest','','Redes_Sociais','Pinterest',NULL,'text',1),(30,'social_blog','','Redes_Sociais','Blog',NULL,'text',1),(31,'social_foursquare','','Redes_Sociais','Foursquare',NULL,'text',1),(32,'social_youtube','','Redes_Sociais','Youtube',NULL,'text',1),(33,'social_slideshare','','Redes_Sociais','Slideshare',NULL,'text',1);

#
# Structure for table "core_page"
#

CREATE TABLE `core_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `binding` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "core_page"
#


#
# Structure for table "core_sessions"
#

CREATE TABLE `core_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "core_sessions"
#

INSERT INTO `core_sessions` VALUES ('056cc96cd2b7219346b87de4dad1a131','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437144457,''),('08957f3eb5dc32c29395e61ea3fb576f','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436826026,'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"954db5125f34794b1d2931a3553e7e50\";}s:21:\"area_restrita_captcha\";s:7:\"1jD4HgB\";s:13:\"area_restrita\";O:8:\"stdClass\":3:{s:4:\"nome\";s:16:\"Sandro Griebeler\";s:5:\"email\";s:25:\"sandrogriebeler@gmail.com\";s:4:\"foto\";s:7:\"jax.png\";}}'),('08da4780927806be0acf2a77fd19ab5f','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',1438285993,''),('11f4e1c55d95760aa7eca10bea54f8cb','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436374963,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('1eb5ab7cf3ff8cc868e4ee73de9ef487','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437419221,'a:2:{s:9:\"user_data\";s:0:\"\";s:6:\"filtro\";a:6:{s:11:\"tipo-imovel\";s:7:\"Aluguel\";s:7:\"bairros\";a:1:{i:0;s:6:\"Azenha\";}s:10:\"categorias\";a:2:{i:0;s:4:\"Casa\";i:1;s:9:\"Depósito\";}s:7:\"cidades\";a:1:{i:0;s:12:\"Porto Alegre\";}s:13:\"palavra-chave\";s:0:\"\";s:6:\"codigo\";s:0:\"\";}}'),('1f62f07becb0855d55ba246c5370f1f7','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',1438174981,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"3465fe56e66ebf747e0ec2b9c1f4504c\";}}'),('1febcffd32f9c0bb6727ca6ec981d854','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436803852,''),('205a1505dba84f95e40b10d8100575a2','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',1438348770,''),('2f2d41733b62e22a39188ca6f6ccc9f5','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437509367,''),('3a558881c8913d83f35e7688b28b77a2','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436380185,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"3a558881c8913d83f35e7688b28b77a2\";}}'),('3c6c4dd8b5883dc124c0174bb8dc8262','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437510785,''),('43ff42674c0de4616eef85aa8e13f76a','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',1438265179,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"9824ce599c9c9f3330d5d0ea11899fa6\";}}'),('444308e2c6b8202cac5a60342f2d8e5e','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437143644,''),('4654f0ac2fa4e6ec65e4eee979e6385c','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',1438116696,''),('4916072fdad984c2c6753d5a271375f3','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436795255,''),('510a7f62c48530a7af0623a1551b0d25','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437684673,'a:3:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"510a7f62c48530a7af0623a1551b0d25\";}s:15:\"flash:old:alert\";a:2:{s:4:\"type\";s:7:\"success\";s:3:\"msg\";a:1:{i:0;s:25:\"Dados salvos com sucesso!\";}}}'),('54690379de977bcbd6dd55afbd30a1b5','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436377666,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"54690379de977bcbd6dd55afbd30a1b5\";}}'),('591cc787a9f81df5a9de519de32288e4','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437506536,''),('5ddd16f77345c23a1ba6b02499fd9107','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436559890,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"5ddd16f77345c23a1ba6b02499fd9107\";}}'),('60945434659f306e62363268049e384e','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',1438194518,''),('62e2678a9adda79b93d4fc24473a86d9','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436455335,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('63c4b961010be473ddf710d6d460bea1','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436463557,''),('6751ccd21a941f5634611015cb382e25','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36',1438001535,''),('6eab1aecd614bbe6c9eb1f24978c54a2','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437406027,''),('6fba26fbc7747a82fee5d899a6a35cdf','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436455367,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('70bdad0ab1129f34803859fee3342baa','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',1438367381,''),('73465612c70ece630602b5c69999130d','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36',1438001530,'a:3:{s:9:\"user_data\";s:0:\"\";s:7:\"captcha\";s:7:\"pXGsjxn\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"ed1c79e403f2f91bfbeb8381a286d1be\";}}'),('744c70c5ee828f0a0010a1a558d3a090','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36',1438108675,'a:4:{s:9:\"user_data\";s:0:\"\";s:7:\"captcha\";s:7:\"iPpeyaC\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"937735971a46cbccddf03fd6dbd02122\";}s:13:\"area_restrita\";O:8:\"stdClass\":3:{s:4:\"nome\";s:16:\"Sandro Griebeler\";s:5:\"email\";s:25:\"sandrogriebeler@gmail.com\";s:4:\"foto\";s:38:\"avatar-sandro-griebeler-1438024192.png\";}}'),('7cc487f025fb70940d0574980971ab0c','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437404713,''),('83a5a8024ccb26d51c3bf63b4d4aa7f5','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437742318,'a:3:{s:13:\"area_restrita\";O:8:\"stdClass\":3:{s:4:\"nome\";s:16:\"Sandro Griebeler\";s:5:\"email\";s:25:\"sandrogriebeler@gmail.com\";s:4:\"foto\";s:38:\"avatar-sandro-griebeler-1436991452.png\";}s:7:\"captcha\";s:7:\"oGntbhx\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"1a333566efdc9696945bbdbee3cf254e\";}}'),('8a64cc02f9fb82604ce69390b31223d1','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36',1437761594,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('8bc336a9dfba3c6227f1081876afa345','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437156975,''),('9023931200e836c8dddc3b6c574fa19f','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',1438194506,''),('94e7784d47053c05b0b29597e5afa1f5','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1437066252,'a:5:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"7cfdc7a195c45990f9e526070788fda6\";}s:21:\"area_restrita_captcha\";s:7:\"7rSplJV\";s:7:\"captcha\";s:7:\"Mnz0njT\";s:13:\"area_restrita\";O:8:\"stdClass\":3:{s:4:\"nome\";s:16:\"Sandro Griebeler\";s:5:\"email\";s:25:\"sandrogriebeler@gmail.com\";s:4:\"foto\";s:38:\"avatar-sandro-griebeler-1436991452.png\";}}'),('9a301ac96129d69dc09dcaccfb8fad9d','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',1438348810,''),('9b866cca5c835146e9c422177c8df5e3','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436376619,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('a5e9a5a556c1b5f515546de45c64c91f','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437509590,''),('ac2e056144b9545c9842bec16c1d8693','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437499410,'a:3:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"a192250bebd9df99acb147e41a533698\";}s:13:\"area_restrita\";O:8:\"stdClass\":3:{s:4:\"nome\";s:16:\"Sandro Griebeler\";s:5:\"email\";s:25:\"sandrogriebeler@gmail.com\";s:4:\"foto\";s:38:\"avatar-sandro-griebeler-1436991452.png\";}}'),('ac32b2369416de1120662c4d52b1cffe','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437406035,''),('b279bfaae736cda1c5d617b3925f9ade','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436455749,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"b279bfaae736cda1c5d617b3925f9ade\";}}'),('b6e92ddb5244a8fed3c694d80a1e75a1','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',1436376545,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('b7b48bb9c3b5fec7ce6280f83ad9fb9e','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',1438179104,'a:2:{s:9:\"user_data\";s:0:\"\";s:28:\"flash:old:newsletter_success\";s:38:\"Seu e-mail foi cadastrado com sucesso!\";}'),('b7d006b424c6ee40de297d56b12c51e3','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36',1438607160,'a:4:{s:9:\"user_data\";s:0:\"\";s:7:\"captcha\";s:7:\"mURJARC\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"d089f4181275b21d20de3c75c75fe019\";}s:13:\"area_restrita\";O:8:\"stdClass\":3:{s:4:\"nome\";s:16:\"Sandro Griebeler\";s:5:\"email\";s:25:\"sandrogriebeler@gmail.com\";s:4:\"foto\";s:38:\"avatar-sandro-griebeler-1438024192.png\";}}'),('b8e5c0c99260c01ee0ee2e980fe11783','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436550304,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"b8e5c0c99260c01ee0ee2e980fe11783\";}}'),('c06adf15eb22b1d07753bb03faeb1f1f','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437152172,'a:1:{s:7:\"captcha\";s:7:\"tnD3v0V\";}'),('c7a36c7963b3cb37bfad2e37593c932d','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437398974,'a:1:{s:9:\"user_data\";s:0:\"\";}'),('ca967cf0ad3b3c937f432a1d84a91d5c','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',1438368756,''),('d547d1b8d8f5f30e76f2192a5a1d8a7d','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436376738,'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"d547d1b8d8f5f30e76f2192a5a1d8a7d\";}}'),('d5865e982a1aa7cda7dd35617410f124','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437506979,''),('e00b4d703b85d259e2ea6fecc34c1944','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',1438368553,''),('e0cdac320ab6811ff1fd1dc313a80c28','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437141735,'a:3:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:9:{s:7:\"user_id\";s:1:\"3\";s:14:\"user_client_id\";N;s:9:\"user_name\";s:5:\"Admin\";s:10:\"user_email\";s:18:\"admin@reweb.com.br\";s:9:\"user_nick\";s:5:\"admin\";s:13:\"user_group_id\";s:1:\"1\";s:16:\"user_group_title\";s:10:\"superadmin\";s:10:\"user_token\";s:40:\"77de68daecd823babbb58edb1c8e14d7106e83bb\";s:17:\"user_session_auth\";s:32:\"36e8638bc322983f3d134c75e17347b3\";}s:7:\"captcha\";s:7:\"Pwffg4V\";}'),('e48e203c1e765a279b24527aeef3521f','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437406033,''),('e5adb532a0da2c5328f13132e4a6c8ca','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36',1438175514,''),('fef421a6711ff105908c479251ae37c4','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36',1436796528,''),('ff2c7a076f63b2fbd3df9eb0aa574d9a','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36',1437404574,'');

#
# Structure for table "core_staticblock"
#

CREATE TABLE `core_staticblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `binding` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "core_staticblock"
#

INSERT INTO `core_staticblock` VALUES (1,'Clientes','clientes-footer','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh'),(2,'Sobre a Dallasanta','sobre-a-dallasanta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue diam eget luctus rutrum. Donec non massa nunc. Nullam et accumsan neque, vel rhoncus velit. Quisque tortor sapien, pretium a vestibulum elementum, lacinia ut dolor. Nam eu purus ac purus mattis fringilla quis a sem. In nec lorem massa. Maecenas iaculis, urna at dignissim dapibus, ipsum elit bibendum enim, at mollis velit mi ullamcorper nisi. Curabitur eleifend, orci non mattis molestie, lorem nisl iaculis neque, in porttitor ante mi eget lacus. Integer ipsum massa, malesuada vel tellus sit amet, viverra pellentesque justo. In ac dui aliquam, congue quam aliquam, pellentesque lectus. Nunc mollis maximus elit.');

#
# Structure for table "core_url"
#

CREATE TABLE `core_url` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` text,
  `title` text,
  `keywords` text,
  `description` text,
  `url_old` text,
  `spam` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "core_url"
#

INSERT INTO `core_url` VALUES (1,'','Dallasanta','','','home',''),(2,'imoveis/aluguel','Imóveis para alugar','','','imoveis/busca/aluguel',''),(3,'imoveis/venda','Imoveis a venda','','','imoveis/busca/venda','');

#
# Structure for table "empreendimento_tipo"
#

CREATE TABLE `empreendimento_tipo` (
  `id_tipo_empreendimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tipo_empreendimento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "empreendimento_tipo"
#

INSERT INTO `empreendimento_tipo` VALUES (1,'MALLS E SHOPPING CENTERS'),(2,'LOJAS');

#
# Structure for table "empreendimento"
#

CREATE TABLE `empreendimento` (
  `id_empreendimento` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_empreendimento` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_empreendimento`),
  UNIQUE KEY `nome` (`nome`),
  KEY `fk_id_tipo_empreendimento` (`id_tipo_empreendimento`),
  CONSTRAINT `fk_id_tipo_empreendimento` FOREIGN KEY (`id_tipo_empreendimento`) REFERENCES `empreendimento_tipo` (`id_tipo_empreendimento`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

#
# Data for table "empreendimento"
#

INSERT INTO `empreendimento` VALUES (1,1,'lorem-ipsum-dolar-absurm','LOREM IPSUM DOLAR ABSURM','img_developments.jpg'),(5,1,'lorem-ipsum-dolar-absurm-2','LOREM IPSUM DOLAR ABSURM 2','img_developments2.jpg'),(6,2,'lorem-ipsum-dolar-absurm-3','LOREM IPSUM DOLAR ABSURM 3','img_developments.jpg'),(7,2,'lorem-ipsum-dolar-absurm-4','LOREM IPSUM DOLAR ABSURM 4','img_developments2.jpg'),(9,1,'lorem-ipsum-dolar-absurm-5','LOREM IPSUM DOLAR ABSURM','img_developments2.jpg'),(10,1,'lorem-ipsum-dolar-absurm-6','LOREM IPSUM DOLAR ABSURM','img_developments.jpg');

#
# Structure for table "gallery"
#

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `rel` varchar(255) NOT NULL,
  `rel_id` int(11) NOT NULL,
  `color` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `subtype` varchar(45) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '999',
  `status` int(11) DEFAULT '1',
  `date_register` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138054 DEFAULT CHARSET=utf8;

#
# Data for table "gallery"
#

INSERT INTO `gallery` VALUES (138039,NULL,'banner',1,NULL,NULL,NULL,'46f459ad46ae620ead56bb440fd0c0af.jpg',999,1,'2015-07-10 15:25:24','2015-07-10 16:29:18'),(138041,NULL,'banner',2,NULL,NULL,NULL,'ebb1d3b7fc6db6b3e92c0423d5b01145.jpg',999,1,'2015-07-10 16:27:47','2015-07-10 16:29:23'),(138042,NULL,'banner',3,NULL,NULL,NULL,'bf2bac48f5fe2815aea71d6f2830fdd0.jpg',999,1,'2015-07-29 10:44:37','2015-07-29 10:44:37'),(138043,NULL,'conteudo',4,NULL,NULL,NULL,'b77003a6d340e3149a0d17e7eb5cab3e.png',6,1,'2015-07-29 15:52:09','2015-07-29 17:26:22'),(138044,NULL,'conteudo',4,NULL,NULL,NULL,'356eaa41afb869a66833dccd4852af65.png',4,1,'2015-07-29 15:52:09','2015-07-29 17:26:22'),(138045,NULL,'conteudo',4,NULL,NULL,NULL,'f15922dd3cafa6504c6f3574dde1de57.jpg',8,1,'2015-07-29 16:23:12','2015-07-29 17:26:22'),(138048,NULL,'conteudo',4,NULL,NULL,NULL,'48d84c11c490b34175a1214b2d176516.png',1,1,'2015-07-29 16:32:36','2015-07-29 17:26:22'),(138049,NULL,'conteudo',4,NULL,NULL,NULL,'be4e253e5bc4731870840704c2676e6e.jpg',3,1,'2015-07-29 16:32:36','2015-07-29 17:26:22'),(138050,NULL,'conteudo',4,NULL,NULL,NULL,'51cbc9412450ec377998ee277fa8ba09.png',7,1,'2015-07-29 16:32:36','2015-07-29 17:26:22'),(138051,NULL,'conteudo',4,NULL,NULL,NULL,'e6b8ffd89a8d395788a54f8684b68866.png',9,1,'2015-07-29 16:32:37','2015-07-29 17:26:22'),(138052,NULL,'conteudo',4,NULL,NULL,NULL,'2e077f3c3365e41b48df8ee12203b875.png',5,1,'2015-07-29 16:32:37','2015-07-29 17:26:22'),(138053,NULL,'conteudo',4,NULL,NULL,NULL,'83a29566466d6d069639694c3d5edae2.png',2,1,'2015-07-29 16:32:37','2015-07-29 17:26:23');

#
# Structure for table "newsletter"
#

CREATE TABLE `newsletter` (
  `email` varchar(150) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "newsletter"
#

INSERT INTO `newsletter` VALUES ('sandrogriebeler@gmail.com','Sandro Augusto Griebeler','2015-07-28 17:19:32'),('sandrogriebeler@hotmail.com','Sandro','2015-07-29 11:10:18');

#
# Structure for table "parceiro"
#

CREATE TABLE `parceiro` (
  `id_parceiro` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `url_site` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_parceiro`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "parceiro"
#

INSERT INTO `parceiro` VALUES (1,'Logo Mark','img_logomark.jpg',NULL),(2,'Logo Mark 2','img_logomark.jpg',NULL),(3,'Teste','6b2d68b5b286856d132a302f75e2adee.jpg','http://teste.com');

#
# Structure for table "user_group"
#

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "user_group"
#

INSERT INTO `user_group` VALUES (1,'superadmin','teste'),(2,'admin',NULL),(3,'public',NULL);

#
# Structure for table "user"
#

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_last` varchar(255) DEFAULT NULL,
  `nick` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `user_group_id` int(11) NOT NULL DEFAULT '3',
  `image` varchar(255) DEFAULT NULL,
  `session_auth` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `showsite` int(11) DEFAULT NULL,
  `nomenclatura` varchar(30) DEFAULT NULL,
  `numero` varchar(30) DEFAULT NULL,
  `corretor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_user_group1` (`user_group_id`),
  CONSTRAINT `fk_user_user_group1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (3,'Admin','Reweb','admin','admin@reweb.com.br','5ab9ca48099be151d30d03efb163829f',1,'user.jpg','d089f4181275b21d20de3c75c75fe019',1,NULL,NULL,NULL,NULL);

#
# Structure for table "core_acl_acos_group"
#

CREATE TABLE `core_acl_acos_group` (
  `acl_acos_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  KEY `fk_acl_acos_group_acl_acos` (`acl_acos_id`),
  KEY `fk_acl_acos_group_user_group1` (`user_group_id`),
  CONSTRAINT `fk_acl_acos_group_acl_acos` FOREIGN KEY (`acl_acos_id`) REFERENCES `core_acl_acos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_acl_acos_group_user_group1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "core_acl_acos_group"
#

INSERT INTO `core_acl_acos_group` VALUES (758,1),(758,2),(759,1),(759,2),(760,1),(760,2),(769,1),(769,2),(770,1),(770,2),(771,1),(771,2),(772,1),(772,2),(773,1),(773,2),(774,1),(774,2),(775,1),(775,2),(776,1),(776,2),(785,1),(785,2),(786,1),(786,2),(787,1),(787,2),(788,1),(788,2),(789,1),(789,2),(790,1),(790,2),(791,1),(791,2),(792,1),(792,2),(793,1),(793,2),(794,1),(794,2),(795,1),(803,1),(803,2),(804,1),(804,2),(804,3),(805,1),(805,2),(805,3),(806,1),(806,2),(806,3),(807,1),(807,2),(808,1),(808,2),(809,1),(809,2),(810,1),(810,2),(831,1),(831,2),(832,1),(832,2),(833,1),(833,2),(834,1),(834,2),(835,1),(835,2),(892,1),(892,2),(893,1),(893,2),(894,1),(894,2),(895,1),(895,2),(917,1),(917,2),(918,1),(918,2),(919,1),(919,2),(920,1),(920,2),(921,1),(921,2),(922,1),(922,2),(923,1),(923,2),(924,1),(924,2),(925,1),(925,2),(926,1),(926,2),(927,1),(927,2),(928,1),(928,2),(930,1),(930,2),(930,3),(931,1),(931,2),(931,3),(932,1),(932,2),(932,3),(933,1),(933,2),(933,3),(934,1),(934,2),(934,3),(935,1),(935,2),(935,3),(936,1),(936,2),(936,3),(937,1),(937,2),(937,3),(938,1),(938,2),(938,3),(939,1),(939,2),(939,3),(940,1),(940,2),(940,3),(941,1),(941,2),(941,3),(947,1),(947,2),(947,3),(948,1),(948,2),(948,3),(951,1),(951,2),(951,3),(952,1),(952,2),(952,3),(956,1),(956,2),(956,3),(957,1),(957,2),(957,3),(958,1),(958,2),(958,3),(960,1),(960,2),(960,3),(972,1),(972,2),(972,3),(973,1),(973,2),(973,3),(974,1),(974,2),(974,3),(976,1),(976,2),(976,3),(978,1),(978,2),(978,3),(1003,1),(1003,2),(1003,3),(1006,1),(1006,2),(1006,3),(962,1),(962,2),(962,3),(963,1),(963,2),(963,3),(964,1),(964,2),(964,3),(987,1),(987,2),(987,3),(988,1),(988,2),(988,3),(989,1),(989,2),(989,3),(990,1),(990,2),(990,3),(991,1),(991,2),(991,3),(966,1),(966,2),(966,3),(967,1),(967,2),(967,3),(968,1),(968,2),(968,3),(969,1),(969,2),(969,3),(970,1),(970,2),(970,3),(971,1),(971,2),(971,3),(992,1),(992,2),(992,3),(1009,1),(1009,2),(1009,3),(1007,1),(1007,2),(1007,3),(1008,1),(1008,2),(1008,3),(1011,1),(1011,2),(1011,3),(983,1),(983,2),(983,3),(985,1),(985,2),(985,3),(994,1),(994,2),(994,3),(995,1),(995,2),(995,3),(996,1),(996,2),(996,3),(997,1),(997,2),(997,3);
