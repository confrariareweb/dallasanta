<?php
error_reporting(E_ERROR);
header("Pragma: no-cache");
header("Cache: no-cahce");

define('MAX_WIDTH', $_REQUEST['x']);
define('MAX_HEIGHT', $_REQUEST['y']);

$image_file = str_replace('..', '', $_REQUEST['img']);
$image_path = $image_file;

$img = null;

$extensao = strtolower(end(explode('.',$image_path)));

if ($extensao == 'jpg' || $extensao == 'jpeg') {
    $img = @imagecreatefromjpeg($image_path);
} else if ($extensao == 'png') {
    $img = @imagecreatefrompng($image_path);
    $isTrueColor = imageistruecolor($img);
    
    imagealphablending($img, false);
	imagesavealpha  ( $img  , true );
    
    // Se a vers�o do GD incluir suporte a GIF, mostra...
} elseif ($extensao == 'gif') {
    $img = @imagecreatefromgif($image_path);
}

// Se a imagem foi carregada com sucesso, testa o tamanho da mesma
if ($img) {
    // Pega o tamanho da imagem e propor��o de resize
    $width = imagesx($img);
    $height = imagesy($img);
    $scale = min(MAX_WIDTH/$width, MAX_HEIGHT/$height);
     // Se a imagem � maior que o permitido, encolhe ela!
    if ($scale < 1) {
    	
        $new_width = floor($scale * $width);
        $new_height = floor($scale * $height);
        // Cria uma imagem tempor�ria
        $tmp_img = imagecreatetruecolor($new_width, $new_height);
        
        /* alpha for png */
        if($extensao == 'png') {
        	if ($isTrueColor) {
			    imagealphablending($tmp_img, false);
			    imagesavealpha  ( $tmp_img  , true );
			} else {
				$tmp_img  = imagecreate( $new_width, $new_height );
				imagealphablending( $tmp_img, false );
				$transparent = imagecolorallocatealpha( $tmp_img, 0, 0, 0, 127 );
				imagefill( $tmp_img, 0, 0, $transparent );
				imagesavealpha( $tmp_img,true );
				imagealphablending( $tmp_img, true );
			}
        }
        
        // Copia e resize a imagem velha na nova
        imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        imagedestroy($img);
        $img = $tmp_img;

    }
}
// Cria uma imagem de erro se necess�rio
if (!$img) {
	$logo 	= imagecreatefromjpeg("assets/site/img/bg/img.jpg");
   	$img 	= imagecreate(MAX_WIDTH, MAX_HEIGHT);
    imagecopymerge($img,$logo, ((MAX_WIDTH-50)/2), ((MAX_HEIGHT-50)/2), 0, 0, 50, 50, 100);
}
// Mostra a imagem

if($extensao == 'png') {
	header('Content-type: image/png');
	imagepng($img,null,9);
} else { 
	header('Content-type: image/jpeg');
	imagejpeg($img,null,100);
}
?>
<!-- END arquivo thumb.php -->