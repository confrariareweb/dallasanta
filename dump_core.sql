-- MySQL dump 10.15  Distrib 10.0.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: externo.reweb.com.br    Database: blumare_fiat
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `titulo` varchar(255) NOT NULL DEFAULT '',
  `subtitulo` varchar(255),
  `url` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `descricao` text,
  `status` tinyint(4) DEFAULT '1',
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`tipo_id`),
  KEY `index3` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bannertipo`
--

DROP TABLE IF EXISTS `bannertipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bannertipo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bannertipo`
--

LOCK TABLES `bannertipo` WRITE;
/*!40000 ALTER TABLE `bannertipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `bannertipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `arquivo_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text,
  `date` datetime DEFAULT NULL,
  `date_register` datetime DEFAULT NULL,
  `video` text,
  `image` varchar(255) DEFAULT NULL,
  `destaque` char(1) DEFAULT '1',
  `status` char(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_blog_blog_category1_idx` (`category_id`),
  KEY `fk_blog_blog_arquivos1_idx` (`arquivo_id`),
  CONSTRAINT `fk_blog_blog_category1` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cadastro`
--

DROP TABLE IF EXISTS `cadastro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadastro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(20) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cadastro`
--

LOCK TABLES `cadastro` WRITE;
/*!40000 ALTER TABLE `cadastro` DISABLE KEYS */;
/*!40000 ALTER TABLE `cadastro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conteudo`
--

DROP TABLE IF EXISTS `conteudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conteudo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `url` varchar(255) NULL,
  `descricao` text,
  `ordem` tinyint(2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conteudo`
--

LOCK TABLES `conteudo` WRITE;
/*!40000 ALTER TABLE `conteudo` DISABLE KEYS */;
/*!40000 ALTER TABLE `conteudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conteudotipo`
--

DROP TABLE IF EXISTS `conteudotipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conteudotipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` tinyint(2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`pai_id`),
  KEY `index3` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conteudotipo`
--

LOCK TABLES `conteudotipo` WRITE;
/*!40000 ALTER TABLE `conteudotipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `conteudotipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_acl_acos`
--

DROP TABLE IF EXISTS `core_acl_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_acl_acos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=525 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_acl_acos`
--

LOCK TABLES `core_acl_acos` WRITE;
/*!40000 ALTER TABLE `core_acl_acos` DISABLE KEYS */;
INSERT INTO `core_acl_acos` VALUES (1,NULL,'Admin_dashboard'),(7,NULL,'Admin_login'),(43,NULL,'Admin_acl'),(51,43,'save'),(52,43,'refact'),(55,7,'logon'),(56,7,'logout'),(73,43,'index'),(74,1,'index'),(75,7,'index'),(92,NULL,'Admin_config'),(93,NULL,'Admin_url'),(94,92,'index'),(95,92,'save'),(96,93,'index'),(97,93,'edit'),(98,93,'save'),(99,93,'delete'),(100,NULL,'Admin_user'),(101,100,'index'),(102,100,'edit'),(103,100,'save'),(104,100,'delete'),(110,NULL,'Admin_staticblock'),(111,NULL,'Admin_usergroup'),(112,110,'index'),(113,110,'edit'),(114,110,'save'),(115,110,'delete'),(116,111,'index'),(117,111,'edit'),(118,111,'save'),(119,111,'delete'),(120,NULL,'Admin_page'),(121,120,'index'),(122,120,'edit'),(123,120,'save'),(124,120,'delete'),(125,NULL,'Sitemap'),(126,125,'index'),(127,NULL,'Error404'),(129,127,'index'),(146,NULL,'Admin_emaillog'),(147,NULL,'Admin_emailto'),(148,146,'index'),(149,146,'edit'),(150,146,'delete'),(151,147,'index'),(152,147,'edit'),(153,147,'save'),(154,147,'delete'),(155,NULL,'Admin_banner'),(158,NULL,'Admin_help'),(162,NULL,'Admin_phpinfo'),(164,155,'index'),(165,155,'edit'),(166,155,'save'),(167,155,'delete'),(176,158,'index'),(190,162,'index'),(228,NULL,'Admin_states'),(242,228,'index'),(243,228,'edit'),(244,228,'save'),(245,228,'delete'),(250,NULL,'Admin_blog'),(251,NULL,'Admin_blogcategory'),(252,NULL,'Admin_newsletter'),(253,250,'index'),(254,250,'edit'),(255,250,'save'),(256,250,'delete'),(257,251,'index'),(258,251,'edit'),(259,251,'save'),(260,251,'delete'),(261,252,'index'),(262,252,'edit'),(263,252,'save'),(264,252,'delete'),(265,NULL,'Contact'),(266,NULL,'Home'),(267,NULL,'Swfupload'),(268,265,'index'),(270,266,'index'),(271,267,'index'),(272,267,'save_param'),(273,267,'delete_file'),(274,267,'get_detail'),(275,267,'edit'),(276,267,'save_edit'),(277,267,'order'),(278,267,'upload'),(279,267,'thumbnail'),(309,NULL,'Admin_bannertype'),(316,309,'index'),(317,309,'edit'),(318,309,'save'),(319,309,'delete'),(329,265,'sendMessage'),(365,NULL,'Admin_notice'),(366,NULL,'Admin_noticetype'),(367,365,'index'),(368,365,'edit'),(369,365,'save'),(370,365,'delete'),(371,366,'index'),(372,366,'edit'),(373,366,'save'),(374,366,'delete'),(377,NULL,'Admin_content'),(378,NULL,'Admin_contenttype'),(380,377,'index'),(381,377,'edit'),(382,377,'save'),(383,377,'delete'),(384,378,'index'),(385,378,'edit'),(386,378,'save'),(387,378,'delete'),(396,NULL,'Admin_noticeauthor'),(397,396,'index'),(398,396,'edit'),(399,396,'save'),(400,396,'delete'),(420,NULL,'Admin_store'),(421,NULL,'Admin_storetype'),(423,420,'index'),(424,420,'edit'),(425,420,'save'),(426,420,'delete'),(427,421,'index'),(428,421,'edit'),(429,421,'save'),(430,421,'delete'),(444,NULL,'Admin_product'),(445,NULL,'Admin_productbrand'),(446,NULL,'Admin_productmodel'),(447,NULL,'Admin_producttype'),(449,444,'index'),(450,444,'edit'),(451,444,'save'),(452,444,'delete'),(453,445,'index'),(454,445,'edit'),(455,445,'save'),(456,445,'delete'),(457,446,'index'),(458,446,'edit'),(459,446,'save'),(460,446,'delete'),(461,447,'index'),(462,447,'edit'),(463,447,'save'),(464,447,'delete'),(484,NULL,'Admin_noticecomment'),(485,NULL,'Admin_noticetag'),(486,484,'index'),(487,484,'edit'),(488,484,'save'),(489,484,'delete'),(490,485,'index'),(491,485,'edit'),(492,485,'save'),(493,485,'delete'),(506,265,'paypalCheckout'),(507,265,'checkoutSuccess'),(509,NULL,'Admin_translator'),(510,NULL,'Admin_translatoridiom'),(511,NULL,'Admin_translatortype'),(512,509,'index'),(513,509,'edit'),(514,509,'save'),(515,509,'requestRegisterId'),(516,509,'delete'),(517,510,'index'),(518,510,'edit'),(519,510,'save'),(520,510,'delete'),(521,511,'index'),(522,511,'edit'),(523,511,'save'),(524,511,'delete');
/*!40000 ALTER TABLE `core_acl_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_acl_acos_group`
--

DROP TABLE IF EXISTS `core_acl_acos_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_acl_acos_group` (
  `acl_acos_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  KEY `fk_acl_acos_group_acl_acos` (`acl_acos_id`),
  KEY `fk_acl_acos_group_user_group1` (`user_group_id`),
  CONSTRAINT `fk_acl_acos_group_acl_acos` FOREIGN KEY (`acl_acos_id`) REFERENCES `core_acl_acos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_acl_acos_group_user_group1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_acl_acos_group`
--

LOCK TABLES `core_acl_acos_group` WRITE;
/*!40000 ALTER TABLE `core_acl_acos_group` DISABLE KEYS */;
INSERT INTO `core_acl_acos_group` VALUES (74,1),(74,2),(55,1),(55,2),(55,3),(56,1),(56,2),(56,3),(75,1),(75,2),(75,3),(51,1),(52,1),(73,1),(94,1),(94,2),(95,1),(95,2),(96,1),(97,1),(98,1),(99,1),(101,1),(101,2),(102,1),(102,2),(103,1),(103,2),(104,1),(104,2),(112,1),(112,2),(113,1),(113,2),(114,1),(114,2),(115,1),(115,2),(116,1),(116,2),(117,1),(117,2),(118,1),(118,2),(119,1),(119,2),(121,1),(121,2),(122,1),(122,2),(123,1),(123,2),(124,1),(124,2),(126,1),(126,2),(126,3),(126,4),(129,1),(129,2),(129,3),(129,4),(148,1),(148,2),(149,1),(149,2),(150,1),(150,2),(151,1),(151,2),(152,1),(152,2),(153,1),(153,2),(154,1),(154,2),(164,1),(164,2),(165,1),(165,2),(166,1),(166,2),(167,1),(167,2),(176,1),(176,2),(190,1),(242,1),(242,2),(243,1),(243,2),(244,1),(244,2),(245,1),(245,2),(253,1),(253,2),(254,1),(254,2),(255,1),(255,2),(256,1),(256,2),(257,1),(257,2),(258,1),(258,2),(259,1),(259,2),(260,1),(260,2),(261,1),(261,2),(262,1),(262,2),(263,1),(263,2),(264,1),(264,2),(268,1),(268,2),(268,3),(268,4),(329,1),(329,2),(329,3),(329,4),(506,1),(506,2),(506,3),(506,4),(507,1),(507,2),(507,3),(507,4),(270,1),(270,2),(270,3),(270,4),(271,1),(271,2),(271,3),(271,4),(272,1),(272,2),(272,3),(272,4),(273,1),(273,2),(273,3),(273,4),(274,1),(274,2),(274,3),(274,4),(275,1),(275,2),(275,3),(275,4),(276,1),(276,2),(276,3),(276,4),(277,1),(277,2),(277,3),(277,4),(278,1),(278,2),(278,3),(278,4),(279,1),(279,2),(279,3),(279,4),(316,1),(316,2),(316,3),(316,4),(317,1),(317,2),(317,3),(317,4),(318,1),(318,2),(318,3),(318,4),(319,1),(319,2),(319,3),(319,4),(367,1),(367,2),(368,1),(368,2),(369,1),(369,2),(370,1),(370,2),(371,1),(371,2),(372,1),(372,2),(373,1),(373,2),(374,1),(374,2),(380,1),(380,2),(381,1),(381,2),(382,1),(382,2),(383,1),(383,2),(384,1),(384,2),(385,1),(385,2),(386,1),(386,2),(387,1),(387,2),(397,1),(397,2),(398,1),(398,2),(399,1),(399,2),(400,1),(400,2),(423,1),(423,2),(424,1),(424,2),(425,1),(425,2),(426,1),(426,2),(427,1),(427,2),(428,1),(428,2),(429,1),(429,2),(430,1),(430,2),(449,1),(449,2),(450,1),(450,2),(451,1),(451,2),(452,1),(452,2),(453,1),(453,2),(454,1),(454,2),(455,1),(455,2),(456,1),(456,2),(457,1),(457,2),(458,1),(458,2),(459,1),(459,2),(460,1),(460,2),(461,1),(461,2),(462,1),(462,2),(463,1),(463,2),(464,1),(464,2),(486,1),(486,2),(487,1),(487,2),(488,1),(488,2),(489,1),(489,2),(490,1),(490,2),(491,1),(491,2),(492,1),(492,2),(493,1),(493,2),(512,1),(512,2),(513,1),(513,2),(514,1),(514,2),(515,1),(515,2),(516,1),(516,2),(517,1),(517,2),(518,1),(518,2),(519,1),(519,2),(520,1),(520,2),(521,1),(521,2),(522,1),(522,2),(523,1),(523,2),(524,1),(524,2);
/*!40000 ALTER TABLE `core_acl_acos_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_config`
--

DROP TABLE IF EXISTS `core_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `option` text,
  `value` text,
  `group` text,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `input_type` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_config`
--

LOCK TABLES `core_config` WRITE;
/*!40000 ALTER TABLE `core_config` DISABLE KEYS */;
INSERT INTO `core_config` VALUES (1,'meta_tag_title','','Meta_Tags','Title',NULL,'text',1),
(2,'meta_tag_description','','Meta_Tags','Description',NULL,'textarea',1),
(3,'meta_tag_keywords','','Meta_Tags','Keywords',NULL,'text',1),
(4,'meta_tag_spam','','Meta_Tags','Spam',NULL,'textarea',1),
(5,'meta_tag_author','Reweb Informática Ltda','Meta_Tags','Autor',NULL,'text',1),
(6,'meta_tag_copryright','Copyright Reweb Informática Ltda','Meta_Tags','Copyright',NULL,'text',1),
(7,'facebook_og_type','Website','Facebook','og_type',NULL,'text',1),
(8,'facebook_og_image','','Facebook','og_image',NULL,'image',1),
(9,'facebook_og_site_name','','Facebook','og_site_name',NULL,'text',1),
(13,'meta_tag_backend_title','Gerenciador de conteúdos','Meta_Tags_Backend','Title',NULL,'text',1),
(14,'meta_tag_backend_description','Gerenciador de conteúdos','Meta_Tags_Backend','Description',NULL,'textarea',1),
(16,'meta_tag_backend_keywords','Keywords','Meta_Tags_Backend','Keywords',NULL,'text',1),
(17,'meta_tag_backend_spam','Spam','Meta_Tags_Backend','Spam',NULL,'textarea',1),
(18,'meta_tag_backend_author','Reweb Informática Ltda','Meta_Tags_Backend','Autor',NULL,'text',1),
(19,'meta_tag_backend_copryright','Copyright Reweb Informática Ltda','Meta_Tags_Backend','copyright',NULL,'text',1),
(20,'general_client','','Geral','Nome do Cliente',NULL,'text',1),
(21,'general_logo','','Geral','Logo',NULL,'image',1),
(22,'general_url','http://www.reweb.com.br','Geral','URL',NULL,'text',1),
(23,'facebook_fb_admins','Fb admins','Facebook','fb_admins',NULL,'text',1),
(24,'facebook_app_id','','Facebook','APP ID',NULL,'text',1),
(25,'facebook_token','','Facebook','Token',NULL,'text',1),
(26,'social_facebook','http://facebook.com/','Redes_Sociais','Facebook',NULL,'text',1),
(27,'social_twitter','http://twitter.com','Redes_Sociais','Twitter',NULL,'text',1),
(28,'social_googleplus','http://google.com','Redes_Sociais','Google plus',NULL,'text',1),
(29,'social_pinterest','','Redes_Sociais','Pinterest',NULL,'text',1),
(30,'social_blog','','Redes_Sociais','Blog',NULL,'text',1),
(31,'social_foursquare','','Redes_Sociais','Foursquare',NULL,'text',1),
(32,'social_youtube','http://youtube.com','Redes_Sociais','Youtube',NULL,'text',1),
(33,'social_slideshare','','Redes_Sociais','Slideshare',NULL,'text',1);
/*!40000 ALTER TABLE `core_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_page`
--

DROP TABLE IF EXISTS `core_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `binding` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_page`
--

LOCK TABLES `core_page` WRITE;
/*!40000 ALTER TABLE `core_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_sessions`
--

DROP TABLE IF EXISTS `core_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_sessions`
--

LOCK TABLES `core_sessions` WRITE;
/*!40000 ALTER TABLE `core_sessions` DISABLE KEYS */;
INSERT INTO `core_sessions` VALUES ('b1b0044b705d045311be421232eb6d53','127.0.0.1','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0',1426169775,'');
/*!40000 ALTER TABLE `core_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_staticblock`
--

DROP TABLE IF EXISTS `core_staticblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_staticblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `binding` varchar(255) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_staticblock`
--

LOCK TABLES `core_staticblock` WRITE;
/*!40000 ALTER TABLE `core_staticblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_staticblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_url`
--

DROP TABLE IF EXISTS `core_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_url` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` text,
  `title` text,
  `keywords` text,
  `description` text,
  `url_old` text,
  `spam` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_url`
--

LOCK TABLES `core_url` WRITE;
/*!40000 ALTER TABLE `core_url` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emaillog`
--

DROP TABLE IF EXISTS `emaillog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emaillog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `body` longtext,
  `metadata` longtext,
  `session` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emaillog`
--

LOCK TABLES `emaillog` WRITE;
/*!40000 ALTER TABLE `emaillog` DISABLE KEYS */;
INSERT INTO `emaillog` VALUES (1,'2015-03-12 13:36:19',NULL,NULL,NULL,'','{\"field\":\"\",\"textarea\":\"Mensagem:\"}',' - FaBrigadeiros',1);
/*!40000 ALTER TABLE `emaillog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailto`
--

DROP TABLE IF EXISTS `emailto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` varchar(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailto`
--

LOCK TABLES `emailto` WRITE;
/*!40000 ALTER TABLE `emailto` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailto_category`
--

DROP TABLE IF EXISTS `emailto_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailto_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailto_category`
--

LOCK TABLES `emailto_category` WRITE;
/*!40000 ALTER TABLE `emailto_category` DISABLE KEYS */;
INSERT INTO `emailto_category` VALUES (1,'Formulario de contato'),(2,'Recuperar senha'),(3,'Formulario de cadastro'),(4,'Formulario de representante'),(5,'Formulario de cotação'),(6,'Formulario de desconto'),(7,'Formulario de orçamento'),(8,'Formulario trabalhe conosco');
/*!40000 ALTER TABLE `emailto_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `rel` varchar(255) NOT NULL,
  `rel_id` int(11) NOT NULL,
  `color` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `subtype` varchar(45) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '999',
  `status` int(11) DEFAULT '1',
  `date_register` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loja`
--

DROP TABLE IF EXISTS `loja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loja` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pai_id` int(11) DEFAULT NULL,
  `tipo_id` int(11) unsigned DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `estado` varchar(4) DEFAULT NULL,
  `estado2` varchar(100) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `googlemaps` text,
  `link` text,
  `descricao` text,
  `descricaoendereco` text,
  `descricaoatendimento` text,
  `ordem` tinyint(3) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index3` (`pai_id`),
  KEY `index4` (`tipo_id`),
  KEY `index2` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 DELAY_KEY_WRITE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loja`
--

LOCK TABLES `loja` WRITE;
/*!40000 ALTER TABLE `loja` DISABLE KEYS */;
/*!40000 ALTER TABLE `loja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `lojas_lojatipo`
--

DROP TABLE IF EXISTS `lojas_lojatipo`;
/*!50001 DROP VIEW IF EXISTS `lojas_lojatipo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `lojas_lojatipo` (
  `id` tinyint NOT NULL,
  `loja__id` tinyint NOT NULL,
  `loja__pai_id` tinyint NOT NULL,
  `loja__nome` tinyint NOT NULL,
  `loja__titulo` tinyint NOT NULL,
  `loja__estado` tinyint NOT NULL,
  `loja__estado2` tinyint NOT NULL,
  `loja__cidade` tinyint NOT NULL,
  `loja__endereco` tinyint NOT NULL,
  `loja__telefone` tinyint NOT NULL,
  `loja__email` tinyint NOT NULL,
  `loja__googlemaps` tinyint NOT NULL,
  `loja__link` tinyint NOT NULL,
  `loja__descricao` tinyint NOT NULL,
  `loja__ordem` tinyint NOT NULL,
  `loja__status` tinyint NOT NULL,
  `loja__dateupdate` tinyint NOT NULL,
  `lojatipo__nome` tinyint NOT NULL,
  `lojatipo__titulo` tinyint NOT NULL,
  `lojatipo__descricao` tinyint NOT NULL,
  `lojatipo__status` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `lojatipo`
--

DROP TABLE IF EXISTS `lojatipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lojatipo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lojatipo`
--

LOCK TABLES `lojatipo` WRITE;
/*!40000 ALTER TABLE `lojatipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `lojatipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticia`
--

DROP TABLE IF EXISTS `noticia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `data` date DEFAULT NULL,
  `destaque` tinyint(1) DEFAULT NULL,
  `visualizacoes` tinyint(4) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index3` (`nome`),
  KEY `index2` (`tipo_id`),
  KEY `index4` (`autor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticia`
--

LOCK TABLES `noticia` WRITE;
/*!40000 ALTER TABLE `noticia` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticia_tag`
--

DROP TABLE IF EXISTS `noticia_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticia_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`noticia_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticia_tag`
--

LOCK TABLES `noticia_tag` WRITE;
/*!40000 ALTER TABLE `noticia_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticia_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticiaautor`
--

DROP TABLE IF EXISTS `noticiaautor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticiaautor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text,
  `email` varchar(100) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticiaautor`
--

LOCK TABLES `noticiaautor` WRITE;
/*!40000 ALTER TABLE `noticiaautor` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticiaautor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticiacomentario`
--

DROP TABLE IF EXISTS `noticiacomentario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticiacomentario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `descricao` text NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`noticia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticiacomentario`
--

LOCK TABLES `noticiacomentario` WRITE;
/*!40000 ALTER TABLE `noticiacomentario` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticiacomentario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticiatag`
--

DROP TABLE IF EXISTS `noticiatag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticiatag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`nome`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticiatag`
--

LOCK TABLES `noticiatag` WRITE;
/*!40000 ALTER TABLE `noticiatag` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticiatag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticiatipo`
--

DROP TABLE IF EXISTS `noticiatipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticiatipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`nome`),
  KEY `index3` (`pai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticiatipo`
--

LOCK TABLES `noticiatipo` WRITE;
/*!40000 ALTER TABLE `noticiatipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticiatipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `preco` decimal(10,2) DEFAULT NULL,
  `ano` smallint(4) DEFAULT NULL,
  `descricao` text,
  `descricaotecnica` text,
  `descricaoacessorio` text,
  `ordem` tinyint(2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  KEY `index3` (`tipo_id`),
  KEY `index6` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto_modelo`
--

DROP TABLE IF EXISTS `produto_modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto_modelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_id` int(11) NOT NULL,
  `modelo_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`produto_id`,`modelo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto_modelo`
--

LOCK TABLES `produto_modelo` WRITE;
/*!40000 ALTER TABLE `produto_modelo` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto_modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtomarca`
--

DROP TABLE IF EXISTS `produtomarca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtomarca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`nome`),
  KEY `index3` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtomarca`
--

LOCK TABLES `produtomarca` WRITE;
/*!40000 ALTER TABLE `produtomarca` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtomarca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtomodelo`
--

DROP TABLE IF EXISTS `produtomodelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtomodelo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  KEY `index3` (`nome`),
  KEY `index4` (`marca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtomodelo`
--

LOCK TABLES `produtomodelo` WRITE;
/*!40000 ALTER TABLE `produtomodelo` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtomodelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtotipo`
--

DROP TABLE IF EXISTS `produtotipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtotipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_id` int(11) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `status` tinyint(1) NOT NULL,
  `dateupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index2` (`pai_id`),
  KEY `index3` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtotipo`
--

LOCK TABLES `produtotipo` WRITE;
/*!40000 ALTER TABLE `produtotipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `produtotipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_last` varchar(255) DEFAULT NULL,
  `nick` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `user_group_id` int(11) NOT NULL DEFAULT '3',
  `image` varchar(255) DEFAULT NULL,
  `session_auth` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_user_user_group1` (`user_group_id`),
  CONSTRAINT `fk_user_user_group1` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'Eduardo Antonio','Messias','duda','duda@reweb.com.br','28e19bfc90e733f2e026ccd7a1bfe620',1,'eb322dfe7d5d4c00521b106d4e62c653.jpg','3a878681c16bad9f044ef0dc3c038306',1),(3,'Admin','Reweb','admin','admin@reweb.com.br','5ab9ca48099be151d30d03efb163829f',1,'user.jpg','c6fc97e532578f2c535d736001e5ba1c',1),(4,'Atendimento','Atendimento','atendimento','atendimento@reweb.com.br','5ab9ca48099be151d30d03efb163829f',2,NULL,NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'superadmin','teste'),(2,'admin',NULL),(3,'public',NULL),(4,'member',NULL);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `lojas_lojatipo`
--

/*!50001 DROP TABLE IF EXISTS `lojas_lojatipo`*/;
/*!50001 DROP VIEW IF EXISTS `lojas_lojatipo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lojas_lojatipo` AS (select `lj`.`id` AS `id`,`lj`.`id` AS `loja__id`,`lj`.`pai_id` AS `loja__pai_id`,`lj`.`nome` AS `loja__nome`,`lj`.`titulo` AS `loja__titulo`,`lj`.`estado` AS `loja__estado`,`lj`.`estado2` AS `loja__estado2`,`lj`.`cidade` AS `loja__cidade`,`lj`.`endereco` AS `loja__endereco`,`lj`.`telefone` AS `loja__telefone`,`lj`.`email` AS `loja__email`,`lj`.`googlemaps` AS `loja__googlemaps`,`lj`.`link` AS `loja__link`,`lj`.`descricao` AS `loja__descricao`,`lj`.`ordem` AS `loja__ordem`,`lj`.`status` AS `loja__status`,`lj`.`dateupdate` AS `loja__dateupdate`,`lojatipo`.`nome` AS `lojatipo__nome`,`lojatipo`.`titulo` AS `lojatipo__titulo`,`lojatipo`.`descricao` AS `lojatipo__descricao`,`lojatipo`.`status` AS `lojatipo__status` from (`loja` `lj` left join `lojatipo` on((`lojatipo`.`id` = `lj`.`tipo_id`))) where ((`lj`.`status` = 1) and (`lojatipo`.`nome` = 'coverage')) order by `lj`.`ordem` limit 10) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-25 14:34:40
