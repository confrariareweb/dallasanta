<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Google_login {

	private $google_client;
	private $oauth;
	private $client_id = '178803543657-nlkaffcsk42q738u6rbasjau75q4tcg3.apps.googleusercontent.com';
	private $client_secret = 'ruodyttm8g0-pQUQm7JSvqDf';
	private $redirect_url = 'area-do-cliente/google_login';

	function __construct() {
		$this->redirect_url = site_url($this->redirect_url);

		require_once APPPATH.'third_party/google_login_api/src/Google_Client.php';
		require_once APPPATH.'third_party/google_login_api/src/contrib/Google_Oauth2Service.php';

		$this->auth();
	}

	private function auth() {
		$this->google_client = new Google_Client();
		//$this->google_client->setApplicationName('Login to Dallasanta');
		$this->google_client->setScopes('profile email');
		$this->google_client->setClientId($this->client_id);
		$this->google_client->setClientSecret($this->client_secret);
		$this->google_client->setRedirectUri($this->redirect_url);

		$this->oauth = new Google_Oauth2Service($this->google_client);
	}

	public function login() {
		$CI =& get_instance();

		// If code is empty, redirect user to google authentication page for code.
		// Code is required to aquire Access Token from google
		// Once we have access token, assign token to session variable
		// and we can redirect user back to page and login.
		if (isset($_GET['code'])) {
			$this->google_client->authenticate($_GET['code']);
			$_SESSION['google_access_token'] = $this->google_client->getAccessToken();
			// redirect($google_redirect_url);
			// return;
		}

		if (isset($_SESSION['google_access_token'])) {
			$this->google_client->setAccessToken($_SESSION['google_access_token']);
		}

		if ($this->google_client->getAccessToken()) {
			$_SESSION['google_access_token'] = $this->google_client->getAccessToken();

			//For logged in user, get details from google using access token
			$user = $this->oauth->userinfo->get();

			$id = $user['id'];
			$email = $user['email'];
			$nome = $user['given_name'];
			$sobrenome = $user['family_name'];
			$tipo_login = 'google';
			$foto = $user['picture'];

			/*
			$qry = $CI->db->select('foto')->from('area_restrita_usuario')->where('email', $email)->get();

			// Teste se o e-mail já está cadastrado
			if ($qry->num_rows() > 0) {
				$foto = $qry->row('foto');

				// Se o usuário não tem foto, busca a do facebook
				if (empty($foto)) {
					$foto = $google_foto;
					$foto_update = ", foto = '$foto'";
				}
			} else {
				$foto = $google_foto;
			}
			*/

			$qry = "INSERT INTO area_restrita_usuario (email, nome, sobrenome, foto, tipo_login, google_id) VALUES ('$email', '$nome', '$sobrenome', '$foto', '$tipo_login', $id)
						     ON DUPLICATE KEY UPDATE tipo_login = '$tipo_login'
						     					   , google_id = '$id'
						     					   , token_ativar_conta = NULL
						     					   , token_recuperar_senha = NULL
						     					   , foto = '$foto'
			";
			if ($CI->db->query($qry)) {
		        $user = (object) array(
		            'nome' => vsprintf('%s %s', array($nome, $sobrenome)),
		            'email' => $email,
		            'foto' => $foto,
		            'tipo_login' => $tipo_login
		        );
		        $CI->session->set_userdata('area_restrita', $user);
		    }
			redirect('area-do-cliente/meus-dados');
		} else {
			//For Guest user, get google login url
			//$auth_url = $this->google_client->createAuthUrl();
			header('Location: '. $this->google_client->createAuthUrl());
		}
	}

	public function logout() {
		unset($_SESSION['google_access_token']);
		$this->google_client->revokeToken();
	}

}