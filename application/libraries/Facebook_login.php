<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_login {

	private $fb;
	private $app_id = '388395911370210';
	private $app_secret = '9674bd33821374c85a434c85e665d988';
	private $redirect_url = 'area-do-cliente/facebook_login';

	function __construct() {
		$this->redirect_url = site_url($this->redirect_url);

		require_once APPPATH.'third_party/facebook_api/src/Facebook/autoload.php';

		$this->auth();
	}

	private function auth() {
		$this->fb = new Facebook\Facebook([
			'app_id' => $this->app_id,
			'app_secret' => $this->app_secret,
			'default_graph_version' => 'v2.4',
		]);
	}

	public function login() {
		$CI =& get_instance();

		$helper = $this->fb->getRedirectLoginHelper();

		if (isset($_GET['code'])) {
			try {
				$access_token = $helper->getAccessToken();
			} catch (Facebook\Exceptions\FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			$_SESSION['facebook_access_token'] = (string) $access_token;


			//For logged in user, get details from google using access token
			try {
				// Returns a `Facebook\FacebookResponse` object
				$response = $this->fb->get('/me?fields=id,email,first_name,last_name,link', $_SESSION['facebook_access_token']);
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			$user = $response->getGraphUser();

			$id = $user['id'];
			$email = $user['email'];
			$nome = $user['first_name'];
			$sobrenome = $user['last_name'];
			$tipo_login = 'facebook';
			$foto = sprintf('https://graph.facebook.com/v2.4/%s/picture?width=223&height=223', $id);

			/*
			$qry = $CI->db->select('foto')->from('area_restrita_usuario')->where('email', $email)->get();

			// Teste se o e-mail já está cadastrado
			if ($qry->num_rows() > 0) {
				$foto = $qry->row('foto');

				// Se o usuário não tem foto, busca a do facebook
				if (empty($foto)) {
					$foto = $fb_foto;
					$foto_update = ", foto = '$foto'";
				}
			} else {
				$foto = $fb_foto;
			}
			*/

			$qry = "INSERT INTO area_restrita_usuario (email, nome, sobrenome, foto, tipo_login, facebook_id) VALUES ('$email', '$nome', '$sobrenome', '$foto', '$tipo_login', $id)
						     ON DUPLICATE KEY UPDATE tipo_login = '$tipo_login'
						     					   , facebook_id = '$id'
						     					   , token_ativar_conta = NULL
						     					   , token_recuperar_senha = NULL
						     					   , foto = '$foto'
			";
			if ($CI->db->query($qry)) {
		        $user = (object) array(
		            'nome' => vsprintf('%s %s', array($nome, $sobrenome)),
		            'email' => $email,
		            'foto' => $foto,
		            'tipo_login' => $tipo_login
		        );
		        $CI->session->set_userdata('area_restrita', $user);
		    }
			redirect('area-do-cliente/meus-dados');
		} else {
			$permissions = ['email']; // Optional permissions
			$login_url = $helper->getLoginUrl($this->redirect_url, $permissions);

			redirect($login_url);
		}
	}

	public function logout() {
		unset($_SESSION['facebook_access_token']);
		// revokeToken();
	}

}