<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sendmail Class
 *
 * @description Implements Sendmail to application controllers.
 * @author Eduardo Messias (eduardo.inf@gmail.com)
 * @package libraries
 */

class Sendmail {

    var $mail;
    var $session;
    var $vars;
    var $subject;
    var $body;
    var $mail_template = false;
    var $bodyComp;
    var $sendFlag;
    var $email_to;
    var $log 	= FALSE;
    var $DEBUG 	= FALSE;

    var $from_name  = 'Agora Imobiliária';
    var $from_email = 'sites01@reweb.com.br';


    /**
     * __construct
     *
     * @access public
     * @return void
     */
    function  __construct() {
    	$this->CI =& get_instance();
    	$this->CI->load->library('email');
    }

    /**
     * log_email function.
     *
     * @description Generate log
     * @access public
     * @param string $name
     * @param string $email
     * @param string $email_to
     * @param string $body
     * @param string $session
     * @param string $status
     * @return void
     */
    public function log_email($name,$email,$email_to,$body,$session,$status) {
        $data = array('date'=>date('Y-m-d H:i:s'), 'name' => $name, 'email' => $email, 'email_to' => $email_to, 'body' => $body, 'metadata' => $this->metadata , 'session' => $session, 'status' => $status);

        $this->CI->db->insert('emaillog', $data);
    }

    /**
     * format_email
     *
     * @description Apply template mail form data
     * @access public
     * @return void
     */
    public function format_email() {
        $data['subject'] = $this->subject;
        $data['body'] 	 = $this->body;
        $data['title'] 	 = "";

        return $this->CI->load->view('frontend/includes/mail.php',$data,TRUE);
    }

    /**
     * get_email_session
     *
     * @description Get emails to send in session
     * @access public
     * @param mixed $session
     * @param mixed $ids
     * @return void
     */
    public function get_email_session($session,$ids) {
        if($ids != '') {
            $in = implode(',',$ids);
            $where = " AND id IN ($in) ";
        }

		$query = $this->CI->db->from('emailto')->where("status IN ('1') AND category_id = '{$session}' {$where}")->get();
        return $query->result_array();
    }

	/**
     * get_email_category
     *
     * @description Get category email to create subject and session
     * @access public
     * @param mixed $session
     * @param mixed $ids
     * @return void
     */
    public function get_email_category($id) {
        $query = $this->CI->db->from('emailto_category')->where("id = '{$id}'")->get();
        $id = $query->result_array();
		foreach ($id as $title) {
			$title_category = $title['title'];
		}
		return $title_category;
    }

    /**
     * clear
     *
     * @description Clear recpients
     * @access public
     * @return void
     */
    public function clear() {
        unset($this->email_to);
	    $this->CI->email->clear();
    }

    /**
     * send
     *
     * @description Send email
     * @access public
     * @return void
     */
    function send() {

        $this->CI->email->from($this->from_email,$this->from_name);

		$name = '$this->send_'.$this->session.'();';
		eval($name);

		$body = str_replace('\r\n','<br />',$this->format_email());

		if($this->DEBUG == TRUE) die($body);

		$this->CI->email->message($body);
		$this->CI->email->subject($this->subject);

        $this->CI->email->bcc("marketing@reweb.com.br");
        //$this->CI->email->to('sgriebeler@reweb.com.br');
        

        if($this->CI->email->send()) $this->status = '1';
        else                    	 $this->status = '0';

        if($this->log == TRUE) {
            // insert log email
            $this->log_email($this->name,
                             $this->email,
                             implode(',',$this->email_to),
                             $this->body.$this->bodyComp,
                             $this->session,
                             $this->status);
        }
    }

    /**
     * send_contact
     *
     * @description Configure a send email contact
     * @access public
     * @return void
     */
    function send_email() {
        $title_category_email = $this->get_email_category($this->vars['session_id']);

        $this->session = $title_category_email.'';
        $this->subject = $title_category_email.': '. $this->vars['email'];
        //

		if($this->vars['subject'])       {$this->body .= '<b>Assunto :</b>      ' . $this->vars['subject'] . '<br/>';}
        if($this->vars['imovel'])		{$this->body .= '<b>Imóvel :</b> 		' . $this->vars['imovel'] . '<br/><br/>';}
        if($this->vars['name'])		{$this->body .= '<b>Nome :</b> 		' . $this->vars['name'] . '<br/>';}
        if($this->vars['sobrenome'])     {$this->body .= '<b>Sobrenome :</b>      ' . $this->vars['sobrenome'] . '<br/>';}
		if($this->vars['email'])	{$this->body .= '<b>E-mail :</b> 	' . $this->vars['email'] . '<br/>';}
        if($this->vars['order_email'])    {$this->body .= '<b>E-mail :</b>    ' . $this->vars['order_email'] . '<br/>';}
        if($this->vars['senha'])    {$this->body .= '<b>Senha :</b>    ' . $this->vars['senha'] . '<br/>';}
		if($this->vars['phone'])	{$this->body .= '<b>Telefone :</b> 	' . $this->vars['phone'] . '<br/>';}
        if($this->vars['cellphone'])	{$this->body .= '<b>Celular :</b> 	' . $this->vars['cellphone'] . '<br/>';}
        if($this->vars['company'])   {$this->body .= '<b>Empresa :</b>      ' . $this->vars['company'] . '<br/>';}
        if($this->vars['bike'])   {$this->body .= '<b>Moto :</b>      ' . $this->vars['bike'] . '<br/>';}
		if($this->vars['stores'])	{$this->body .= '<b>Loja :</b> 		' . $this->vars['stores'] . '<br/>';}
		if($this->vars['model'])	{$this->body .= '<b>MOdelo :</b> 	' . $this->vars['model'] . '<br/>';}
        if($this->vars['concessionaria'])    {$this->body .= '<b>Concessionaria :</b>    ' . $this->vars['concessionaria'] . '<br/>';}
        if($this->vars['melhor_dia'])    {$this->body .= '<b>Melhor dia :</b>    ' . $this->vars['melhor_dia'] . '<br/>';}
        if($this->vars['melhor_hora'])    {$this->body .= '<b>Melhor hora :</b>    ' . $this->vars['melhor_hora'] . '<br/>';}
        if($this->vars['box_size'])  {$this->body .= '<b>Size :</b>   ' . $this->vars['box_size'] . '<br/>';}
		if($this->vars['product'])	{$this->body .= '<b>Product :</b> 	' . $this->vars['product'] . '<br/>';}
        if($this->vars['type_event'])  {$this->body .= '<b>Event Type :</b>   ' . $this->vars['type_event'] . '<br/>';}
        if($this->vars['how_many_people'])  {$this->body .= '<b>How many people :</b>   ' . $this->vars['how_many_people'] . '<br/>';}
        if($this->vars['pais'])     {$this->body .= '<b>Country :</b>    ' . $this->vars['pais'] . '<br/>';}
		if($this->vars['city'])		{$this->body .= '<b>Cidade :</b> 	' . $this->vars['city'] . '<br/>';}
        if($this->vars['bairro'])     {$this->body .= '<b>Bairro :</b>    ' . $this->vars['bairro'] . '<br/>';}
		if($this->vars['states'])	{$this->body .= '<b>UF :</b> 		' . $this->vars['states'] . '<br/>';}
        if($this->vars['zip'])   {$this->body .= '<b>CEP :</b>        ' . $this->vars['zip'] . '<br/>';}
        if($this->vars['quantity'])   {$this->body .= '<b>Amount :</b>        ' . $this->vars['quantity'] . '<br/>';}
        if($this->vars['adress1'])   {$this->body .= '<b>Address 1:</b>        ' . $this->vars['adress1'] . '<br/>';}
        if($this->vars['adress2'])   {$this->body .= '<b>Address 2:</b>        ' . $this->vars['adress2'] . '<br/>';}
        if($this->vars['interesse'])   {$this->body .= '<b>Interesse :</b>        ' . $this->vars['interesse'] . '<br/>';}
        if($this->vars['sector'])   {$this->body .= '<b>Setor :</b>        ' . $this->vars['sector'] . '<br/>';}
        if($this->vars['product_price'])   {$this->body .= '<b>Price/Value :</b>        ' . $this->vars['product_price'] . '<br/>';}
        if($this->vars['product_shipping'])   {$this->body .= '<b>Shipping :</b>        ' . $this->vars['product_shipping'] . '<br/>';}
        if($this->vars['product_price_tax'])   {$this->body .= '<b>Tax :</b>        ' . $this->vars['product_price_tax'] . '<br/>';}
		if($this->vars['message'])	{$this->body .= '<b>Message :</b> 	' . $this->vars['message'] . '<br/>';}
        if($this->vars['descricao'])	{$this->body .= '<b>Descrição :</b> 	' . $this->vars['descricao'] . '<br/>';}
        if($this->vars['newsletter_name'])  {$this->body .= '<b>Newsletter Nome :</b>  ' . $this->vars['newsletter_name'] . '<br/>';}
        if($this->vars['newsletter_email'])  {$this->body .= '<b>Newsletter Email:</b>  ' . $this->vars['newsletter_email'] . '<br/>';}
        if($this->vars['ano_moto'])  {$this->body .= '<b>Ano da Moto:</b>  ' . $this->vars['ano_moto'] . '<br/>';}
        if($this->vars['km'])  {$this->body .= '<b>KM:</b>  ' . $this->vars['km'] . '<br/>';}
        if($this->vars['curriculo'])  {$this->body .= '<b>Curriculo:</b>  ' . $this->vars['curriculo'] . '<br/>';}
        if($this->vars['tab_dict'])  {$this->body .= $item_str . '<br/>';}
        
        // Dados do imóvel (em array [páginas: Encontramos seu imóvel])
        if (isset($this->vars['ar_imovel'])) {
            $this->body .= '<br><h3>Dados do imóvel:</h3>';
            if($this->vars['ar_imovel']['tipo']) {$this->body .= '<b>Tipo: </b>' . $this->vars['ar_imovel']['tipo'] . '<br/>'; }
            if($this->vars['ar_imovel']['bairro']) {$this->body .= '<b>Bairro: </b>' . $this->vars['ar_imovel']['bairro'] . '<br/>'; }
            if($this->vars['ar_imovel']['cidade']) {$this->body .= '<b>Cidade: </b>' . $this->vars['ar_imovel']['cidade'] . '<br/>'; }
            if($this->vars['ar_imovel']['area']) {$this->body .= '<b>Área: </b>' . $this->vars['ar_imovel']['area'] . '<br/>'; }
            if($this->vars['ar_imovel']['valor_minimo']) {$this->body .= '<b>Valor mínimo: </b>R$ ' . $this->vars['ar_imovel']['valor_minimo'] . '<br/>'; }
            if($this->vars['ar_imovel']['valor_maximo']) {$this->body .= '<b>Valor mínimo: </b>R$ ' . $this->vars['ar_imovel']['valor_maximo'] . '<br/>'; }
            if($this->vars['ar_imovel']['descricao']) {$this->body .= '<b>Descrição: </b>' . $this->vars['ar_imovel']['descricao'] . '<br/>'; }
        }

		if($this->vars['attach']) 	{$this->CI->email->attach($this->vars['attach']);}

        $this->CI->email->reply_to($this->vars['email'], $this->vars['name']);

		$rs = $this->get_email_session($this->vars['session_id']);
        foreach ($rs as $r) $this->email_to[] = $r['email'];

        if (!empty($this->vars["order_email"])) {
            $this->email_to[] = $this->vars["order_email"];
        }

        $this->CI->email->to($this->email_to);
    }
    
    /**
     * send_portalnewsletter
     *
     * @description Configure to send a newsletter email
     * @access public
     * @return void
     */
    public function send_portalnewsletter() {
        $this->email_to = $this->vars['emails_clientes'];
                
        $this->session = 'portalnewsletter';
        $this->subject = $this->vars['subject'];
        $this->body = $this->vars['body'];
        
        $this->CI->email->reply_to($this->vars['email'], $this->vars['name']);
        $this->CI->email->to($this->email_to);
    }
    
    /**
     * send_messagetofriend
     *
     * @description Configure to send a email to a friend
     * @access public
     * @return void
     */
    public function send_messagetofriend() {
        $this->email_to = addslashes(trim($this->vars['amigo_email']));
                
        $this->session = 'messagetofriend';
        $this->subject = $this->vars['subject'];
        $this->body = $this->vars['body'];
        
        $this->CI->email->reply_to(addslashes(trim($this->vars['email'])), addslashes(trim($this->vars['name'])));
        $this->CI->email->to($this->email_to);
    }
}
