<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Error Class
 *
 * @description Implements Error to application controllers.
 * @author Eduardo Messias (eduardo.inf@gmail.com)
 * @package libraries
 */

class Form {

	private $form = array();
	private $meio_captacao = NULL;

	function __construct() {
		$CI =& get_instance();
	}

	/**
	 * set_form
	 *
	 * @description cria o array do formulário
	 * @param array $fields array com os campos
	 * @param string $name identificação do form
	 * @param string $meio_captacao meio de captação gerado pelo crm
	 * @return void
	 */
	public function set_form($fields, $name, $meio_captacao) {
		$this->meio_captacao = $meio_captacao;

		$this->form[$name] = (object) array(
			'meio_captacao' => $meio_captacao,
			'fields' => $fields
		);

		/*
		** Exemplo de formulário:
		* type default = text
		* required default = true

		array(
			array(
				'label' => 'Nome',
				'name' => 'nome',
			),
			array(
				'type' => 'email',
				'label' => 'E-mail',
				'name' => 'email',
			),
			array(
				'label' => 'Telefone',
				'name' => 'telefone',
				'class' => 'telefone',
				'required' => FALSE,
			),
			array(
				'type' => 'textarea',
				'label' => 'Mensagem',
				'name' => 'mensagem',
			),
			array(
				'type' => 'submit',
				// 'label' => 'Enviar',
				// 'loading' => 'Enviando...',
				// 'class' => '',
			),
		);
		*/
	}

	/**
	 * get_form
	 *
	 * @description retorna o html do formulário
	 * @return string html
	 */
	public function get_form($name) {
		if ($name) {
			$form = $this->form[$name];

			$html = '';
			if ($form->meio_captacao)
				$html = '<input type="hidden" name="meio_captacao" value="'. $form->meio_captacao .'">';

			foreach ($form->fields as $field) {
				// if (is_array($field)) {}

				$type = (isset($field['type'])) ? $field['type'] : 'text';
				$label = (isset($field['label'])) ? $field['label'] : '';
				$name = (isset($field['name'])) ? $field['name'] : '';
				$value = (isset($field['value'])) ? $field['value'] : NULL;
				$class = (isset($field['class'])) ? $field['class'] : '';
				$required = (isset($field['required']) && $field['required'] == FALSE) ? '' : 'required area-required="true"';

				$sub_label = (isset($field['label'])) ? $field['label'] : 'Enviar';
				$sub_loading = (isset($field['loading'])) ? $field['loading'] : 'Enviando...';

				if ($type != 'submit') {
					if ($type != 'hidden')
						$html .= '<div class="form-group">';

					$html .= <<<EOF
						<input type="hidden" name="label[$name]" value="$label">
EOF;

					if ($type == 'textarea') {
						$html .= <<<EOF
							<textarea type="$type" name="field[$name]" class="textarea form-control $class" placeholder="$label" $required>$value</textarea>
EOF;
					} else {
						$html .= <<<EOF
							<input type="$type" name="field[$name]" class="input form-control $class" value="$value" placeholder="$label" $required>
EOF;
					}

					if ($type != 'hidden')
						$html .= '</div>';
				} else {
					$html .= <<<EOF
						<button type="submit" class="btn button $class" data-loading-text="$sub_loading">
							<span></span>
							$sub_label
						</button>
EOF;
				}
			}

			//echo '<pre>'; print_r($html); die;
			return $html;
		}
	}
}