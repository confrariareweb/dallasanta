<?php

class Model_area_restrita extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function check_auth() {
        if (isset($this->session->userdata['area_restrita'])) {
            return TRUE;
        } else{
            return FALSE;
        }
    }

    // Dados do usuário
    public function salvar($post) {
        $fiels = $values = array();
        $duplicate_qry = ' ON DUPLICATE KEY UPDATE ';

        if (isset($post['senha']))
            $post['senha'] = crypt($post['senha']);

        if (isset($_FILES['foto']) && $_FILES['foto']['error'] === 0) {
            // upload
            $config['upload_path'] = './upload/area-restrita/';
    		$config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = vsprintf('avatar-%s-%s-%s', array_map('slug', array($post['nome'], $post['sobrenome'], strtotime('now'))));
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload');
            $this->upload->initialize($config);

    		if (!$this->upload->do_upload('foto')) {
    			die($this->upload->display_errors('', ''));
    		} else {
    			$upload_data = $this->upload->data();
    		}

            if (isset($upload_data)) {
                $post['foto'] = $upload_data['file_name'];

                // Pega os dados atuais para atualizar a session
                $area_restrita = $this->session->userdata['area_restrita'];
                $foto_old = $area_restrita->foto;

                // Seta a nova imagem de perfil
                $area_restrita->foto = $post['foto'];
                $this->session->set_userdata('area_restrita', $area_restrita);

                if (!empty($foto_old) && !empty($area_restrita->foto)) {
                    // Delete a foto antiga
                    if (is_file($config['upload_path'].$foto_old) && file_exists($config['upload_path'].$foto_old))
                        unlink($config['upload_path'].$foto_old);
                }
            }
        } else
            unset($post['foto']);

        //var_dump($post); die;

        foreach ($post as $key => $val) {
            if ($key != 'nova_senha') {
                $val = mysql_real_escape_string(trim($val));

                if (!empty($val) || $val == 'NULL') {
                    if ($val != 'NULL')
                        $val = sprintf("'%s'",$val);

                    $fields[] = $key;
                    $values[] = $val;

                    if ($key !== 'email')
                        $duplicate_qry .= sprintf("%s = %s, ", $key, $val);
                }
            }
        }

        $qry = vsprintf("INSERT INTO area_restrita_usuario (%s) VALUES (%s)", array(implode(',', $fields), implode(',', $values)));
        $qry .= substr($duplicate_qry, 0, strrpos($duplicate_qry, ','));

        return $this->db->query($qry);
    }

    public function get_dados_usuario($email = NULL, $token_recuperar_senha = NULL, $token_ativar_conta = NULL) {
        if ($token_recuperar_senha !== NULL) { // Se existir o token de recuperar senha
            // Pega o 'sha1' do token
            $token = $this->decrypt_token($token_recuperar_senha);

            $this->db->where('token_recuperar_senha', $token);
        } else if ($token_ativar_conta !== NULL) { // Se existir o token de ativação de conta
            // Pega o 'sha1' do token
            $token = $this->decrypt_token($token_ativar_conta);

            $this->db->where('token_ativar_conta', $token);
        } else { // Se não, login normal
            if ($this->check_auth()) {
                $email = $this->session->userdata['area_restrita']->email;
            } else if ($email !== NULL) {
                $email = mysql_real_escape_string($email);
            }

            $this->db->where('email', $email);
        }

        $usuario = NULL;
        if (!empty($this->db->ar_where)) {
            $this->db->select('*', FALSE)
                     ->from('area_restrita_usuario');
            $qry = $this->db->get();

            if ($qry->num_rows > 0)
                $usuario = $qry->row_array();
        }

        return $usuario;
    }

    // Favoritos
    public function favoritar_imovel($id_imovel) {
        try {

            $id_imovel = intval(mysql_real_escape_string($id_imovel));

            if (empty($id_imovel)) {
                throw new Exception('Nenhum imóvel foi informado.');
            }

            $this->load->model('model_imovel');
            $imovel = $this->model_imovel->get_imovel($id_imovel); // Verifica se o imóvel existe

            if (empty($imovel)) {
                throw new Exception('Imóvel inexistente');
            }

            // Verifica se o imóvel já é favorito
            $this->db->select('count(1) as count')
                     ->from('area_restrita_usuario_favorito')
                     ->where('email', $this->session->userdata['area_restrita']->email)
                     ->where('id_imovel', $id_imovel);
            $qry = $this->db->get();
            $acao = (intval($qry->row('count')) > 0) ? 'remover' : 'adicionar';

            $data = array(
                'id_imovel' => $id_imovel,
                'email' => $this->session->userdata['area_restrita']->email
            );

            if ($acao == 'remover') {
                if (!$this->db->delete('area_restrita_usuario_favorito', $data))
                    return FALSE;
            } else{
                if (!$this->db->insert('area_restrita_usuario_favorito', $data))
                    return FALSE;
            }

            return $acao;

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function get_imoveis_favoritos() {
        if (!$this->check_auth())
            return array();

        $this->db->select('GROUP_CONCAT(id_imovel) AS favoritos')
                 ->from('area_restrita_usuario_favorito')
                 ->where('email', $this->session->userdata['area_restrita']->email);
        $favoritos = $this->db->get()->row('favoritos');

        return array_map('intval', explode(',', $favoritos));
    }

    public function gera_token($email, $tipo = 'token_recuperar_senha') {
        if (!empty($email)) {
            if ($tipo == 'token_recuperar_senha' || $tipo == 'token_ativar_conta') {
                // Token de segurança (o que importa é o 'sha1')
                $sha = sha1(time());
                $token = base64_encode(random_string('alnum', 8) . $sha . random_string('alnum', 6));

                $data = array(
                    $tipo => $sha
                );
                $where = array(
                    'email' => mysql_real_escape_string($email)
                );

                if ($this->db->update('area_restrita_usuario', $data, $where)) {
                    return $token;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    public function decrypt_token($token) {
        // Pega o 'sha1' do token
        return substr(base64_decode(mysql_real_escape_string($token)), 8, 40);
    }

}
