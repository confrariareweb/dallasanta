<?php

class Model_imovel extends CI_Model {

    private $tokens = '_authTokenId=5PX8mdgiAX5V9v7ZxM0NueVcIM5w9vES9aOxiEbr3O4=;_authTokenNs=dallasanta;';
    private $uri_rest_imoveis = 'http://my.konecty.com/rest/data/Product/find';
    private $uri_rest_empreendimentos = 'http://my.konecty.com/rest/data/Development/find';
    private $uri_rest_banners = 'http://my.konecty.com/rest/data/WebElement/find';
    private $uri_rest_clientes_parceiros = 'http://my.konecty.com/rest/data/WebElement/find';

    public $type = 'imoveis';
    public $uri_images = 'http://cdn-my.konecty.com/rest/image/%s/%s/%s/dallasanta/%s?etag=%s'; // procesor, width, height, key, etag
    public $uri_images_all = 'http://cdn-my.konecty.com/rest/image/dallasanta/%s';
    public $status_pt = array(
        'Available' => 'Disponível',
        'Negotiations' => 'Negociação',
        'Rented' => 'Alugado',
        'Processing' => 'Processamento'
    );

    function __construct() {
        parent::__construct();
    }

    public function get_imoveis($filtro = NULL, $sort = NULL, $limit = 10, $page = NULL) {

        $http_qry = array(
            '_dc' => strtotime('now'),
            'limit' => $limit
        );


        if ($page !== NULL) {
            $http_qry['page'] = $page;
            $http_qry['start'] = (($page-1) * $limit);
        }


        $conditions = array();

        if ($this->type == 'imoveis') {
            $conditions[] = array(
                'term' => 'editorialStatus',
                'operator' => 'equals',
                'value' => 'Aprovado'
            );
        } else {
            $conditions[] = array(
                'term' => 'status',
                'operator' => 'equals',
                'value' => 'Ativo'
            );
        }
//        $this->model_imovel->type = 'banners';
//        $conditions = array(
//            array(
//            'term' => 'type',
//            'operator' => 'in',
//            'value' => array("","Parceiros")
//            )
//        );

        
        if ($filtro !== NULL) {
            if(isset($filtro['textSearch'])){
                $textSearch = $filtro['textSearch'];
                unset($filtro['textSearch']);
            }
            foreach ($filtro as $condition) {
                $conditions[] = $condition;
            }
        }

        if (!empty($conditions)) {
            $filter = array(
                'match' => 'and',
                'conditions' => $conditions

                // array(
                //     'term' => 'type',
                //     'operator' => 'in',
                //     'value' => array('Loja', 'Sala')
                // ),
            );
            if(isset($textSearch)){
                $filter['textSearch'] = $textSearch;
                unset($textSearch);
            }
            $filter = json_encode($filter);
            //echo '<pre>'; print(json_encode(json_decode($filter), JSON_PRETTY_PRINT)); //die;
            //print_r($this->type);
            $http_qry['filter'] = $filter;

            //echo '<pre>'; print_r($filter); echo '</pre>';
        }


        if ($sort !== NULL) {
            $http_qry['sort'] = json_encode($sort);
        }

        eval('$uri = $this->uri_rest_'.$this->type.';');

        // Chama a url passando os parâmetros
        $ch = curl_init($uri.'?'.http_build_query($http_qry));
        curl_setopt($ch, CURLOPT_COOKIE, $this->tokens);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $rest_result = json_decode(curl_exec($ch));
        curl_close($ch);
        //var_dump($rest_result); die;
        //echo '<pre>'; print_r($rest_result);die();


        // Se retornar tudo certo, crio meu próprio array
        $imoveis = (object) array(
            'total' => 0,
            'imoveis' => array()
        );
        if ($rest_result && $rest_result->success == TRUE) {
            if ($rest_result->total > 0) {

                $imoveis->total = $rest_result->total;
                $data = $rest_result->data;

                switch($this->type) {

                    case 'banners':
                        $Banner = array();
                        foreach ($data as $banner) {
                            $Banner[] = (object)array(
                                    'banner' => $banner->file[0]->key,
                                    'link'   => $banner->link,
                                    'name'   => $banner->name,
                                    'html'   => $banner->html
                            );
                        }
                        return $Banner;

                    break;
                    case 'clientes_parceiros':
                        $Cliente_parceiros = array();
                        foreach ($data as $cliente) {
                            $Cliente_parceiros[] =  (object)array(
                                    'datas'                      => array(
                                    'name' => $cliente->name,
                                    'file' => $cliente->file[0]->key,
                                    'link' => $cliente->link
                                ));

                        }
                        return $Cliente_parceiros;

                        break;
                    default:

                        foreach ($data as $imovel) {
                            $categoria = (isset($imovel->type)) ? $imovel->type : null;
                            $imoveis->imoveis[] = (object)array(
                                'datas'                      => array(
                                    'insert' => $imovel->_createdAt->{'$date'},
                                    'update' => $imovel->_updatedAt->{'$date'}
                                ),
                                'id'                         => $imovel->code,
                                'name_empreendimento'        => $imovel->name,
                                'destaque'                   => (isset($imovel->highlight)) ? $imovel->highlight : false,
                                'tipo_imovel'                => (isset($imovel->businessType)) ? $imovel->businessType : null,
                                'categoria'                  => $categoria,
                                'titulo'                     => sprintf('%s no bairro %s em %s, %s', ($categoria) ? $categoria : 'Imóvel', $imovel->address->district, $imovel->address->city, $imovel->address->state),
                                'subtitulo'                  => sprintf('%s %s, %s', $imovel->address->placeType, $imovel->address->place, $imovel->address->number),
                                'descricao'                  => (isset($imovel->description)) ? $imovel->description : null,
                                'descricao_empreendimento'   => (isset($imovel->developmentDescription)) ? $imovel->developmentDescription : null,
                                'endereco'                   => (isset($imovel->address)) ? $imovel->address : null,
                                'area_privativa'             => (isset($imovel->areaPrivate)) ? $imovel->areaPrivate : null,
                                'area_total'                 => (isset($imovel->areaTotal)) ? $imovel->areaTotal : null,
                                'preco_aluguel'              => (isset($imovel->rent)) ? $imovel->rent :  null,
                                'preco_venda'                => (isset($imovel->sale)) ? $imovel->sale :  null,
                                'condominio'                 => (isset($imovel->condominiumAmount)) ? $imovel->condominiumAmount : null,
                                'iptu'                       => null,
                                'dormitorios'                => (isset($imovel->bedrooms)) ? $imovel->bedrooms : null,
                                'status'                     => (isset($imovel->status)) ? $imovel->status : null,
                                'vagas'                      => (isset($imovel->parkingSpaces)) ? $imovel->parkingSpaces : null,
                                'caracteristicas'            => (object)array(
                                    'suites'    => (isset($imovel->suites)) ? $imovel->suites : null,
                                    'banheiros' => (isset($imovel->bathrooms)) ? $imovel->bathrooms : null,
                                ),
                                'fotos'                      => (isset($imovel->pictures)) ? $imovel->pictures : null,
                                'empreendimento'             => (isset($imovel->development)) ? $imovel->development : null,
                                'fotos_empreendimento'       => (isset($imovel->developmentPictures)) ? $imovel->developmentPictures : null,
                                'videos'                     => (isset($imovel->videos)) ? $imovel->videos : null,
                                'street_view'                => (isset($imovel->streetView)) ? $imovel->streetView : null,
                                'tour_virtual'               => (isset($imovel->developmentVirtualTour)) ? $imovel->developmentVirtualTour : null,
                                'caracteristicas_condominio' => (isset($imovel->features)) ? $imovel->features : null,
                                'status_editorial'           => (isset($imovel->editorialStatus)) ? $imovel->editorialStatus : false,
                                'showprice'           => (isset($imovel->showPrice)) ? $imovel->showPrice : null
                            );
                        }

                    break;
                }
            }
        } else {
            die($rest_result->errors[0]->message);
        }
        //var_dump($imoveis); die;

        return $imoveis;
    }

    function get_imovel($id_imovel) {
        if (!empty($id_imovel)) {
            $filtro = array(
                array(
                    'term' => 'code',
                    'operator' => 'equals',
                    'value' => intval($id_imovel)
                )
            );

            $imovel = $this->get_imoveis($filtro);

            if ($imovel->total > 0) {
                $imovel = $imovel->imoveis[0];
            } else {
                $imovel = NULL;
            }
          //  echo '----------------';
           // echo '<pre>'; print_r($imovel); die;
            return $imovel;
        } else
            return null; //die('Nenhum imóvel foi informado.');
			
    }

}

?>
