<?php

    class Model_parceiro extends CI_Model {
        
        function __construct() {
            //parent::__construct();
            $this->table = 'parceiro';
        }
        
        public function get_parceiro($id_parceiro = NULL) {
            $this->db->select('*')->from($this->table)->order_by('nome');
            
            if ($id_parceiro) {
                $this->db->where('id_parceiro', intval($id_parceiro));
            }
            
            $qry = $this->db->get();
            
            $parceiro = array();
            if ($qry->num_rows > 0)
                $parceiro = $qry->result();
                
            return $parceiro;
        }
        
    }

?>
