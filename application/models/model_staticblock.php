<?php

class Model_Staticblock extends CI_Model {
    private $field;

    function __construct() {
        parent::__construct();
    }

    public function select($field = null) {
        if (empty($field)) {
            $this->field = null;

            return $this;
        }

        if (!is_array($field)) {
            throw new Exception("field error(array required)");
            
        }

        $this->field = $field;

        return $this;
    }

    public function filter($staticblock__id = null, $staticblock__binding = null) {
        $order_by ="core_staticblock.id desc";
        $where_list = array();

        if (!empty($staticblock__id)) {
            $where_list["core_staticblock.id"] = $staticblock__id;
        }

        if (!empty($staticblock__binding)) {
            $where_list["core_staticblock.binding"] = $staticblock__binding;
        }

        if (!empty($this->field)) {
            $field = implode(", ",$this->field);

            $staticblock_list = $this->db->select($field);

        } else {
            $staticblock_list = $this->db->select("
                core_staticblock.id id,
                core_staticblock.id staticblock__id,
                core_staticblock.title staticblock__title,
                core_staticblock.binding staticblock__binding,
                core_staticblock.content staticblock__content");
        }

        $staticblock_list = $staticblock_list
            ->from("core_staticblock")
            ->order_by($order_by);

        if (!empty($where_list)) {
            $staticblock_list = $staticblock_list->where($where_list)->get()->result_object();

        } else {
            $staticblock_list = $staticblock_list->get()->result_object();            
        }

        if (empty($staticblock_list)) {
            return $staticblock_list;
        }

        if (!empty($staticblock__id) || !empty($staticblock__binding)) {
            $staticblock_list = $staticblock_list[0];
        }

        return $staticblock_list;
    }
}

?>