<?php

class Model_Banner extends CI_Model {
    private $field;

    function __construct() {
        parent::__construct();

        $this->table = 'banner';
    }

    public function get_banners($filtro = NULL, $imagens = TRUE) {
        
        
        
        $this->db->select('banner.*')
                  ->from($this->table)
                  ->join('bannertipo', 'bannertipo.id = banner.tipo_id')
                  ->where('banner.status', 1)
                  ->order_by('ordem');


    
    
    
       if ($filtro) {
           
            foreach ($filtro as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        $qry = $this->db->get();

        if ($qry->num_rows > 0) {
            $banners = $qry->result();

            $this->db->select('id, rel_id, title, file')
                     ->from('gallery')
                     ->where('status', 1)
                     ->where('rel', 'banner')
                     ->order_by('order');
            $qry = $this->db->get();

            if ($qry->num_rows > 0) {
                $imagens = $qry->result();

                foreach ($banners as $i => $banner) {
                    $banners[$i]->galeria = array();

                    foreach ($imagens as $imagem) {
                        if ($imagem->rel_id == $banner->id) {
                            $banners[$i]->galeria[] = $imagem;
                        }
                    }
                }
            }
        } else
            $banners = NULL;

        return $banners;
    }
}

?>
