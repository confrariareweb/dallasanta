<?php

    class Model_empreendimento extends CI_Model {

        private $tokens = '_authTokenId=5PX8mdgiAX5V9v7ZxM0NueVcIM5w9vES9aOxiEbr3O4=;_authTokenNs=dallasanta;';
        private $uri_rest = 'http://my.konecty.com/rest/data/Product/find';

        public $uri_images = 'http://cdn-my.konecty.com/rest/image/%s/%s/%s/dallasanta/%s?etag=%s'; // procesor, width, height, key, etag
        public $status_pt = array(
            'Available' => 'Disponível',
            'Negotiations' => 'Negociação',
            'Rented' => 'Alugado',
            'Processing' => 'Processando'
        );

        function __construct() {
            //parent::__construct();

            $this->load->model('model_imovel');
            var_dump($this->model_imovel->uri_images);

            $this->table = 'empreendimento';
        }

        public function get_tipos_empreendimento() {
            $this->db->select('id_tipo_empreendimento, titulo')
                     ->from($this->table.'_tipo')
                     ->order_by('titulo');
            $qry = $this->db->get();

            $tipos = array();
            if ($qry->num_rows > 0)
                $tipos = $qry->result();

            return $tipos;
        }

        public function get_empreendimentos($id_empreendimento = NULL) {
            $this->db->select('*')->from($this->table)->order_by('titulo');

            if ($id_empreendimento) {
                $this->db->where('id_empreendimento', intval($id_empreendimento));
            }

            $qry = $this->db->get();

            $cliente = array();
            if ($qry->num_rows > 0)
                $cliente = $qry->result();

            return $cliente;
        }

        public function get_empreendimentos_by_tipo() {
            $tipos = $this->get_tipos_empreendimento();
            $empreendimentos = array();

            if ($tipos) {
                $all_empreendimentos = $this->get_empreendimentos();

                foreach ($tipos as $i => $tipo) {
                    $empreendimentos[$i] = (object) array(
                        'id_tipo_empreendimento' => $tipo->id_tipo_empreendimento,
                        'titulo' => $tipo->titulo
                    );

                    $empreendimentos[$i]->empreendimentos = array_values(array_filter($all_empreendimentos, function ($empreendimento) use ($tipo) {
                        if ($tipo->id_tipo_empreendimento == $empreendimento->id_tipo_empreendimento) {
                            return $empreendimento;
                        }
                    }));
                }
            }
            //var_dump($empreendimentos); die;
            return $empreendimentos;
        }

    }

?>
