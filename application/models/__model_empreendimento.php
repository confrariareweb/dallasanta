<?php

    class Model_empreendimento extends CI_Model {

        function __construct() {
            //parent::__construct();
            $this->table = 'empreendimento';
        }

        public function get_tipos_empreendimento() {
            $this->db->select('id_tipo_empreendimento, titulo')
                     ->from($this->table.'_tipo')
                     ->order_by('titulo');
            $qry = $this->db->get();

            $tipos = array();
            if ($qry->num_rows > 0)
                $tipos = $qry->result();

            return $tipos;
        }

        public function get_empreendimentos($id_empreendimento = NULL) {
            $this->db->select('*')->from($this->table)->order_by('titulo');

            if ($id_empreendimento) {
                $this->db->where('id_empreendimento', intval($id_empreendimento));
            }

            $qry = $this->db->get();

            $cliente = array();
            if ($qry->num_rows > 0)
                $cliente = $qry->result();

            return $cliente;
        }

        public function get_empreendimentos_by_tipo() {
            $tipos = $this->get_tipos_empreendimento();
            $empreendimentos = array();

            if ($tipos) {
                $all_empreendimentos = $this->get_empreendimentos();

                foreach ($tipos as $i => $tipo) {
                    $empreendimentos[$i] = (object) array(
                        'id_tipo_empreendimento' => $tipo->id_tipo_empreendimento,
                        'titulo' => $tipo->titulo
                    );

                    $empreendimentos[$i]->empreendimentos = array_values(array_filter($all_empreendimentos, function ($empreendimento) use ($tipo) {
                        if ($tipo->id_tipo_empreendimento == $empreendimento->id_tipo_empreendimento) {
                            return $empreendimento;
                        }
                    }));
                }
            }
            //var_dump($empreendimentos); die;
            return $empreendimentos;
        }

    }

?>
