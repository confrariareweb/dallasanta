<?php

    class Model_cliente extends CI_Model {
        
        function __construct() {
            //parent::__construct();
            $this->table = 'cliente';
        }
        
        public function get_cliente($id_cliente = NULL) {
            $this->db->select('*')->from($this->table)->order_by('nome');
            
            if ($id_cliente) {
                $this->db->where('id_cliente', intval($id_cliente));
            }
            
            $qry = $this->db->get();
            
            $cliente = array();
            if ($qry->num_rows > 0)
                $cliente = $qry->result();
                
            return $cliente;
        }
        
    }

?>
