<?php

class Model_Content extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function get_conteudo($nome) {
        $this->db->select('*')
                 ->from('conteudo')
                 ->where('conteudo.nome', $nome);
        $qry = $this->db->get();

        if ($qry->num_rows > 0) {
            $conteudo = $qry->row();

            $this->db->select('id, file')
                     ->from('gallery')
                     ->where('rel_id', $conteudo->id)
                     ->order_by('order');
            $qry = $this->db->get();

            if ($qry->num_rows > 0)
                $conteudo->gallery = $qry->result();
            else
                $conteudo->gallery = array();
        }

        return $conteudo;
    }
}
