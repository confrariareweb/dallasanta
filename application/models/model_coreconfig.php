<?php

class Model_Coreconfig extends CI_Model {
    private $field;

    function __construct() {
        parent::__construct();
    }

    public function select($field = null) {
        if (empty($field)) {
            $this->field = null;

            return $this;
        }

        if (!is_array($field)) {
            throw new Exception("field error(array required)");
            
        }

        $this->field = $field;

        return $this;
    }

    public function filter($coreconfig__option = null, $coreconfig__group = null) {
        $order_by ="core_config.id asc";
        $where_list = array("core_config.status" => 1);

        if (!empty($coreconfig__option)) {
            $where_list["core_config.option"] = $coreconfig__option;
        }

        if (!empty($coreconfig__group)) {
            $where_list["core_config.group"] = $coreconfig__group;
        }

        if (!empty($this->field)) {
            $field = implode(", ",$this->field);

            $coreconfig_list = $this->db->select($field);

        } else {
            $coreconfig_list = $this->db->select("
                core_config.id id,
                core_config.id coreconfig__id,
                core_config.option coreconfig__option,
                core_config.value coreconfig__value,
                core_config.group coreconfig__group,
                core_config.title coreconfig__title,
                core_config.description coreconfig__description,
                core_config.input_type coreconfig__input_type,
                core_config.status coreconfig__status,");
        }

        $coreconfig_list = $coreconfig_list
            ->from("core_config")
            ->order_by($order_by)
            ->where($where_list)
            ->get()
            ->result_object();

        if (empty($coreconfig_list)) {
            return $coreconfig_list;
        }

        if (!empty($coreconfig__option)) {
            $coreconfig_list = $coreconfig_list[0];
        }

        return $coreconfig_list;
    }
}

?>