<?php
$img_src = null;
$file_explode = explode(".", $file);
$file_count = count($file_explode);

if ($file_explode[$file_count - 1] == "pdf") {
    $img_src = "media/assets/frontend/img/thumb_pdf.jpg";
} else if ($file_explode[$file_count - 1] == "doc" || $file_explode[$file_count - 1] == "docx") {
    $img_src = "media/assets/frontend/img/thumb_word.jpg";
} else {
    $img_src = 'media/upload/' . $folder . '/' . $file;
}
?>
<li data-file="<?php echo $file; ?>">
<?php echo img(image($img_src, '250x200')); ?>
    <span>
        <a href="swfupload/edit/<?php echo $rel; ?>/<?php echo $file; ?>/load" class="name ajax"><?php echo $title . " | " . $type; ?></a>
        <a href="swfupload/edit/<?php echo $rel; ?>/<?php echo $file; ?>/load" class="edit ajax"></a>
        <a href="<?php echo 'upload/' . $folder . '/' . $file; ?>" class="view"></a>
        <a class="delete"></a>
    </span>
    <div class="move_icon"><img src="assets/backend/img/move.png"  height="17" alt="Para ordenar arraste suas imagens" title="Para ordenar arraste suas imagens" /></div>
</li>

