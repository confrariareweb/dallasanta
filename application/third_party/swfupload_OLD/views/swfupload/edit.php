<div class="photoEdit">
    <form id="editphoto" action="swfupload/save_edit/<?= $rel; ?>/<?= $file; ?>" data-file="<?= $file; ?>" method="post" class="stdform quickform2">
        <h3>Edição dos detalhes da imagem</h3>
        <br />
        <div class="notifyMessage">Atualizado</div>
        <p>
            <label>Título</label>
            <input type="text" name="title" value="<?= $title; ?>" />
        </p>
        <p>
            <label>Tipo/Categoria</label>
            <select name="type">
                <option value="normal" <?php if ($type == "normal") { ?>selected="selected"<?php } ?> >Normal</option>
                <!--option value="mini" <?php if ($type == "mini") { ?>selected="selected"<?php } ?> >Miniatura</option-->
                <!--option value="featured" <?php if ($type == "featured") { ?>selected="selected"<?php } ?> >Destaque</option-->
                <option value="pdf_doc" <?php if ($type == "pdf_doc") { ?>selected="selected"<?php } ?> >PDF/DOC</option>

                <optgroup label="Tipo de Upload">
                    <?php foreach ($this->db->get('portal_tipo_upload')->result_array() as $tu) { ?>
                        <option value="<?php echo $tu['id']; ?>" <?php if ($type_id == $tu['id']) { ?>selected="selected"<?php } ?> ><?php echo $tu['nome']; ?></option>
                    <?php } ?>
                </optgroup>

            </select>
        </p>
        <p>
            <label>Status</label>
            <select name="status">
                <option value="1" <?php if ($status == 1) echo 'selected="selected"'; ?>>Ativo</option>
                <option value="0" <?php if ($status == 0) echo 'selected="selected"'; ?>>Inativo</option>
            </select>
        </p>
        <span class="clear clearfix">
            <p class="action">
                <button class="submit radius2">Salvar alterações</button> &nbsp;
                <button class="cancel radius2">Fechar</button>
            </p>
            <br />
    </form>
</div><!--photoEdit-->