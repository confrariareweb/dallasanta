<?php

class Clientes extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('model_cliente');
        $this->load->model('model_imovel');
        $this->view = 'clientes';
    }

    public function index() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Clientes' => ''
        );


        $this->model_imovel->type = 'clientes_parceiros';
        $filtro = array(
            array(
                'term' => 'type',
                'operator' => 'equals',
                'value' => 'Clientes'
            )
        );
        $clientes = $this->model_imovel->get_imoveis($filtro, null, 100);

        $data = array(
            'clientes' => $clientes
        );
        $this->load->front_view($this->view, $data);
    }
}
