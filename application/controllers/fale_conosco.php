<?php

class Fale_conosco extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->helper('captcha');
        $this->load->library('email');

        define('VIEW_DIR', 'fale-conosco');
    }

    public function index() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => ''
        );



        // Indicar para amigo
        $form_indicar = array(
            array(
                'label' => 'Seu Nome',
                'name' => 'nome',
            ),
            array(
                'type' => 'email',
                'label' => 'Seu e-mail',
                'name' => 'email',
            ),
            array(
                'label' => 'Nome do amigo',
                'name' => 'nome_amigo',
            ),
            array(
                'type' => 'email',
                'label' => 'E-mail do amigo',
                'name' => 'email_amigo',
            ),
            array(
                'type' => 'submit',
            ),
        );
        $this->form->set_form($form_indicar, 'imovel_indicar_para_amigo', 'SITE_IMOVEL_ENVIAR_EMAIL');

        $this->load->front_view(__FUNCTION__);
    }

    public function indicar(){
        $email = $_POST['field']['email_amigo'];
        $nome = $_POST['field']['nome'];
        $nome_amigo = $_POST['field']['nome_amigo'];

        if($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->email->from('sgriebeler@reweb.com.br', 'Dallasanta');
            $this->email->to($email);

            $this->email->subject('Indicar para um amigo - Dallasanta');
            //$this->email->message($teste .'Olá '. $nome_amigo. ', o seu amigo '. $nome. ' lhe indicou o imóvel do link abaixo.<br/>'
            $this->email->message("
            <html>
            <head></head>
                <body>
                      <img src='http://homolog.reweb.com.br/dallasanta/assets/frontend/img/logo.png' alt=''><br>
                      <p>Olá $nome_amigo, o seu amigo $nome lhe indicou o imóvel do link abaixo:</p><br>
                    <p>$_SERVER[HTTP_REFERER]</p> <br>
                </body>
            </html>
            ");
            $this->email->send();


            //           echo $this->email->print_debugger();
            $msg = 'Seu e-mail foi enviado com sucesso!';
            if ($this->input->is_ajax_request()) {
                echo json_encode(array(
                    'success' => TRUE,
                    'msg' => $msg
                ));
                exit();
            } else {
                $this->session->set_flashdata('success', $msg);
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function trabalhe_conosco() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Trabalhe Conosco' => ''
        );

        $this->load->front_view(__FUNCTION__);
    }

    public function oferecer_imovel() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Oferecer imóvel' => ''
        );

        $this->load->front_view(__FUNCTION__);
    }

    public function alugar_imovel() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Alugar imóvel' => ''
        );

        $form = array(
            array(
                'label' => 'Nome',
                'name' => 'nome',
            ),
            array(
                'type' => 'email',
                'label' => 'E-mail',
                'name' => 'email',
            ),
            array(
                'label' => 'Telefone',
                'name' => 'telefone',
                'class' => 'telefone',
            ),
            array(
                'type' => 'textarea',
                'label' => 'Mensagem',
                'name' => 'mensagem',
            ),
            array(
                'type' => 'submit',
            ),
        );
        $this->form->set_form($form, 'contato_alugar_imovel', 'SITE_ALUGAR_IMOVEL');

        $this->load->front_view(__FUNCTION__);
    }

    public function assessoria_imprensa() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Assessoria de imprensa' => ''
        );

        $form = array(
            array(
                'label' => 'Nome',
                'name' => 'nome',
            ),
            array(
                'type' => 'email',
                'label' => 'E-mail',
                'name' => 'email',
            ),
            array(
                'label' => 'Telefone',
                'name' => 'telefone',
                'class' => 'telefone',
            ),
            array(
                'type' => 'textarea',
                'label' => 'Mensagem',
                'name' => 'mensagem',
            ),
            array(
                'type' => 'submit',
            ),
        );
        $this->form->set_form($form, 'contato_assessoria', 'SITE_IMPRENSA');

        $this->load->front_view(__FUNCTION__);
    }

    public function fornecedor() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Quero ser fornecedor' => ''
        );

        $form = array(
            array(
                'label' => 'Nome',
                'name' => 'nome',
            ),
            array(
                'type' => 'email',
                'label' => 'E-mail',
                'name' => 'email',
            ),
            array(
                'label' => 'Telefone',
                'name' => 'telefone',
                'class' => 'telefone',
            ),
            array(
                'label' => 'Empresa',
                'name' => 'empresa',
            ),
            array(
                'label' => 'Ramo de atuação',
                'name' => 'ramo_atuacao',
            ),
            array(
                'type' => 'textarea',
                'label' => 'Mensagem',
                'name' => 'mensagem',
            ),
            array(
                'type' => 'submit',
            ),
        );
        $this->form->set_form($form, 'contato_fornecedor', 'SITE_FORNECEDOR');

        $this->load->front_view(__FUNCTION__);
    }

    public function manutencao() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Manutenção' => ''
        );

        $this->load->front_view(__FUNCTION__);
    }

    public function segunda_via() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Fale conosco' => 'fale-conosco',
            'Segunda via' => ''
        );
		$this->css[] = './assets/frontend/css/style.css';

        $this->load->front_view(__FUNCTION__);
    }
}
