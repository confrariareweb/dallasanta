<?php

class Empreendimentos extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('model_imovel');
        $this->model_imovel->type = 'empreendimentos';

        define('VIEW_DIR', 'empreendimentos');
    }

    public function index() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Empreendimentos' => ''
        );

        $empreendimentos = $this->model_imovel->get_imoveis(NULL, NULL, 0);


        if ($empreendimentos->total > 0) {
            $empreendimentos_by_tipo = array();

            foreach ($empreendimentos->imoveis as $empreendimento) {
                $categoria = (!empty($empreendimento->categoria)) ? $empreendimento->categoria : '_semcategoria';
                $categoria_slug = slug($categoria);

                if (!isset($empreendimentos_by_tipo[$categoria_slug]))
                    $empreendimentos_by_tipo[$categoria_slug] = array('titulo' => $categoria);

                $empreendimentos_by_tipo[$categoria_slug]['empreendimentos'][] = $empreendimento;
            }
        }

        asort($empreendimentos_by_tipo);
        //var_dump($empreendimentos_by_tipo); die;

        $data = array(
            'empreendimentos' => $empreendimentos_by_tipo
        );
        //var_dump($data); die;

        $this->load->front_view(__FUNCTION__, $data);
    }

    public function detalhes($id_empreendimento) {
        if (!isset($id_empreendimento))
            redirect('home');

        // Busca detalhes do empreendimento
        $this->model_imovel->type = 'empreendimentos';
        $empreendimento = $this->model_imovel->get_imovel($id_empreendimento);

        if ($empreendimento) {
            // Busca os imóveis do empreendimento
            $filtro = array(
                array(
                    'term' => 'development.code',
                    'operator' => 'equals',
                    'value' => intval($id_empreendimento)
                )
            );
            $this->model_imovel->type = 'imoveis';
            $empreendimento->imoveis = array_chunk($this->model_imovel->get_imoveis($filtro, NULL, 0)->imoveis, 4);


            $this->breadcrumb = array(
                'Home' => 'home',
                'Empreendimentos' => 'empreendimentos',
                $empreendimento->titulo => ''
            );
            $data = array(
                'empreendimento' => $empreendimento
            );
            //var_dump($empreendimento->imoveis); die;

            $this->load->front_view(__FUNCTION__, $data);
        } else {
            show_404();
        }
    }
}
