<?php

class Imoveis extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('model_imovel');
        $this->load->model('model_area_restrita');

        define('VIEW_DIR', 'imoveis');
    }

    public function index() {
        $data = array();
        $this->load->front_view(__FUNCTION__);
    }

    public function aluguel() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Imóveis' => '',
            'Aluguel' => ''
        );
        $this->busca(__FUNCTION__);
    }

    public function venda() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Imóveis' => '',
            'Venda' => ''
        );
        $this->busca(__FUNCTION__);
    }

    public function ordem($ordem) {
        $get = NULL;

        if (isset($ordem) && !empty($ordem)) {
            $ordem = str_replace('_', '-', mysql_real_escape_string(trim($ordem)));
            $allowed = array('mais-atual', 'menos-atual', 'maior-preco', 'menor-preco');

            if (in_array($ordem, $allowed)) {
                if (isset($_SERVER['HTTP_REFERER'])) {
                    $qry_old = explode('?', $_SERVER['HTTP_REFERER']);

                    $get = array();
                    if (isset($qry_old[1])) {
                        parse_str($qry_old[1], $get);

                        if (isset($get['page']))
                            unset($get['page']);
                    }
                    else {
                        $tipo_busca = ucfirst(strtolower(substr(strrchr($_SERVER['HTTP_REFERER'], '/'), 1)));

                        if ($tipo_busca == 'Venda' || $tipo_busca == 'Aluguel') {
                            $get['tipo-imovel'] = $tipo_busca;
                        }
                    }

                    $get['sort'] = $ordem;
                    $get = '?' . http_build_query($get);
                }
            }
        }

        redirect('imoveis/busca/'.$get);
    }

    public function busca($tipo_busca = NULL) {

        if (!isset($_GET['tipo-imovel']) && $tipo_busca != 'aluguel' && $tipo_busca != 'venda')
            $tipo_busca = 'aluguel';

        if (isset($_GET['tipo-imovel']))
            $tipo_busca = strtolower($_GET['tipo-imovel']);

        $data = array();

        $limit = 10;
        $page = (isset($_GET['page']) && !empty($_GET['page'])) ? $_GET['page'] : 1;

        $filtro = array(
            array(
                'term' => 'businessType',
                'operator' => 'equals',
                'value' => ucfirst($tipo_busca)
            )
        );
        if (isset($_GET)) {
            $get = $this->input->get();


            if (isset($_GET['redirect']) && $_GET['redirect'] == 'true') {
                unset($get['redirect']);
                redirect('imoveis/busca/?'.http_build_query($get));
            }

            if (isset($get['categorias']) && !empty($get['categorias'])) {
                $filtro[] = array(
                    'term' => 'type',
                    'operator' => 'in',
                    'value' => array_map('mysql_real_escape_string', array_filter($get['categorias']))
                );
            }

            if (isset($get['dormitorios']) && !empty($get['dormitorios'])) {
                $filtro[] = array(
                    'term' => 'bedrooms',
                    'operator' => 'equals',
                    'value' => intval($get['dormitorios'])
                );
            }

            if (isset($get['cidades']) && !empty($get['cidades'])) {
                $filtro[] = array(
                    'term' => 'address.city',
                    'operator' => 'in',
                    'value' => array_map('mysql_real_escape_string', array_filter($get['cidades']))
                );
            }

            if (isset($get['palavra-chave']) && !empty($get['palavra-chave'])) {
                $palavra_chave = mysql_real_escape_string($get['palavra-chave']);
                $filtro["textSearch"] = $palavra_chave;
                /*
                $filtro[] = array(
                    'term' => 'description',
                    'operator' => 'contains',
                    'value' => $palavra_chave
                );
                */
            }

            if (isset($get['bairros']) && !empty($get['bairros'])) {
                $filtro[] = array(
                    'term' => 'address.district',
                    'operator' => 'in',
                    'value' => array_map('mysql_real_escape_string', array_filter($get['bairros']))
                );
            }

            $sort = NULL;
            $value_property = ($tipo_busca == 'aluguel') ? 'rent.value' : 'sale.value';
           // $value_property = 'sale.value';

            if (isset($get['valor-min']) && !empty($get['valor-max'])) {
                $filtro[] = array(
                    'term' => $value_property,
                    'operator' => 'between',
                    'value' => array(
                        'greater_or_equals' => intval($get['valor-min']),
                        'less_or_equals' => intval($get['valor-max'])
                    )
                );
            }

            if (isset($get['area-min']) && !empty($get['area-max'])) {
                $filtro[] = array(
                    'term' => 'areaPrivate',
                    'operator' => 'between',
                    'value' => array(
                        'greater_or_equals' => intval($get['area-min']),
                        'less_or_equals' => intval($get['area-max'])
                    )
                );
            }

            if (isset($get['sort']) && !empty($get['sort'])) {
                switch ($get['sort']) {
                    case 'mais-atual':
                        $sort = array(
                            'property' => '_createdAt',
                            'direction' => 'DESC'
                        );
                        break;

                    case 'menos-atual':
                        $sort = array(
                            'property' => '_createdAt',
                            'direction' => 'ASC'
                        );
                        break;

                    case 'maior-preco':
                        $sort = array(
                            'property' => $value_property,
                            'direction' => 'DESC'
                        );
                        break;

                    case 'menor-preco':
                        $sort = array(
                            'property' => $value_property,
                            'direction' => 'ASC'
                        );
                        break;
                }
            }else{
				$sort = array(
                        'property' => $value_property,
                        'direction' => 'ASC'
                    );
			}

            $page_params = $get;
        }

        if (is_array($page_params)) {
            unset($page_params['page']);
            unset($page_params['busca']);

            if (!empty($page_params))
                $page_params = '&' . http_build_query($page_params);
            else
                $page_params = NULL;
        }
        //var_dump($page_params);


        $data['imoveis'] = $this->model_imovel->get_imoveis($filtro, $sort, $limit, $page);

        if (isset($get['codigo']) && !empty($get['codigo'])) {
            $valida = $this->model_imovel->get_imovel($get['codigo']);
            if(!empty($valida)) {
				$url = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $valida->titulo ) );
				$url = str_replace('--','-',preg_replace('/[^a-zA-Z0-9]/','-',$url).'-'.$tipo_busca.'-'.$valida->id.'.html');
                redirect('/'.$url);
                //redirect('imoveis/detalhes/' . $get['codigo']);
            }else{
                $data['imoveis'] = '';
            }
        }

        //echo '<pre>';print_r($data['imoveis']);die();
        // Busca os ids dos imóveis favoritos
        $data['imoveis_favoritos'] = $this->model_area_restrita->get_imoveis_favoritos();

        // Paginação
        $this->load->library('pagination');
        $config = array(
            'base_url' => get_core_url("imoveis/$tipo_busca")."/?".$page_params,
            'total_rows' => $data['imoveis']->total,
            'per_page' => $limit,
			'enable_query_strings' =>TRUE,
			'page_query_string' => TRUE,
			'reuse_query_string' => TRUE,
            'use_page_numbers' => TRUE
        );
        $this->pagination->initialize($config);
        $data['tipo_busca'] = $tipo_busca;

        $this->load->front_view(__FUNCTION__, $data);
    }
    public function detalhes($id_imovel, $empreendimento = FALSE) {

        $id_imovel = intval($id_imovel);

        if (empty($id_imovel))
            show_404();

        if ((bool) $empreendimento) { // Verifica se é empreendimento (temporário, pois não sei se será a mesma tela)
            $this->model_imovel->type = 'empreendimentos';
        }

        $imovel = $this->model_imovel->get_imovel($id_imovel);

        if (!$imovel)
            show_404();

        $data = array(
            'imovel' => $imovel
        );
//echo '<pre>'; print_r($data['imovel']);die();
        if (!empty($imovel->videos)) {
            if (strpos(strtolower($imovel->videos[0]), 'youtube') === FALSE) {
                $link = null;
            } else {
                $link = explode('v=', $imovel->videos[0]);
                $link = explode('&', $link[1]);
                $link = $link[0];
            }

            $imovel->video = $link;
        }

        if ((bool) $empreendimento) {
            // Busca os imóveis do empreendimento
            $filtro = array(
                array(
                    'term' => 'development.code',
                    'operator' => 'equals',
                    'value' => intval($id_imovel)
                )
            );
            $this->model_imovel->type = 'imoveis';
            $data['imovel']->empreendimento_imoveis = array_chunk($this->model_imovel->get_imoveis($filtro, NULL, 0)->imoveis, 4);

            $this->breadcrumb = array(
                'Home' => 'home',
                'Empreendimentos' => 'empreendimentos',
                $data['imovel']->titulo => ''
            );
        } else {
            // Busca os imóveis semelhantes
            $value_property = strrpos($this->uri->uri_string(),'aluguel')? 'rent.value' : 'sale.value';
			$valormin = strrpos($this->uri->uri_string(),'aluguel')? ((int)$imovel->preco_aluguel->value - (0.2 * (int)$imovel->preco_aluguel->value)) : ((int)$imovel->preco_venda->value - (0.2 * (int)$imovel->preco_venda->value));
			$valormax = strrpos($this->uri->uri_string(),'aluguel')? ((int)$imovel->preco_aluguel->value + (0.2 * (int)$imovel->preco_aluguel->value)) : ((int)$imovel->preco_venda->value + (0.2 * (int)$imovel->preco_venda->value));
            $filtro = array(
                // array(
                    // 'term' => 'address.city',
                    // 'operator' => 'equals',
                    // 'value' => $imovel->endereco->city
                // ),
                // array(
                    // 'term' => 'address.state',
                    // 'operator' => 'equals',
                    // 'value' => $imovel->endereco->state
                // ),
		array(
                    'term' => 'type',
                    'operator' => 'equals',
                    'value' => $imovel->categoria
                ),
		array(
                    'term' => $value_property,
                    'operator' => 'between',
                    'value' => array(
                        'greater_or_equals' => $valormin,
                        'less_or_equals' => $valormax
                    )
                ),
		array(
                    'term' => 'address.district',
                    'operator' => 'equals',
                    'value' => $imovel->endereco->district
				),
                array(
                    'term' => 'businessType',
                    'operator' => 'equals',
                    'value' => $imovel->tipo_imovel
                )             
            );
            $this->model_imovel->type = 'imoveis';
            $data['imovel']->imoveis_semelhantes = array_chunk($this->model_imovel->get_imoveis($filtro, NULL, 20)->imoveis, 4);
            //echo '<pre>';print_r($data['imovel']);die();
            $data['tipo_busca'] = strrpos($this->uri->uri_string(),'aluguel')? 'aluguel':'venda';

            $this->breadcrumb = array(
                'Home' => 'home',
                'Imóveis para '.$data['imovel']->tipo_imovel[0] => 'imoveis/'.strtolower($data['imovel']->tipo_imovel),
                $data['imovel']->titulo => ''
            );
        }


        // Busca os ids dos imóveis favoritos
        $data['imoveis_favoritos'] = $this->model_area_restrita->get_imoveis_favoritos();


        // Form de contato
        $form_default = array(
            array(
                'type' => 'hidden',
                'label' => 'Código do imovel',
                'name' => 'id_imovel',
                'value' => $data['imovel']->id
            ),
            array(
                'type' => 'hidden',
                'label' => 'Endereço',
                'name' => 'endereceo',
                'value' => $data['imovel']->endereco->placeType . ' ' . $data['imovel']->endereco->place
            ),
            array(
                'type' => 'hidden',
                'label' => 'Link',
                'name' => 'link',
                'value' => "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
            ),
            array(
                'label' => 'Nome',
                'name' => 'nome',
            ),
            array(
                'type' => 'email',
                'label' => 'E-mail',
                'name' => 'email',
            ),
            array(
                'label' => 'Telefone',
                'name' => 'telefone',
                'class' => 'telefone',
            ),
            array(
                'type' => 'textarea',
                'label' => 'Mensagem',
                'name' => 'mensagem',
            ),
            array(
                'type' => 'submit',
            ),
        );
        $this->form->set_form($form_default, 'imovel_contato', 'SITE_IMOVEL_CONTATO');

        // Enviar por e-mail
        $form_enviar_email = $form_default;
        unset($form_enviar_email[1], $form_enviar_email[3], $form_enviar_email[4]);
        $this->form->set_form($form_enviar_email, 'imovel_enviar_email', 'SITE_IMOVEL_ENVIAR_EMAIL');

        // Indicar para amigo
        $form_indicar = array(
            $form_default[0],
            array(
                'label' => 'Seu Nome',
                'name' => 'nome',
            ),
            array(
                'type' => 'email',
                'label' => 'Seu e-mail',
                'name' => 'email',
            ),
            array(
                'label' => 'Nome do amigo',
                'name' => 'nome_amigo',
            ),
            array(
                'type' => 'email',
                'label' => 'E-mail do amigo',
                'name' => 'email_amigo',
            ),
            
            $form_default[5],
            array(
                'type' => 'submit',
            ),
        );
        $this->form->set_form($form_indicar, 'imovel_indicar_para_amigo', 'SITE_IMOVEL_ENVIAR_EMAIL');

        // Agendar visita
        $this->form->set_form($form_default, 'imovel_agendar_visita', 'SITE_IMOVEL_AGENDAR_VISITA');


        // Ligamos para você

        // Fazer proposta
        $this->form->set_form($form_default, 'imovel_fazer_proposta', 'SITE_IMOVEL_PROPOSTA');


        //var_dump($data); die;
        $this->load->front_view(__FUNCTION__, $data);
    }

    public function comparar() {
        if (isset($_GET['imoveis'])) {
            $imoveis = array_values(array_filter($_GET['imoveis']));

            if (!empty($imoveis)) {
                $filtro = array(
                    array(
                        'term' => 'code',
                        'operator' => 'in',
                        'value' => array_map('intval', $imoveis)
                    )
                );
                $imoveis = $this->model_imovel->get_imoveis($filtro, NULL, 0);

                if ($imoveis->total > 0) {
                    $data = array(
                        'imoveis' => $imoveis->imoveis
                    );

                    // Busca os ids dos imóveis favoritos
                    $favoritos = $this->model_area_restrita->get_imoveis_favoritos();
                    $data['imoveis_favoritos'] = $favoritos;

                    $this->load->front_view(__FUNCTION__, $data);
                }
            }
        }
    }

    public function download_fotos($id_imovel, $empreendimento = 'false') {
        if ($empreendimento == 'true') {
            $this->model_imovel->type = 'empreendimentos';
            $tipo_download = 'empreendimento';
        } else{
            $tipo_download = 'imovel';
        }

        // Busca as infos
        $imovel = $this->model_imovel->get_imovel($id_imovel);

        if ($imovel) {
            $dir = "./upload/tmp_fotos_imoveis/";

            if ($tipo_download == 'imovel') {
                // Download de fotos do imóvel
                $fotos = $imovel->fotos;
                $dir .= "imovel/$id_imovel/";
            } else{
                // Download de fotos do empreendimento
                $dir .= "empreendimento/$id_imovel/";
                $fotos = $imovel->fotos_empreendimento;
            }

            if (!is_dir($dir))
                mkdir($dir, 0777, TRUE);

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                foreach ($fotos as $foto) {
                    $ext = substr(strrchr($foto->name, '.'), 1);
                    $allowed = array('jpg', 'jpeg', 'png');

                    if (in_array($ext, $allowed)) {
                        $image = sprintf($this->model_imovel->uri_images, 'inner', 500, 500, $foto->key, $foto->etag);
                        $img = @imagecreatefromstring(file_get_contents($image));

                        if ($img) {
                            switch ($ext) {
                                case 'jpg':
                                case 'jpeg':
                                    // Salva a imagem
                                    imagejpeg($img, $dir.$foto->name);
                                    break;
                                case 'png':
                                    // Salva a imagem
                                    imagepng($img, $dir.$foto->name);
                                    break;
                            }

                            // Libera a memória
                            imagedestroy($img);
                        }
                    }
                }

            } else {

                $this->load->library('zip');
                $this->load->helper('file');

                // Cria um .zip da pasta
                $zip = $this->zip->read_dir($dir, FALSE);

                if ($zip) {
                    if (is_dir($dir)) {
                        delete_files($dir, TRUE);
                        rmdir($dir);
                    }

                    // Faz download do arquivo
                    $this->zip->download(sprintf('Fotos-%s-%s.zip', $tipo_download, $id_imovel));
                }

            }
        }

        exit();
    }

}
