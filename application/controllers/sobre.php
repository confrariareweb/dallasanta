<?php

class Sobre extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->view = 'sobre';
    }

    public function index() {
    	$this->breadcrumb = array(
    		'Home' => 'home',
    		'A Dallasanta' => ''
     	);

        $this->load->model('model_content');

        $conteudo = $this->model_content->get_conteudo('a-dallasanta');
        $conteudo->gallery = array_chunk($conteudo->gallery, 3);

        if (!empty($conteudo->video)) {
            if (strpos(strtolower($conteudo->video), 'youtube') === FALSE) {
                $link = null;
            } else {
                $link = explode('v=', $conteudo->video);
                $link = explode('&', $link[1]);
                $link = $link[0];
            }

            $conteudo->video = $link;
        }

        $data['conteudo_pagina'] = $conteudo;

        $this->load->front_view($this->view, $data);
    }
}
