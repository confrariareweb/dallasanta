<?php

class Contact extends MY_Controller {
    function __construct() {
        parent::__construct();

        if (!isset($this->uri->segments[2]))
            $this->send();
    }

    public function index() {}

    public function send() {
        // echo json_encode(array(
        //     'msg' => $_POST
        // )); die;

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
            try {

                if (!isset($_POST['field']))
                    throw new Exception('Crie o formulário no formato de field[] e label[].');

                $post = $this->input->post(null, null);

                $fields = $post['field'];
                $labels = $post['label'];

                if (!isset($post['meio_captacao']) || empty($post['meio_captacao']))
                    throw new Exception('Informe o meio de captação.');

                // Verifica se o captcha corresponde (se existir)
                $this->load->helper('captcha');
                check_captcha($post['meio_captacao']);
                unset($post['captcha']);

                $message = '';
                foreach ($fields as $name => $value) {

                    if (is_array($value)) {
                        $value = implode(', ', array_map('utf8_decode', array_filter($value)));
                    } else {
                        $value = trim($value);
                        if (empty($value))
                            $value = 'Não informado';
                    }

                    //$message .= '<b>' . ucfirst(strtolower(utf8_decode($labels[$name]))) . '</b>' . ': ' . $value . '</br>';
                    $message .= sprintf('<b>%s</b>: %s<br>', ucfirst(strtolower(utf8_decode($labels[$name]))), utf8_decode($value));
                }

                if (isset($_FILES)) {
                    $this->load->library('upload');

                    $anexos_dir = './upload/anexos/';
                    foreach ($_FILES as $name => $file) {
                        if ($file['error'] === 0) {
                            $config['upload_path'] = $anexos_dir;
                            $config['allowed_types'] = '*';
                            $config['max_size'] = intval(ini_get('upload_max_filesize')) * 1024;
                            $config['max_width'] = 0;
                            $config['max_height'] = 0;
                            $config['encrypt_name'] = TRUE;

                            $this->upload->initialize($config);

                            if ($this->upload->do_upload($name)) {
                                $anexo = $this->upload->data();

                                $uri_anexo = base_url(substr($anexos_dir, 2).$anexo['file_name']);
                                $message .= sprintf('<b>%s</b>: <a href="%s">%s</a><br>', $labels[$name], $uri_anexo, $uri_anexo);
                            } else {
                                throw new Exception($this->upload->display_errors('', ''));
                            }
                        }
                    }
                }

                // var_dump($post);
                // echo $message; //die;

                 $HTTP_HOST = $_SERVER['HTTP_HOST'];
                 $_SERVER['HTTP_HOST'] = 'dallasanta.com.br';

                $admanager = new AdManagerAPI();
                $admanager->registraLead($post['meio_captacao'], $fields['nome'], $fields['email'], $fields['telefone'], $fields['cidade'], $fields['estado'], 'Brasil', $message);

                 $_SERVER['HTTP_HOST'] = $HTTP_HOST;

                $msg = 'Seu e-mail foi enviado com sucesso!';
                if ($this->input->is_ajax_request()) {
                    echo json_encode(array(
                        'success' => TRUE,
                        'msg' => $msg
                    ));
                    exit();
                } else {
                    $this->session->set_flashdata('success', $msg);
                    redirect($_SERVER['HTTP_REFERER']);
                }

            } catch (Exception $e) {
                if ($this->input->is_ajax_request()) {
                    echo json_encode(array(
                        'success' => FALSE,
                        'msg' => $e->getMessage()
                    ));
                    exit();
                } else {
                    $this->session->set_flashdata('error', $e->getMessage());
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }

        //redirect('home');
    }

    public function newsletter() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            try {
                $post = $this->input->post();

                if (empty($post['nome']) || empty($post['email']))
                    throw new Exception('Informe todos os campos obrigatórios.');

                $email = $post['email'];
                $nome = $post['nome'];

                $qry = "INSERT INTO newsletter (email, nome) VALUES ('$email', '$nome') ON DUPLICATE KEY UPDATE nome = '$nome'";
                if ($this->db->query($qry)) {
					
					$HTTP_HOST = $_SERVER['HTTP_HOST'];
					$_SERVER['HTTP_HOST'] = 'dallasanta.com.br';

					$admanager = new AdManagerAPI();
					$admanager->registraLead('FORM_NEWSLETTER', $nome, $email, '', '','', 'Brasil', 'Cadastro de Newsletter - '.$nome.'('.$email.')');

					$_SERVER['HTTP_HOST'] = $HTTP_HOST;
					
                    echo json_encode(array(
                        'success' => TRUE,
                        'msg' => 'Seu e-mail foi cadastrado com sucesso!'
                    ));
                    exit();
                } else {
                    throw new Exception('Erro ao cadastrar seu e-mail. Tente novamente.');
                }
            } catch (Exception $e) {
                echo json_encode(array(
                    'success' => FALSE,
                    'msg' => $e->getMessage()
                ));
                exit();
            }
        }
    }

}