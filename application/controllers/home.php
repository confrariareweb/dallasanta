<?php

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->view = 'home';
    }

    public function index() {
        
        $this->load->model('model_banner');
        $this->load->model('model_imovel');
        $this->load->model('model_area_restrita');

        $data = array();
		
      
        $filtro = array(
            
               
                'tipo_id' => '1'
            
        );
        
        $data['banner'] = $this->model_banner->get_banners($filtro, $sort, 0);
       
       // $data['banner'] = array_chunk($banner->banner, 4);
        // print_r($data['banner']);die;
		$sort = array(
            'property' => 'code',
            'direction' => 'RAND()'
        );

        // Imóveis para aluguel
        $filtro = array(
            array(
                'term' => 'businessType',
                'operator' => 'equals',
                'value' => 'Aluguel'
            ),
            array(
                'term' => 'highlight',
                'operator' => 'equals',
                'value' => 1
            ),
        );
        
        $imoveis = $this->model_imovel->get_imoveis($filtro, $sort, 0);
        
        $data['imoveis_aluguel'] = array_chunk($imoveis->imoveis, 4);

        // Imóveis a venda
        $filtro = array(
            array(
                'term' => 'businessType',
                'operator' => 'equals',
                'value' => 'Venda'
            ),
            array(
                'term' => 'highlight',
                'operator' => 'equals',
                'value' => 1
            ),
        );
        
        $imoveis = $this->model_imovel->get_imoveis($filtro, $sort, 0);
        $data['imoveis_venda'] = array_chunk($imoveis->imoveis, 4);

        // Busca os ids dos imóveis favoritos
        $data['imoveis_favoritos'] = $this->model_area_restrita->get_imoveis_favoritos();

        
    
        
        $this->load->front_view($this->view, $data);
    }
}
