<?php

class Venda_seu_imovel extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->helper('captcha');

        $this->view = 'venda-seu-imovel';
    }

    public function index() {
        $data = array();

        $this->load->front_view($this->view, $data);
    }
}
