<?php

class Documentos extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->view = 'documentos';
    }

    public function index() {
        $data = array();

        $this->load->front_view($this->view, $data);
    }
}
