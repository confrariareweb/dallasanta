<?php

class Parceiros extends MY_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('model_parceiro');
        $this->load->model('model_imovel');
        $this->view = 'parceiros';
    }

    public function index() {
        $this->breadcrumb = array(
            'Home' => 'home',
            'Parceiros' => ''
        );

        $this->model_imovel->type = 'clientes_parceiros';
        $filtro = array(
            array(
                'term' => 'type',
                'operator' => 'equals',
                'value' => 'Parceiros'
            )
        );
        $parceiros = $this->model_imovel->get_imoveis($filtro, null, 100);

        $data = array(
            'parceiros' => $parceiros
        );
        $this->load->front_view($this->view, $data);
    }
}
