<?php

class Area_do_cliente extends MY_Controller {
    private $token_ativar_conta;

    function __construct() {
        parent::__construct();

        $this->load->model('model_area_restrita');
        $this->load->helper('captcha');
        $this->load->library('session');
        $this->load->library('email');

        $this->load->library('google_login');
        $this->load->library('facebook_login');
        //$this->load->library('linkedin_login');

        define('VIEW_DIR', 'area-do-cliente');
    }

    public function index() {
        $data = array();

        if ($this->model_area_restrita->check_auth())
            redirect('area-do-cliente/meus-dados');

        $this->load->front_view(__FUNCTION__);
    }

    public function google_login() {
        $this->google_login->login();
    }

    public function facebook_login() {
        $this->facebook_login->login();
    }

    // public function linkedin_login() {
    //     $this->linkedin_login->login();
    // }

    public function favorito($id_imovel) {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (!$this->model_area_restrita->check_auth()) {
                redirect('area-do-cliente');
                exit();
            }

            if ($id_imovel) {
                $fav = $this->model_area_restrita->favoritar_imovel($id_imovel);

                if (!$fav) {
                    $ret = array(
                        'success' => FALSE,
                        'msg' => 'Não foi possível favoritar este imóvel. Tente novamente.'
                    );
                } else{
                    $msg = ($fav == 'remover') ? 'Este imóvel foi removido dos seus favoritos.' : 'Este imóvel foi adicionado aos seus favoritos.';
                    $ret = array(
                        'success' => TRUE,
                        'msg' => $msg,
                        'acao' => $fav
                    );
                }

                echo json_encode($ret);
                exit();
            }
        } else {
            redirect('area-do-cliente');
        }
    }

    public function ordem($ordem) {
        $get = NULL;

        if (isset($ordem) && !empty($ordem)) {
            $ordem = str_replace('_', '-', mysql_real_escape_string(trim($ordem)));
            $allowed = array('mais-atual', 'menos-atual', 'maior-preco', 'menor-preco');

            if (in_array($ordem, $allowed)) {
                if (isset($_SERVER['HTTP_REFERER'])) {
                    $qry_old = explode('?', $_SERVER['HTTP_REFERER']);

                    $get = array();
                    if (isset($qry_old[1])) {
                        parse_str($qry_old[1], $get);

                        if (isset($get['page']))
                            unset($get['page']);
                    }
                    else {
                        $tipo_busca = ucfirst(strtolower(substr(strrchr($_SERVER['HTTP_REFERER'], '/'), 1)));

                        if ($tipo_busca == 'Venda' || $tipo_busca == 'Aluguel') {
                            $get['tipo-imovel'] = $tipo_busca;
                        }
                    }

                    $get['sort'] = $ordem;
                    $get = '?' . http_build_query($get);
                }
            }
        }

        redirect('area-do-cliente/meus-favoritos/'.$get);
    }

    public function meus_favoritos() {
        $this->view_default(__FUNCTION__);
    }
    public function meus_dados() {
        $this->view_default(__FUNCTION__);
    }

    public function view_default($active = 'meus_dados') {
        if (!$this->model_area_restrita->check_auth())
            redirect('area-do-cliente');

        $this->load->model('model_imovel');

        $data = array();

        // Busca os dados do usuário
        $data['usuario'] = $this->model_area_restrita->get_dados_usuario();

        // Busca os ids dos imóveis favoritos
        $favoritos = $this->model_area_restrita->get_imoveis_favoritos();
        $data['imoveis_favoritos'] = $favoritos;

        $sort = null;
        $get = $this->input->get();
        if (isset($get['sort']) && !empty($get['sort'])) {
            switch ($get['sort']) {
                case 'mais-atual':
                    $sort = array(
                        'property' => '_createdAt',
                        'direction' => 'DESC'
                    );
                    break;

                case 'menos-atual':
                    $sort = array(
                        'property' => '_createdAt',
                        'direction' => 'ASC'
                    );
                    break;

                case 'maior-preco':
                    $sort = array(
                        'property' => $value_property,
                        'direction' => 'DESC'
                    );
                    break;

                case 'menor-preco':
                    $sort = array(
                        'property' => $value_property,
                        'direction' => 'ASC'
                    );
                    break;
            }
        }
        $filtro = array(
            array(
                'term' => 'code',
                'operator' => 'in',
                'value' => array_map('intval', $favoritos)
            )
        );
        $data['favoritos'] = $this->model_imovel->get_imoveis($filtro, $sort, 0)->imoveis;
        $data['tab_ativa'] = $active;

        $this->load->front_view('default', $data);
    }

    public function create_session() {
        $user = (object) array(
            'nome' => vsprintf('%s %s', array($this->userdata['nome'], $this->userdata['sobrenome'])),
            'email' => $this->userdata['email'],
            'foto' => (isset($this->userdata['foto'])) ? $this->userdata['foto'] : NULL
        );
        $this->session->set_userdata('area_restrita', $user);
        //redirect('area-do-cliente/meus-dados');
    }

    public function salvar() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            redirect('area-do-cliente');
            exit();
        }

        $this->uri_redirect = 'meus-dados';
        $this->create_userdata_session = FALSE;

        if (isset($_POST['token'])) { // Recuperar senha
            $usuario = $this->model_area_restrita->get_dados_usuario(NULL, $this->input->post('token'));

            if ($usuario) {
                unset($_POST['token']);

                $_POST['email'] = $usuario['email'];
                $_POST['nome'] = $usuario['nome'];
                $_POST['sobrenome'] = $usuario['sobrenome'];
                $_POST['foto'] = $usuario['foto'];
                $_POST['token_recuperar_senha'] = 'NULL';

                $this->create_userdata_session = TRUE;
            } else {

            }
        }

        $this->cadastro();
    }

    public function cadastro() {
        //var_dump($_POST); //die;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // post do cadastro

            try {
                $post = $this->input->post();

                if (empty($post['email']) || empty($post['nome']) || empty($post['sobrenome'])) {
                    throw new Exception('Informe todos os campos obrigatórios.');
                }

                if (isset($post['nova_senha']) && isset($post['nova_senha_confirmar'])) {
                    $post['nova_senha'] = trim($post['nova_senha']);
                    $post['nova_senha_confirmar'] = trim($post['nova_senha_confirmar']);

                    if (!empty($post['nova_senha']) && !empty($post['nova_senha_confirmar'])) {
                        if (strcmp($post['nova_senha'], $post['nova_senha_confirmar']) !== 0) {
                            throw new Exception('As senhas não conferem.');
                        }

                        $post['senha'] = $post['nova_senha'];
                        $post['nova_senha'] = TRUE;
                        unset($post['nova_senha_confirmar']);
                    }
                }

                // Verifica se o captcha corresponde (se existir)
                check_captcha('SITE_CADASTRO_CLIENTE');
                unset($post['captcha']);

                // Cria o cadastro do usuário
                $success = $this->model_area_restrita->salvar($post);

                if ($success === TRUE) {
                    if ($this->uri->rsegments[2] == 'cadastro') {
                        $this->create_userdata_session = FALSE;
                        $token = $this->envia_email_confirmacao($post['email'], $post['nome']);
                    }

                    if (!isset($this->create_userdata_session) || $this->create_userdata_session === TRUE) {
                        // Cria a sessão com os dados do usuário
                        $this->userdata = $post;
                        $this->create_session();
                    }

                    if ($this->uri->rsegments[2] == 'cadastro') {
                        echo json_encode(array(
                            'success' => TRUE,
                            'msg' => '
                                Seu cadastro foi concluído com sucesso!<br>
                                Você receberá um e-mail para confirmar seu cadastro.<br><br>
                                <a href="'. site_url('area-do-cliente/reenviar-email/'.$token) .'" id="btn-reenviar-confirmacao" class="btn button">
                                    <span></span>
                                    Reeviar e-mail
                                </a>
                            ',
                            //'redirect' => site_url('area-do-cliente')
                        ));
                        exit();
                    }
                }
            } catch (Exception $e) {
                if ($this->input->is_ajax_request()) {
                    echo json_encode(array(
                        'success' => FALSE,
                        'msg' => $e->getMessage()
                    ));
                    exit();
                } else {
                    $this->session->set_flashdata('error', $e->getMessage());

                    $uri = site_url('area-do-cliente');
                    if (strpos($_SERVER['HTTP_REFERER'], 'area-do-cliente') !== FALSE)
                        $uri = $_SERVER['HTTP_REFERER'];

                    redirect($uri);
                }
            }

            if (!$this->uri_redirect)
                $this->uri_redirect = '';

            redirect('area-do-cliente/'.$this->uri_redirect);
        }

        $this->load->front_view(__FUNCTION__);
    }

    public function envia_email_confirmacao($email = NULL, $nome = NULL) {
        if (($email && $nome) || $this->token_ativar_conta) {
            if ($this->token_ativar_conta) {
                // Busca dados do usuário de acordo com o token
                $usuario = $this->model_area_restrita->get_dados_usuario(NULL, NULL, $this->token_ativar_conta);

                if (!$usuario) {
                    echo json_encode(array(
                        'success' => FALSE,
                        'msg' => 'Token inválido'
                    ));
                    exit();
                }

                //$this->token_ativar_conta = NULL;
                $email = $usuario['email'];
                $nome = $usuario['nome'];
            }

            $email = mysql_real_escape_string(trim($email));

            if (!empty($email)) {
                // Gera um token para confirmar a conta
                $token = $this->model_area_restrita->gera_token($email, 'token_ativar_conta');

                if ($token) {
                    $uri_ativar = site_url('area-do-cliente/ativar-conta/'.$token);

                    // Manda e-mail de confirmação para o usuário
                    $this->email->from('sgriebeler@reweb.com.br', 'Dallasanta');
                    $this->email->to($email);

                    $this->email->subject('Confirmação de cadastro - Dallasanta');
                    $this->email->message('
                        Olá, '. $nome .'.<br><br>

                        Para confirmar sua conta, acesse o link abaixo:<br>
                        <a target="new" href="'. $uri_ativar .'">'. $uri_ativar .'</a><br><br>

                        <small>Se você não fez esta solicitação, ignore este e-mail.</small>
                    ');
                    return (($this->email->send()) ? $token : FALSE);
                }
            }
        }

        return FALSE;
    }

    public function reenviar_email($token) {
        $this->token_ativar_conta = $token;
        if ($this->envia_email_confirmacao()) {
            $sent = array(
                'success' => TRUE,
                'msg' => 'Seu e-mail foi enviado!'
            );
        } else{
            $sent = array(
                'success' => FALSE,
                'msg' => 'Não foi possível enviar seu e-mail no momento.<br>Para ativar sua conta, entre em contato com a Dallasanta.'
            );
        }

        echo json_encode($sent);
        exit();
    }

    public function ativar_conta($token) {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if ($token) {
                // Busca dados do usuário de acordo com o token
                $usuario = $this->model_area_restrita->get_dados_usuario(NULL, NULL, $token);

                if ($usuario) {
                    $set = array(
                        'token_ativar_conta' => NULL
                    );
                    $where = array(
                        'email' => $usuario['email']
                    );
                    $this->db->update('area_restrita_usuario', $set, $where);

                    $this->userdata = array(
                        'nome' => $usuario['nome'],
                        'sobrenome' => $usuario['sobrenome'],
                        'email' => $usuario['email']
                    );
                    $this->create_session();

                    $this->session->set_flashdata('success', 'Sua conta foi ativada com sucesso!');
                    redirect('area-do-cliente/meus-dados');
                }
            }

            $this->session->set_flashdata('error', 'Este código de segurança não é válido.');
            redirect('area-do-cliente');
        }
    }

    public function login() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            redirect('area-do-cliente');
            exit();
        }

        try {
            if (empty($_POST['email']) || empty($_POST['senha'])) {
                throw new Exception('Informe seu usuário e senha');
            }

            $post = $this->input->post();
            $user = $this->model_area_restrita->get_dados_usuario($post['email']);

            if (empty($user)) {
                throw new Exception('Usuário e/ou senha incorretos.');
            } else if (!empty($user['token_ativar_conta'])) {
                throw new Exception('Esta conta ainda não foi ativada.<br>Clique no link enviado para o seu e-mail para fazer a confirmacão da mesma.');
            }

            if (crypt($post['senha'], $user['senha']) == $user['senha']) {
                // Cria a sessão
                $this->userdata = array(
                    'nome' => $user['nome'],
                    'sobrenome' => $user['sobrenome'],
                    'email' => $user['email'],
                    'foto' => $user['foto'],
                );
                $this->create_session();

                echo json_encode(array(
                    'success' => TRUE,
                    'msg' => 'Dados corretos. Redirecionando...',
                    'redirect' => site_url('area-do-cliente')
                ));
                exit();
            } else {
                throw new Exception('Usuário e/ou senha incorretos.');
            }
        } catch (Exception $e) {
            echo json_encode(array(
                'success' => FALSE,
                'msg' => $e->getMessage()
            ));
            exit();
        }
    }

    public function logout() {
        if (isset($this->session->userdata['area_restrita']->tipo_login)) {
            switch (mysql_real_escape_string($this->session->userdata['area_restrita']->tipo_login)) {
                case 'facebook':

                    $this->facebook_login->logout();

                    break;

                case 'google':

                    $this->google_login->logout();

                    break;

                case 'linkedin':

                    // $this->linkedin_login->logout();

                    break;

                default:
                    # code...
                    break;
            }
        }

        $uri = 'area-do-cliente';
        if (isset($_GET['redirect'])) {
            $uri = mysql_real_escape_string($_GET['redirect']);
        }

        $this->session->unset_userdata('area_restrita');
        redirect($uri);
    }

    public function recuperar_senha($token = NULL) {
        if (isset($token)) { // Quando o usuário clica no link enviado para o email
            // Busca os dados do usuário baseado no token informado
            $usuario = $this->model_area_restrita->get_dados_usuario(NULL, $token);

            if ($usuario) {
                // Verifica se o token informado é válido
                if (strcmp($usuario['token_recuperar_senha'], $this->model_area_restrita->decrypt_token($token)) === 0) {
                    $data = array(
                        'token' => $token
                    );
                    $this->load->front_view('index', $data);
                }
            }

            if (!isset($data)) {
                $this->session->set_flashdata('error', 'Este código de segurança não é válido.');
                redirect('area-do-cliente');
            }

        } else if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Action do esqueci minha senha

            try {
                // Gera o token e salva no banco
                if (isset($_POST['email']) && !empty($_POST['email'])) {
                    $email = mysql_real_escape_string(trim($this->input->post('email')));

                    // Busca os dados do usuário para verificar se o email existe
                    $usuario = $this->model_area_restrita->get_dados_usuario($email);

                    if ($usuario) {
                        // Se o email existir, gera o token
                        $token = $this->model_area_restrita->gera_token($email);

                        if ($token !== FALSE) {
                            // Envia email para o usuário
                            $uri_redefinir = site_url('area-do-cliente/recuperar-senha/'.$token);

                            $this->email->from('sgriebeler@reweb.com.br', 'Dallasanta');
                            $this->email->to($email);

                            $this->email->subject('Recuperar senha - Dallasanta');
                            $this->email->message('
                                Olá, '. $usuario['nome'] .'.<br><br>

                                Para redefinir sua senha clique no link abaixo:<br>
                                <a target="new" href="'. $uri_redefinir .'">'. $uri_redefinir .'</a><br><br>

                                <small>Se você não fez esta solicitação, ignore este e-mail.</small>
                            ');
                            $sent = $this->email->send();
                            //echo $this->email->print_debugger();

                            if ($sent) {
                                echo json_encode(array(
                                    'success' => TRUE,
                                    'msg' => 'Um e-mail com as instruções de como recuperar sua senha foi enviado para você.'
                                ));
                                exit();
                                //$this->session->set_flashdata('success', 'Um e-mail com as instruções de como recuperar sua senha foi enviado para você.');
                            }
                            else
                                throw new Exception('Erro ao enviar o e-mail. Por favor, tente novamente.');
                        }
                    } else {
                        throw new Exception('Este e-mail não existe em nosso cadastro.<br>Informe o e-mail que você usa para fazer login no sistema.');
                    }
                } else {
                    throw new Exception('Informe seu e-mail.');
                }
            } catch (Exception $e) {
                echo json_encode(array(
                    'success' => FALSE,
                    'msg' => $e->getMessage()
                ));
                exit();
                //$this->session->set_flashdata('error', $e->getMessage());
            }

            //var_dump($token); die;
            redirect('area-do-cliente');
        }
    }

}
