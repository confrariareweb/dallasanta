<?php

class Error404 extends MY_Controller {
	function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$data = array();

		header("HTTP/1.0 404 Not Found");

		$this->load->view("frontend/error404",$data);
	}
}