<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| PAGINATION
| -------------------------------------------------------------------------
| This file lets you config your pagination.
|
*/

$config['per_page'] 		= '';
$config['uri_segment'] 		= 3;
$config['full_tag_open'] 	= '<div class="pagination2"><ul>';
$config['full_tag_close'] 	= '</ul></div>';
$config['cur_tag_open'] 	= '<li class="active"><a>';
$config['cur_tag_close'] 	= '</a></li>';
$config['first_link'] 		= '';
$config['last_link'] 		= '';
$config['last_tag_open'] 	= '';
$config['last_tag_close'] 	= '';
$config['next_link'] 		= 'Próximo';
$config['next_tag_open'] 	= '<li class="next">';
$config['next_tag_close'] 	= '</a></li>';
$config['prev_link'] 		= 'Anterior';
$config['prev_tag_open'] 	= '<li class="prev">';
$config['prev_tag_close'] 	= '</a></li>';
$config['num_links']		= 2;
$config['num_tag_open'] 	= '<li>';
$config['num_tag_close'] 	= '</li>';

$config['page_query_string']    = TRUE;
$config['query_string_segment'] = 'page';
