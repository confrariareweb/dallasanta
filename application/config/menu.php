<?php

$config["menu_admin"]["dashboard"]["dashboard"] = array("title" => "Dashboard", "link" => "admin/dashboard", "permissions" => "1,2", "class" => "widgets");

$config["menu_admin"]["dashboard"]["banner"] = array("title" => "Banners", "link" => "#banner_submenu", "permissions" => "1,2", "class" => "gallery");
$config["menu_sub_admin"]["banner"][] = array("title" => "Listagem", "link" => "admin/banner", "permissions" => "1,2");
$config["menu_sub_admin"]["banner"][] = array("title" => "Categorias / Sessões", "link" => "admin/bannertype", "permissions" => "1,2");


$config["menu_admin"]["dashboard"]["layout"] = array("title" => "Blocos Estáticos", "link" => "admin/staticblock", "permissions" => "1,2", "class" => "widgets");

$config["menu_admin"]["dashboard"]["parceiros"] = array("title" => "Parceiros", "link" => "admin/parceiro", "permissions" => "1,2", "class" => "widgets");

$config["menu_admin"]["dashboard"]["clientes"] = array("title" => "Clientes", "link" => "admin/cliente", "permissions" => "1,2", "class" => "widgets");

$config['menu_admin']['dashboard']['content'] = array('title' => 'Conteudo', 'link' => '#content_submenu', 'permissions' => '1,2', 'class' => 'tables');
$config['menu_sub_admin']['content'][] = array('title' => 'Listagem', 'link' => 'admin/content', 'permissions' => '1,2');
$config['menu_sub_admin']['content'][] = array('title' => 'Tipos', 'link' => 'admin/contenttype', 'permissions' => '1,2');

 $config["menu_admin"]["dashboard"]["newsletter"] = array("title" => "Newsletter","link" => "admin/newsletter","permissions" => "1,2","class" => "editor");

/* - - */
$config["menu_admin"]["user"]["user"] = array("title" => "Usuários", "link" => "admin/user", "permissions" => "1,2", "class" => "editor");
$config["menu_admin"]["user"]["usergroup"] = array("title" => "Grupos de Usuários", "link" => "admin/usergroup", "permissions" => "1,2", "class" => "tables");
$config["menu_admin"]["user"]["acl"] = array("title" => "ACL", "link" => "admin/acl", "permissions" => "1", "class" => "support");

/* - - */
$config["menu_admin"]["config"]["config"] = array("title" => "Configurações", "link" => "admin/config", "permissions" => "1,2", "class" => "tables");
$config["menu_admin"]["config"]["url"] = array("title" => "Urls", "link" => "admin/url", "permissions" => "1", "class" => "editor");
$config["menu_admin"]["config"]["modules"] = array("title" => "Gerencia de Módulos", "link" => "admin/modules", "permissions" => "@0.2", "class" => "widgets");
$config["menu_admin"]["config"]["help"] = array("title" => "Ajuda", "link" => "admin/help", "permissions" => "1,2", "class" => "support");
$config["menu_admin"]["config"]["phpinfo"] = array("title" => "Informações do Servidor", "link" => "admin/phpinfo", "permissions" => "1", "class" => "support");

$config["menu_top_admin"]["dashboard"] = array("title" => "Conteúdos", "link" => "admin/dashboard", "permissions" => "1,2", "ico" => "flatscreen");
// $config["menu_top_admin"]["blog"]      = array("title" => "Blog","link" => "admin/blog","permissions" => "1,2","ico" => "message");
//$config["menu_top_admin"]["message"] = array("title" => "Formulários", "link" => "admin/emailto", "permissions" => "1,2", "ico" => "message");
$config["menu_top_admin"]["user"] = array("title" => "Usuários", "link" => "admin/user", "permissions" => "1,2", "ico" => "users");
$config["menu_top_admin"]["config"] = array("title" => "Configurações", "link" => "admin/config", "permissions" => "1,2", "ico" => "pencil");
