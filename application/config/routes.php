<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    // commom
    $route['default_controller'] = 'home';

    // site
	$route['empreendimentos/detalhes/(:any)'] = 'imoveis/detalhes/$1/true';


    //$route['imoveis/detalhes/(:any)/(:any)'] = 'imoveis/detalhes/$1';
	
	$route['(:any)-(:any)-(:num).html'] = 'imoveis/detalhes/$3';

    // admin
    $route['admin'] = 'admin_dashboard';
    $route['admin/([a-zA-Z0-9/]+)'] = 'admin_$1';

?>
