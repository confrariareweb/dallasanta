<?php

$active_group = 'producao';
$active_record = TRUE;

$db['interface']['hostname'] = 'externo.reweb.com.br';
$db['interface']['username'] = 'root';
$db['interface']['password'] = 'r3w3b1gv4cv';
$db['interface']['database'] = 'dallasanta';
$db['interface']['dbdriver'] = 'mysql';
$db['interface']['dbprefix'] = '';
$db['interface']['pconnect'] = FALSE;
$db['interface']['db_debug'] = TRUE;
$db['interface']['cache_on'] = FALSE;
$db['interface']['cachedir'] = '';
$db['interface']['char_set'] = 'utf8';
$db['interface']['dbcollat'] = 'utf8_general_ci';
$db['interface']['swap_pre'] = '';
$db['interface']['autoinit'] = TRUE;
$db['interface']['stricton'] = FALSE;

$db['producao']['hostname'] = 'mysql.dallasanta.com.br';
$db['producao']['username'] = 'dallasanta';
$db['producao']['password'] = 'c34ee4e7';
$db['producao']['database'] = 'dallasanta';
$db['producao']['dbdriver'] = 'mysql';
$db['producao']['dbprefix'] = '';
$db['producao']['pconnect'] = FALSE;
$db['producao']['db_debug'] = TRUE;
$db['producao']['cache_on'] = FALSE;
$db['producao']['cachedir'] = '';
$db['producao']['char_set'] = 'utf8';
$db['producao']['dbcollat'] = 'utf8_general_ci';
$db['producao']['swap_pre'] = '';
$db['producao']['autoinit'] = TRUE;
$db['producao']['stricton'] = FALSE;

$db['localhost']['hostname'] = 'localhost';
$db['localhost']['username'] = 'root';
$db['localhost']['password'] = '';
$db['localhost']['database'] = 'dallasanta';
$db['localhost']['dbdriver'] = 'mysql';
$db['localhost']['dbprefix'] = '';
$db['localhost']['pconnect'] = FALSE;
$db['localhost']['db_debug'] = TRUE;
$db['localhost']['cache_on'] = FALSE;
$db['localhost']['cachedir'] = '';
$db['localhost']['char_set'] = 'utf8';
$db['localhost']['dbcollat'] = 'utf8_general_ci';
$db['localhost']['swap_pre'] = '';
$db['localhost']['autoinit'] = TRUE;
$db['localhost']['stricton'] = FALSE;
