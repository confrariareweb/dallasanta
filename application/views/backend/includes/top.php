<div class="topheader">
    <div class="left">
    	<h1 class="logo">
    	<?php if(read_file($this->admin_model->config_directory.$this->coreconfig_logo->coreconfig__value)): ?>
            <?php echo img(array('src'=>image("media/".$this->admin_model->config_directory.$this->coreconfig_logo->coreconfig__value, "0x40"),'alt'=>$this->core_config['general_client'])); ?>
        <?php else: ?>
            <?php echo $this->core_config['general_client']; ?>
        <?php endif; ?>
        </h1>
        <span class="slogan">GERENCIADOR DE CONTÉUDOS</span>

        <!-- #@! general search div class="search">
        	<form action="#" method="post">
            	<input type="text" name="keyword" id="keyword" value="Enter keyword(s)" />
                <button class="submitbutton"></button>
            </form>
        </div><!--search-->

        <br clear="all" />

    </div><!--left-->

    <div class="right">
    	<!-- #@! notification -->
    	<!--div class="notification">
            <a class="count" href="ajax/notifications.html"><span>9</span></a>
    	</div-->
        <div class="userinfo">
        	<?php
        		if(read_file('./upload/user/'.$user['image'])):
            		echo img(array('src'=>image("media/upload/user/".$user['image'], "23x23"),'alt'=>$user['name']));
            	else:
            		echo img(image("media/upload/user/default.jpg", "23x23"));
            	endif;
        	?>
            <span><?php echo $user['name']; ?> <?php echo $user['name_last']; ?> </span>
        </div><!--userinfo-->

        <div class="userinfodrop"><div class="avatar">
            	<a href="javascript:void(0);">
                	<?php
	            		if(read_file('./upload/user/'.$user['image'])):
		            		echo img(array('src'=>image("media/upload/user/".$user['image'], "95x95"),'alt'=>$user['name']));
		            	else:
		            		echo img(image("media/upload/user/default.jpg", "95x95"));
		            	endif;
	            	?>
            	</a>
                <div class="changetheme">
                	Alterar Tema: <br />
                	<a class="default"></a>
                    <a class="blueline"></a>
                    <a class="greenline"></a>
                    <a class="contrast"></a>
                    <a class="custombg"></a>
                </div>
            </div><!--avatar-->
            <div class="userdata">
            	<h4><?php echo $user['name']; ?> <?php echo $user['name_last']; ?></h4><br  />
                <span class="email"><?php echo $user['email']; ?></span>
                <ul>
                	<li><a href="<?php echo site_url('admin/user/edit/'.$user['id']); ?>">Editar Dados</a></li>
                    <li><a href="<?php echo site_url('admin/config'); ?>">Configurações</a></li>
                    <li><a href="<?php echo site_url('admin/help'); ?>">Ajuda</a></li>
                    <li><a href="<?php echo site_url('admin/login/logout'); ?>">Sair</a></li>
                </ul>
            </div><!--userdata-->
        </div><!--userinfodrop-->
    </div><!--right-->
</div><!--topheader-->
