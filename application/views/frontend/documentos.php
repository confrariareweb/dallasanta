<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box documents-list">
        <ul>
            <li class="press-office diffrential">
                <h1>FICHAS CADASTRAIS PARA DOWNLOAD</h1>
                <ul>
                    <li>
                        Fiador Pessoa Física
                        <ul>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/fiador_pf.pdf') ?>">Versão PDF</a>
                            </li>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/fiador_pf.xlsx') ?>">Versão Excel</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Fiador Pessoa Jurídica
                        <ul>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/fiador_pj.pdf') ?>">Versão PDF</a>
                            </li>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/fiador_pj.xlsx') ?>">Versão Excel</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Locatário Pessoa Física
                        <ul>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/locatario_pf.pdf') ?>">Versão PDF</a>
                            </li>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/locatario_pf.xlsx') ?>">Versão Excel</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Lotarário Pessoa Jurídica
                        <ul>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/locatario_pj.pdf') ?>">Versão PDF</a>
                            </li>
                            <li>
                                <a target="new" href="<?php echo base_url('upload/documentos/locatario_pj.xlsx') ?>">Versão Excel</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <h1>FIADOR</h1>
                <!-- <p>
                    Fiador é quando uma pessoa se compromete, em nome de outra, a satisfazer as obrigações do contrato de locação, caso o devedor locatário não as cumpra. Solicitamos a apresentação de um fiador que possua dois imóveis quitados, ou dois fiadores com um imóvel cada, ambos quitados e livre de ônus, cujo devem estar registrados em nome do pretendendo a fiador do contrato de locação.
                	</p> -->
                <p>
                	Fiador é quando uma pessoa se compromete, em nome de outra, a satisfazer as obrigações do contrato de locação, caso o devedor locatário não as cumpra. Solicitamos a apresentação de um fiador que possua dois imóveis quitados, ou dois fiadores com um imóvel cada, ambos quitados e livres de ônus, os quais devem estar registrados em nome do pretendente á fiador do contrato de locação.
                </p>
            </li>
            <li>
                <h1>TÍTULO DE CAPITALIZAÇÃO</h1>
                <!-- <p>
                    Consiste na aquisição de um título de capitalização equivalente a 12 vezes o valor do aluguel mais as taxas do imóvel a ser locado, cujo deverá ser pago a vista junto a assinatura do contrato, onde a validade é exatamente o tempo que durar a locação. Esse título, poderá ser resgatado pelo locatário no final da locação, devidamente corrigido, após a entrega das chaves do imóvel e quando não houver débitos.
                	</p> -->
                <p>
                	Consiste na aquisição de um título de capitalização equivalente a 12 vezes o valor do aluguel mais as taxas do imóvel a ser locado, o qual deverá ser pago a vista junto à assinatura do contrato, onde a validade é exatamente o tempo que durar a locação. Esse título poderá ser resgatado pelo locatário no final da locação, devidamente corrigido, após a entrega das chaves do imóvel e quando não houver débitos.
                </p>	
            </li>
            <li>
                <h1>CAUÇÃO DE BEM IMÓVEL | HIPOTECA</h1>
                <!-- <p>
                    Quando o candidato a locação, concede seu bem imóvel como garantia de fiança. A caução de bem imóvel equivale a hipoteca. Consiste em um documento, identificado como instrumento público lavrado em tabelionato. O imóvel considerado como bem, é descrito no instrumento de caução e avaliado, cujo será registrado a garantia junto à margem da matrícula do bem, no registro de imóveis. O bem deve estar registrado em nome de quem está prestando a caução.
                	</p> -->
                <p>
                	Quando o candidato a locação, concede um bem imóvel como garantia. Consiste em um instrumento público lavrado em tabelionato, que será registrado junto à margem da matrícula do bem, no registro de imóveis.
                </p>
            </li>
            <li>
                <h1>SEGURO FIANÇA</h1>
                <!-- <p>
                    O contrato de seguro é celebrado por 12 meses, mediante pagamento de um prêmio que fica sobre a responsabilidade do locatário, calculado pela Porto Seguro, podendo ser sucessivamente renovado, enquanto durar a locação. O candidato a locação passa por uma análise cadastral da Seguradora (Porto Seguro) e estando aprovado o seguro é parcelado em até 4X, a 1ª parcela é paga no ato do recebimento das chaves e as seguintes são incluídas nos próximos doc´s de aluguel. Este pagamento se equivale ao prazo de 12 meses, sendo que após esse período o locatário deverá renová-lo pagando o valor por mais 12 meses, e assim sucessivamente até o encerramento da locação.
                	</p> -->
            	<p>
            	O contrato de seguro é celebrado por 12 meses, mediante pagamento de um prêmio que fica sobre a responsabilidade do locatário, calculado pela seguradora, devendo ser sucessivamente renovado, enquanto durar a locação. O candidato a locação passa por uma análise cadastral da seguradora e estando aprovado o seguro é parcelado em até quatro (04) vezes. A 1ª parcela é paga no ato do recebimento das chaves e as seguintes são incluídas nos próximos boletos de aluguel. Este pagamento se equivale ao prazo de 12 meses, sendo que após esse período o locatário deverá renová-lo pagando o valor por mais 12 meses, e assim sucessivamente até o encerramento da locação.	
            	</p>
            </li>
        </ul>
    </div>
    <!-- end box -->
</div>
<!-- end container -->
