<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
    	<!-- start institutional -->
    	<div class="institutional clearfix">
        	<h3 class="heading">a dallasanta</h3>
            <div class="descricao admin-description">
                <?php echo htmlspecialchars_decode($conteudo_pagina->descricao) ?>
            </div>

            <!-- start carousel2 -->
            <div class="carousel2 clearfix">
                <?php if (array_sum(array_map("count", $conteudo_pagina->gallery)) > 3): ?>
                	<a href="#" title="Anterior" class="prev" id="galeria-sobre-prev">prev</a>
                    <a href="#" title="Próximo" class="next" id="galeria-sobre-next">next</a>
                <?php endif ?>

                <div class="cycle-slideshow"
                     data-allow-wrap="false",
                     data-cycle-fx="scrollHorz"
                     data-cycle-timeout="0"
                     data-cycle-prev="#galeria-sobre-prev"
                     data-cycle-next="#galeria-sobre-next"
                     data-cycle-slides="> ul">

                    <?php foreach ($conteudo_pagina->gallery as $ul): ?>
                    	<ul class="clearfix">
                            <?php foreach ($ul as $img): ?>
                            	<li>
                                    <a href="<?php echo base_url('upload/gallery/'.$img->file) ?>" class="fancybox" rel="gallery">
                                        <?php
                                            $image = explode('.', $img->file);
                                        ?>
                                        <img src="<?php echo base_url('media/upload/gallery/'.$image[0].'-370x169.'.$image[1]) ?>" alt="">
                                    </a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    <?php endforeach ?>
                </div>
            </div>

            <!-- <?php if ($conteudo_pagina->video): ?>
                <div class="video-box">
                	<!--
                        <a href="" ref="#" title="Play" class="play">Play</a>
                    
                    <iframe width="100%" height="559" src="https://www.youtube.com/embed/<?php echo $conteudo_pagina->video ?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
               </div>
            <?php endif ?>-->

           <div class="location">
           		<h4>localização</h4>
                <address>Rua Voluntários da Pátria, 2822 - 3º Andar <span>Bairro Floresta • Porto Alegre-RS • CEP 90.230-010</span></address>
                <!-- start map -->
                <div class="map" style="float: left;margin-right: 70px;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.969679342115!2d-51.209942100000006!3d-30.009027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95197990bdb56663%3A0x14a3fc0226e1f616!2sR.+Volunt%C3%A1rios+da+P%C3%A1tria%2C+2822+-+Centro+Hist%C3%B3rico%2C+Porto+Alegre+-+RS%2C+90030-001!5e0!3m2!1spt-BR!2sbr!4v1436805716094" width="550" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="map" style="float:left;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1spt-BR!2sbr!4v1445016504366!6m8!1m7!1stNrHabS-h6yGJS0eslZL3w!2m2!1d-30.00926066899729!2d-51.20972486042309!3f343.5235352562577!4f5.685363381220526!5f0.7820865974627469" width="550" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <!-- end map -->
           </div>
           <!-- end location -->
        </div>
        <!-- end institutional -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
