<?php

    $page_with_maps_api = array('imoveis', 'empreendimentos');

//header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
//header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//header("Cache-Control: no-store, no-cache, must-revalidate");
//header("Cache-Control: post-check=0, pre-check=0", false);
//header("Pragma: no-cache");

echo doctype("html5");
?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">

        <meta name="title" content="{title}">
        <meta name="description" content="{description}">
        <meta name="keywords" content="{keywords}">
        <meta name="author" content="{author}">
        <meta name="robots" content="index,follow">
        <meta name="copyright" content="{copyright}">

        <meta property="og:title" content="{og_title}">
        <meta property="og:type" content="{og_type}">
        <meta property="og:url" content="{og_url}">
        <meta property="og:image" content="{og_image}">
        <meta property="og:site_name" content="{og_site_name}">
        <meta property="fb:admins" content="{fb_admins}">
        <meta property="og:description" content="{og_description}">
        <meta name="google-site-verification" content="L7rmjfOoFJ2xLX5ameQbOZiZgl1hdG0PT5aYl-NLhws" />
        <title>{title}</title>
        <base href="<?php echo base_url(); ?>" />

        <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon3.ico">
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>apple-touch-icon.png">

        <?php
            echo link_tag('assets/frontend/js/plugins/bootstrap/css/bootstrap.min.css'),
                 link_tag('assets/frontend/css/main.css');
        ?>

        {css}
        {js}

        <?php if (in_array($this->uri->rsegments[1], $page_with_maps_api)): ?>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEny5PWNO7nGU0JMH45K-PzyAu8AKxtcg"></script>
            <script type="text/javascript">
                function init_google_maps(lat, lng, streetview) {
                    var location = new google.maps.LatLng(lat, lng);

                    if (streetview == true) {
                        var options = {
                            position: location,
                            pov: {
                                heading: 165,
                                pitch: 0
                            },
                            zoom: 1
                        };

                        var myPano = new google.maps.StreetViewPanorama(document.getElementById('map-canvas-streetview'), options);
                            myPano.setVisible(true);
                    } else{
                        var options = {
                            zoom: 15,
                            scrollwheel:  false,
                            center: location
                        };
                        
                        
                        var map = new google.maps.Map(document.getElementById('map-canvas'), options);
                        
                        map.addListener('click', function() {
                            map.set('scrollwheel', true);
                        });

                        var marker = new google.maps.Marker({
                            map: map,
                            position: map.getCenter()
                        });
                    }
                }
            </script>
        <?php endif ?>
    </head>
    <body id="<?php echo str_replace('_', '-', sprintf('%s-%s', $this->uri->rsegments[1], $this->uri->rsegments[2])) ?>" class="<?php echo str_replace('_', '-', $this->uri->rsegments[1]) ?>">
        <div id="fb-root"></div>
        <script>
            (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        {top}
        {breadcrumb}
        {main}
        {bottom}

        <script>
            var base_url = "<?php echo base_url(); ?>",
                site_url = "<?php echo site_url(); ?>",
                privacy_policy = "<?php echo strip_tags(html_entity_decode($this->elements->get('politica_privacidade'))); ?>";
        </script>

        <?php echo script_tag('assets/frontend/js/plugins/jquery/jquery-1.11.3.min.js'); ?>

        <?php
            echo script_tag('assets/frontend/js/plugins/bootstrap/js/bootstrap.min.js'),
                 script_tag('assets/frontend/js/plugins/fancybox/jquery.fancybox.pack.js'),
                 script_tag('assets/frontend/js/plugins/cycle/jquery.cycle2.min.js'),
                 script_tag('assets/frontend/js/plugins/jquery-maskedinput/jquery.maskedinput.min.js'),
                 script_tag('assets/frontend/js/select.js'),
                 link_tag('assets/frontend/js/plugins/fancybox/jquery.fancybox.css');
        ?>

        <?php
            // Plugins da página de imóveis
            if (in_array($this->uri->rsegments[1], $page_with_maps_api)):
                echo script_tag('assets/frontend/js/plugins/noUiSlider.7.0.10/jquery.nouislider.all.min.js'),
                     script_tag('assets/frontend/js/plugins/numeraljs/numeral.min.js'),
                     script_tag('assets/frontend/js/plugins/numeraljs/languages.min.js'),
                     link_tag('assets/frontend/js/plugins/noUiSlider.7.0.10/jquery.nouislider.min.css'),
                     link_tag('assets/frontend/js/plugins/noUiSlider.7.0.10/jquery.nouislider.pips.min.css');
            endif;
        ?>

        <?php echo script_tag('assets/frontend/js/main.js'); ?>
		<!--Start of Zopim Live Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		$.src="//v2.zopim.com/?3d3QQD08z0ea8geqVdIx92OIDSzCsFgm";z.t=+new Date;$.
		type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<!--End of Zopim Live Chat Script-->
		
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
			{ (i[r].q=i[r].q||[]).push(arguments)}
			,i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-41356923-1', 'auto');
			ga('send', 'pageview');
		</script>
        <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 923539892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/923539892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


		
    </body>
</html>
