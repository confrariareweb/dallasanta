<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the current printer page size */
        margin: 7mm;  /* this affects the margin in the printer settings */
    }
    body {
        background-color:#FFFFFF;
        border: 0;
        margin: 0px;  /* the margin on the content before printing */
    }

    form,
    .top-bar,
    .header-bottom,
    .breadcrumb,
    .enterprises .col1 span,
    .download,
    .rightcol,
    .newsletter-box,
    .clients,
    .footer{
        display: none;
    }

    a{
        display: block;
    }
    a[href]:after {
        content: none!important;
    }

    .contact-info{
        float: left;
    }
    .contact-info li:nth-child(n+3){
        display: none;
    }

    .leftcol{
        width: 100%;
        float: none;
    }
    .enterprises-info{
        float: left;
        margin-left: 30px;
    }
    .enterprises-info .row{
        margin: 0;
        color: #d5190b;
    }
    .enterprises-info .price{
        float: none;
        color: #d5190b;
    }
    .enterprises-info .row strong{
        float: left;
        margin-right: 15px;
        color: #d5190b;
    }

    .location2{
        page-break-before: always;
        margin: 0;
    }
</style>

<!-- start container -->
<div class="container clearfix">
    <!-- start box -->
    <div class="box">
        <div class="clearfix">
            <!-- start leftcol -->
            <div class="leftcol">
                <!-- start enterprises -->
                <div class="enterprises clearfix">
                    <h4>
                        <?php echo $empreendimento->titulo ?>
                    </h4>
                    <div class="clearfix">
                        <!-- start col1 -->
                        <div class="col1">
                            <!-- start imgb -->
                            <div class="imgb">
                                <?php if ($empreendimento->fotos): ?>
                                    <img src="<?php echo sprintf($this->model_imovel->uri_images, 'inner', 478, 391, $empreendimento->fotos[0]->key, $empreendimento->fotos[0]->etag) ?>" alt="">
                                <?php else: ?>
                                    <img src="<?php echo assets_url('img/img_developments.jpg') ?>" alt="<?php echo $empreendimento->titulo ?>">
                                <?php endif ?>

                                <!-- <a href="#" title="tour virtual" class="tour-virtual">tour virtual</a> -->
                            </div>
                            <!-- end imgb -->
                            <span>Obs.: Imagem ilustrativa</span>
                        </div>
                        <!-- end col1 -->
                        <!-- start enterprises info -->
                        <div class="enterprises-info">
                            <div class="row clearfix">
                                <span class="price">
                                    <?php
                                        if ($empreendimento->precos)
                                            echo sprintf('R$ %s', number_format($empreendimento->precos->value, 2, ',', '.'))
                                    ?>
                                </span>
                                <strong>
                                    <?php
                                        if ($empreendimento->tipo_imovel == 'Aluguel'):
                                            echo 'Aluguel';
                                        else:
                                            echo 'Valor';
                                        endif;
                                    ?>
                                </strong>
                            </div>
                            <dl>
                                <dt>CÓDIGO:</dt>
                                <dd><?php echo $empreendimento->id ?></dd>

                                <dt>TIPO:</dt>
                                <dd><?php echo ($empreendimento->categoria) ? $empreendimento->categoria : 'Consulte'; ?></dd>

                                <dt>CIDADE:</dt>
                                <dd><?php echo sprintf('%s - %s', $empreendimento->endereco->city, $empreendimento->endereco->state) ?></dd>

                                <dt>BAIRRO</dt>
                                <dd><?php echo $empreendimento->endereco->district ?></dd>

                                <dt>Endereço:</dt>
                                <dd><?php echo sprintf('%s %s', $empreendimento->endereco->placeType, $empreendimento->endereco->place) ?></dd>

                                <dt>Dormitórios:</dt>
                                <dd><?php echo ($empreendimento->dormitorios) ? $empreendimento->dormitorios : 'Consulte'; ?></dd>

                                <dt>Vagas:</dt>
                                <dd><?php echo ($empreendimento->vagas) ? $empreendimento->vagas : 'Consulte'; ?></dd>

                                <dt>Área:</dt>
                                <dd class="area"><?php echo ($empreendimento->area_privativa) ? sprintf('%sm²', str_replace(',00', '', number_format($empreendimento->area_privativa, 2, ',', '.'))) : 'CONSULTE'; ?></dd>

                                <dt>Situação:</dt>
                                <dd>
                                    <?php echo $empreendimento->status ?>
                                </dd>

                                <dt>Iptu:</dt>
                                <dd><?php echo ($empreendimento->iptu) ? sprintf('R$ %s', number_format($empreendimento->iptu, 2, ',', '.')) : 'Consulte'; ?></dd>

                                <dt>Condomínio:</dt>
                                <dd><?php echo ($empreendimento->condominio) ? sprintf('R$ %s', number_format($empreendimento->condominio->value, 2, ',', '.')) : 'Consulte'; ?></dd>
                            </dl>
                        </div>
                        <!-- start enterprises info -->
                    </div>

                    <?php if ($empreendimento->descricao): ?>
                        <div class="property-description clearfix">
                            <h3 class="heading">DESCRIÇÃO DO IMÓVEL</h3>
                            <p style="font-size: 15px;">
                                <?php echo nl2br(trim($empreendimento->descricao)) ?>
                            </p>
                        </div>
                    <?php endif ?>

                    <div class="featured clearfix">
                        <h3 class="heading">CARACTERÍSTICAS DO IMÓVEL</h3>
                        <ul>
                            <?php
                                if ($empreendimento->dormitorios):
                                    $plural = ($empreendimento->dormitorios > 1) ? 's' : '';
                                    echo sprintf('<li>%s dormitório%s</li>', $empreendimento->dormitorios, $plural);
                                endif;

                                if ($empreendimento->caracteristicas->suites):
                                    $plural = ($empreendimento->caracteristicas->suites > 1) ? 's' : '';
                                    echo sprintf('<li>%s suíte%s</li>', $empreendimento->caracteristicas->suites, $plural);
                                endif;

                                if ($empreendimento->caracteristicas->banheiros):
                                    $plural = ($empreendimento->caracteristicas->banheiros > 1) ? 's' : '';
                                    echo sprintf('<li>%s banheiro%s</li>', $empreendimento->caracteristicas->banheiros, $plural);
                                endif;

                                if ($empreendimento->vagas):
                                    $plural = ($empreendimento->vagas > 1) ? 's' : '';
                                    echo sprintf('<li>%s vaga de garagem%s</li>', $empreendimento->vagas, $plural);
                                endif;

                                if ($empreendimento->area_privativa):
                                    echo sprintf('<li>%sm² área total</li>', str_replace(',00', '', number_format($empreendimento->area_privativa, 0, ',', '.')));
                                endif;

                                if ($empreendimento->area_total):
                                    echo sprintf('<li>%sm² área total</li>', str_replace(',00', '', number_format($empreendimento->area_total, 0, ',', '.')));
                                endif;

                                if ($empreendimento->status):
                                    echo sprintf('<li>Imóvel %s</li>', mb_strtolower($empreendimento->status), 'UTF-8');
                                endif;
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- end enterprises -->
            </div>
            <!-- end leftcol -->
            <!-- start rightcol -->
            <div class="rightcol">
                <!--
                <div class="icons-set clearfix">
                    <ul>
                        <li><a href="#" title="list">list</a></li>
                        <li><a href="#" title="play">play</a></li>
                        <li><a href="#" title="gallery">gallery</a></li>
                        <li><a href="#" title="location">location</a></li>
                    </ul>
                </div>
                -->

                <div class="clear"></div>
                <!-- start fillform -->
                <div class="fillform clearfix">
                    <h4>
                        Entre em contato<span>Nossa equipe está à sua disposição</span>
                    </h4>
                    <form method="post" action="<?php echo site_url('contact') ?>">
                        <div>
                            <input type="hidden" name="meio_captacao" value="">
                            <input type="text" name="Nome" placeholder="Nome:" class="input">
                            <input type="email" name="E-mail" placeholder="E-mail:" class="input">
                            <input type="text" name="Telefone" placeholder="Telefone:" class="input telefone">
                            <textarea name="Mensagem" placeholder="Mensagem:" rows="2" cols="2"  class="textarea"></textarea>
                            <button type="submit" class="button">
                                <span></span>ENVIAR
                            </button>
                        </div>
                    </form>
                </div>
                <!-- end fillform -->
                <!-- start contact info2 -->
                <div class="contact-info2">
                    <ul>
                        <li class="first">
                            <a href="javascript:window.print();">IMPRIMIR</a>
                        </li>
                        <li class="second">
                            <a href="#modal-enviar-por-email" class="fancybox">ENVIAR POR E-MAIL</a>
                        </li>
                        <li class="third">
                            <a href="#modal-indicar-para-amigo" class="fancybox">INDICAR P/ UM AMIGO</a>
                        </li>
                        <li class="fourth">
                            <a href="#modal-agendar-visita" class="fancybox">AGENDAR VISITA</a>
                        </li>
                        <li class="fifth">
                            <a href="#modal-ligamos-para-voce" class="fancybox">LIGAMOS PARA VOCÊ</a>
                        </li>
                        <li class="sixth">
                            <a href="#modal-fazer-proposta" class="fancybox">FAZER PROPOSTA</a>
                        </li>
                    </ul>
                </div>
                <!-- end contact info2 -->

                <div class="hide">
                    <div id="modal-enviar-por-email" class="modal-default">
                        <h1>Enviar por e-mail</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_enviar_email') ?>
                        </form>
                    </div>

                    <div id="modal-indicar-para-amigo" class="modal-default">
                        <h1>Indicar para um amigo</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_indicar_para_amigo') ?>
                        </form>
                    </div>

                    <div id="modal-agendar-visita" class="modal-default">
                        <h1>Agendar visita</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_agendar_visita') ?>
                        </form>
                    </div>

                    <div id="modal-fazer-proposta" class="modal-default">
                        <h1>Fazer proposta</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_fazer_proposta') ?>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end rightcol -->
        </div>

        <!-- start video -->
        <!--
        <div class="video clearfix">
            <div class="icons-set clearfix">
                <ul>
                    <li><a href="#" title="list">list</a></li>
                    <li><a href="#" title="play">play</a></li>
                    <li><a href="#" title="gallery">gallery</a></li>
                    <li><a href="#" title="location">location</a></li>
                </ul>
            </div>
            <h3 class="heading">VÍDEO DO IMÓVEL</h3>
            <div class="video-box clearfix">
                <a href="#" title="video"><img src="<?php echo assets_url('img/img_video_box2.jpg'); ?>" alt="video" /></a>
                <a href="#" title="play" class="play">Play</a>
            </div>
        </div>
        -->
        <!-- end video -->

        <!-- start property list -->
        <div class="property-list clearfix">
            <!--
            <div class="icons-set clearfix">
                <ul>
                    <li><a href="#" title="list">list</a></li>
                    <li><a href="#" title="play">play</a></li>
                    <li><a href="#" title="gallery">gallery</a></li>
                    <li><a href="#" title="location">location</a></li>
                </ul>
            </div>
            -->

            <?php if ($empreendimento->fotos): ?>
                <h3 class="heading">FOTOS DO IMÓVEL</h3>
                <ul class="clearfix">
                    <?php foreach ($empreendimento->fotos as $i => $foto): ?>
                    <li class="<?php echo ($i == 0) ? 'active' : ''; ?>">
                        <a href="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 500, 350, $foto->key, $foto->etag) ?>" class="fancybox" rel="gallery">
                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 138, 115, $foto->key, $foto->etag) ?>" alt="">
                        </a>
                        <!-- <a href="#" class="tour-virtual">Tour virtual</a> -->
                    </li>
                    <?php endforeach ?>
                </ul>
                <a href="<?php echo site_url('imoveis/download-fotos/'.$empreendimento->id) ?>" class="download">DOWNLOAD DAS FOTOS</a>
            <?php endif ?>
        </div>
        <!-- end property list -->
        <!-- start property list -->
        <div class="property-list clearfix">
            <h3 class="heading">FOTOS DO EMPREENDIMENTO - ROSSI ESTILO</h3>
            <ul class="clearfix">
                <li>
                    <a href="#" title="property"><img src="<?php echo assets_url('img/img_property.jpg'); ?>" alt="property" /></a>
                    <!-- <a href="#" title="tour virtual" class="tour-virtual">tour virtual</a> -->
                </li>
                <li>
                    <a href="#" title="property"><img src="<?php echo assets_url('img/img_property.jpg'); ?>" alt="property" /></a>
                    <!-- <a href="#" title="tour virtual" class="tour-virtual">tour virtual</a> -->
                </li>
            </ul>
            <a href="#" title="DOWNLOAD DAS FOTOS" class="download">DOWNLOAD DAS FOTOS</a>
        </div>
        <!-- end property list -->
        <!-- start location2 -->
        <div class="location2 clearfix">
            <!-- start row -->
            <div class="row clearfix">
                <!-- start icons set -->
                <!--<div class="icons-set clearfix">
                    <ul>
                        <li><a href="#" title="list">list</a></li>
                        <li><a href="#" title="play">play</a></li>
                        <li><a href="#" title="gallery">gallery</a></li>
                        <li><a href="#" title="location">location</a></li>
                    </ul>
                </div>-->
                <!-- end icon set -->
                <h3 class="heading">LOCALIZAÇÃO DO IMÓVEL</h3>
            </div>
            <!-- end row -->

            <?php if (isset($empreendimento->endereco->geolocation) && !empty($empreendimento->endereco->geolocation)): ?>
                <div class="map left">
                    <!-- <iframe src="https://www.google.com/maps/embed/v1/place?q=<?php echo $empreendimento->endereco->geolocation[1] .','. $empreendimento->endereco->geolocation[0] ?>&key=AIzaSyAEny5PWNO7nGU0JMH45K-PzyAu8AKxtcg" width="545" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> -->

                    <div id="map-canvas" style="width:545px; height:400px;"></div>
                    <script type="text/javascript">
                        init_google_maps(<?php echo $empreendimento->endereco->geolocation[1] .','. $empreendimento->endereco->geolocation[0] ?>);
                    </script>
                </div>
                <div class="map right">
                    <div id="map-canvas-streetview" style="width:545px; height:400px;"></div>
                    <script type="text/javascript">
                        init_google_maps(<?php echo $empreendimento->endereco->geolocation[1] .','. $empreendimento->endereco->geolocation[0] ?>, true);
                    </script>
                </div>
            <?php endif ?>
        </div>
        <!-- end location2 -->
        <!-- start property listing -->
        <div class="property-listing clearfix">
            <h3>IMÓVEIS DISPONÍVEIS NO EMPREENDIMENTO</h3>

            <?php if (array_sum(array_map("count", $empreendimento->imoveis)) > 4): ?>
                <a href="#" title="prev" class="prev" id="imoveis-empreendimento-nav-prev">prev</a>
                <a href="#" title="next" class="next" id="imoveis-empreendimento-nav-next">next</a>
            <?php endif; ?>

            <div class="cycle-slideshow property-listing"
                 data-allow-wrap="false",
                 data-cycle-fx="scrollHorz"
                 data-cycle-timeout="0"
                 data-cycle-prev="#imoveis-empreendimento-nav-prev"
                 data-cycle-next="#imoveis-empreendimento-nav-next"
                 data-cycle-slides="> ul">

                <?php foreach ($empreendimento->imoveis as $im): ?>
                    <ul class="clearfix">
                        <?php
                        foreach ($im as $empreendimento):
                            $empreendimento->tipo_view = 'inline';
                            echo '
                                <li>
                                    '. $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $empreendimento), TRUE) .'
                                </li>
                            ';
                        endforeach;
                        ?>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
        <!-- end property listing -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
