<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
    	<!-- start developments -->
    	<div class="developments clearfix">
        	<h3 class="heading">
                Empreendimentos
            </h3>
            <!-- <p>
                Procurando imóveis comerciais para locação? Aqui na Dallasanta você encontra os melhores escritórios, lojas e salas comerciais em locações privilegiadas, como Malls e Shopping Centers, em empreendimentos diferenciados nos melhores bairros de Porto Alegre.
            </p> -->
            <ul class="clearfix">
                <?php foreach ($empreendimentos as $tipo): ?>
                    <?php if ($tipo['titulo'] != '_semcategoria'): ?>
                	<li>
                        <a href="<?php echo site_url('empreendimentos#'.slug($tipo['titulo'])) ?>">
                            <?php echo $tipo['titulo'] ?>
                        </a>
                    </li>
                    <?php endif ?>
                <?php endforeach ?>
            </ul>

            <?php foreach ($empreendimentos as $tipo): ?>
                <?php if($tipo['titulo'] != '_semcategoria'){ ?>
                <!-- start common -->
                <div class="common clearfix" name="<?php echo slug($tipo['titulo']) ?>" id="<?php echo slug($tipo['titulo']) ?>">
                	<h3 class="heading">
                        <?php echo str_replace('_semcategoria', '#', $tipo['titulo']) ?>
                    </h3>

                    <ul class="row">
                    	<?php foreach ($tipo['empreendimentos'] as $empreendimento): ?>
                            <li class="col-sm-6">
                                <div>
                                    <a href="<?php echo site_url('empreendimentos/detalhes/'.$empreendimento->id) ?>">
                                        <?php if ($empreendimento->fotos_empreendimento): ?>
                                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'inner', 570, 427, $empreendimento->fotos_empreendimento[0]->key, $empreendimento->fotos_empreendimento[0]->etag) ?>" alt="<?php echo $empreendimento->titulo ?>">
                                        <?php else: ?>
                                            <img width="570" height="427" src="<?php echo assets_url('img/img_developments.jpg') ?>" alt="<?php echo $empreendimento->titulo ?>">
                                        <?php endif ?>
                                    </a>
                                    <div class="caption" style="top: 0px;">
                                        <a href="<?php echo site_url('empreendimentos/detalhes/'.$empreendimento->id) ?>" title="Ver detalhes" class="arrow">
                                            Ver detalhes
                                        </a>
                                        <h4 style="font-size: 16px;">
                                            <?php echo $empreendimento->name_empreendimento ?>
                                        </h4>
                                    </div>
                                    <div class="caption">
                                        <a href="<?php echo site_url('empreendimentos/detalhes/'.$empreendimento->id) ?>" title="Ver detalhes" class="arrow">
                                            Ver detalhes
                                        </a>
                                        <h4 style="font-size: 12px;">
                                            <?php echo $empreendimento->subtitulo; ?> | <?php echo $empreendimento->endereco->city;?>
                                        </h4>

                                       
                                    </div>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <!-- end common -->
            <?php } ?>
            <?php endforeach ?>
        </div>
        <!-- end developments -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
