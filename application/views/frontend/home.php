<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
    	<!-- start property listing -->
        <div class="property-listing clearfix">
        	<h3>ALUGUEL IMÓVEIS COMERCIAIS EM DESTAQUE</h3>

            <?php if (array_sum(array_map("count", $imoveis_aluguel)) > 4): ?>
                <a href="#" title="prev" class="prev" id="imoveis-aluguel-nav-prev">prev</a>
                <a href="#" title="next" class="next" id="imoveis-aluguel-nav-next">next</a>
            <?php endif; ?>

            <div class="cycle-slideshow property-listing"
                 data-allow-wrap="false",
                 data-cycle-fx="scrollHorz"
                 data-cycle-timeout="0"
                 data-cycle-prev="#imoveis-aluguel-nav-prev"
                 data-cycle-next="#imoveis-aluguel-nav-next"
                 data-cycle-slides="> ul">

                <?php foreach ($imoveis_aluguel as $im): ?>
                    <ul class="row">
                        <?php
                        foreach ($im as $imovel):
                            $imovel->tipo_view = 'inline';
                            ?>
                            <li class="col-md-3 col-sm-6">
                                <?php echo $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $imovel,'tipo_busca' => 'aluguel'), TRUE); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
        <!-- end property listing -->
        <!-- start property listing -->
        <div class="property-listing clearfix">
        	<h3>VENDA IMÓVEIS EM DESTAQUE</h3>

            <?php if (array_sum(array_map("count", $imoveis_venda)) > 4): ?>
                <a href="#" title="prev" class="prev" id="imoveis-venda-nav-prev">prev</a>
                <a href="#" title="next" class="next" id="imoveis-venda-nav-next">next</a>
            <?php endif; ?>

            <div class="cycle-slideshow property-listing"
                 data-allow-wrap="false",
                 data-cycle-fx="scrollHorz"
                 data-cycle-timeout="0"
                 data-cycle-prev="#imoveis-venda-nav-prev"
                 data-cycle-next="#imoveis-venda-nav-next"
                 data-cycle-slides="> ul">

                <?php foreach ($imoveis_venda as $im): ?>
                    <ul class="row">
                        <?php
                        foreach ($im as $imovel):
                            $imovel->tipo_view = 'inline';
                            ?>
                            <li class="col-md-3 col-sm-6">
                                <?php echo $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $imovel,'tipo_busca' => 'venda'), TRUE); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
        <!-- end property listing -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
