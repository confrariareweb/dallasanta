<div class="select-bar">
	<a href="javascript:;" class="download"></a>
    <button onclick="modal_loading(); $('#form-compara-imoveis').submit();" type="button" class="button">
        <span></span>COMPARAR SELECIONADOS
    </button>
	<div class="style-select">
        <select name="ordem" class="select" id="change-order-view">
            <option value="" <?php echo (!isset($_GET['sort'])) ? 'selected' : ''; ?>>Ordenar por:</option>
            <option value="mais-atual" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'mais-atual') ? 'selected' : ''; ?>>Mais atual</option>
            <option value="menos-atual" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'menos-atual') ? 'selected' : ''; ?>>Menos atual</option>
            <option value="maior-preco" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'maior-preco') ? 'selected' : ''; ?>>Maior preço</option>
            <option value="menor-preco" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'menor-preco') ? 'selected' : ''; ?>>Menor preço</option>
        </select>
    </div>
</div>

<div class="property-listing2 clearfix">
    <form method="get" action="<?php echo site_url('imoveis/comparar') ?>" id="form-compara-imoveis">
        <?php
        foreach ($favoritos as $imovel):
            echo $this->load->view(FRONTEND_VIEW.'/includes/imovel', array('imovel' => $imovel), TRUE);
        endforeach;
        ?>
    </form>
</div>