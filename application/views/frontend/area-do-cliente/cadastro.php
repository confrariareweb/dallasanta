<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
    	<!-- start register -->
    	<div class="maintenance register clearfix">
            <form class="form-ajax" method="post" action="<?php echo site_url('area-do-cliente/login') ?>">
            	<div class="row3 clearfix">
                	<h4>LOGIN</h4>
                    <input type="email" name="email" value="" class="input" placeholder="E-mail" required>
                	<input type="password" name="senha" value="" class="input none" placeholder="Senha" required maxlength="30">
                    <button type="submit" class="btn button" data-loading-text="Entrando...">
                        <span></span>Entrar
                    </button>
                </div>
            </form>

            <form method="post" action="<?php echo site_url('area-do-cliente/cadastro') ?>" class="form-ajax">
            	<div class="row row4 clearfix">
                	<div class="social2">
                        <!-- <a href="<?php echo site_url('area-do-cliente/facebook_login') ?>" title="LINKEDIN" class="linkedin">LOGIN COM LINKEDIN<span></span></a> -->

                        <a href="<?php echo site_url('area-do-cliente/google_login') ?>" title="G+" class="google">LOGIN COM GOOGLE<span></span></a>
                        <a href="<?php echo site_url('area-do-cliente/facebook_login') ?>" title="FACEBOOK" class="facebook">LOGIN COM FACEBOOK<span></span></a>
                    </div>
            		<h4>CADASTRO</h4>
                </div>
                <div class="row clearfix">
                	<input type="text" name="nome" value="" class="input" placeholder="Nome" maxlength="80" required>
                    <input type="text" name="sobrenome" value="" class="input" placeholder="Sobrenome" maxlength="80" required>
                    <!-- <input type="text" name="field" value="" class="input none" placeholder="E-mail"> -->
                    <input type="text" name="telefone" value="" class="input none telefone" placeholder="Telefone" maxlength="15" required>
                    <input type="email" name="email" value="" class="input" placeholder="E-mail" maxlength="150" required>
                    <input type="password" name="senha" value="" class="input none" placeholder="Senha" maxlength="25" required>
                </div>
                <div class="row2 clearfix">
                	<h4>Digite o código abaixo</h4>

                    <?php echo get_captcha('SITE_CADASTRO_CLIENTE') ?>

                    <button type="submit" class="btn button" data-loading-text="Enviando...">
                        <span></span>
                        Enviar
                    </button>
                </div>
            </form>

        </div>
        <!-- end register -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
