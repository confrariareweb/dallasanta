<div class="container clearfix">
    <div class="box">
        <div class="clearfix">
            <div class="column-left">
                <div class="side-bar clearfix">
                    <h4>Área do cliente</h4>
                    <div class="user-info">
                        <div class="imgb">
                            <?php
                                if (isset($this->session->userdata['area_restrita']->foto) && !empty($this->session->userdata['area_restrita']->foto)):
                                    if (strpos($this->session->userdata['area_restrita']->foto, 'http') === FALSE):
                                        $img = explode('.', $this->session->userdata['area_restrita']->foto);
                                        $img = base_url('media/upload/area-restrita/'. $img[0] .'-223x223.'. $img[1]);
                                    else:
                                        $img = $usuario['foto'];
                                    endif;
                                else:
                                    $img = assets_url('img/img_avatar.jpg');
                                endif;
                            ?>
                            <img src="<?php echo $img; ?>" alt="Avatar <?php echo $this->session->userdata['area_restrita']->nome ?>">
                        </div>

                        <strong>
                            <?php echo $this->session->userdata['area_restrita']->nome ?>
                        </strong>
                        <a href="<?php echo site_url('area-do-cliente/logout') ?>" class="underline-link">(sair)</a>
                    </div>

                    <?php $this->load->front_view('../includes/infos-contato') ?>

                    <div class="social-media2">
                        <ul class="clearfix">
                            <li>
                                <a href="#" title="facebook">facebook</a>
                            </li>
                            <li>
                                <a href="#" title="twitter">twitter</a>
                            </li>
                            <li>
                                <a href="#" title="gplus">gplus</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="column-right">
                <div class="clearfix">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="<?php echo ($tab_ativa == 'meus_dados') ? 'active' : ''; ?>">
                            <a href="#tab-meus-dados" aria-controls="tab-meus-dados" role="tab" data-toggle="tab">Meus dados</a>
                        </li>
                        <li role="presentation" class="<?php echo ($tab_ativa == 'meus_favoritos') ? 'active' : ''; ?>">
                            <a href="#tab-meus-favoritos" aria-controls="tab-meus-favoritos" role="tab" data-toggle="tab">Favoritos (<?php echo count($favoritos) ?>)</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade <?php echo ($tab_ativa == 'meus_dados') ? 'in active' : ''; ?>" id="tab-meus-dados">
                            <?php $this->load->front_view('meus-dados') ?>
                        </div>
                        <div role="tabpanel" class="tab-pane fade <?php echo ($tab_ativa == 'meus_favoritos') ? 'in active' : ''; ?>" id="tab-meus-favoritos">
                            <?php $this->load->front_view('meus-favoritos') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>