<!-- start container -->
<div class="container clearfix">
    <!-- start box -->
    <div class="box">
		<div class="content">
			<form method="post" action="<?php echo site_url('area-do-cliente/login') ?>" class="form-ajax" id="form-login-area-cliente">
				<h1>FAÇA SEU LOGIN</h1>
				<div class="form-group">
					<input type="email" name="email" value="" placeholder="E-mail:" class="input" required>
				</div>
				<div class="form-group">
					<input type="password" name="senha" value="" placeholder="Senha:" class="input" required>
				</div>
				<button type="submit" class="btn button white" data-loading-text="Entrando...">
					<span></span>
					Entrar
				</button>
				<a href="#modal-esqueci-minha-senha" class="underline-link fancybox" id="esqueci-minha-senha">
					Esqueci minha senha
				</a>
			</form>
			
			<form method="post" action="#" class="form-ajax" id="form-area-restrita">
				<h1>ACESSO RESTRITO</h1>
				<div class="form-group">
					<ul>
						<li><a target="_blnak" href="http://192.168.2.1/i/">Acesso Interno</a></li>
						<li><a target="_blnak" href="http://qualidade.dallasanta.com.br:8082/q">Acesso Externo</a></li>
					</ul>
				</div>
				<div class="form-group">
					<p>Somente para colaboradores</p>
				</div>
			</form>
		</div>
		
        <div style="display:none;">
            <div id="modal-esqueci-minha-senha" class="modal-default">
                <h1>Esqueci minha senha</h1>
                <p>Informe seu e-mail abaixo e enviaremos instruções para recuperar sua senha.</p>

                <form method="post" action="<?php echo site_url('area-do-cliente/recuperar-senha') ?>" class="form-ajax">
                    <div class="form-group">
                        <input type="email" name="email" value="" placeholder="E-mail:" class="input form-control" required autofocus>
                    </div>
                    <button type="submit" class="btn button" data-loading-text="Enviando...">
                        <span></span>
                        Enviar
                    </button>
                </form>
            </div>

            <?php if (isset($token)): ?>
                <div id="modal-recuperar-senha" class="modal-default">
                    <h1>Recuperar senha</h1>
                    <p>Digite abaixo sua nova senha.</p>

                    <form method="post" action="<?php echo site_url('area-do-cliente/salvar') ?>">
                        <div class="form-group">
                            <input type="hidden" name="token" value="<?php echo $token ?>">
                            <input type="password" name="nova_senha" value="" placeholder="Senha:" class="input form-control" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="nova_senha_confirmar" value="" placeholder="Digite a senha novamente:" class="input form-control" required>
                        </div>
                        <button type="submit" class="btn button" style="margin-right:10px;" data-loading-text="Enviando...">
                            <span></span>
                            Enviar
                        </button>

                        <a href="javascript:$.fancybox.close();" class="underline-link" onclick="$.fancybox.close()">
                            Cancelar
                        </a>
                    </form>
                </div>
            <?php endif ?>
        </div>

        <div class="acessar-redes-sociais">
            <div class="cadastro">
                <a href="<?php echo site_url('area-do-cliente/cadastro') ?>" class="button white">
                    <span></span>
                    Faça seu cadastro
                </a>
            </div>
            <!--div class="facebook">
                <a href="<?php echo site_url('area-do-cliente/facebook_login') ?>" class="button white">
                    <span></span>
                    Acesse com seu Facebook
                </a>
            </div-->
            <div class="google-plus">
                <a href="<?php echo site_url('area-do-cliente/google_login') ?>" class="button white">
                    <span></span>
                    Acesse com seu G+
                </a>
            </div>
        </div>
    </div>
    <!-- end box -->
</div>
<!-- end container -->