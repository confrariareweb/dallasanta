<div class="content clearfix">
    <h3 class="heading">EDIÇÃO DE PERFIL</h3>
    <!-- start col1 -->
    <div class="coll">
        <h4>DADOS GERAIS</h4>
        <span>
            <strong>E-mail:</strong>
            <?php echo $this->session->userdata['area_restrita']->email ?>
        </span>
        <form action="<?php echo site_url('area-do-cliente/salvar') ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="email" value="<?php echo $usuario['email'] ?>">
            <input type="text" name="nome" class="input" value="<?php echo $usuario['nome'] ?>" placeholder="Nome" required>
            <input type="text" name="sobrenome" class="input" value="<?php echo $usuario['sobrenome'] ?>" placeholder="Sobrenome" required>
            <input type="text" name="telefone" class="input telefone" value="<?php echo $usuario['telefone'] ?>" placeholder="Telefone">
            <input type="text" name="celular" class="input telefone" value="<?php echo $usuario['celular'] ?>" placeholder="Celular">
            <input type="text" name="estado" class="input" value="<?php echo $usuario['estado'] ?>" placeholder="Estado">
            <input type="text" name="cidade" class="input" value="<?php echo $usuario['cidade'] ?>" placeholder="Cidade">
            <input type="text" name="bairro" class="input" value="<?php echo $usuario['bairro'] ?>" placeholder="Bairro">
            <input type="text" name="endereco" class="input" value="<?php echo $usuario['endereco'] ?>" placeholder="Endereço">
            <input type="text" name="site" class="input" value="<?php echo $usuario['site'] ?>" placeholder="Site">
            <textarea name="descricao" rows="" cols="" placeholder="Descrição"  class="textarea"><?php echo $usuario['descricao'] ?></textarea>
            <!-- start row2 -->
            <div class="row2 clearfix">
                <h4>
                    ARQUIVO DE IMAGEM <small>(Formato permitido: JPG - 223x223px)</small>
                </h4>
                <div class="button-attachment">
                    <input type="text" name="foto" class="input" value="" placeholder="Arquivo..." readonly disabled>
                    <div>
                        <input type="file" name="foto">
                        <button type="button" class="button">
                            <span></span> Anexar
                        </button>
                    </div>
                </div>
            </div>
            <!-- end row2 -->
            <h4>SENHA</h4>
            <input type="password" name="nova_senha" value="" class="input" placeholder="Nova senha" maxlength="30">
            <input type="password" name="nova_senha_confirmar" value="" class="input" placeholder="Confirme nova senha" maxlength="30">
            <div class="clear"></div>
            <button type="submit" class="btn button" data-loading-text="Salvando...">
                <span></span>
                Salvar
            </button>
            <!-- <a href="#" title="Excluir conta" class="account" id="delete-account">Excluir conta</a> -->
         </form>
     </div>
     <!-- end col1 -->
</div>