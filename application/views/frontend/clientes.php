<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
    	<!-- start partners -->
    	<div class="partners clearfix">
        	<h3 class="heading">
                Clientes
            </h3>
            <p style="font-size: 14px;margin: 15px 0 15px 0;">Veja alguns dos nossos principais clientes.</p>
            <ul>
                <?php foreach ($clientes as $cliente){ ?>
                    <!--li>
                        <a href="<?php echo isset($cliente->datas['link'])? 'http://' . $cliente->datas['link'] : 'javascript:void(0);'; ?>" target="_blank">
                            <img src="<?php echo sprintf($this->model_imovel->uri_images,'crop','180','150',$cliente->datas['file'],''); ?>" title="<?php echo $cliente->datas['name']; ?>" alt="<?php echo $cliente->datas['name']; ?>">
                        </a>
                    </li-->
					
					<li style="    width: 180px;    height: 180px;">
                        <a href="<?php echo isset($cliente->datas['link'])? 'http://' . $cliente->datas['link'] : 'javascript:void(0);'; ?>" target="_blank"  style=" display: block;  width: 180px;  height: 180px;  background-size: cover;">
                            <img src="<?php echo str_replace('crop/180/150/','',sprintf($this->model_imovel->uri_images,'crop','180','150',$cliente->datas['file'],'')); ?>" title="<?php echo $cliente->datas['name']; ?>" style=" max-height: 176px;width: 176px;position: relative;top: 50%;transform: translateY(-50%); " alt="<?php echo $cliente->datas['name']; ?>">
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- end partners -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
