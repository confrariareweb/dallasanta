<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
    	<!-- start partners -->
    	<div class="partners clearfix">
        	<h3 class="heading">
                Parceiros
            </h3>
            <p style="font-size: 14px;margin: 15px 0 15px 0;">Veja alguns dos nossos principais parceiros.</p>
            <ul>
                <?php foreach ($parceiros as $parceiro){ ?>
                    <li style="    width: 180px;    height: 180px;">
                        <a href="<?php echo isset($parceiro->datas['link'])? 'http://' . $parceiro->datas['link'] : 'javascript:void(0);'; ?>" target="_blank"  style=" display: block;  width: 180px;  height: 180px;  background-size: cover;">
                            <img src="<?php echo str_replace('crop/180/150/','',sprintf($this->model_imovel->uri_images,'crop','180','150',$parceiro->datas['file'],'')); ?>" title="<?php echo $parceiro->datas['name']; ?>" style=" max-height: 176px;width: 176px;position: relative;top: 50%;transform: translateY(-50%);" alt="<?php echo $parceiro->datas['name']; ?>">
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- end partners -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
