<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
        <div class="clearfix">
            <h3 class="heading">
                Venda seu imóvel
            </h3>
            <p>
                A Dallasanta está sempre atenta a grandes oportunidades de negócio, incluindo a aquisição de imóveis comerciais.
                <br>
                Se você tem interesse em vender seu imóvel comercial, entre em contato conosco, utilizando o formulário abaixo para enviar sua mensagem.
            </p>
        </div>

        <div class="maintenance">
            <form action="<?php echo site_url('contact') ?>" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>DADOS PESSOAIS</legend>
                    <div class="row clearfix">
                        <input type="hidden" name="meio_captacao" value="SITE_VENDA_IMOVEL">

                        <input type="text" name="field[nome]" value="" placeholder="Nome:" class="input" required area-required="true">
                        <input type="hidden" name="label[nome]" value="Nome">

                        <input type="text" name="field[imobiliaria]" value="" placeholder="Imobiliária:" class="input" required area-required="true">
                        <input type="hidden" name="label[imobiliaria]" value="Imobiliária">

                        <input type="email" name="field[email]" value="" placeholder="E-mail:" class="input none" required area-required="true">
                        <input type="hidden" name="label[email]" value="E-mail">

                        <input type="text" name="field[cidade]" value="" placeholder="Cidade:" class="input" required area-required="true">
                        <input type="hidden" name="label[cidade]" value="Cidade">

                        <input type="text" name="field[telefone]" placeholder="Telefone:" class="input telefone" required>
                        <input type="hidden" name="label[telefone]" value="Telefone">

                        <input type="text" name="field[celular]" placeholder="Celular:" class="input telefone none" required>
                        <input type="hidden" name="label[celular]" value="Celular">
                    </div>
                </fieldset>

                <fieldset>
                    <legend>DADOS DO IMÓVEL</legend>
                    <div class="clearfix">
                        <div class="style-select input">
                            <select name="field[imovel_tipo]" class="select" required area-required="true">
                                <option value="" selected>Tipo:</option>
                                 <option value="Apartamento">Apartamento</option>
                                 <option value="Casa">Casa</option>
                                 <option value="Depósito">Depósito</option>
                                 <option value="Imóvel Comercial">Imóvel Comercial</option>
                                 <option value="Loja">Loja</option>
                                 <option value="Sala">Sala</option>
                                 <option value="Terreno">Terreno</option>

                            </select>
                            <input type="hidden" name="label[imovel_tipo]" value="Tipo de imóvel">
                        </div>

                        <input type="text" name="field[imovel_endereco]" placeholder="Endereço:" class="input" required>
                        <input type="hidden" name="label[imovel_endereco]" value="Endereço do imóvel">

                        <input type="text" name="field[imovel_bairro]" placeholder="Bairro:" class="input none" required>
                        <input type="hidden" name="label[imovel_bairro]" value="Bairro do imóvel">

                        <input type="text" name="field[imovel_cidade]" value="" placeholder="Cidade:" class="input" required>
                        <input type="hidden" name="label[imovel_cidade]" value="Cidade do imóvel">

                        <div class="style-select input">
                            <select name="field[ocupacao]" class="select" required area-required="true">
                                <option value="" selected>Ocupação:</option>
                                <option value="Disponível">Disponível</option>
                                <option value="Inquilino ">Inquilino </option>
                                <option value="Proprietário">Proprietário</option>
                            </select>
                            <input type="hidden" name="label[ocupacao]" value="Ocupaçao do imóvel">

                        </div>

                        <input type="text" name="field[valor_aluguel_atual]" value="" placeholder="Valor do aluguel atual:" class="input none" required area-required="true">
                        <input type="hidden" name="label[valor_aluguel_atual]" value="Valor do aluguel atual">

                        <input type="text" name="field[area_util]" value="" placeholder="Área útil m²:" class="input" required area-required="true">
                        <input type="hidden" name="label[area_util]" value="Área útil">

                        <input type="text" name="field[area_total]" value="" placeholder="Área total m²:" class="input" required area-required="true">
                        <input type="hidden" name="label[area_total]" value="Área total">

                        <input type="text" name="field[valor_pretendido]" value="" placeholder="Valor pretendido:" class="input none" required area-required="true">
                        <input type="hidden" name="label[valor_pretendido]" value="Valor pretendido">

                        <div class="col1">
                            <textarea name="field[descricao]" placeholder="Descrição:" class="textarea" required area-required="true"></textarea>
                            <input type="hidden" name="label[descricao]" value="Descrição">
                        </div>
                        <div class="col2">
                            <h4>Anexar documentos/arquivos</h4>
                            <div class="button-attachment">
                                <input type="text" name="documentos" class="input" value="" placeholder="Arquivo.doc..." readonly disabled>
                                <div>
                                    <input type="file" name="documentos">
                                    <input type="hidden" name="label[documentos]" value="Documentos">
                                    <button type="button" class="button">
                                        <span></span> Anexar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>DIGITE O CÓDIGO ABAIXO</legend>

                    <?php echo get_captcha('SITE_VENDA_IMOVEL') ?>

                    <button type="submit" class="btn button" data-loading-text="Enviando...">
                        <span></span>Enviar
                    </button>
                </fieldset>
            </form>
        </div>
    </div>
    <!-- end box -->
</div>
<!-- end container -->
