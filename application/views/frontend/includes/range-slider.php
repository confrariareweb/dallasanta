<div>
    <div class="range-slider-content"
         data-target="<?php echo $target ?>"
         data-min="<?php echo $min ?>"
         data-max="<?php echo $max ?>"
         data-min-active="<?php echo $min_active ?>"
         data-max-active="<?php echo $max_active ?>"
         data-step="<?php echo $step ?>"
         data-prefix="<?php echo $prefix ?>"
         data-sufix="<?php echo $sufix ?>"
         >
    </div>
    <div class="range-slider clearfix" id="<?php echo str_replace('#', '', $target) ?>">
        <span class="min-range">
            <?php echo $prefix . ' ' . $min . ' ' . $sufix ?>
        </span>
        <span class="max-range">
            <?php echo $prefix . ' ' . $max . ' ' . $sufix ?>
        </span>

        <input type="hidden" name="<?php echo $input_name ?>-min" class="input-min-range" value="">
        <input type="hidden" name="<?php echo $input_name ?>-max" class="input-max-range" value="">
    </div>
</div>

