<?php

if (!isset($this->session->userdata['area_restrita'])):
    $fav_title = 'Faça login para favoritar este imóvel';
    //$fav_class = 'disabled';
    $fav_class = 'fancybox';
    //$fav_href = 'javascript:;';
    $fav_href = '#modal-cadast';
else:
    $fav_title = 'Adicionar aos meus favoritos';
    $fav_class = NULL;
    $fav_href = site_url('area-do-cliente/favorito/'.$imovel->id);

    if (isset($imoveis_favoritos)) {
        if (in_array($imovel->id, $imoveis_favoritos)) {
            $fav_class = ' active';
            $fav_title = 'Remover dos meus favoritos';
        }
    }
endif;
//cria a url amigavel
$url = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $imovel->titulo ) );

$url = str_replace('--','-',preg_replace('/[^a-zA-Z0-9]/','-',$url).'-'.$tipo_busca.'-'.$imovel->id.'.html');

if (isset($imovel->tipo_view) && $imovel->tipo_view == 'inline'): // Imóveis inline (home)
    ?>

    <div>
        <div class="imgb">
            <!--<a href="<?php echo site_url('imoveis/detalhes/'.$imovel->id.'/'.$tipo_busca) ?>">-->
            <a href="<?php echo site_url($url) ?>">
                <img src="<?php echo (!$imovel->fotos) ? '' : sprintf($this->model_imovel->uri_images, 'crop', 270, 220, $imovel->fotos[0]->key, $imovel->fotos[0]->etag) ?>" alt="<?php echo $imovel->titulo ?>">
            </a>
        </div>
        <div class="txtb">
            <h4><?php echo $imovel->titulo ?></h4>
            <span><?php echo $imovel->subtitulo ?></span>
            <div>
                <ul class="clearfix">
                    <li class="valor">
                        <?php
                        if ($imovel->tipo_imovel == 'Aluguel'):
                            //echo '<span>Aluguel</span>';
                        endif;
                        ?>
                        <strong>
                            <?php
                                if($tipo_busca == 'venda'){
                                    $price = $imovel->preco_venda->value;
                                }else{
                                    $price = $imovel->preco_aluguel->value;
                                }

                                if ($imovel->showprice == 'Sim'):
                                    echo sprintf('R$ %s', str_replace(',00', '', number_format($price, 2, ',', '.')));
                                else:
                                    echo 'Consulte o valor';
                                endif;
                            ?>
                        </strong>
                    </li>
                    <li>
                        <?php echo sprintf('%sm²', str_replace(',00', '', number_format($imovel->area_privativa, 2, ',', '.'))) ?>
                    </li>
                </ul>
            </div>
            <!--<a href="<?php echo site_url('imoveis/detalhes/'.$imovel->id.'/'.$tipo_busca) ?>" class="button">-->
			<a href="<?php echo site_url($url); ?>" class="button">
                <span></span>VER DETALHES
            </a>
            <a href="<?php echo $fav_href ?>" title="<?php echo $fav_title ?>" class="rank <?php echo $fav_class ?>"></a>
        </div>
    </div>

<?php else: // Imóveis em bloco ?>

    <div class="common clearfix">
        <div class="imgb">
            <!--<a href="<?php echo site_url('imoveis/detalhes/'.$imovel->id.'/'.$tipo_busca) ?>">-->
            <a href="<?php echo site_url($url) ?>">
                <img src="<?php echo (!$imovel->fotos) ? '' : sprintf($this->model_imovel->uri_images, 'crop', 270, 220, $imovel->fotos[0]->key, $imovel->fotos[0]->etag) ?>" alt="<?php echo $imovel->titulo ?>">
            </a>
        </div>
        <div class="txtb">
            <h4>
                <?php echo $imovel->titulo ?>
                <span><?php echo $imovel->subtitulo ?> <?php if(!empty($imovel->endereco->complement)){echo $imovel->endereco->complement;} ?></span>
            </h4>

            <p>
                <?php echo character_limiter(trim(strip_tags($imovel->descricao)), 100) ?>
            </p>

            <div class="col1">
                <dl>
                    <dt>Tipo:</dt>
                    <dd><?php echo $imovel->categoria ?></dd>
                </dl>
                <dl>
                    <dt>Área:</dt>
                    <dd class="area">
                        <?php echo sprintf('%sm²', str_replace(',00', '', number_format($imovel->area_privativa, 2, ',', '.'))) ?>
                    </dd>
                </dl>
                <dl>
                    <dt>Valor:</dt>
                    <dd class="price" style="width: 240px !important;">

                        <?php
                            if($tipo_busca == 'venda'){
                                $price = $imovel->preco_venda->value;
                            }else{
                                $price = $imovel->preco_aluguel->value;
                            }

                            if ($imovel->showprice == 'Sim'):
                                echo sprintf('R$ %s', str_replace(',00', '', number_format($price, 2, ',', '.')));
                            else:
                                echo 'Consulte o valor';
                            endif;
                        ?>
                    </dd>
                </dl>
            </div>


            <div class="col2">
                <!--<a href="<?php echo site_url('imoveis/detalhes/'.$imovel->id.'/'.$tipo_busca) ?>" class="button">-->
				<a href="<?php echo site_url($url); ?>" class="button">
                    <span></span>VER DETALHES
                </a>
                <a href="<?php echo $fav_href ?>" title="<?php echo $fav_title ?>" class="rank2 <?php echo $fav_class ?>"></a>
            </div>
        </div>
        <div class="label">
            <input name="imoveis[]" type="checkbox" class="checkbox" value="<?php echo $imovel->id ?>">
        </div>
    </div>

<?php endif; ?>