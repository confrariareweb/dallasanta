<div class="container breadcrumb-container clearfix">
    <div class="box">
        <ul class="breadcrumb clearfix">
            <?php  foreach ($this->breadcrumb as $label => $link): ?>
                <li>
                    <?php if (!empty($link)): ?>
                        <a href="<?php echo site_url($link) ?>">
                            <?php echo $label ?>
                        </a>
                    <?php
                    else:
                        echo $label;
                    endif;
                    ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

