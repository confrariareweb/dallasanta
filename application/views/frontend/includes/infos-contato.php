<!-- start contact info -->
<div class="contact-info">
    <ul class="clearfix">
        <li class="first">
        	<span>ENTRE EM CONTATO</span>(51) 4063.6065
        </li>
        <li class="second">
        	<span>WHATSAPP</span>(51) 8912.9888
		</li>
        <li class="third">
        	<a href="#" title="CHAT ONLINE">CHAT ONLINE</a>
        </li>
        <li class="fourth">
        	<a href="#modal-ligamos-para-voce" class="fancybox">NÓS LIGAMOS<span>PRA VOCÊ</span></a>
        </li>
        <li class="fifth">
        	<a href="#modal-atendimento-email" class="fancybox">ATENDIMENTO <small>POR E-MAIL</small></a>
        </li>
    </ul>
<?php //var_dump($imovel); die; ?>
    <div style="display:none;">
	    <div id="modal-ligamos-para-voce" class="modal-default">
	    	<h1>Nós ligamos para você</h1>
	    	<p>
	    		Preencha o formulário abaixo e um consultor entrará em contato o mais rápido possível.
	    	</p>

	    	<form method="post" action="<?php echo site_url('contact') ?>">
	    		<div class="form-group">
	    			<?php if (($this->uri->segments[1] == 'imoveis'
	    				 	|| $this->uri->segments[1] == 'empreendimentos')
	    				 	&& $this->uri->segments[2] == 'detalhes'
	    				 	&& (isset($imovel) && !empty($imovel->id))):

	    					if ($this->uri->segments[1] == 'empreendimentos'):
	    						$label_id_imovel = 'Código do empreendimento';
	    					else:
	    						$label_id_imovel = 'Código do imóvel';
	    					endif;
	    			?>

	    				<input type="hidden" name="field[id_imovel]" value="<?php echo $this->uri->segments[3] ?>">
	    				<input type="hidden" name="label[id_imovel]" value="<?php echo $label_id_imovel ?>">

	    				<input type="hidden" name="meio_captacao" value="SITE_IMOVEL_LIGAMOS">
	    			<?php else: ?>
	    				<input type="hidden" name="meio_captacao" value="SITE_LIGAMOS">
	    			<?php endif ?>

	    			<input type="text" name="field[nome]" value="" placeholder="Nome:" class="input form-control" required area-required="true">
	    			<input type="hidden" name="label[nome]" value="Nome">
	    		</div>
	    		<div class="form-group">
	    			<input type="email" name="field[email]" value="" placeholder="Email:" class="input form-control" required area-required="true">
	    			<input type="hidden" name="label[email]" value="E-mail">
	    		</div>
	    		<div class="form-group half-input">
	    			<input type="text" name="field[cidade]" value="" placeholder="Cidade:" class="input form-control" required area-required="true">
	    			<input type="text" name="field[estado]" value="" placeholder="Estado:" class="input form-control" required area-required="true">

	    			<input type="hidden" name="label[cidade]" value="Cidade">
	    			<input type="hidden" name="label[estado]" value="Estado">
	    		</div>
	    		<div class="form-group">
	    			<input type="text" name="field[telefone]" value="" placeholder="Telefone:" class="input form-control telefone" required area-required="true">
	    			<input type="hidden" name="label[telefone]" value="Telefone">
	    		</div>
	    		<div class="form-group">
	    			<textarea name="field[mensagem]" class="textarea form-control" placeholder="Mensagem:" required area-required="true"></textarea>
	    			<input type="hidden" name="label[mensagem]" value="Mensagem">
	    		</div>
	    		<button type="submit" class="btn button" data-loading-text="Enviando...">
	    			<span></span>
	    			Enviar
	    		</button>
	    	</form>
	    </div>

	    <div id="modal-atendimento-email" class="modal-default">
	    	<h1>Atendimento por e-mail</h1>
	    	<p>
	    		Preencha o formulário abaixo e um consultor entrará em contato o mais rápido possível.
	    	</p>

	    	<form method="post" action="<?php echo site_url('contact') ?>">
	    		<div class="form-group">
	    			<input type="hidden" name="meio_captacao" value="SITE_ATENDIMENTO">

	    			<input type="text" name="field[nome]" value="" placeholder="Nome:" class="input form-control" required area-required="true">
	    			<input type="hidden" name="label[nome]" value="Nome">
	    		</div>
	    		<div class="form-group">
	    			<input type="email" name="field[email]" value="" placeholder="Email:" class="input form-control" required area-required="true">
	    			<input type="hidden" name="label[email]" value="E-mail">
	    		</div>
	    		<div class="form-group half-input">
	    			<input type="text" name="field[cidade]" value="" placeholder="Cidade:" class="input form-control" required area-required="true">
	    			<input type="text" name="field[estado]" value="" placeholder="Estado:" class="input form-control" required area-required="true">

	    			<input type="hidden" name="label[cidade]" value="Cidade">
	    			<input type="hidden" name="label[estado]" value="Estado">
	    		</div>
	    		<div class="form-group">
	    			<input type="text" name="field[telefone]" value="" placeholder="Telefone:" class="input form-control telefone" required area-required="true">
	    			<input type="hidden" name="label[telefone]" value="Telefone">
	    		</div>
	    		<div class="form-group">
	    			<textarea name="field[mensagem]" class="textarea form-control" placeholder="Mensagem:" required area-required="true"></textarea>
	    			<input type="hidden" name="label[mensagem]" value="Mensagem">
	    		</div>
	    		<button type="submit" class="btn button" data-loading-text="Enviando...">
	    			<span></span>
	    			Enviar
	    		</button>
	    	</form>
	    </div>
    </div>
</div>
<!-- end contact info -->