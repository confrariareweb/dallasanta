<!-- start about box -->
<div class="container newsletter-box" id="newsletter">
    <?php echo alert('newsletter') ?>

    <div class="about-box clearfix">
        <!-- start col1 -->
        <div class="col1">
            <h4>SOBRE A DALLASANTA</h4>
            <p style="text-align: justify;">
                <?php echo $this->elements->get('sobre-a-dallasanta') ?>
            </p>
        </div>
        <!-- end coll -->
        <!-- start col1 -->
        <div class="col1">
            <h4>ONDE ESTAMOS</h4>

				<div class="tapamapa"></div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6909.93935868423!2d-51.209942!3d-30.009027!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95197990bdb56663%3A0x14a3fc0226e1f616!2sR.+Volunt%C3%A1rios+da+P%C3%A1tria%2C+2822+-+Centro+Hist%C3%B3rico%2C+Porto+Alegre+-+RS%2C+90030-001%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1445863941289" width="100%" height="170" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>
        <!-- end coll -->
        <!-- start newsletter -->

        <div class="newsletter">
            <h4>RECEBA NOVIDADES</h4>
            <p>Se você deseja ficar sabendo das novidades da Dallasanta, basta informar o seu e-mail.</p>
            <form method="post" action="<?php echo site_url('contact/newsletter') ?>" class="clearfix">
                <input name="nome" type="text" value="" placeholder="Nome" class="newsletter-input left" required="true" area-required="true">
                <input name="email" type="email" value="" placeholder="E-mail" class="newsletter-input right" required="true" area-required="true">
                <button type="submit" class="btn button" data-loading-text="Enviando...">
                    <span></span>ENVIAR
                </button>
            </form>
        </div>
        <!-- end newsletter -->
    </div>
</div>
<!-- end about box -->

<!-- start clients -->
<?php if($_SERVER['REQUEST_URI'] === '/dallasanta/'){ ?>
<div class="clients clearfix">
	<!-- start box -->
    <div class="container">
    	<!-- start col1 -->
        <div class="col1">
        	<h4>CLIENTES</h4>
            <p style="width: 290px;">
                    <?php echo $this->elements->get('clientes-footer') ?>
            </p>
        </div>
        <!-- end col1 -->
        <!-- start carousel -->
        <div class="carousel">
            <?php if (array_sum(array_map("count", $this->clientes_footer)) > 5): ?>
                <a href="#" title="prev" class="prev" id="clientes-footer-nav-prev">prev</a>
                <a href="#" title="next" class="next" id="clientes-footer-nav-next">next</a>
            <?php endif; ?>

            <div class="cycle-slideshow"
                 data-cycle-fx="scrollHorz"
                 data-cycle-timeout="6000"
                 data-cycle-prev="#clientes-footer-nav-prev"
                 data-cycle-next="#clientes-footer-nav-next"
                 data-cycle-slides="> ul">

                 <?php
                     foreach ($this->logos_bottom as $cl):
                         ?>
                    <ul class="clearfix">
                        <?php
                        foreach ($cl as $cliente):
                            if ($cliente->banner): ?>
                            <li>
                                <a target="new" target="_blank" href="http://<?php echo (!empty($cliente->link)) ? $cliente->link : 'javascript:void(0)'; ?>" title="<?php echo $cliente->name ?>">
                                    <img src="<?php echo sprintf($this->model_imovel->uri_images,'crop','120','60',$cliente->banner,''); ?>" title="<?php echo $cliente->name; ?>" alt="<?php echo $cliente->name; ?>">
                                </a>
                            </li>
                        <?php
                            endif;
                        endforeach; ?>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
        <!-- end carousel -->
    </div>
    <!-- end box -->
</div>
<?php } ?>
<!-- end clients -->
<!-- start footer -->
<div class="footer clearfix">
	<!-- start box -->
    <div class="container">
    	<!-- start footer top -->
        <div class="footer-top clearfix">
        	<!-- start footer links -->
            <div class="footer-links clearfix">
            	<!-- satrt col1 -->
                <div class="col col1">
                	<h5>A DALLASANTA</h5>
                    <ul>
                    	<li>
                            <a href="<?php echo site_url('sobre') ?>" title="Sobre a empresa">
                                Sobre a empresa
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end col1 -->
                <!-- satrt col2 -->
                <div class="col col2">
                	<h5>ALUGUEL DE IMÓVEIS COMERCIAIS</h5>
                    <ul>
                    	<li>
                            <a href="<?php echo site_url('imoveis/aluguel/?categorias[]=Casa') ?>">Casa</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('imoveis/aluguel/?categorias[]=Depósito') ?>">Depósito / Pavilhão</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('imoveis/aluguel/?categorias[]=Loja') ?>">Loja</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('imoveis/aluguel/?categorias[]=Prédio') ?>">Prédio Comercial</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('imoveis/aluguel/?categorias[]=Sala') ?>">Sala Comercial</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('imoveis/aluguel/?categorias[]=Terreno') ?>">Terreno</a>
                        </li>
                    </ul>
                </div>
                <!-- end col2 -->
                <!-- satrt col3 -->
                <div class="col col3">
                	<h5>IMÓVEIS À VENDA</h5>
                    <ul>
                    	<li>
                            <a href="<?php echo site_url('imoveis/venda/?categorias[]=Apartamento&dormitorios=1&tipo-imovel=venda') ?>">Apartamentos de 1 dorm.</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('imoveis/venda/?categorias[]=Apartamento&dormitorios=3&tipo-imovel=venda') ?>">Apartamentos de 3 dorm.</a>
                        </li>
                    </ul>
                </div>
                <!-- end col3 -->
                <!-- satrt col4 -->
                <div class="col col4">
                	<h5>EMPREENDIMENTOS</h5>
                    <ul>
                    	<li>
                            <a href="<?php echo site_url('empreendimentos#condominio') ?>">Condomínio</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('empreendimentos#flat') ?>">Flat</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('empreendimentos#predio') ?>">Prédio</a>
                        </li>
                    </ul>
                </div>
                <!-- end col4 -->
                <!-- satrt col5 -->
                <div class="col col5">
                    <h5>
                        <a href="<?php echo site_url('clientes') ?>">CLIENTES</a>
                    </h5>
                </div>
                <!-- end col5 -->
                <!-- satrt col6 -->
                <div class="col col6">
                	<h5>
                        <a href="<?php echo site_url('venda-seu-imovel') ?>">VENDA SEU IMÓVEL</a>
                    </h5>
                </div>
                <!-- end col6 -->
            </div>
            <!-- end footer links -->
        	<!-- start row -->
            <div class="clearfix">
            	<!-- start social plugin -->
                <div class="social-plugin clearfix">
                	<!-- <a href="#" title="facebook plugin"><img src="<?php echo assets_url('img/img_social_plugin.png') ?>" alt="facebook plugin" /></a> -->
                    <div class="gp">
                        <g:plusone size="medium" data-href="" data-width="57px"></g:plusone>
                        <script type="text/javascript">
                          (function() {
                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                            po.src = 'https://apis.google.com/js/plusone.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                          })();
                        </script>
                    </div>
                    <div class="fb">
                        <div class="fb-like" data-href="https://www.facebook.com/DallasantaImoveis" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                    </div>
                </div>
                <!-- end social plugin -->
                <!-- start footer logo -->
                <div class="footer-logo">
                	<a href="<?php echo site_url('home') ?>" title="Voltar à página inicial">
                        <img src="<?php echo assets_url('img/footer_logo.png') ?>" alt="Dallasanta">
                    </a>
                </div>
                <!-- end footer logo -->
                <!-- start txtb -->
                <div class="txtb">
                	<address>Rua Voluntários da Pátria, 2822 - 3° Andar - Bairro Floresta - Porto Alegre - RS - CEP 90.230-010</address>
                	<span>Horário de atendimento: das 8h às 17h45.</span>
                </div>
                <!-- end txtb -->
            </div>
            <!-- end row -->
        </div>
        <!-- end footer top -->
        <div class="footer-bottom clearfix <?php echo (isset($_COOKIE['infos_bottom_closed']) && $_COOKIE['infos_bottom_closed'] == 'true') ? 'hide' : ''; ?>">
            <!-- <div class="row clearfix">
                    <?php // $this->load->view(FRONTEND_VIEW.'includes/infos-contato') ?>
                <a href="javascript:;" id="close-infos-bottom" title="Fechar" class="close">close</a>
            </div> -->
            <p>
            	<?php if($this->core_url['spam'] != ''){
                    echo $this->core_url['spam'];
                } else {
                    echo $this->core_url['spam'] = 'A Dallasanta é uma empresa focada na comercialização de imóveis próprios para locação e aquisição
                    de ativos para renda. Atuamos no segmento de Imóveis Comerciais, desde a Incorporação, Construção, Locação e
                    Administração de Empreendimentos próprios. Aliamos a expertise no segmento, com a filosofia empresarial de
                    prestar serviço com qualidade, atendimento personalizado, ética profissional e comprometimento com os clientes,
                    buscando atender suas expectativas imobiliárias.';
                } ?>
            </p>
			<h1>{h1}</h1>
        </div>
    </div>
    <!-- end box -->
</div>
<!-- end footer -->
