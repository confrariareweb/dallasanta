<?php echo alert(); ?>

<!-- start top bar -->
<div class="top-bar clearfix">
	<!-- start box -->
    <div class="container">
        <?php
        if (isset($this->session->userdata['area_restrita'])):
            $uri = str_replace('_', '-', $this->uri->rsegments[1]);

            if (isset($this->uri->segments[2])):
                $uri .= '/'.$this->uri->segments[2];
            endif;
            ?>
            <div class="welcome-message">
                <span>Olá, <?php echo $this->session->userdata['area_restrita']->nome ?></span>
                <a href="<?php echo site_url('area-do-cliente/logout/?redirect='.$uri) ?>">(sair)</a>
            </div>
        <?php endif ?>

    	<ul class="clearfix">
        	<li>
                <a href="<?php echo site_url('venda-seu-imovel') ?>" title="VENDA SEU IMÓVEL">
                    VENDA SEU IMÓVEL
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('documentos') ?>" title="DOCUMENTOS E GARANTIAS">
                    DOCUMENTOS E GARANTIAS
                </a>
            </li>
            <li>
                <a target="new" href="http://2vialocacao.suprisoft.com.br/2viaAlug.ASP?SIGLA=DSF&ORIGEM=www.dallasanta.com.br&EMPRESA=dallasanta&IMAGEMORIGEM=http://www.dallasanta.com.br/imagens/logodsf.jpg" title="2&deg; VIA DOC DE ALUGUEL">
                    2&deg; VIA DOC DE ALUGUEL
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('area-do-cliente') ?>" title="ÁREA DO CLIENTE">
                    ÁREA DO CLIENTE
                </a>
            </li>
        </ul>
    </div>
    <!-- end box -->
</div>
<!-- end top bar -->
<!-- start header -->
<div class="header clearfix">
	<!-- start header top -->
    <div class="header-top clearfix">
    	<!-- start box -->
        <div class="container">
        	<!-- start logo -->
            <div class="logo">
            	<a href="<?php echo site_url('home') ?>" title="Voltar à página inicial">
                    <img src="<?php echo assets_url('img/logo.png') ?>" alt="Dallasanta">
                </a>
            </div>
            <!-- end logo -->

            <?php $this->load->view(FRONTEND_VIEW.'includes/infos-contato') ?>
        </div>
        <!-- end box -->
    </div>
    <!-- end header top -->

    <!-- start header top -->
    <div class="top_fixo" style="width: 1170px;margin: 0 auto; display: none;">
        <div class="header-top clearfix" style="position: fixed;top: 0;background: white;z-index: 9999;box-shadow: 50em 0 0 0 #FFFFFF,-50em 0 0 0 #FFFFFF;">
            <!-- start box -->
            <div class="container">
                <!-- start logo -->
                <div class="logo">
                    <a href="<?php echo site_url('home') ?>" title="Voltar à página inicial">
                        <img src="<?php echo assets_url('img/logo.png') ?>" alt="Dallasanta">
                    </a>
                </div>
                <!-- end logo -->

                <?php $this->load->view(FRONTEND_VIEW.'includes/infos-contato') ?>
            </div>
            <!-- end box -->
        </div>
    </div>
    <!-- end header top -->
    <!-- start header bottom -->
    <div class="header-bottom clearfix">
    	<!-- start box -->
        <div class="container">
        	<a href="<?php echo site_url('home') ?>" title="Voltar à página inicial" class="index">
                home
            </a>
            <!-- start social -->
            <div class="social">
            	<ul>
                	<li>
                        <a href="https://www.facebook.com/pages/Dallasanta/1432576087007891?fref=ts" title="facebook">facebook</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/Dallasanta_Emp" title="twitter">twitter</a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/+DallasantaBR" title="gplus">google plus</a>
                    </li>
                </ul>
            </div>
            <!-- end social -->
            <!-- start navigation -->
            <div class="navigation">
            	<ul class="clearfix">
                	<li>
                        <a href="<?php echo site_url('sobre') ?>" title="A DALLASANTA">A DALLASANTA</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('imoveis/aluguel') ?>" title="ALUGUEL">ALUGUEL</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('imoveis/venda') ?>" title="VENDA">VENDA</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('empreendimentos') ?>" title="EMPREENDIMENTOS">EMPREENDIMENTOS</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('clientes') ?>" title="CLIENTES">CLIENTES</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('parceiros') ?>" title="PARCEIROS">PARCEIROS</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('fale-conosco') ?>" title="CONTATO">CONTATO</a>
                    </li>
                </ul>
            </div>
            <!-- end navigation -->
        </div>
        <!-- end box -->
    </div>
    <!-- end header bottom -->
</div>
<!-- end header -->

<?php if ($this->uri->rsegments[1] == 'home'): ?>
    <div class="conceitual-banners">
        <div class="cycle-slideshow"
             data-cycle-fx="scrollHorz"
             data-cycle-timeout="10000"
             data-cycle-pager="#conceitual-banners-pager"
             data-cycle-slides="> div">
                
            <?php 
            
            $b =count($banner);
            
            
            for ($i = 0; $i < $b; $i++) {// echo site_url('upload/gallery/'.$banner[$i]->galeria[0]->file).'<br>';
            ?>
               
                <div>
                    <!-- start showcase -->
                    <a href="<?php echo $banner[$i]->url; ?>" >
                    <div class="showcase" style="background:url(<?php echo site_url('upload/gallery/'.$banner[$i]->galeria[0]->file); ?>) 50% 0% / 100% 100% no-repeat" 
                         data-banner-desktop="<?php echo site_url('upload/gallery/'.$banner[$i]->galeria[0]->file); ?> 50% 0% / 100% 100% no-repeat" data-banner-mobile="<?php echo site_url('upload/gallery/'.$banner[$i]->galeria[0]->file); ?> 50% 0% / 100% 100% no-repeat">
                       <!-- start box -->
                        <div class="container">
                            <!-- start caption -->
                         
                            <!-- end caption -->
                        </div>
                        <!-- end box -->
                    </div>
                    <!-- end showcase -->
                </div>
                 </a>
            <?php
            }
            ?>

 		</div>
        
        <!-- start pagination -->
        <div class="pagination" id="conceitual-banners-pager"></div>
        <!-- end pagination -->
    </div>
<?php endif; ?>

<!-- start filter -->
<div class="filter clearfix">
	<!-- start box -->
    <div class="container">
    	<form action="<?php echo site_url('imoveis/busca') ?>" method="get" id="form-busca-principal">
        	<!-- start col1 -->
            <div class="col1">
            	<span class="search">BUSCAR IMÓVEIS</span>
                <div class="input-radio-style clearfix">
                    <label class="button button2 <?php echo ($this->busca_principal['checked']->aluguel) ? 'button3' : ''; ?>" for="busca-principal-tipo-aluguel" id="btn_aluguel">
                        <input <?php echo $this->busca_principal['checked']->aluguel ?> name="tipo-imovel" type="radio" value="Aluguel" id="busca-principal-tipo-aluguel" class="radio-input">
                        Aluguel
                    </label>
                    <label class="button button2 <?php echo ($this->busca_principal['checked']->venda) ? 'button3' : ''; ?>" for="busca-principal-tipo-venda" id="btn_venda">
                        <input <?php echo $this->busca_principal['checked']->venda ?> name="tipo-imovel" type="radio" value="Venda" id="busca-principal-tipo-venda" class="radio-input">
                        Venda
                    </label>
                </div>
            </div>
            <!-- end col1 -->
            <!-- start col2 -->
            <div class="col1 col2">
            	<!-- start common -->
             <!--   <div class="common">
            		<h4>Tipo de Imóvel</h4>
                	<div class="style-select select-with-checkbox">
                        <span class="select">
                            Selecione...
                        </span>
                        <div class="checkbox">
                            <?php foreach ($this->busca_principal['categorias'] as $tipo): ?>
                                <label>
                                    <input type="checkbox" name="categorias[]" value="<?php echo $tipo ?>">
                                    <?php echo $tipo ?>
                                </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div> -->
                <div class="common" id="aluguel_busca" >
                    <h4>Tipo de Imóvel</h4>
                    <div class="style-select select-with-checkbox">
                        <span class="select">
                            Selecione...
                        </span>
                        <div class="checkbox">
                            <?php foreach ($this->busca_principal['categorias_aluguel'] as $tipo): ?>
                                <label>
                                    <input type="checkbox" name="categorias[]" value="<?php echo $tipo ?>">
                                    <?php echo $tipo ?>
                                </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="common" id="venda_busca" style="display:none;">
                    <h4>Tipo de Imóvel</h4>
                    <div class="style-select select-with-checkbox">
                        <span class="select">
                            Selecione...
                        </span>
                        <div class="checkbox">
                            <?php foreach ($this->busca_principal['categorias_venda'] as $tipo): ?>
                                <label>
                                    <input type="checkbox" name="categorias[]" value="<?php echo $tipo ?>">
                                    <?php echo $tipo ?>
                                </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- end common -->
                <!-- start common -->
                <div class="common">
            		<h4>Cidade</h4>
                    <div class="style-select style-select2 select-with-checkbox">
                        <span class="select">
                            Selecione...
                        </span>
                        <div class="checkbox">
                            <?php foreach ($this->busca_principal['cidades'] as $cidade): ?>
                                <label>
                                    <input type="checkbox" name="cidades[]" value="<?php echo $cidade ?>">
                                    <?php echo $cidade ?>
                                </label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- end common -->
            	<button type="submit" class="btn button" data-loading-text="Buscando...">
                    <span></span>BUSCAR
                </button>
            </div>
            <!-- end col1 -->
            <!-- start col3 -->
            <div class="col1 col3">
            	<h4>Palavra-chave</h4>
            	<input name="palavra-chave" type="text" value="" class="input2">
                <button type="submit" class="btn button" data-loading-text="Buscando...">
                    <span></span>BUSCAR
                </button>
            </div>
            <!-- end col3 -->
            <!-- start col4 -->
            <div class="col1 col4">
            	<h4>Código</h4>
                <input name="codigo" type="text" value="" class="input2 input3">
                <button type="submit" class="btn button" data-loading-text="Buscando...">
                    <span></span>BUSCAR
                </button>
            </div>
            <!-- end col4 -->
        </form>
    </div>
    <!-- end box -->
</div>

<!-- end filter -->
<div style="display:none;">
    <div id="modal-cadast" class="modal-default">
        <h1 style="margin-left: 15px;font-size: 21px;background: black;color: white;width: 340px;padding: 13px 0 13px 0;">COMPLETE O CADASTRO ABAIXO</h1>

        <form method="post" action="<?php echo site_url('area-do-cliente/cadastro') ?>" class="form-ajax">
            <div class="clearfix" style="margin-left: 15px;">
                <input type="text" name="nome" value="" class="input" placeholder="Nome" maxlength="80" required style="margin-bottom: 10px;">
                <input type="text" name="sobrenome" value="" class="input" placeholder="Sobrenome" maxlength="80" required style="margin-bottom: 10px;">
                <input type="text" name="telefone" value="" class="input none telefone" placeholder="Telefone" maxlength="15" required style="margin-bottom: 10px;">
                <input type="email" name="email" value="" class="input" placeholder="E-mail" maxlength="150" required style="margin-bottom: 10px;">
                <input type="password" name="senha" value="" class="input none" placeholder="Senha" maxlength="25" required>
                <h4 style="margin-top:15px;">Digite o código abaixo</h4>

                <?php echo get_captcha('SITE_CADASTRO_CLIENTE') ?>
            </div>
            <div class="clearfix" style="text-align: center;width: 280px;margin: 0 auto;">
                <a href="<?php echo site_url('area-do-cliente/facebook_login') ?>" title="FACEBOOK" style="background: #3C5A9A;padding: 0px 55px 0px 55px;margin-top: 20px;display: inline-block;border-radius: 10px;color: white;">
                    <span style="background: url(assets/frontend/img/sprite_facebook.png) no-repeat left top;height: 20px;width: 20px;display: inline-block;float: left;margin-top: 5px;"></span>
                    <p style="float: left;margin-left: 15px;line-height: 24px;position: relative;top: 3px;">Entrar com o Facebook</p>
                </a>
                <a href="<?php echo site_url('area-do-cliente/google_login') ?>" title="G+" style="background: #DD4C3B;padding: 0px 68px 0px 56px;margin-top: 5px;display: inline-block;border-radius: 10px;color: white;">
                    <span style="background: url(assets/frontend/img/sprite_google.png) no-repeat left top;height: 20px;width: 20px;display: inline-block;float: left;margin-top: 5px;"></span>
                    <p style="float: left;margin-left: 15px;line-height: 24px;position: relative;top: 3px;">Entrar com o Google</p>
                </a> 
                <button type="submit" class="btn button" data-loading-text="Enviando..." style="margin-top:20px;">
                    <span></span>
                    Efetuar Cadastro
                </button>   

                <p style="margin-top: 15px;font-size: 14px;font-weight: bold;">Se você já possui cadastro <a href="<?php echo site_url('area-do-cliente') ?>" style="color:#CE142B;">clique aqui</a></p>
            </div>
        </form>
    </div>
</div>