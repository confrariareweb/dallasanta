<div class="container clearfix">
    <div class="box">
    	<div class="clearfix">
        	<h3 class="heading">Quero alugar um Dallasanta</h3>
            <p>
                A Dallasanta possui os melhores imóveis de locação comercial. Se você tem interesse em alugar um de nossos imóveis, preencha o formulário abaixo e um de nossos corretores entrará em contato.
            </p>

            <form action="<?php echo site_url('contact') ?>" method="post" class="form-row">
            	<fieldset>
                    <?php echo $this->form->get_form('contato_alugar_imovel') ?>
                </fieldset>
            </form>
        </div>
    </div>
</div>