<div class="container clearfix">
    <div class="box">
    	<div class="clearfix">
        	<h3 class="heading">Quero ser fornecedor</h3>
            <p>
                A Dallasanta está sempre buscando profissionais qualificados para atuar nos mais diferentes ramos, prestando serviços para a empresa. Se você tem interesse em ser fornecedor da Dallasanta, preencha o formulário abaixo e entraremos em contato.
            </p>

            <form action="<?php echo site_url('contact') ?>" method="post" class="form-row">
            	<fieldset>
                	<?php echo $this->form->get_form('contato_fornecedor') ?>
                </fieldset>
            </form>
        </div>
    </div>
</div>