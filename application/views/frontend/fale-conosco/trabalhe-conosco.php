<div class="container clearfix maintenance">
    <div class="box">
    	<div class="clearfix">
        	<h3 class="heading">
                Trabalhe Conosco
            </h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>

            <form action="<?php echo site_url('contact') ?>" method="post" enctype="multipart/form-data">
            	<fieldset>
                    <input type="hidden" name="meio_captacao" value="SITE_TRABALHE">

                    <div class="form-group">
                        <input type="text" name="field[nome]" value="" placeholder="Nome:" class="input" required area-required="true">
                        <input type="hidden" name="label[nome]" value="Nome">

                        <input type="email" name="field[email]" value="" placeholder="Email:" class="input" required area-required="true">
                        <input type="hidden" name="label[email]" value="E-mail">

                        <input type="text" name="field[telefone]" value="" placeholder="Telefone:" class="input telefone none" required area-required="true">
                        <input type="hidden" name="label[telefone]" value="Telefone">
                    </div>
                    <div class="form-group">
                        <input type="text" name="field[celular]" value="" placeholder="Celular:" class="input telefone" required area-required="true">
                        <input type="hidden" name="label[celular]" value="Celular">

                        <input type="text" name="field[cidade]" value="" placeholder="Cidade:" class="input" required area-required="true">
                        <input type="hidden" name="label[cidade]" value="Cidade">

                        <input type="text" name="field[estado]" value="" placeholder="Estado:" class="input none" required area-required="true">
                        <input type="hidden" name="label[estado]" value="Estado">
                    </div>
                    <div class="form-group">
                        <input type="text" name="field[bairro]" value="" placeholder="Bairro" class="input" required area-required="true">
                        <input type="hidden" name="label[bairro]" value="Bairro">

                        <input type="text" name="field[endereco]" value="" placeholder="Endereço" class="input" required area-required="true">
                        <input type="hidden" name="label[endereco]" value="Endereço">
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Anexar currículo</legend>
                    <div class="button-attachment">
                        <input type="text" name="curriculo" class="input" value="" placeholder="Arquivo..." readonly disabled>
                        <input type="hidden" name="label[curriculo]" value="Currículo">

                        <div>
                            <input type="file" name="curriculo">
                            <button type="button" class="button">
                                <span></span> Anexar
                            </button>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Disponibilidade</legend>
                    <div class="form-group">
                        <label>
                            <input type="radio" name="field[disponibilidade]" value="Imediata" checked>
                            <input type="hidden" name="label[disponibilidade]" value="Disponibilidade">
                            Imediata
                        </label>
                        <label>
                            <input type="radio" name="field[disponibilidade]" value="A combinar">
                            A combinar
                        </label>
                    </div>
                    <div class="clear"></div>
                    <div class="form-group" style="margin-bottom:10px;">
                        <textarea name="field[obs]" placeholder="Observações" class="textarea"></textarea>
                        <input type="hidden" name="label[obs]" value="Observações">
                    </div>
                </fieldset>

                <button type="submit" class="btn button" data-loading-text="Enviando...">
                    <span></span>Enviar
                </button>
            </form>
        </div>
    </div>
</div>