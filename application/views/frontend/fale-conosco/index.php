<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
        <!-- start contact -->
        <div class="contact clearfix">
            <a href="#modal-indicar-para-amigo" class="fancybox indicate">
                INDICAR P/ UM AMIGO
            </a>

            <div style="display: none;">
                <div id="modal-indicar-para-amigo" class="modal-default">
                    <h1>Indicar para um amigo</h1>
                    <p></p>

                    <form method="post" action="<?php echo site_url('fale_conosco/indicar') ?>" class="form-ajax">
                        <?php echo $this->form->get_form('imovel_indicar_para_amigo') ?>
                    </form>
                </div>
            </div>

        	<h3 class="heading">FALE CONOSCO</h3>
            <ul class="clearfix">
            	<li class="first">
                    <a href="<?php echo site_url('area-do-cliente') ?>">
                    	<h4>Sou um cliente dallasanta</h4>
                        <p>Faça seu login e tenha acesso a serviços exclusivos.</p>
                    </a>
                </li>
                <li class="second">
                    <a href="<?php echo site_url(VIEW_DIR.'/trabalhe-conosco') ?>">
                    	<h4>Quero trabalhar na dallasanta</h4>
                        <p>Mande seu currículo e confira as vagas disponíveis.</p>
                    </a>
                </li>
                <li class="third">
                    <a href="<?php echo site_url('venda-seu-imovel') ?>">
                    	<h4>Quero oferecer um imóvel</h4>
                        <p>Envie maiores detalhes do imóvel à Dallasanta.</p>
                    </a>
                </li>
                <li class="fourth">
                    <a href="<?php echo site_url(VIEW_DIR.'/alugar-imovel') ?>">
                    	<h4>Quero alugar um Dallasanta</h4>
                        <p>Informe o imóvel que você deseja e um corretor entrará em contato.</p>
                    </a>
                </li>
                <!-- <li class="fifth">
                    <a href="<?php echo site_url(VIEW_DIR.'/assessoria-imprensa') ?>">
                    	<h4>Quero falar com a assessoria de imprensa</h4>
                        <p>Informe alguns dados e deixe sua mensagem.</p>
                    </a>
                </li> -->
                <li class="sixth">
                    <a href="<?php echo site_url(VIEW_DIR.'/fornecedor') ?>">
                    	<h4>Quero ser um fornecedor</h4>
                        <p>Ofereça seus serviços ou produtos ao nosso departamento de compras.</p>
                    </a>
                </li>
                <li class="seventh">
                    <a href="<?php echo site_url(VIEW_DIR.'/manutencao') ?>">
                    	<h4>Manutenção</h4>
                        <p>Deixe sua mensagem.</p>
                    </a>
                </li>
                <li class="eighth">
                    <!--<a target="new" href="http://2vialocacao.suprisoft.com.br/2viaAlug.ASP?SIGLA=DSF&ORIGEM=www.dallasanta.com.br&EMPRESA=dallasanta&IMAGEMORIGEM=http://www.dallasanta.com.br/imagens/logodsf.jpg">
                    	<h4>2ª via doc de aluguel</h4>
                        <p>Lorem ipsum dolor magnar tad.</p>
                    </a>-->
					<a href="<?php echo site_url(VIEW_DIR.'/segunda-via')?>">
                    	<h4>2ª via doc de aluguel</h4>
                        <p></p>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end contact -->
        <!-- start social media -->
        <div class="social-media clearfix">
        	<h4>redes sociais</h4>
            <p>A Dallasanta acompanha as mudanças do mundo para estar perto de você de forma inovadora e moderna. Clique em um dos itens abaixo e nos encontre nas redes sociais.</p>
            <ul>
            	<li><a target="_blank" href="https://www.facebook.com/pages/Dallasanta/1432576087007891?fref=ts" title="facebook">facebook</a></li>
                <li><a target="_blank" href="https://www.linkedin.com/company/4790021?trk=tyah&trkInfo=idx%3A2-1-3%2CtarId%3A1424786013800%2Ctas%3Adallasanta" title="linkedin">linkedin</a></li>
                <li><a target="_blank" href="https://twitter.com/Dallasanta_Emp" title="twitter">twitter</a></li>
                <li><a target="_blank" href="https://plus.google.com/+DallasantaBR" title="gplus">gplus</a></li>
                <li><a target="_blank" href="https://www.youtube.com/channel/UCxfZLSZRmMHrzY5xfh_7wEA" title="youtube">youtube</a></li>
                <li><a target="_blank" href="https://pt.foursquare.com/v/dallasanta-empreendimentos-e-incorpora%C3%A7%C3%B5es/5433fd2e498e92eb4a788e1b" title="save">save</a></li>
                <li><a target="_blank" href="https://www.pinterest.com/dallasantaempre/" title="pinterest">pinterest</a></li>
            </ul>
        </div>
        <!-- end social media -->
        <!-- start location -->
        <div class="location">
            <h4>localização</h4>
            <address>Rua Voluntários da Pátria, 2822 - 3º Andar <span>Bairro Floresta • Porto Alegre-RS • CEP 90.230-010</span></address>
            <!-- start map -->
            <div class="map" style="float:left; margin-right: 50px;">
                <!-- <a href="#" title="map"><img src="<?php echo assets_url('img/img_map.jpg'); ?>" alt="map" /></a> -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.969679342115!2d-51.209942100000006!3d-30.009027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95197990bdb56663%3A0x14a3fc0226e1f616!2sR.+Volunt%C3%A1rios+da+P%C3%A1tria%2C+2822+-+Centro+Hist%C3%B3rico%2C+Porto+Alegre+-+RS%2C+90030-001!5e0!3m2!1spt-BR!2sbr!4v1436805716094" width="500" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="map" style="float:left;">
                <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1spt-BR!2sbr!4v1445016504366!6m8!1m7!1stNrHabS-h6yGJS0eslZL3w!2m2!1d-30.00926066899729!2d-51.20972486042309!3f343.5235352562577!4f5.685363381220526!5f0.7820865974627469" width="500" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

            <!-- end map -->
        </div>
        <!-- end location -->
    </div>
    <!-- end box -->
</div>
<!-- end container -->
