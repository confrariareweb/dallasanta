<div class="container clearfix">
    <div class="box">
        <div class="contact-us clearfix">
        	<div class="press-office">
            	<h3 class="heading">assessoria de imprensa</h3>
                <p>Aqui você encontra materiais exclusivos e muita informação sobre a Dallasanta.</p>
                <ul>
                	<li><a href="#" title="Manual de identidade visual">Manual de identidade visual</a></li>
                    <li><a href="#" title="logotipo dallasanta - versão horizontal">logotipo dallasanta - versão horizontal</a></li>
                    <li><a href="#" title="logotipo dallasanta - versão vertical">logotipo dallasanta - versão vertical</a></li>
                </ul>
                <span>
                    Não encontrou o que procurava? <a href="<?php echo site_url('fale-conosco') ?>">Clique aqui e fale conosco.</a>
                </span>
            </div>

            <div class="contact-form">
            	<h3 class="heading">
                    Fale conosco
                </h3>

            	<form action="<?php echo site_url('contact') ?>" method="post">
                    <?php echo $this->form->get_form('contato_assessoria') ?>
                </form>
            </div>
        </div>
    </div>
</div>