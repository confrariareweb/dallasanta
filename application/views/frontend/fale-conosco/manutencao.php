<div class="container clearfix">
    <div class="box">
    	<div class="maintenance clearfix">
        	<h3 class="heading">Manutenção</h3>

            <form method="post" action="<?php echo site_url('contact') ?>" enctype="multipart/form-data">
            	<h4>DADOS DO SOLICITANTE</h4>
            	<div class="row clearfix">
                    <input type="hidden" name="meio_captacao" value="SITE_MANUTENCAO">

                	<input type="text" name="field[nome]" placeholder="Nome:" class="input" required>
                    <input type="hidden" name="label[nome]" value="Nome">

                    <input type="email" name="field[email]" placeholder="E-mail:" class="input" required>
                    <input type="hidden" name="label[email]" value="E-mail">

                    <input type="text" name="field[endereco]" placeholder="Endereço:" class="input none" required>
                    <input type="hidden" name="label[endereco]" value="Endereço">

                    <input type="text" name="field[bairro]" placeholder="Bairro:" class="input" required>
                    <input type="hidden" name="label[bairro]" value="Bairro">

                    <input type="text" name="field[telefone]" placeholder="Telefone:" class="input telefone" required>
                    <input type="hidden" name="label[telefone]" value="Telefone">

                    <input type="text" name="field[celular]" placeholder="Celular:" class="input telefone none" required>
                    <input type="hidden" name="label[celular]" value="Celular">
                </div>

                <div class="row clearfix">
                	<span>Selecione os itens que necessitam de manutenção:</span>
                    <label>
                        <input name="field[itens][]" value="Elétrica" type="checkbox" class="checkbox">
                        Elétrica
                    </label>
                    <label>
                        <input name="field[itens][]" value="Esgoto" type="checkbox" class="checkbox">
                        Esgoto
                    </label>
                    <label>
                        <input name="field[itens][]" value="Hidráulica" type="checkbox" class="checkbox">
                        Hidráulica
                    </label>
                    <label>
                        <input name="field[itens][]" value="Telhado" type="checkbox" class="checkbox">
                        Telhado
                    </label>
                    <div class="clear"></div>

                    <input type="text" name="field[outros_itens]" placeholder="Outros" class="input none">
                    <input type="hidden" name="label[itens]" value="Itens">
                    <input type="hidden" name="label[outros_itens]" value="Outros itens">
                </div>

                <div class="row clearfix">
                    <div class="col1">
                        <h4>MAIS INFORMAÇÕES</h4>
                        <textarea name="field[mensagem]" rows="2" cols="2" class="textarea" placeholder="Mensagem" required></textarea>
                        <input type="hidden" name="label[mensagem]" value="Mensagem">
                    </div>
                    <div class="col2">
                    	<h4>Anexar documentos/arquivos</h4>
                        <div class="button-attachment">
                            <input type="text" name="" class="input" value="" placeholder="Arquivo..." readonly disabled>
                            <div>
                                <input type="file" name="documentos">
                                <input type="hidden" name="label[documentos]" value="Documentos">
                                <button type="button" class="button">
                                    <span></span> Anexar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row2">
                 	<h4>Digite o código abaixo</h4>
                    <?php echo get_captcha('SITE_MANUTENCAO') ?>
                    <button type="submit" class="btn button" data-loading-text="Enviando...">
                        <span></span>Enviar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>