<div class="container clearfix">
    <!-- start box -->
    <div class="box">
        <!-- start property listing -->
        <div class="property-listing clearfix">
            <h3>
                <?php
                    $get_imoveis = $_GET['imoveis'];
                    sort($get_imoveis);
                    echo 'COMPARANDO IMÓVEIS <span class="cod-imovel-hover">', implode('</span>, <span class="cod-imovel-hover">', $get_imoveis), '</span>';
                ?>
            </h3>

            <div class="property-listing">
                <ul class="row">
                <?php
                $qry_string = $_SERVER['QUERY_STRING'];
                parse_str($qry_string, $filtro_atual);

                foreach ($imoveis as $i => $imovel):
                    if (!isset($this->session->userdata['area_restrita'])):
                        $fav_title = 'Faça login para favoritar este imóvel';
                        $fav_class = 'disabled';
                        $fav_href = 'javascript:;';
                    else:
                        $fav_title = 'Adicionar aos meus favoritos';
                        $fav_class = NULL;
                        $fav_href = site_url('area-do-cliente/favorito/'.$imovel->id);

                        if (isset($imoveis_favoritos)) {
                            if (in_array($imovel->id, $imoveis_favoritos)) {
                                $fav_class = ' active';
                                $fav_title = 'Remover dos meus favoritos';
                            }
                        }
                    endif;
                    ?>

                    <li id="imovel-compara-<?php echo $imovel->id ?>" class="col-md-3 col-sm-6">
                        <div>
                            <?php
                                $http_query = $filtro_atual;
                                unset($http_query['imoveis'][array_search($imovel->id, $http_query['imoveis'])]);
                            ?>
                            <a href="<?php echo site_url('imoveis/comparar/?'.http_build_query($http_query)) ?>" class="remove-imovel"></a>

                            <div class="imgb">
                                <a href="<?php echo site_url('imoveis/detalhes/'.$imovel->id) ?>">
                                    <img src="<?php echo (!$imovel->fotos) ? '' : sprintf($this->model_imovel->uri_images, 'crop', 270, 220, $imovel->fotos[0]->key, $imovel->fotos[0]->etag) ?>" alt="<?php echo $imovel->titulo ?>">
                                </a>
                            </div>
                            <div class="txtb">
                                <h4><?php echo $imovel->titulo ?></h4>
                                <span><?php echo $imovel->subtitulo ?></span>
                                <div class="clearfix caracteristicas">
                                    <span>Características:</span>
                                    <ul>
                                        <li>
                                            Cidade:
                                            <b>
                                                <?php echo sprintf('%s - %s', $imovel->endereco->city, $imovel->endereco->state) ?>
                                            </b>
                                        </li>
                                        <li>
                                            Tipo:
                                            <b>
                                                <?php echo $imovel->categoria ?>
                                            </b>
                                        </li>
<!--                                        <li>-->
<!--                                            Dormitórios:-->
<!--                                            <b>-->
<!--                                                --><?php //echo ($imovel->dormitorios) ? $imovel->dormitorios : 'consulte'; ?>
<!--                                            </b>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            Vagas de garagem:-->
<!--                                            <b>-->
<!--                                                --><?php //echo ($imovel->vagas) ? $imovel->vagas : 'consulte'; ?>
<!--                                            </b>-->
<!--                                        </li>-->
                                        <li>
                                            Área:
                                            <b>
                                                <?php echo ($imovel->area_privativa) ? sprintf('%sm²', str_replace(',00', '', number_format($imovel->area_privativa, 2, ',', '.'))) : 'consulte'; ?>
                                            </b>
                                        </li>
                                        <li>
<!--                                            --><?php
//                                                echo ($imovel->tipo_imovel[0] == 'Aluguel') ? 'Aluguel:' : 'Valor:';
//                                            ?>
<!--                                            --><?php //if($imovel->tipo_imovel[0] == 'Aluguel'){ ?>
<!--                                                <b>-->
<!--                                                    --><?php //echo sprintf('R$ %s', number_format($imovel->preco_aluguel->value, 2, ',', '.')) ?>
<!--                                                </b>-->
<!--                                            --><?php //}else{ ?>
<!--                                                <b>-->
<!--                                                    --><?php //echo sprintf('R$ %s', number_format($imovel->precos->value, 2, ',', '.')) ?>
<!--                                                </b>-->
<!--                                            --><?php //} ?>

                                            <?php
                                                echo ($this->uri->segment(3) == 'aluguel') ? 'Aluguel: ' : 'Valor: ';
                                                if($this->uri->segment(3) == 'aluguel'){

                                                    $price = $imovel->preco_aluguel->value;
                                                }else{
                                                    $price = $imovel->preco_venda->value;
                                                }
                                                if ($imovel->showprice == 'Sim') {
                                                    echo '<b>'.sprintf('R$ %s', str_replace(',00', '', number_format($price, 2, ',', '.'))).'</b>';
                                                }else{
                                                    if($this->uri->segment(3) == 'Aluguel'){
                                                        echo '<b>Consulte o aluguel</b>';
                                                    }else{
                                                        echo '<b>Consulte o valor</b>';
                                                    }
                                                }
                                            ?>

                                        </li>
                                    </ul>
                                </div>
                                <a href="<?php echo site_url('imoveis/detalhes/'.$imovel->id) ?>" class="button">
                                    <span></span>VER DETALHES
                                </a>
                                <a href="<?php echo $fav_href ?>" title="<?php echo $fav_title ?>" class="rank <?php echo $fav_class ?>"></a>
                            </div>
                        </div>
                    </li>

                    <?php
                        if (($i+1)%4 === 0)
                            echo '<b class="clear"></b>';
                    endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- end box -->
</div>
<!-- end container -->