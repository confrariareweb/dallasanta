<?php

if (!isset($this->session->userdata['area_restrita'])):
    $fav_title = 'Faça login para favoritar este imóvel';
    //$fav_class = 'disabled';
    $fav_href = 'javascript:;';
else:
    $fav_title = 'Adicionar aos meus favoritos';
    $fav_class = NULL;
    $fav_href = site_url('area-do-cliente/favorito/'.$imovel->id);

    if (isset($imoveis_favoritos)) {
        if (in_array($imovel->id, $imoveis_favoritos)) {
            $fav_class = ' active';
            $fav_title = 'Remover dos meus favoritos';
        }
    }
endif;

?>

<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the current printer page size */
        margin: 7mm;  /* this affects the margin in the printer settings */
    }
    body {
        background-color:#FFFFFF;
        border: 0;
        margin: 0px;  /* the margin on the content before printing */
    }

    form,
    .top-bar,
    .header-bottom,
    .breadcrumb,
    .enterprises .col1 span,
    .download,
    .rightcol,
    .newsletter-box,
    .clients,
    .footer{
        display: none;
    }

    a{
        display: block;
    }
    a[href]:after {
        content: none!important;
    }

    .contact-info{
        float: left;
    }
    .contact-info li:nth-child(n+3){
        display: none;
    }

    .leftcol{
        width: 100%;
        float: none;
    }
    .enterprises-info{
        float: left;
        margin-left: 30px;
    }
    .enterprises-info .row{
        margin: 0;
        color: #d5190b;
    }
    .enterprises-info .price{
        float: none;
        color: #d5190b;
    }
    .enterprises-info .row strong{
        float: left;
        margin-right: 15px;
        color: #d5190b;
    }

    .location2{
        page-break-before: always;
        margin: 0;
    }
</style>

<?php if($this->uri->segment(1) != 'empreendimentos'){ ?>
<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
        <div class="clearfix">
            <!-- start leftcol -->
            <div class="leftcol">
                <!-- start enterprises -->
                <div class="enterprises clearfix">
                    <h4>
                        <?php echo $imovel->titulo ?> | <?php echo $imovel->subtitulo ?>
                    </h4>

                    <div class="clearfix">
                        <div class="col1">
                            <div class="imgb">
                                <?php if($imovel->tour_virtual){ ?>
                                    <iframe src="<?php echo $imovel->tour_virtual ?>" width="484" height="391" frameborder="0" style="border:0" allowfullscreen></iframe>
                                <?php }else{
                                    if ($imovel->fotos):
                                        $foto_principal = $imovel->fotos[0];
                                    elseif ($imovel->fotos_empreendimento):
                                        $foto_principal = $imovel->fotos_empreendimento[0];
                                    else:
                                        $foto_principal = null;
                                    endif;
                                }
                                 ?>

                                <?php if ($foto_principal): ?>
                                    <a href="<?php echo sprintf($this->model_imovel->uri_images_all,$foto_principal->key) ?>" class="fancybox" rel="gallery">
                                        <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 484, 391, $foto_principal->key, $foto_principal->etag) ?>" alt="">
                                    </a>
                                <?php endif ?>

<!--                                --><?php //if ($imovel->tour_virtual): ?>
<!--                                    <a target="new" href="--><?php //echo $imovel->tour_virtual ?><!--" title="Tour Virtual" class="tour-virtual">tour virtual</a>-->
<!--                                --><?php //endif ?>
                            </div>
                            <span>Obs.: Imagem ilustrativa</span>
                        </div>
                        <div class="enterprises-info">
                            <div class="row clearfix">
                                <span class="price">
                                    <?php
                                        if($tipo_busca == 'venda'){
                                            $price = $imovel->preco_venda->value;
                                        }else{
                                            $price = $imovel->preco_aluguel->value;
                                        }

                                        if ($imovel->showprice == 'Sim') {
                                            echo sprintf('R$ %s', str_replace(',00', '', number_format($price, 2, ',', '.')));
                                        }else{
                                            if($tipo_busca){
                                                echo 'Consulte o valor';
                                            }else{
                                                echo 'Consulte o aluguel';
                                            }
                                        }
                                    ?>
                                </span>
                                <strong>
                                    <?php
                                        if ($tipo_busca == 'venda'):
                                            echo 'Valor:';
                                        else:
                                            echo 'Aluguel:';
                                        endif;
                                    ?>
                                </strong>
                            </div>
                            <dl class="clearfix">
                                <dt>CÓDIGO:</dt>
                                <dd><?php echo $imovel->id ?></dd>

                                <dt>TIPO:</dt>
                                <dd><?php echo $imovel->categoria ?></dd>

                                <dt>CIDADE:</dt>
                                <dd><?php echo sprintf('%s - %s', $imovel->endereco->city, $imovel->endereco->state) ?></dd>

                                <dt>BAIRRO:</dt>
                                <dd><?php echo $imovel->endereco->district ?></dd>

                                <dt>ENDEREÇO:</dt>
                                <dd><?php echo sprintf('%s %s', $imovel->endereco->placeType, $imovel->endereco->place) ?></dd>

                                <?php if($imovel->endereco->complement){ ?>
                                    <dt>COMPLEMENTO:</dt>
                                    <dd><?php echo $imovel->endereco->complement; ?></dd>
                                <?php } ?>
                                <?php if($imovel->dormitorios){ ?>
                                    <dt>dormitórios:</dt>
                                    <dd><?php echo ($imovel->dormitorios) ? $imovel->dormitorios : 'Consulte'; ?></dd>
                                <?php } ?>

                                <?php if($imovel->vagas){ ?>
                                    <dt>vagas:</dt>
                                    <dd><?php echo ($imovel->vagas) ? $imovel->vagas : 'Consulte'; ?></dd>
                                <?php } ?>
                                <dt>área:</dt>
                                <dd class="area"><?php echo ($imovel->area_privativa) ? sprintf('%sm²', str_replace(',00', '', number_format($imovel->area_privativa, 2, ',', '.'))) : 'CONSULTE'; ?></dd>

                                <dt>situação:</dt>
                                <dd>
                                    <?php
                                        if (isset($this->model_imovel->status_pt[$imovel->status])):
                                            echo $this->model_imovel->status_pt[$imovel->status];
                                        else:
                                            echo $imovel->status;
                                        endif;
                                    ?>
                                </dd>

                                <dt>iptu:</dt>
                                <?php if($imovel->iptu != '0,00'){?>
                                    <dd><?php echo ($imovel->iptu) ? sprintf('R$ %s', number_format($imovel->iptu, 2, ',', '.')) : 'Consulte'; ?></dd>
                                <?php } ?>
                                <dt>condomínio:</dt>
                                <dd><?php echo ($imovel->condominio) ? sprintf('R$ %s', number_format($imovel->condominio->value, 2, ',', '.')) : 'Consulte'; ?></dd>
                            </dl>
                        </div>
                        <!-- start enterprises info -->
                    </div>

                    <div id="descricao-do-imovel">
                        <?php if ($imovel->descricao): ?>
                            <div class="property-description clearfix">
                                <h3 class="heading">DESCRIÇÃO DO IMÓVEL</h3>
                                <p>
                                    <?php echo nl2br(trim($imovel->descricao)) ?>
                                </p>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <!-- end enterprises -->

                <?php if ($imovel->caracteristicas_condominio): ?>
                <div class="featured clearfix">
                    <h3 class="heading">CARACTERÍSTICAS DO IMÓVEL</h3>
                    <?php $count = 0;
                        foreach ($imovel->caracteristicas_condominio as $carac){
                            $count++; ?>
                            <?php
                                if($count%3 == 1){
                                    echo '<ul class="carac_imovel">';
                                }
                            ?>
                            <li>
                                <?php echo $carac; ?>
                            </li>
                            <?php
                                if($count%3 == 0){
                                    echo '</ul>';
                                }
                            ?>
                    <?php } ?>
                </div>
                <?php endif; ?>
            </div>
            <!-- end leftcol -->

            <!-- start rightcol -->
            <div class="rightcol">
                <div class="icons-set clearfix">
                    <ul>
                        <li>
                            <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                        </li>
                        <?php if (isset($imovel->video)): ?>
                            <li>
                                <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                            </li>
                         <?php endif ?>
                        <li>
                            <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                        </li>
                        <li>
                            <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                        </li>
                    </ul>
                </div>

                <div class="clear"></div>
                <!-- start fillform -->
                <div class="fillform clearfix">
                    <h4>
                        Entre em contato<span>Nossa equipe está à sua disposição</span>
                    </h4>
                    <form method="post" action="<?php echo site_url('contact') ?>">
                        <?php echo $this->form->get_form('imovel_contato'); ?>

                        <!--
                        <div>
                            <input type="hidden" name="meio_captacao" value="SITE_IMOVEL_CONTATO">
                            <input type="text" name="Nome" placeholder="Nome:" class="input" required area-required="true">
                            <input type="email" name="E-mail" placeholder="E-mail:" class="input" required area-required="true">
                            <input type="text" name="Telefone" placeholder="Telefone:" class="input telefone" required area-required="true">
                            <textarea name="Mensagem" placeholder="Mensagem:" rows="2" cols="2"  class="textarea" required area-required="true"></textarea>
                            <button type="submit" class="btn button" data-loading-text="Enviando...">
                                <span></span>ENVIAR
                            </button>
                        </div>
                        -->
                    </form>
                </div>
                <!-- end fillform -->
                <!-- start contact info2 -->
                <div class="contact-info2">
                    <ul>
                        <li class="first">
                            <a href="javascript:window.print();">IMPRIMIR</a>
                        </li>
                        <li class="second">
                            <a href="#modal-enviar-por-email" class="fancybox">ENVIAR POR E-MAIL</a>
                        </li>
                        <li class="third">
                            <a href="#modal-indicar-para-amigo" class="fancybox">INDICAR P/ UM AMIGO</a>
                        </li>
                        <li class="fourth">
                            <a href="#modal-agendar-visita" class="fancybox">AGENDAR VISITA</a>
                        </li>
                        <li class="fifth">
                            <a href="#modal-ligamos-para-voce" class="fancybox">LIGAMOS PARA VOCÊ</a>
                        </li>
                        <li class="sixth">
                            <a href="#modal-fazer-proposta" class="fancybox">FAZER PROPOSTA</a>
                        </li>
                        <li class="seventh">
                            <a href="<?php echo $fav_href ?>" class="rank <?php echo $fav_class ?>" title="<?php echo $fav_title ?>"></a>
                        </li>
                    </ul>
                </div>
                <!-- end contact info2 -->

                <div class="hide">
                    <div id="modal-enviar-por-email" class="modal-default">
                        <h1>Enviar por e-mail</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_enviar_email') ?>
                        </form>
                    </div>

                    <div id="modal-indicar-para-amigo" class="modal-default">
                        <h1>Indicar para um amigo</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('fale_conosco/indicar') ?>">
                            <?php echo $this->form->get_form('imovel_indicar_para_amigo') ?>
                        </form>
                    </div>

                    <div id="modal-agendar-visita" class="modal-default">
                        <h1>Agendar visita</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_agendar_visita') ?>
                        </form>
                    </div>

                    <div id="modal-fazer-proposta" class="modal-default">
                        <h1>Fazer proposta</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_fazer_proposta') ?>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end rightcol -->
        </div>

        <!-- start video -->
        <?php if (isset($imovel->video)): ?>
            <div class="icons-set clearfix">
                <ul>
                    <li>
                        <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                    </li>
                    <?php if (isset($imovel->video)): ?>
                        <li>
                            <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                        </li>
                    <?php endif ?>
                    <li>
                        <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                    </li>
                    <li>
                        <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                    </li>
                </ul>
            </div>
            <div class="video clearfix" id="video-do-imovel">
                <h3 class="heading">VÍDEO DO IMÓVEL</h3>
                <div class="video-box clearfix">
                    <iframe width="100%" height="716" src="https://www.youtube.com/embed/<?php echo $imovel->video ?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <!-- <a href="#" title="play" class="play">Play</a> -->
                </div>
            </div>
        <?php endif ?>
        <!-- end video -->

        <!-- start property list -->
        <div class="icons-set clearfix">
            <ul>
                <li>
                    <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                </li>
                <?php if (isset($imovel->video)): ?>
                    <li>
                        <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                    </li>
                <?php endif ?>
                <li>
                    <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                </li>
                <li>
                    <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                </li>
            </ul>
        </div>
        <div class="property-list clearfix" id="fotos-do-imovel">
                       <?php if ($imovel->fotos): ?>
                <h3 class="heading">FOTOS DO IMÓVEL</h3>
                <ul class="clearfix">
                    <?php foreach ($imovel->fotos as $i => $foto): ?>
                    <li class="<?php echo ($i == 0) ? 'active' : ''; ?>">
                        <a href="<?php echo sprintf($this->model_imovel->uri_images_all,$foto->key) ?>" class="fancybox" rel="gallery">
                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 138, 115, $foto->key, $foto->etag) ?>" alt="">
                        </a>
                    	<!-- <a href="#" class="tour-virtual">Tour virtual</a> -->
                    </li>
                    <?php endforeach ?>
                </ul>
                <a href="<?php echo site_url('imoveis/download-fotos/'.$imovel->id) ?>" class="download download-fotos-imovel">DOWNLOAD DAS FOTOS</a>
            <?php endif ?>
        </div>
        <!-- end property list -->

        <!-- start property list -->
        <div class="property-list clearfix" id="fotos-do-empreendimento">
            <?php if ($imovel->fotos_empreendimento): ?>
                <h3 class="heading">
                    FOTOS DO EMPREENDIMENTO <?php echo ($imovel->empreendimento) ? ' - ' . $imovel->empreendimento->name : ''; ?>
                </h3>
                <ul class="clearfix">
                    <?php foreach ($imovel->fotos_empreendimento as $i => $foto): ?>
                    <li class="<?php echo ($i == 0) ? 'active' : ''; ?>">
                        <a href="<?php echo sprintf($this->model_imovel->uri_images_all,$foto->key) ?>" class="fancybox" rel="gallery">
                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 138, 115, $foto->key, $foto->etag) ?>" alt="">
                        </a>

                        <?php /*if ($imovel->tour_virtual): ?>
                            <a target="new" href="<?php echo $imovel->tour_virtual ?>" class="tour-virtual">
                                Tour virtual
                            </a>
                        <?php endif*/ ?>

                    </li>
                    <?php endforeach ?>
                </ul>
                <a href="<?php echo site_url('imoveis/download-fotos/'.$imovel->id.'/true') ?>" class="download download-fotos-imovel">
                    DOWNLOAD DAS FOTOS
                </a>
            <?php endif ?>
        </div>
        <!-- end property list -->
        <!-- start location2 -->
        <div class="icons-set clearfix">
            <ul>
                <li>
                    <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                </li>
                <?php if (isset($imovel->video)): ?>
                    <li>
                        <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                    </li>
                <?php endif ?>
                <li>
                    <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                </li>
                <li>
                    <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                </li>
            </ul>
        </div>
        <div class="location2 clearfix" id="localizacao-do-imovel">
        	<!-- start row -->
            <div class="row clearfix">
                <!-- start icons set -->
                <h3 class="heading">LOCALIZAÇÃO DO IMÓVEL</h3>
            </div>
            <!-- end row -->
            <?php if (isset($imovel->endereco->geolocation) && !empty($imovel->endereco->geolocation)): ?>
                <div class="map left">
                    <!-- <iframe src="https://www.google.com/maps/embed/v1/place?q=<?php echo $imovel->endereco->geolocation[1] .','. $imovel->endereco->geolocation[0] ?>&key=AIzaSyAEny5PWNO7nGU0JMH45K-PzyAu8AKxtcg" width="545" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> -->

                    <div id="map-canvas" style="width:545px; height:400px;"></div>
                    <script type="text/javascript">
                        init_google_maps(<?php echo $imovel->endereco->geolocation[1] .','. $imovel->endereco->geolocation[0] ?>);
                    </script>
                </div>
            <?php endif ?>

            <?php if ($imovel->street_view): ?>
                <div class="map right">
                    <!--
                    <div id="map-canvas-streetview" style="width:545px; height:400px;"></div>
                    <script type="text/javascript">
                        init_google_maps(<?php echo $imovel->endereco->geolocation[1] .','. $imovel->endereco->geolocation[0] ?>, true);
                    </script>
                    -->

                    <iframe src="<?php echo $imovel->street_view ?>" width="545" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            <?php endif ?>
        </div>
        <!-- end location2 -->

        <?php if ($this->uri->segments[1] == 'empreendimentos'): ?>
            <?php if (isset($imovel->empreendimento_imoveis)): ?>
                <div class="property-listing clearfix">
                    <h3>IMÓVEIS DISPONÍVEIS NO EMPREENDIMENTO</h3>

                    <?php if (array_sum(array_map("count", $imovel->empreendimento_imoveis)) > 4): ?>
                        <a href="#" title="prev" class="prev" id="imoveis-empreendimento-nav-prev">prev</a>
                        <a href="#" title="next" class="next" id="imoveis-empreendimento-nav-next">next</a>
                    <?php endif; ?>

                    <div class="cycle-slideshow property-listing"
                         data-allow-wrap="false",
                         data-cycle-fx="scrollHorz"
                         data-cycle-timeout="0"
                         data-cycle-prev="#imoveis-empreendimento-nav-prev"
                         data-cycle-next="#imoveis-empreendimento-nav-next"
                         data-cycle-slides="> ul">

                        <?php foreach ($imovel->empreendimento_imoveis as $im): ?>
                            <ul class="row">
                                <?php
                                foreach ($im as $empreendimento):
                                    $empreendimento->tipo_view = 'inline';
                                    echo '
                                        <li class="col-md-3 col-sm-6">
                                            '. $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $empreendimento), TRUE) .'
                                        </li>
                                    ';
                                endforeach;
                                ?>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
        <?php else: ?>
            <?php if (isset($imovel->imoveis_semelhantes) && !empty($imovel->imoveis_semelhantes)): ?>
                <div class="property-listing clearfix">
                    <h3>IMÓVEIS SEMELHANTES</h3>

                    <?php if (array_sum(array_map("count", $imovel->imoveis_semelhantes)) > 4): ?>
                        <a href="#" title="prev" class="prev" id="imoveis-semelhantes-nav-prev">prev</a>
                        <a href="#" title="next" class="next" id="imoveis-semelhantes-nav-next">next</a>
                    <?php endif; ?>

                    <div class="cycle-slideshow property-listing"
                         data-allow-wrap="false",
                         data-cycle-fx="scrollHorz"
                         data-cycle-timeout="0"
                         data-cycle-prev="#imoveis-semelhantes-nav-prev"
                         data-cycle-next="#imoveis-semelhantes-nav-next"
                         data-cycle-slides="> ul">

                        <?php foreach ($imovel->imoveis_semelhantes as $im): ?>
                            <ul class="row">
                                <?php
                                foreach ($im as $imovel):
                                    $imovel->tipo_view = 'inline';
                                    echo '
                                        <li class="col-md-3 col-sm-6">
                                            '. $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $imovel), TRUE) .'
                                        </li>
                                    ';
                                endforeach;
                                ?>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <!-- end box -->
</div>
<!-- end container -->
<?php }else{ ?>

<!-- start container -->
<div class="container clearfix">
    <!-- start box -->
    <div class="box">
        <div class="clearfix">
            <!-- start leftcol -->
            <div class="leftcol">
                <!-- start enterprises -->
                <div class="enterprises clearfix">
                    <h4>
                        <?php echo $imovel->name_empreendimento ?> | <?php echo $imovel->titulo ?> | <?php echo $imovel->subtitulo ?>
                    </h4>
                    <div class="clearfix">
                        <div class="col1">
                            <div class="imgb" style="width:860px;">

                                <?php if($imovel->tour_virtual){ ?>
                                    <iframe src="<?php echo $imovel->tour_virtual ?>" width="860" height="391" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    <?php }else {
                                        if ($imovel->fotos):
                                            $foto_principal = $imovel->fotos[0];
                                        elseif ($imovel->fotos_empreendimento):
                                            $foto_principal = $imovel->fotos_empreendimento[0];
                                        else:
                                            $foto_principal = null;
                                        endif;
                                    }
                                    ?>

                                    <?php if ($foto_principal): ?>
                                        <a href="<?php echo sprintf($this->model_imovel->uri_images_all,$foto_principal->key) ?>" class="fancybox" rel="gallery">
                                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 860, 540, $foto_principal->key, $foto_principal->etag) ?>" alt="">
                                        </a>
                                    <?php endif ?>

<!--                                --><?php //if ($imovel->tour_virtual): ?>
<!--                                    <a target="new" href="--><?php //echo $imovel->tour_virtual ?><!--" title="Tour Virtual" class="tour-virtual">tour virtual</a>-->
<!--                                --><?php //endif ?>
                            </div>
                            <span>Obs.: Imagem ilustrativa</span>
                        </div>

                    </div>

                    <div id="descricao-do-imovel">
                        <?php if ($imovel->descricao_empreendimento){ ?>
                            <div class="property-description clearfix">
                                <h3 class="heading">DESCRIÇÃO DO EMPREENDIMENTO</h3>
                                <p>
                                    <?php echo nl2br(trim($imovel->descricao_empreendimento)) ?>
                                </p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- end enterprises -->
                <?php if ($imovel->caracteristicas_condominio){?>

                <div class="featured clearfix">
                    <h3 class="heading">CARACTERÍSTICAS DO EMPREENDIMENTO</h3>
                    <?php $count = 0;
                        foreach ($imovel->caracteristicas_condominio as $carac){
                         $count++; ?>
                            <?php
                                if($count%3 == 1){
                                    echo '<ul class="carac_imovel">';
                                }
                            ?>
                            <li>
                                <?php echo $carac; ?>
                            </li>
                            <?php
                                if($count%3 == 0){
                                    echo '</ul>';
                                }
                            ?>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
            <!-- end leftcol -->

            <!-- start rightcol -->
            <div class="rightcol">
                <div class="icons-set clearfix">
                    <ul>
                        <li>
                            <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                        </li>
                        <?php if (isset($imovel->video)): ?>
                            <li>
                                <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                            </li>
                         <?php endif ?>
                        <li>
                            <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                        </li>
                        <li>
                            <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                        </li>
                    </ul>
                </div>

                <div class="clear"></div>
                <!-- start fillform -->
                <div class="fillform clearfix">
                    <h4>
                        Entre em contato<span>Nossa equipe está à sua disposição</span>
                    </h4>
                    <form method="post" action="<?php echo site_url('contact') ?>">
                        <?php echo $this->form->get_form('imovel_contato'); ?>

                        <!--
                        <div>
                            <input type="hidden" name="meio_captacao" value="SITE_IMOVEL_CONTATO">
                            <input type="text" name="Nome" placeholder="Nome:" class="input" required area-required="true">
                            <input type="email" name="E-mail" placeholder="E-mail:" class="input" required area-required="true">
                            <input type="text" name="Telefone" placeholder="Telefone:" class="input telefone" required area-required="true">
                            <textarea name="Mensagem" placeholder="Mensagem:" rows="2" cols="2"  class="textarea" required area-required="true"></textarea>
                            <button type="submit" class="btn button" data-loading-text="Enviando...">
                                <span></span>ENVIAR
                            </button>
                        </div>
                        -->
                    </form>
                </div>
                <!-- end fillform -->
                <!-- start contact info2 -->
                <div class="contact-info2">
                    <ul>
                        <li class="first">
                            <a href="javascript:window.print();">IMPRIMIR</a>
                        </li>
                        <li class="second">
                            <a href="#modal-enviar-por-email" class="fancybox">ENVIAR POR E-MAIL</a>
                        </li>
                        <li class="third">
                            <a href="#modal-indicar-para-amigo" class="fancybox">INDICAR P/ UM AMIGO</a>
                        </li>
                        <li class="fourth">
                            <a href="#modal-agendar-visita" class="fancybox">AGENDAR VISITA</a>
                        </li>
                        <li class="fifth">
                            <a href="#modal-ligamos-para-voce" class="fancybox">LIGAMOS PARA VOCÊ</a>
                        </li>
                        <li class="sixth">
                            <a href="#modal-fazer-proposta" class="fancybox">FAZER PROPOSTA</a>
                        </li>
                        <li class="seventh">
                            <a href="<?php echo $fav_href ?>" class="rank <?php echo $fav_class ?>" title="<?php echo $fav_title ?>"></a>
                        </li>
                    </ul>
                </div>
                
                
                <!-- end contact info2 -->

                <div class="hide">
                    
                    
                    <div id="cadastrodeclientes" class="modal-default">
                        <h1>Central do cliente</h1>
     
                        <!-- start container -->
                        <div class="container clearfix" style="width:360px;">
                                <!-- start box -->
                            <div class="box">
                                <!-- start register -->
                                <div class="maintenance register clearfix">
                                    <form class="form-ajax" method="post" action="<?php echo site_url('area-do-cliente/login') ?>">
                                        <div class="row3 clearfix">
                                            <h4>LOGIN</h4>
                                            <input type="email" name="email" value="" class="input" placeholder="E-mail" required>
                                            <br>
                                            <input type="password" name="senha" value="" class="input none" placeholder="Senha" required maxlength="30">
                                            <br>
                                            <button type="submit" class="btn button" data-loading-text="Entrando...">
                                                <span></span>Entrar
                                            </button>
                                        </div>
                                    </form>

                                    <form method="post" action="<?php echo site_url('area-do-cliente/cadastro') ?>" class="form-ajax">
                                        <div class="row row4 clearfix">
                                            <!--div class="social2">
                                                <a href="<?php echo site_url('area-do-cliente/facebook_login') ?>" title="LINKEDIN" class="linkedin">LOGIN COM LINKEDIN<span></span></a>
                                                <a href="<?php echo site_url('area-do-cliente/google_login') ?>" title="G+" class="google">LOGIN COM GOOGLE<span></span></a>
                                                <a href="<?php echo site_url('area-do-cliente/facebook_login') ?>" title="FACEBOOK" class="facebook">LOGIN COM FACEBOOK<span></span></a>
                                            </div-->
                                                <h4>CADASTRO</h4>
                                        </div>
                                        <div class="row clearfix">
                                            <input type="text" name="nome" value="" class="input" placeholder="Nome" maxlength="80" required>
                                            <br>
                                            <input type="text" name="sobrenome" value="" class="input" placeholder="Sobrenome" maxlength="80" required>
                                            <br>
                                            <!-- <input type="text" name="field" value="" class="input none" placeholder="E-mail"> -->
                                            <input type="text" name="telefone" value="" class="input none telefone" placeholder="Telefone" maxlength="15" required>
                                            <br>
                                            <input type="email" name="email" value="" class="input" placeholder="E-mail" maxlength="150" required>
                                            <br>
                                            <input type="password" name="senha" value="" class="input none" placeholder="Senha" maxlength="25" required>
                                        </div>
                                        <div class="row2 clearfix">
                                            <!--h4>Digite o código abaixo</h4-->

                                            <?php //echo get_captcha('SITE_CADASTRO_CLIENTE'); ?>

                                            <button type="submit" class="btn button" data-loading-text="Enviando...">
                                                <span></span>
                                                Enviar
                                            </button>
                                        </div>
                                    </form>

                                </div>
                                <!-- end register -->
                            </div>
                            <!-- end box -->
                        </div>
                        <!-- end container -->

                    </div>
                    
                    
                    
                    <div id="modal-enviar-por-email" class="modal-default">
                        <h1>Enviar por e-mail</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_enviar_email') ?>
                        </form>
                    </div>

                    <div id="modal-indicar-para-amigo" class="modal-default">
                        <h1>Indicar para um amigo</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('fale_conosco/indicar') ?>">
                            <?php echo $this->form->get_form('imovel_indicar_para_amigo') ?>
                        </form>
                    </div>

                    <div id="modal-agendar-visita" class="modal-default">
                        <h1>Agendar visita</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_agendar_visita') ?>
                        </form>
                    </div>

                    <div id="modal-fazer-proposta" class="modal-default">
                        <h1>Fazer proposta</h1>
                        <p></p>

                        <form method="post" action="<?php echo site_url('contact') ?>">
                            <?php echo $this->form->get_form('imovel_fazer_proposta') ?>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end rightcol -->
        </div>

        <!-- start video -->
        <?php if (isset($imovel->video)): ?>
            <div class="icons-set clearfix">
                <ul>
                    <li>
                        <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                    </li>
                    <?php if (isset($imovel->video)): ?>
                        <li>
                            <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                        </li>
                    <?php endif ?>
                    <li>
                        <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                    </li>
                    <li>
                        <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                    </li>
                </ul>
            </div>
            <div class="video clearfix" id="video-do-imovel">
                <h3 class="heading">VÍDEO DO EMPREENDIMENTO</h3>
                <div class="video-box clearfix">
                    <iframe width="100%" height="716" src="https://www.youtube.com/embed/<?php echo $imovel->video ?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <!-- <a href="#" title="play" class="play">Play</a> -->
                </div>
            </div>
        <?php endif ?>
        <!-- end video -->

        <!-- start property list -->
        <div class="property-list clearfix" id="fotos-do-imovel">
                        <?php if ($imovel->fotos): ?>
                <h3 class="heading">FOTOS DO IMÓVEL</h3>
                <ul class="clearfix">
                    <?php foreach ($imovel->fotos as $i => $foto): ?>
                    <li class="<?php echo ($i == 0) ? 'active' : ''; ?>">
                        <a href="<?php echo sprintf($this->model_imovel->uri_images, 'outer', 551, 400, $foto->key, $foto->etag) ?>" class="fancybox" rel="gallery">
                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 138, 115, $foto->key, $foto->etag) ?>" alt="">
                        </a>
                        <!-- <a href="#" class="tour-virtual">Tour virtual</a> -->
                    </li>
                    <?php endforeach ?>
                </ul>
                <a href="<?php echo site_url('imoveis/download-fotos/'.$imovel->id) ?>" class="download download-fotos-imovel">DOWNLOAD DAS FOTOS</a>
            <?php endif ?>
        </div>
        <!-- end property list -->

        <!-- start property list -->
        <div class="icons-set clearfix">
                <ul>
                    <li>
                        <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                    </li>
                    <?php if (isset($imovel->video)): ?>
                        <li>
                            <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                        </li>
                    <?php endif ?>
                    <li>
                        <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                    </li>
                    <li>
                        <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                    </li>
                </ul>
            </div>
        <div class="property-list clearfix" id="fotos-do-empreendimento">
            <?php if ($imovel->fotos_empreendimento): ?>
                <h3 class="heading">
                    FOTOS DO EMPREENDIMENTO <?php echo ($imovel->empreendimento) ? ' - ' . $imovel->empreendimento->name : ''; ?>
                </h3>
                <ul class="clearfix">
                    <?php foreach ($imovel->fotos_empreendimento as $i => $foto): ?>
                    <li class="<?php echo ($i == 0) ? 'active' : ''; ?>">
                        <a href="<?php echo sprintf($this->model_imovel->uri_images_all,$foto->key) ?>" class="fancybox" rel="gallery">
                            <img src="<?php echo sprintf($this->model_imovel->uri_images, 'crop', 138, 115, $foto->key, $foto->etag) ?>" alt="">
                        </a>

                        <?php /*if ($imovel->tour_virtual): ?>
                            <a target="new" href="<?php echo $imovel->tour_virtual ?>" class="tour-virtual">
                                Tour virtual
                            </a>
                        <?php endif*/ ?>

                    </li>
                    <?php endforeach ?>
                </ul>
                <a href="<?php echo site_url('imoveis/download-fotos/'.$imovel->id.'/true') ?>" class="download download-fotos-imovel">
                    DOWNLOAD DAS FOTOS
                </a>
            <?php endif ?>
        </div>
        <!-- end property list -->

        <!-- start location2 -->
        <div class="location2 clearfix localizacao-do-imovel-none" id="localizacao-do-imovel" style="">
            <!-- start row -->
            <div class="row clearfix">
                <!-- start icons set -->
                <div class="icons-set clearfix">
                    <ul>
                        <li>
                            <a href="<?php echo current_url() ?>#descricao-do-imovel" title="Descrição do ímovel" class="list">list</a>
                        </li>
                        <?php if (isset($imovel->video)): ?>
                            <li>
                                <a href="<?php echo current_url() ?>#video-do-imovel" title="Vídeo do ímovel" class="play">play</a>
                            </li>
                        <?php endif ?>
                        <li>
                            <a href="<?php echo current_url() ?>#fotos-do-imovel" title="Fotos do ímovel" class="gallery">gallery</a>
                        </li>
                        <li>
                            <a href="<?php echo current_url() ?>#localizacao-do-imovel" title="Localização do ímovel" class="location">location</a>
                        </li>
                    </ul>
                </div>
                <!-- end icon set -->
                <h3 class="heading">LOCALIZAÇÃO DO EMPREENDIMENTO</h3>
            </div>
            <!-- end row -->
            <?php if (isset($imovel->endereco->geolocation) && !empty($imovel->endereco->geolocation)): ?>
                <div class="map left">
                    <!-- <iframe src="https://www.google.com/maps/embed/v1/place?q=<?php echo $imovel->endereco->geolocation[1] .','. $imovel->endereco->geolocation[0] ?>&key=AIzaSyAEny5PWNO7nGU0JMH45K-PzyAu8AKxtcg" width="545" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> -->

                    <div id="map-canvas" style="width:545px; height:400px;"></div>
                    <script type="text/javascript">
                        init_google_maps(<?php echo $imovel->endereco->geolocation[1] .','. $imovel->endereco->geolocation[0] ?>);
                    </script>
                </div>
            <?php endif ?>

            <?php if ($imovel->street_view): ?>
                <div class="map right">
                    <!--
                    <div id="map-canvas-streetview" style="width:545px; height:400px;"></div>
                    <script type="text/javascript">
                        init_google_maps(<?php echo $imovel->endereco->geolocation[1] .','. $imovel->endereco->geolocation[0] ?>, true);
                    </script>
                    -->

                    <iframe src="<?php echo $imovel->street_view ?>" width="545" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            <?php endif ?>
        </div>
        <!-- end location2 -->

        <?php if ($this->uri->segments[1] == 'empreendimentos'): ?>
            <?php if (isset($imovel->empreendimento_imoveis)): ?>
                <div class="property-listing clearfix">
                    <h3>IMÓVEIS DISPONÍVEIS NO EMPREENDIMENTO</h3>

                    <?php if (array_sum(array_map("count", $imovel->empreendimento_imoveis)) > 4): ?>
                        <a href="#" title="prev" class="prev" id="imoveis-empreendimento-nav-prev">prev</a>
                        <a href="#" title="next" class="next" id="imoveis-empreendimento-nav-next">next</a>
                    <?php endif; ?>

                    <div class="cycle-slideshow property-listing"
                         data-allow-wrap="false",
                         data-cycle-fx="scrollHorz"
                         data-cycle-timeout="0"
                         data-cycle-prev="#imoveis-empreendimento-nav-prev"
                         data-cycle-next="#imoveis-empreendimento-nav-next"
                         data-cycle-slides="> ul">

                        <?php foreach ($imovel->empreendimento_imoveis as $im): ?>
                            <ul class="row">
                                <?php
                                foreach ($im as $empreendimento):
                                    $empreendimento->tipo_view = 'inline';
                                    echo '
                                        <li class="col-md-3 col-sm-6">
                                            '. $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $empreendimento), TRUE) .'
                                        </li>
                                    ';
                                endforeach;
                                ?>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
        <?php else: ?>
            <?php if (isset($imovel->imoveis_semelhantes) && !empty($imovel->imoveis_semelhantes)): ?>
                <div class="property-listing clearfix">
                    <h3>IMÓVEIS SEMELHANTES</h3>

                    <?php if (array_sum(array_map("count", $imovel->imoveis_semelhantes)) > 4): ?>
                        <a href="#" title="prev" class="prev" id="imoveis-semelhantes-nav-prev">prev</a>
                        <a href="#" title="next" class="next" id="imoveis-semelhantes-nav-next">next</a>
                    <?php endif; ?>

                    <div class="cycle-slideshow property-listing"
                         data-allow-wrap="false",
                         data-cycle-fx="scrollHorz"
                         data-cycle-timeout="0"
                         data-cycle-prev="#imoveis-semelhantes-nav-prev"
                         data-cycle-next="#imoveis-semelhantes-nav-next"
                         data-cycle-slides="> ul">

                        <?php foreach ($imovel->imoveis_semelhantes as $im): ?>
                            <ul class="row">
                                <?php
                                foreach ($im as $imovel):
                                    $imovel->tipo_view = 'inline';
                                    echo '
                                        <li class="col-md-3 col-sm-6">
                                            '. $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $imovel), TRUE) .'
                                        </li>
                                    ';
                                endforeach;
                                ?>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <!-- end box -->
</div>
<!-- end container -->

<?php }?>