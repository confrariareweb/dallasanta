<?php
    $range = array();

    if ($this->busca_principal['checked']->aluguel):
        $min = 0;
        $max = 3000;

        $range['area'] = array(
            'target'     => '#range-slider-area',
            'input_name' => 'area',
            'min'        => $min,
            'max'        => $max,
            'min_active' => (isset($_GET['area-min']) && !empty($_GET['area-min'])) ? $_GET['area-min'] : $min,
            'max_active' => (isset($_GET['area-max']) && !empty($_GET['area-max'])) ? $_GET['area-max'] : $max,
            'step'       => '10',
            'prefix'     => '',
            'sufix'      => 'm²'
        );
    else:
        $min = 0;
        $max = 5000;

        $range['area'] = array(
            'target'     => '#range-slider-area',
            'input_name' => 'area',
            'min'        => $min,
            'max'        => $max,
            'min_active' => (isset($_GET['area-min']) && !empty($_GET['area-min'])) ? $_GET['area-min'] : $min,
            'max_active' => (isset($_GET['area-max']) && !empty($_GET['area-max'])) ? $_GET['area-max'] : $max,
            'step'       => '10',
            'prefix'     => '',
            'sufix'      => 'm²'
        );
    endif;

    // Verifica se é busca para aluguel ou venda
    if ($this->busca_principal['checked']->aluguel):
        $min = 0;
        $max = 100000;

        $range['preco'] = array(
            'target' => '#range-slider-preco',
            'input_name' => 'valor',
            'min' => $min,
            'max' => $max,
            'min_active' => (isset($_GET['valor-min']) && !empty($_GET['valor-min'])) ? $_GET['valor-min'] : $min,
            'max_active' => (isset($_GET['valor-max']) && !empty($_GET['valor-max'])) ? $_GET['valor-max'] : $max,
            'step' => '500',
            'prefix' => 'R$',
            'sufix' => ''
        );
    else:
        $min = 0;
        $max = 5000000;

        $range['preco'] = array(
            'target' => '#range-slider-preco',
            'input_name' => 'valor',
            'min' => $min,
            'max' => $max,
            'min_active' => (isset($_GET['valor-min']) && !empty($_GET['valor-min'])) ? $_GET['valor-min'] : $min,
            'max_active' => (isset($_GET['valor-max']) && !empty($_GET['valor-max'])) ? $_GET['valor-max'] : $max,
            'step' => '50000',
            'prefix' => 'R$',
            'sufix' => ''
        );
    endif;
?>

<!-- start container -->
<div class="container clearfix">
	<!-- start box -->
    <div class="box">
        <div class="clearfix">
            <!-- start column left -->
            <div class="column-left">
            	<!-- start side bar -->
            	<div class="side-bar">
                    <!-- start optimize-search -->
                    <div class="optimize-search clearfix">
                    	<h4>Otimize sua busca</h4>
                        <form name="form_optimize_search" action="<?php echo site_url('imoveis/busca') ?>" method="get" id="form-optimize-search" onsubmit="return true">
                            <div class="input-radio-style clearfix">
                                <label class="button button2 <?php echo ($this->busca_principal['checked']->aluguel) ? 'button3' : ''; ?>" for="busca-tipo-aluguel"  id="btn_aluguel_ot">
                                    <input <?php echo $this->busca_principal['checked']->aluguel ?> name="tipo-imovel" type="radio" value="Aluguel" id="busca-tipo-aluguel" class="radio-input">
                                    Aluguel
                                </label>
                                <label class="button button2 <?php echo ($this->busca_principal['checked']->venda) ? 'button3' : ''; ?>" for="busca-tipo-venda"  id="btn_venda_ot">
                                    <input <?php echo $this->busca_principal['checked']->venda ?> name="tipo-imovel" type="radio" value="Venda" id="busca-tipo-venda" class="radio-input">
                                    Venda
                                </label>
                            </div>

                            <div class="filtro-bairros">
                                <span class="form-label">Bairros:</span>
                                <div class="bairros">
                                    <!--
                                    <label class="todos-bairros">
                                        <input type="checkbox" name="todos_bairros" value="1" <?php echo ((isset($_GET['todos_bairros']) && $_GET['todos_bairros'] == '1') || empty($_GET)) ? 'checked' : ''; ?>>
                                        Todos
                                    </label>
                                    -->

                                    <?php

                                    $qry_string = $_SERVER['QUERY_STRING'];
                                    parse_str($qry_string, $filtro_atual);

                                    foreach ($this->busca_principal['bairros'] as $i => $cidade):
                                        sort($cidade['bairros']);
                                        ?>

                                        <div class="cidade">
                                            <div class="clearfix">
                                                <h3>
                                                    <?php echo $cidade['cidade'] ?>
                                                </h3>
                                            <?php
                                            if (isset($_GET['cidades'])):
                                                $http_query = $filtro_atual;
                                                unset($http_query['cidades'][array_search($cidade['cidade'], $http_query['cidades'])]);

                                                echo '
                                                    <input type="hidden" name="cidades[]" value="'. $cidade['cidade'] .'">
                                                    <a href="'. site_url('imoveis/busca/?'.http_build_query($http_query)) .'" class="remove-city" title="Remover \''. $cidade['cidade'] .'\' da busca"></a>
                                                ';
                                            endif;
                                            ?>
                                        </div>

                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="todos_bairros_cidade_<?php echo $i ?>" value="<?php echo $i ?>" class="todos-bairros-cidade">
                                                <u>Todos</u>
                                            </label>
                                        </div>

                                        <?php
                                            foreach ($cidade['bairros'] as $bairro):
                                                $checked = NULL;
                                                if (isset($_GET['bairros']) && !empty($_GET['bairros'])):
                                                    if (in_array($bairro, $_GET['bairros'])):
                                                        $checked = 'checked';
                                                    endif;
                                                endif;
                                                ?>

                                                <div class="checkbox">
                                                    <label>
                                                        <input <?php echo $checked ?> type="checkbox" name="bairros[]" value="<?php echo $bairro ?>" class="bairro-cidade-<?php echo $i ?>">
                                                        <?php echo $bairro ?>
                                                    </label>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>

                                    <?php endforeach; ?>
                                </div>
                            </div>

                        	<button class="button button4 fechar" id="fechar-selecao">
                                <span></span>
                                <b>FECHAR SELEÇÃO</b>
                            </button>

                            <div>
                                <span class="form-label">Preço:</span>
                                <?php echo $this->load->view(FRONTEND_VIEW.'includes/range-slider', $range['preco'], TRUE) ?>
                            </div>

                            <div class="clearfix" id="aluguel_busca_ot">
                            	<span class="form-label">Tipo:</span>
                                <?php
                                foreach ($this->busca_principal['categorias_aluguel'] as $tipo):
                                    $checked = NULL;
                                    if (isset($_GET['categorias']) && !empty($_GET['categorias'])):
                                        if (in_array($tipo, $_GET['categorias'])):
                                            $checked = 'checked';
                                        endif;
                                    endif;
                                ?>
                                    <div class="checkbox">
                                        <label>
                                            <input <?php echo $checked ?> type="checkbox" name="categorias[]" value="<?php echo $tipo ?>">
                                            <?php echo $tipo ?>
                                        </label>
                                    </div>
                                <?php endforeach ?>
                            </div>

                            <div class="clearfix" style="display: none;" id="venda_busca_ot">
                            	<span class="form-label">Tipo:</span>
                                <?php
                                    foreach ($this->busca_principal['categorias_venda'] as $tipo):
                                        $checked = NULL;
                                        if (isset($_GET['categorias']) && !empty($_GET['categorias'])):
                                            if (in_array($tipo, $_GET['categorias'])):
                                                $checked = 'checked';
                                            endif;
                                        endif;
                                        ?>
                                        <div class="checkbox">
                                        <label>
                                            <input <?php echo $checked ?> type="checkbox" name="categorias[]" value="<?php echo $tipo ?>">
                                            <?php echo $tipo ?>
                                        </label>
                                    </div>
                                <?php endforeach ?>
                            </div>

                            <div>
                            	<span class="form-label">ÁREA:</span>
                                <?php echo $this->load->view(FRONTEND_VIEW.'includes/range-slider', $range['area'], TRUE) ?>
                            </div>

                            <div>
                                <button type="submit" class="btn button">
                                    <span></span>
                                    Buscar
                                </button>
                            </div>
                        </form>
                    <!-- end form -->
                    </div>
                    <!-- end optimize search -->
                </div>
                <!-- end side bar -->
            </div>
            <!-- end column left -->
            <!-- start column right -->
            <div class="column-right">
            	<!-- start tab -->
                <div class="tab gap clearfix">
                    <!-- start row2 -->

                    <div class="row2 clearfix">

                        <?php if(!empty($imoveis->total)){?>
                        <div class="left">
                            <h4>
                                <?php echo sprintf('Foram encontrados %s imóveis', $imoveis->total); ?>
                            </h4>
                        </div>
                        <?php }else{ ?>
                        <h4 >
                            <?php echo 'Foram encontrados 0 imóveis para este critérios de busca'; ?>
                        </h4>
                        <?php } ?>


                        <?php if ($imoveis->total > 0): ?>
                            <div class="select-bar">
                            	<a href="javascript:;" class="download"></a>
                                <button onclick="modal_loading(); $('#form-compara-imoveis').submit();" type="button" class="button">
                                    <span></span>COMPARAR SELECIONADOS
                                </button>
                            	<div class="style-select">
                                    <select name="ordem" class="select" id="change-order-view">
                                        <option value="" <?php echo (!isset($_GET['sort'])) ? 'selected' : ''; ?>>Ordenar por:</option>
                                        <option value="mais-atual" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'mais-atual') ? 'selected' : ''; ?>>Mais atual</option>
                                        <option value="menos-atual" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'menos-atual') ? 'selected' : ''; ?>>Menos atual</option>
                                        <option value="maior-preco" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'maior-preco') ? 'selected' : ''; ?>>Maior preço</option>
                                        <option value="menor-preco" <?php echo (isset($_GET['sort']) && $_GET['sort'] == 'menor-preco') ? 'selected' : ''; ?>>Menor preço</option>
                                    </select>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    <!-- end row2 -->
                    <!-- start tab content -->
                    <div class="tab-content clearfix">
                    	<!-- start property listing2 -->
                        <div class="property-listing2 clearfix">
                        	<form method="get" action="<?php echo site_url('imoveis/comparar').'/'.$tipo_busca; ?>" id="form-compara-imoveis">
                                <?php
                                foreach ($imoveis->imoveis as $imovel):
                                    echo $this->load->view(FRONTEND_VIEW.'includes/imovel', array('imovel' => $imovel), TRUE);
                                endforeach; ?>
                            </form>
                        </div>
                        <!-- end property listing2 -->

                        <!-- start pagination -->
                        <?php echo $this->pagination->create_links(); ?>
                        <!-- end pagination -->
                    </div>
                    <!-- end tab content -->
                </div>
                <!-- end tab -->
            </div>
            <!-- end column right -->
        </div>
    </div>
    <!-- end box -->
</div>
<!-- end container -->