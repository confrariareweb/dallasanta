<?php

class Admin_bannertype extends MY_Controller {
    function __construct() {
        parent::__construct();
        $label = "Banner tipo/categooria";

        $this->load->model("admin/admin_model");
        $this->load->model("admin_bannertype/model_admin_bannertype");

        $this->admin_model->table = "bannertipo";

        $this->layout = "backend/layouts/backend";
        $this->body_cfg = "class='withvernav'";

        $this->breadcrumbs[] = array("link" => "javascript:void(0);","title" => $label);
        $this->breadcrumbs[] = array("link" => "javascript:void(0);","title" => $label);

        $this->data["info"] = array(
            "title" => $label,
            "description" => $label,
            "menu_active" => "dashboard",
            "submenu_active" => "banner"
        );

        $this->validate = array(
            array(
                "field" => "titulo",
                "label" => "Título",
                "rules" => "required|trim|"));
    }

    public function index() {
        $data = array();

        $bannertipo_list = $this->model_admin_bannertype->bannerTypeList(
            $bannertype__id = null,
            $bannertype__nome = null);

        $data["data"] = $bannertipo_list;

        $this->data["content"] = $this->load->view("list",$data,TRUE);
        $this->load->view("structure",$this->data);
    }

    public function edit($id = null) {
        $data = array();

        if (!empty($id)) {
            $bannertipo_list = $this->model_admin_bannertype->bannerTypeList(
                $bannertype__id = $id,
                $bannertype__nome = null);
        }

        $data["data"] = $bannertipo_list;
        $data["status_list"] = array("1" => "Ativo","0" => "Inativo");

        $this->data["content"] = $this->load->view("edit",$data,TRUE);
        $this->load->view("structure",$this->data);
    }

    public function save() {
        $this->form_validation->set_rules($this->validate);

        if ($this->form_validation->run() == FALSE) {
            $this->error->set($validate);

            $this->admin_model->setAlert(array(
                "type" => "error",
                "msg" => array("Erro no envio dos dados!")));

            redirect("/admin/".$this->uri->segment(2).'/edit/'.$this->input->post("id"), "location");

        } else {
            $data = $this->input->post(NULL,TRUE);
            $data["nome"] = slug($data["titulo"]);

            $id = $this->admin_model->save($data);

            $this->admin_model->setAlert(array(
                "type" => "success",
                "msg" => array("Dados salvos com sucesso!")));

            redirect("/admin/".$this->uri->segment(2), "location");
        }
    }

    public function delete() {
        $id = $this->input->post("id");

        if (is_array($id)) {
            $this->admin_model->delete($id);
            $this->admin_model->setAlert(array(
                "type" => "success",
                "msg" => array("Dados excluídos com sucesso!")));

        } else {
            $this->admin_model->setAlert(array(
                "type" => "error",
                "msg" => array("Nenhum parâmetro passado")));

        }

        redirect("/admin/".$this->uri->segment(2), "location");
    }
}
