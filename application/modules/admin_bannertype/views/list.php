<div id="list" class="subcontent">
	<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable1" data-source="admin/<?php echo $this->uri->segment(2); ?>/index/list">
	    <colgroup>
	        <col class="con1" style="width: 5%" />
	        <col class="con1" style="width: 5%" />
	        <col class="con1" style="width: 90%" />
	    </colgroup>
		    <thead>
		        <tr>
		          	<th class="head1 nosort"><a class="btn btn_trash" href="javascript:void(0);"><span>Excluir</span></a></th>
		            <th class="head1">ID</th>
		            <th class="head1">Título</th>
		        </tr>
		    </thead>
		    <thead>
		    	<tr class="unique_search">
		          	<th class="head1 nosort"><input type="text" value="" style="visibility:hidden;" /></th>
		            <th class="head1"><input type="text" class="search_init" value="" placeholder="" /></th>
		            <th class="head1"><input type="text" class="search_init" value="" placeholder="buscar" /></th>
		        </tr>
		    </thead>
		    <tfoot>
		        <tr>
		          	<th class="head1 nosort"><a class="btn btn_trash" href="javascript:void(0);"><span>Excluir</span></a></th>
		            <th class="head1">ID</th>
		            <th class="head1">Título</th>
		        </tr>
		    </tfoot>
		    <tbody data-rel="<?=site_url('admin/'.$this->uri->segment(2).'/edit/'); ?>">
		    	<?php $i = 0; foreach($data as $_data): ?>
		    		<tr class="gradeX con<?=($i % 2 == 0 ? '0' : '1'); ?>" data-rel="<?=site_url('admin/'.$this->uri->segment(2).'/edit/'.$_data->bannertipo__id); ?>">
						<td align="center">
							<span class="center"><input type="checkbox" name="delete_id[]" value="<?=$_data->bannertipo__id; ?>" /></span>
						</td>
						<td><?=$_data->bannertipo__id; ?></td>
						<td><?=$_data->bannertipo__titulo; ?></td>
					</tr>
		    	<?php $i++; endforeach; ?>
		    </tbody>
	</table>
	<form  id="form_delete" method="post" action="<?=site_url('admin/'.$this->uri->segment(2).'/delete'); ?>"></form>
</div>