<div id="add" class="subcontent">
   	<form id="form1" class="stdform" method="post" action="<?=site_url('admin/'.$this->uri->segment(2).'/save'); ?>" enctype="multipart/form-data" >
   		<input type="hidden" name="id" value="<?=$data->bannertipo__id; ?>" />
        <input type="hidden" name="nome" value="<?=$data->bannertipo__nome; ?>" />
        <p>
        	<label>Título</label>
            <span class="field">
            	<input type="text" name="titulo" id="titulo" class="longinput <?php if($this->error->form_error('titulo') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->bannertipo__titulo) != '') echo htmlspecialchars($data->bannertipo__titulo); else echo $this->error->set_value('titulo');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Título é obrigatório'}}}" />
	            <?=$this->error->form_error('titulo'); ?>
	        </span>
        </p>
        <p>
            <label>Status</label>
            <span class="field">
                <?=form_dropdown("status",$status_list,$data->banner__status,""); ?>
            </span>
        </p>
        <p class="stdformbutton">
        	<button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>