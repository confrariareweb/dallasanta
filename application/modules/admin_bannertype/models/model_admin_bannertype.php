<?php

class Model_admin_Bannertype extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function bannerTypeList($bannertype__id = null, $bannertype__nome = null) {
        $order_by ="bannertipo.id desc";
        $where_list = array("bannertipo.status in(0,1)" => null);

        if (!empty($bannertype__id)) {
            $where_list["bannertipo.id"] = $bannertype__id;
        }

        if (!empty($bannertype__nome)) {
            $where_list["bannertipo.nome"] = $bannertype__nome;
        }

        $bannertype_list = $this->db->select("
            bannertipo.id id,
            bannertipo.id bannertipo__id,
            bannertipo.nome bannertipo__nome,
            bannertipo.titulo bannertipo__titulo,
            bannertipo.dateupdate bannertipo__dateupdate,")
        ->from("bannertipo")
        ->order_by($order_by)
        ->where($where_list)
        ->get()
        ->result_object();

        if (empty($bannertype_list)) {
            return $bannertype_list;
        }

        if (!empty($bannertype__id) || !empty($bannertype__nome)) {
            $bannertype_list = $bannertype_list[0];
        }

        return $bannertype_list;
    }
}

?>
