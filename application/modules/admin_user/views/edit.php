<div id="add" class="subcontent">
    <form id="form1" class="stdform" method="post" action="<?php echo site_url('admin/' . $this->uri->segment(2) . '/save'); ?>" enctype="multipart/form-data" >
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>" />

        <p>
            <label>Nome</label>
            <span class="field">
                <input type="text" name="name" id="name" class="longinput <?php if ($this->error->form_error('name') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['name']) != '')
                    echo htmlspecialchars($data['name']);
                else
                    echo $this->error->set_value('name');
                ?>" data-validate="{validate:{required:true, messages:{required:'O campo Nome é obrigatório'}}}" />
                       <?php echo ($this->error->form_error('name')); ?>
            </span>
        </p>

        <p>
            <label>Email</label>
            <span class="field">
                <input type="text" name="email" id="email" class="longinput <?php if ($this->error->form_error('email') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['email']) != '')
                    echo htmlspecialchars($data['email']);
                else
                    echo $this->error->set_value('email');
                ?>" data-validate="{validate:{required:true, email: true, messages:{required:'O campo Email é obrigatório',email:'O campo Email é inválido'}}}" />
                       <?php echo ($this->error->form_error('email')); ?>
            </span>

        </p>

        <p>
            <label>Nomenclatura</label>
            <span class="field">
                <input type="text" name="nomenclatura" id="nomenclatura" class="small2input <?php if ($this->error->form_error('nomenclatura') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['nomenclatura']) != '')
                    echo htmlspecialchars($data['nomenclatura']);
                else
                    echo $this->error->set_value('nomenclatura');
                ?>" />
                       <?php echo ($this->error->form_error('nomenclatura')); ?>

                Número
                <input type="text" name="numero" id="numero" class="small2input <?php if ($this->error->form_error('numero') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['numero']) != '')
                    echo htmlspecialchars($data['numero']);
                else
                    echo $this->error->set_value('numero');
                ?>" />
                       <?php echo ($this->error->form_error('numero')); ?>
            </span>
        </p>

        <p>
            <label>Grupo de usuários</label>
            <span class="field">
                <?php
                $group_value = $data['user_group_id'] != '' ? htmlspecialchars($data['user_group_id']) : $this->error->set_value('user_group_id');
                foreach ($data_group as $_data_group):
                    $group_option[$_data_group['id']] = $_data_group['title'];
                endforeach;
                ?>
                <?php echo form_dropdown('user_group_id', $group_option, $group_value, ""); ?>
            </span>
        </p>

        <p id="listaCorretor" <?php if ($group_value != 5) { ?>style="display: none;"<?php } ?>>
            <label>Corretor do cliente</label>
            <span class="field">
                <?php
                $corretor_id_value = $data['corretor_id'] != '' ? htmlspecialchars($data['corretor_id']) : $this->error->set_value('corretor_idF');
                $corretor_option[0] = 'Selecione...';
                foreach ($data_group_corretores as $_data_group_corretores):
                    $corretor_option[$_data_group_corretores['id']] = $_data_group_corretores['name'];
                endforeach;
                echo form_dropdown('corretor_id', $corretor_option, $corretor_id_value, "");
                ?>
            </span>
        </p>

        <p>
            <label>Imagem</label>
            <span class="field">
                <?php echo form_upload(array('name' => 'image', 'class' => 'longinput')); ?><br />
                <?php
                if (read_file($this->admin_model->directory . $data['image'])):
                    echo img(array('src' => image("media/" . $this->admin_model->directory . $data['image'], "200x0"), 'class' => 'thumb_image'));
                    echo "<br />" . form_checkbox(array('name' => 'delete_image', 'id' => 'delete_image', 'value' => '1'));
                    echo "Deletar imagem?";
                endif;
                ?>
            </span>
        </p>

        <p>
            <label>Status</label>
            <span class="field">
                <?php
                $status_value = $data['status'] != '' ? htmlspecialchars($data['status']) : $this->error->set_value('status');
                $status_option = array('1' => 'Ativo', '0' => 'Inativo');
                ?>
                <?php echo form_dropdown('status', $status_option, $status_value, ""); ?>
            </span>
        </p>

        <p>
            <label>Mostrar no site</label>
            <span class="field">
                <?php
                $showsite_value = $data['showsite'] != '' ? htmlspecialchars($data['showsite']) : $this->error->set_value('showsite');
                $showsite_option = array('1' => 'Sim', '0' => 'Não');
                ?>
                <?php echo form_dropdown('showsite', $showsite_option, $showsite_value, ""); ?>
            </span>
        </p>

        <p>
            <label>Senha</label>
            <span class="field">
                <input type="password" name="pass" id="pass" class="smallinput" value="" data-validate="{validate:{minlength: 6, messages:{minlength:'O campo Senha deve conter no mínimo 6 caracteres'}}}" />
                <?php echo ($this->error->form_error('pass')); ?>
            </span>
        </p>
        <p>
            <label>Confirmação de senha</label>
            <span class="field">
                <input type="password" name="confirm_pass" id="confirm_pass" class="smallinput" value="" data-validate="{validate:{minlength: 6, equalTo: '#pass', messages:{minlength:'O campo Confirmação de senha deve conter no mínimo 6 caracteres', equalTo:'Digite a confirmação de senha igual acima'}}}" />
            </span>
        </p>

        <p class="stdformbutton">
            <button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>