<?php

class Admin_content extends MY_Controller {
    function __construct() {
        parent::__construct();
        $label = "Conteudo";

        $this->load->model("admin/admin_model");
        $this->load->model("admin_content/model_content");
        $this->load->model("admin_contenttype/model_contenttype");

        $this->admin_model->table = "conteudo";
        $this->admin_model->directory = "./upload/gallery/";

        $this->layout = "backend/layouts/backend";
        $this->body_cfg = "class='withvernav'";

        $this->breadcrumbs[] = array("link" => "javascript:void(0);","title" => $label);
        $this->breadcrumbs[] = array("link" => "javascript:void(0);","title" => $label);

        $this->data["info"] = array(
            "title" => $label,
            "description" => $label,
            "menu_active" => "dashboard",
            "submenu_active" => "content"
        );

        $this->validate = array(
            array(
                "field" => "titulo",
                "label" => "Título",
                "rules" => "required|xss_clean|trim"),
            array(
                "field" => "tipo_id",
                "label" => "Tipo/Categoria",
                "rules" => "required|xss_clean|trim"));

        $this->load->add_package_path(APPPATH."third_party/swfupload/",TRUE);
        $this->load->model("swfupload_model","swfu");

        $this->swfu->rel = "conteudo";
        $this->swfu->folder = "gallery";
    }
    
    public function index() {
        $data = array();

        $content_list = $this->model_content->contentList(
            $content__id = null, 
            $content__nome = null, 
            $contenttype__id = null, 
            $contenttype__nome = null, 
            $contenttype__pai_id = null, 
            $contenttype__pai__nome = null, 
            $page = null, 
            $page_limit = null);

        $data["data"] = $content_list;
        
        $this->data["content"] = $this->load->view("list",$data,TRUE);
        $this->load->view("structure",$this->data);
    }

    public function edit($id = null) {
        $data = array();

        $content_list = null;

        if (!empty($id)) {
            $content_list = $this->model_content->contentList(
                $content__id = $id, 
                $content__nome = null, 
                $contenttype__id = null, 
                $contenttype__nome = null, 
                $contenttype__pai_id = null, 
                $contenttype__pai__nome = null, 
                $page = null, 
                $page_limit = null);
        }

        $contenttype_all_list = $this->model_contenttype->contentTypeList(
            $contenttype__id = null, 
            $contenttype__nome = null, 
            $contenttype__pai_id = null, 
            $contenttype__pai__nome = null, 
            $page = null, 
            $page_limit = null);

        $contentcategory_list = $this->model_contenttype->contentCategoryList(
            $contentcategory__id = null, 
            $contentcategory__nome = null, 
            $page = null, 
            $page_limit = null);

        $contentcategory_list_ = array("" => "-- selecione --");

        foreach ($contentcategory_list as $i => $contentcategory) {
            $contentcategory_list_[$contentcategory->conteudocategoria__titulo] = array($contentcategory->conteudocategoria__id => $contentcategory->conteudocategoria__titulo);

            foreach ($contenttype_all_list as $ii => $contenttype_all) {
                if ($contentcategory->conteudocategoria__id == $contenttype_all->conteudotipo__pai_id) {
                    $contentcategory_list_[$contentcategory->conteudocategoria__titulo][$contenttype_all->conteudotipo__id] = $contenttype_all->conteudotipo__titulo;
                }
            }
        }

        $content_total_row = $this->model_content->contentTotalRow(
            $content__id = null, 
            $content__nome = null, 
            $contenttype__id = null, 
            $contenttype__nome = null, 
            $contenttype__pai_id = null, 
            $contenttype__pai__nome = null);

        $product_ordem_list = array("" => "selecione");

        for ($i = $content_total_row; $i >= 0; --$i) {
            $product_ordem_list[$i] = $i;
        }

        $data["data"] = $content_list;
        $data["status_list"] = array("1" => "Ativo","0" => "Inativo");
        $data["ordem_list"] = $product_ordem_list;
        $data["contenttype_list"] = $contentcategory_list_;

        $this->data["content"] = $this->load->view("edit",$data,TRUE);
        $this->load->view("structure",$this->data);
    }

    public function save() {
        $this->form_validation->set_rules($this->validate); 

        if ($this->form_validation->run() == FALSE) {
            $this->error->set($validate);

            $this->admin_model->setAlert(array(
                "type" => "error",
                "msg" => array("Erro no envio dos dados!")));

            redirect("/admin/".$this->uri->segment(2).'/edit/'.$this->input->post("id"), "location");

        } else {
            $data = $this->input->post(NULL,TRUE);
            $data["nome"] = slug($data["titulo"]);
            $data["descricao"] = htmlspecialchars($_POST["descricao"]);

            $id = $this->admin_model->save($data);
            $this->swfu->save($id);

            $this->admin_model->setAlert(array(
                "type" => "success",
                "msg" => array("Dados salvos com sucesso!")));

            redirect("/admin/".$this->uri->segment(2), "location");
        }
    }

    public function delete() {
        $id = $this->input->post("id");

        if (is_array($id)) {
            $this->admin_model->delete($id);
            $this->admin_model->setAlert(array(
                "type" => "success",
                "msg" => array("Dados excluídos com sucesso!")));

        } else {
            $this->admin_model->setAlert(array(
                "type" => "error",
                "msg" => array("Nenhum parâmetro passado")));

        }

        redirect("/admin/".$this->uri->segment(2), "location");
    }
}