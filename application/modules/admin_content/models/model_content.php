<?php

class Model_Content extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function contentList($content__id = null, $content__nome = null, $contenttype__id = null, $contenttype__nome = null, $contenttype__pai_id = null, $contenttype__pai__nome = null, $page = null, $page_limit = null) {
        $order_by ="conteudo.ordem asc";
        $where_list = array("conteudo.status in(0,1)" => null);

        if (!empty($content__id)) {
            $where_list["conteudo.id"] = $content__id;
        }

        if (!empty($content__nome)) {
            $where_list["conteudo.nome"] = $content__nome;
        }

        if (!empty($contenttype__id)) {
            $where_list["ct.id"] = $contenttype__id;
        }

        if (!empty($contenttype__nome)) {
            $where_list["ct.nome"] = $contenttype__nome;
        }

        if (!empty($contenttype__pai_id)) {
            $where_list["ct.pai_id"] = $contenttype__pai_id;
        }

        if (!empty($contenttype__pai__nome)) {
            $where_list["cc.nome"] = $contenttype__pai__nome;
        }

        if (empty($page)) {
            $page = 0;

        } else if ($page >= 1) {
            $page -= 1;
        }

        if (empty($page_limit)) {
            $page_limit = 999999;
        }

        $content_list = $this->db->select("
            conteudo.id id,
            conteudo.id conteudo__id,
            conteudo.tipo_id conteudo__tipo_id,
            conteudo.nome conteudo__nome,
            conteudo.titulo conteudo__titulo,
            conteudo.url conteudo__url,
            conteudo.video conteudo__video,
            conteudo.subtitulo conteudo__subtitulo,
            conteudo.descricao conteudo__descricao,
            conteudo.ordem conteudo__ordem,
            conteudo.status conteudo__status,
            conteudo.dateupdate conteudo__dateupdate,
            ct.id conteudotipo__id,
            ct.nome conteudotipo__nome,
            ct.titulo conteudotipo__titulo,
            ct.status conteudotipo__status,
            ct.dateupdate conteudotipo__dateupdate,
            cc.id conteudocategoria__id,
            cc.pai_id conteudocategoria__pai_id,
            cc.nome conteudocategoria__nome,
            cc.titulo conteudocategoria__titulo,
            cc.status conteudocategoria__status,
            cc.dateupdate conteudocategoria__dateupdate")
            ->from("conteudo")
            ->join("conteudotipo ct","ct.id = conteudo.tipo_id","left")
            ->join("conteudotipo cc","cc.id = ct.pai_id","left")
            ->order_by($order_by)
            ->where($where_list)
            ->limit($page_limit,$page * $page_limit)
            ->get()
            ->result_object();

        if (empty($content_list)) {
            return $content_list;
        }

        $gallery_list = $this->db->select("
            gallery.id id,
            gallery.id gallery__id,
            gallery.title gallery__title,
            gallery.order gallery__order,
            gallery.rel gallery__rel,
            gallery.type gallery__type,
            gallery.rel_id gallery__rel_id,
            gallery.file gallery__file,
            gallery.status gallery__status,")
            ->from("gallery")
            ->where(array("gallery.status" => 1,"gallery.rel" => "conteudo"))
            ->order_by("gallery.order asc")
            ->get()
            ->result_object();

        if (!empty($gallery_list)) {
            foreach ($content_list as $i => $content) {
                $content_list[$i]->gallery = array();

                foreach ($gallery_list as $gallery) {
                    if ($gallery->gallery__rel_id == $content->conteudo__id) {
                        $content_list[$i]->gallery[] = $gallery;
                    }
                }
            }
        }

        if (!empty($content__id) || !empty($content__nome)) {
            $content_list = $content_list[0];
        }

        return $content_list;
    }

    public function contentTotalRow($content__id = null, $content__nome = null, $contenttype__id = null, $contenttype__nome = null, $contenttype__pai_id = null, $contenttype__pai__nome = null) {
        $order_by ="conteudo.ordem asc";
        $where_list = array("conteudo.status in(0,1)" => null);

        if (!empty($content__id)) {
            $where_list["conteudo.id"] = $content__id;
        }

        if (!empty($content__nome)) {
            $where_list["conteudo.nome"] = $content__nome;
        }

        if (!empty($contenttype__id)) {
            $where_list["ct.id"] = $contenttype__id;
        }

        if (!empty($contenttype__nome)) {
            $where_list["ct.nome"] = $contenttype__nome;
        }

        if (!empty($contenttype__pai_id)) {
            $where_list["ct.pai_id"] = $contenttype__pai_id;
        }

        if (!empty($contenttype__pai__nome)) {
            $where_list["cc.nome"] = $contenttype__pai__nome;
        }

        $product_total_row = $this->db->select("
            count(1) total")
            ->from("conteudo")
            ->join("conteudotipo ct","ct.id = conteudo.tipo_id","left")
            ->join("conteudotipo cc","cc.id = ct.pai_id","left")
            ->order_by($order_by)
            ->where($where_list)
            ->get()
            ->result_object();

        $product_total_row = $product_total_row[0]->total;

        return $product_total_row;
    }
}

?>