<div id="add" class="subcontent">
    <form id="form1" class="stdform" method="post" action="<?=site_url('admin/'.$this->uri->segment(2).'/save'); ?>" enctype="multipart/form-data" >
        <input type="hidden" name="id" value="<?=$data->conteudo__id; ?>" />
        <input type="hidden" name="nome" value="<?=$data->conteudo__nome; ?>" />
        <p>
            <label>Título</label>
            <span class="field">
                <input type="text" name="titulo" id="titulo" class="longinput <?php if($this->error->form_error('titulo') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->conteudo__titulo) != '') echo htmlspecialchars($data->conteudo__titulo); else echo $this->error->set_value('titulo');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Título é obrigatório'}}}" />
                <?=$this->error->form_error("titulo"); ?>
            </span>
        </p>

        <!--
        <p>
            <label>Sub-Título</label>
            <span class="field">
                <input type="text" name="subtitulo" id="subtitulo" class="longinput <?php if($this->error->form_error('subtitulo') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->conteudo__subtitulo) != '') echo htmlspecialchars($data->conteudo__subtitulo); else echo $this->error->set_value('subtitulo');  ?>" data-validate="{validate:{required:false, messages:{required:'O campo subtitulo é obrigatório'}}}" />
                <?=$this->error->form_error("subtitulo"); ?>
            </span>
        </p>
        <p>
            <label>Url</label>
            <span class="field">
                <input type="text" name="url" id="url" class="longinput <?php if($this->error->form_error('url') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->conteudo__url) != '') echo htmlspecialchars($data->conteudo__url); else echo $this->error->set_value('url');  ?>" data-validate="{validate:{required:false, messages:{required:'O campo url é obrigatório'}}}" />
                <?=$this->error->form_error("url"); ?>
            </span>
        </p>
        -->

        <p>
            <label>Tipo/Categoria</label>
            <span class="field">
                <?=form_dropdown("tipo_id",$contenttype_list,$data->conteudotipo__id,""); ?>
            </span>
        </p>

        <!--
        <p>
            <label>Ordem</label>
            <span class="field">
                <?=form_dropdown("ordem",$ordem_list,$data->conteudo__ordem,""); ?>
            </span>
        </p>
         -->

        <p>
            <label>Status</label>
            <span class="field">
                <?=form_dropdown("status",$status_list,$data->conteudo__status,""); ?>
            </span>
        </p>

        <p>
            <label>Vídeo (Youtube)</label>
            <span class="field">
                <input type="text" name="video" class="longinput" value="<?php echo $data->conteudo__video ?>" data-validate="{validate:{ url: true, messages:{ url: 'Informe uma url válida' }}}">
                <?=$this->error->form_error("video"); ?>
            </span>
        </p>

        <p>
            <label>Descricao</label>
            <span class="field">
                <textarea id="descricao" name="descricao" rows="15" style="width: 80%" class="tinymce"><?=htmlspecialchars_decode($data->conteudo__descricao); ?></textarea>
            </span>
        </p>
        <?php $this->swfu->run($data->conteudo__id); ?>
        <p class="stdformbutton">
            <button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>