<div id="add" class="subcontent">
    <form id="form1" class="stdform" method="post" action="<?=site_url('admin/'.$this->uri->segment(2).'/save'); ?>" enctype="multipart/form-data" >
        <input type="hidden" name="id" value="<?=$data->conteudotipo__id; ?>" />
        <input type="hidden" name="nome" value="<?=$data->conteudotipo__nome; ?>" />
        <p>
            <label>Título</label>
            <span class="field">
                <input type="text" name="titulo" id="titulo" class="longinput <?php if($this->error->form_error('titulo') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->conteudotipo__titulo) != '') echo htmlspecialchars($data->conteudotipo__titulo); else echo $this->error->set_value('titulo');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Título é obrigatório'}}}" />
                <?=$this->error->form_error('titulo'); ?>
            </span>
        </p>
        <p>
            <label>Categoria/Tipo</label>
            <span class="field">
                <?=form_dropdown("pai_id",$contentcategory_list,$data->conteudotipo__pai_id,""); ?>
            </span>
        </p>
        <p>
            <label>Status</label>
            <span class="field">
                <?=form_dropdown("status",$status_list,$data->conteudotipo__status,""); ?>
            </span>
        </p>
        <p class="stdformbutton">
            <button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>