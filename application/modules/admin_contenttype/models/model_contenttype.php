<?php

class Model_Contenttype extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function contentTypeList($contenttype__id = null, $contenttype__nome = null, $contenttype__pai_id = null, $contenttype__pai__nome = null, $page = null, $page_limit = null) {
        $order_by ="ct.ordem asc";
        $where_list = array("ct.status in(0,1)" => null);

        if (!empty($contenttype__id)) {
            $where_list["ct.id"] = $contenttype__id;
        }

        if (!empty($contenttype__nome)) {
            $where_list["ct.nome"] = $contenttype__nome;
        }

        if (!empty($contenttype__pai_id)) {
            $where_list["ct.pai_id"] = $contenttype__pai_id;
        }

        if (!empty($contenttype__pai__nome)) {
            $where_list["cc.nome"] = $contenttype__pai__nome;
        }

        if (empty($page)) {
            $page = 0;

        } else if ($page >= 1) {
            $page -= 1;
        }

        if (empty($page_limit)) {
            $page_limit = 99999;
        }

        $contenttype_list = $this->db->select("
            ct.id conteudotipo__id,
            ct.pai_id conteudotipo__pai_id,
            ct.nome conteudotipo__nome,
            ct.titulo conteudotipo__titulo,
            ct.status conteudotipo__status,
            ct.dateupdate conteudotipo__dateupdate,
            cc.id conteudocategoria__id,
            cc.pai_id conteudocategoria__pai_id,
            cc.nome conteudocategoria__nome,
            cc.titulo conteudocategoria__titulo,
            cc.status conteudocategoria__status,
            cc.dateupdate conteudocategoria__dateupdate,")
            ->from("conteudotipo ct")
            ->join("conteudotipo cc","cc.id = ct.pai_id","left")
            ->order_by($order_by)
            ->where($where_list)
            ->limit($page_limit,$page * $page_limit)
            ->get()
            ->result_object();

        if (empty($contenttype_list)) {
            return $contenttype_list;
        }

        $gallery_list = $this->db->select("
            gallery.id id, 
            gallery.id gallery__id, 
            gallery.title gallery__title,
            gallery.order gallery__order,  
            gallery.rel gallery__rel, 
            gallery.type gallery__type, 
            gallery.rel_id gallery__rel_id, 
            gallery.file gallery__file, 
            gallery.status gallery__status,")
            ->from("gallery")
            ->where(array("gallery.status" => 1,"gallery.rel" => "conteudotipo"))
            ->order_by("gallery.order asc")
            ->get()
            ->result_object();

        if (!empty($gallery_list)) {
            foreach ($contenttype_list as $i => $contenttype) {
                $contenttype_list[$i]->gallery = array();

                foreach ($gallery_list as $gallery) {
                    if ($gallery->gallery__rel_id == $contenttype->conteudotipo__id) {
                        $contenttype_list[$i]->gallery[] = $gallery;
                    }
                }
            }
        }

        if (!empty($contenttype__id) || !empty($contenttype__nome)) {
            $contenttype_list = $contenttype_list[0];
        }

        return $contenttype_list;
    }

    public function contentCategoryList($contentcategory__id = null, $contentcategory__nome = null, $page = null, $page_limit = null) {
        $order_by ="ct.ordem asc";
        $where_list = array("ct.status in(0,1)" => null,"ct.pai_id is null" => null);

        if (!empty($contentcategory__id)) {
            $where_list["ct.id"] = $contentcategory__id;
        }

        if (!empty($contentcategory__nome)) {
            $where_list["ct.nome"] = $contentcategory__nome;
        }

        if (empty($page)) {
            $page = 0;

        } else if ($page >= 1) {
            $page -= 1;
        }

        if (empty($page_limit)) {
            $page_limit = 99999;
        }

        $contentcategory_list = $this->db->select("
            ct.id conteudocategoria__id,
            ct.pai_id conteudocategoria__pai_id,
            ct.nome conteudocategoria__nome,
            ct.titulo conteudocategoria__titulo,
            ct.status conteudocategoria__status,
            ct.dateupdate conteudocategoria__dateupdate,")
            ->from("conteudotipo ct")
            ->order_by($order_by)
            ->where($where_list)
            ->limit($page_limit,$page * $page_limit)
            ->get()
            ->result_object();

        if (empty($contentcategory_list)) {
            return $contentcategory_list;
        }

        $gallery_list = $this->db->select("
            gallery.id id, 
            gallery.id gallery__id, 
            gallery.title gallery__title,
            gallery.order gallery__order,  
            gallery.rel gallery__rel, 
            gallery.type gallery__type, 
            gallery.rel_id gallery__rel_id, 
            gallery.file gallery__file, 
            gallery.status gallery__status,")
            ->from("gallery")
            ->where(array("gallery.status" => 1,"gallery.rel" => "conteudotipo"))
            ->order_by("gallery.order asc")
            ->get()
            ->result_object();

        if (!empty($gallery_list)) {
            foreach ($contentcategory_list as $i => $contenttype) {
                $contentcategory_list[$i]->gallery = array();

                foreach ($gallery_list as $gallery) {
                    if ($gallery->gallery__rel_id == $contenttype->conteudocategoria__id) {
                        $contentcategory_list[$i]->gallery[] = $gallery;
                    }
                }
            }
        }

        if (!empty($contenttype__id) || !empty($contenttype__nome)) {
            $contentcategory_list = $contentcategory_list[0];
        }

        return $contentcategory_list;
    }
}

?>