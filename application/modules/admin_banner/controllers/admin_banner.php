<?php

class Admin_banner extends MY_Controller {
	function __construct() {
		parent::__construct();
		$label = "Banners";

		$this->load->model("admin/admin_model");
		$this->load->model("admin_banner/model_admin_banner");
		$this->load->model("admin_bannertype/model_admin_bannertype");

		$this->admin_model->table = "banner";
		$this->admin_model->directory = "./upload/banner/";

		$this->layout = "backend/layouts/backend";
		$this->body_cfg = "class='withvernav'";

		$this->breadcrumbs[] = array("link" => "javascript:void(0);","title" => $label);
		$this->breadcrumbs[] = array("link" => "javascript:void(0);","title" => $label);

		$this->data["info"] = array(
			"title" => $label,
			"description" => $label,
			"menu_active" => "dashboard",
			"submenu_active" => "banner"
		);

		$this->validate = array(
			array(
				"field" => "titulo",
				"label" => "Título",
				"rules" => "required|xss_clean|trim"));

		$this->load->add_package_path(APPPATH."third_party/swfupload/",TRUE);
        $this->load->model("swfupload_model","swfu");

        $this->swfu->rel = "banner";
        $this->swfu->folder = "gallery";
	}

	public function index() {
		$data = array();

		$banner_list = $this->model_admin_banner->bannerList(
			$banner__id = null,
			$banner__nome = null,
			$bannertipo__id = null,
			$bannertipo__nome = null);

		$data["data"] = $banner_list;

		$this->data["content"] = $this->load->view("list",$data,TRUE);
		$this->load->view("structure",$this->data);
	}

	public function edit($id = null) {
		$data = array();

		if (!empty($id)) {
			$banner_list = $this->model_admin_banner->bannerList(
				$banner__id = $id,
				$banner__nome = null,
				$bannertipo__id = null,
				$bannertipo__nome = null);
		}

		$bannertipo_list = $this->model_admin_bannertype->bannerTypeList(
            $bannertype__id = null,
            $bannertype__nome = null);

		$bannertipo_list_ = array("" => "selecione");

		foreach ($bannertipo_list as $bannertipo) {
			$bannertipo_list_[$bannertipo->bannertipo__id] = $bannertipo->bannertipo__titulo;
		}

		$banner_count_list = $this->model_admin_banner->bannerList(
			$banner__id = null,
			$banner__nome = null,
			$bannertipo__id = null,
			$bannertipo__nome = null);

		$banner_count = count($banner_count_list);
		$banner_count_list = array("" => "selecione");

		for ($i = $banner_count; $i >= 0; --$i) {
			$banner_count_list[$i] = $i;
		}

		$data["data"] = $banner_list;
		$data["status_list"] = array("1" => "Ativo","0" => "Inativo");
		$data["ordem_list"] = $banner_count_list;
		$data["bannertipo_list"] = $bannertipo_list_;

		$this->data["content"] = $this->load->view("edit",$data,TRUE);
		$this->load->view("structure",$this->data);
	}

	public function save() {
		$this->form_validation->set_rules($this->validate);

		if ($this->form_validation->run() == FALSE) {
			$this->error->set($validate);

			$this->admin_model->setAlert(array(
				"type" => "error",
				"msg" => array("Erro no envio dos dados!")));

			redirect("/admin/".$this->uri->segment(2).'/edit/'.$this->input->post("id"), "location");

		} else {
			$data = $this->input->post(NULL,TRUE);
			$data["nome"] = slug($data["titulo"]);
			$data["descricao"] = htmlspecialchars($_POST["descricao"]);
			$data["url"] = $_POST["url"];

			$id = $this->admin_model->save($data);
			$this->swfu->save($id);

			$this->admin_model->setAlert(array(
				"type" => "success",
				"msg" => array("Dados salvos com sucesso!")));

			redirect("/admin/".$this->uri->segment(2), "location");
		}
	}

	public function delete() {
		$id = $this->input->post("id");

		if (is_array($id)) {
			$this->admin_model->delete($id);
			$this->admin_model->setAlert(array(
				"type" => "success",
				"msg" => array("Dados excluídos com sucesso!")));

		} else {
			$this->admin_model->setAlert(array(
				"type" => "error",
				"msg" => array("Nenhum parâmetro passado")));

		}

		redirect("/admin/".$this->uri->segment(2), "location");
	}
}
