<?php

class Model_Admin_Banner extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function bannerList($banner__id = null, $banner__nome = null, $bannertipo__id = null, $bannertipo__nome = null) {
        $order_by ="banner.ordem asc";
        $where_list = array("banner.status in(0,1)" => null);

        if (!empty($banner__id)) {
            $where_list["banner.id"] = $banner__id;
        }

        if (!empty($banner__nome)) {
            $where_list["banner.nome"] = $banner__nome;
        }

        if (!empty($bannertipo__id)) {
            $where_list["bannertipo.id"] = $bannertipo__id;
        }

        if (!empty($bannertipo__nome)) {
            $where_list["bannertipo.nome"] = $bannertipo__nome;
        }

        $banner_list = $this->db->select("
            banner.id id,
            banner.id banner__id,
            banner.tipo_id banner__tipo_id,
            banner.nome banner__nome,
            banner.titulo banner__titulo,
            banner.subtitulo banner__subtitulo,
            banner.descricao banner__descricao,
            banner.url banner__url,
            banner.ordem banner__ordem,
            banner.status banner__status,
            banner.dateupdate banner__dateupdate,
            bannertipo.id bannertipo__id,
            bannertipo.nome bannertipo__nome,
            bannertipo.titulo bannertipo__titulo,
            bannertipo.dateupdate bannertipo__dateupdate,")
        ->from("banner")
        ->join("bannertipo", "bannertipo.id = banner.tipo_id")
        ->order_by($order_by)
        ->where($where_list)
        ->get()
        ->result_object();

        if (empty($banner_list)) {
            return $banner_list;
        }

        $gallery_list = $this->db->select("
            gallery.id id,
            gallery.id gallery__id,
            gallery.title gallery__title,
            gallery.order gallery__order,
            gallery.rel gallery__rel,
            gallery.type gallery__type,
            gallery.rel_id gallery__rel_id,
            gallery.file gallery__file,
            gallery.status gallery__status,")
        ->from("gallery")
        ->where(array("gallery.status" => 1,"gallery.rel" => "banner"))
        ->order_by("gallery.order asc")
        ->get()
        ->result_object();

        if (!empty($gallery_list)) {
            foreach ($banner_list as $i => $banner) {
                $banner_list[$i]->gallery = array();

                foreach ($gallery_list as $gallery) {
                    if ($gallery->gallery__rel_id == $banner->banner__id) {
                        $banner_list[$i]->gallery[] = $gallery;
                    }
                }
            }
        }

        if (!empty($banner__id) || !empty($banner__nome)) {
            $banner_list = $banner_list[0];
        }

        return $banner_list;
    }
}

?>
