<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_url extends MY_Controller {

    function __construct() {
        parent::__construct();

        /* load model admin */
        $this->load->model('admin/admin_model');

        $this->admin_model->table = 'core_url';

        /* layout config for login */
        $this->layout = 'backend/layouts/backend';
        $this->body_cfg = 'class="withvernav"';

        $this->breadcrumbs[] = array('link' => 'javascript:void(0);', 'title' => 'Configurações');
        $this->breadcrumbs[] = array('link' => 'javascript:void(0);', 'title' => 'Url Control');

        /* info dislay */
        $this->data['info'] = array(
            'title' => 'Url Control',
            'description' => 'Controle de urls',
            'menu_active' => 'config',
            'submenu_active' => 'url'
        );

        /* validate */
        $this->validate = array(
            array(
                'field' => 'url',
                'label' => 'URL Nova',
                'rules' => 'required|url_title|trim|is_unique[core_url.url.id.' . $this->input->post('id') . ']'
            ),
            //array('field' => 'resource', 'label' => 'Recurso', 'rules' => 'required|trim'),
            array('field' => 'url_old', 'label' => 'URL Antiga', 'rules' => 'trim'),
            array('field' => 'title', 'label' => 'Título', 'rules' => 'trim'),
            array('field' => 'keywords', 'label' => 'Keywords', 'rules' => 'trim'),
            array('field' => 'description', 'label' => 'Descrição', 'rules' => 'trim'),
            array('field' => 'alt', 'label' => 'Alt', 'rules' => 'trim'),
            array('field' => 'cabecalho', 'label' => 'H1', 'rules' => 'trim'),
            array('field' => 'spam', 'label' => 'Spam', 'rules' => 'trim')
        );
    }

    public function index() {
        $query = $this->db->select('*')->from($this->admin_model->table)->order_by('id DESC')->get();
        $rs['data'] = $query->result_array();

        $this->data['content'] = $this->load->view('list', $rs, TRUE);

        $this->load->view('structure', $this->data);
    }

    public function edit($id = null) {
        if ($id) {
            $query = $this->db->select('*')->from($this->admin_model->table)->where(array('id' => $id))->get();
            $rs['data'] = $query->first_row('array');
        }

        $acos = $this->db->select('
                parent.id, 
                parent.parent_id, 
                CASE 
                    WHEN 
                        child.value IS NULL THEN parent.value
                    ELSE 
                        child.value
                END AS controller,
                CASE 
                    WHEN 
                        parent.parent_id IS NULL THEN ""
                    ELSE 
                        parent.value 
                END AS method
            ', false)
            ->from('core_acl_acos AS parent')
            ->join('core_acl_acos AS child', 'parent.parent_id = child.id', 'left')
            ->order_by('controller ASC, method ASC')
            ->get()
            ->result_array();

        $rs['acos'] = array();
        foreach ($acos as $aco) {
            //exceções a parte para os controllers
            $acosExcepts = array('Swfupload');

            //nao exibir as controller do Admin
            if (strpos($aco['controller'], 'Admin_') === false && in_array($aco['controller'], $acosExcepts) === false) {
                $rs['acos'][] = strtolower($aco['controller'] . ($aco['method'] ? '/' . $aco['method'] : ''));
            }
        }
        

        $this->data['content'] = $this->load->view('edit', $rs, TRUE);

        $this->load->view('structure', $this->data);
    }

    public function save() {
        $this->form_validation->set_rules($this->validate);

        if ($this->form_validation->run() == FALSE) {

            $this->error->set($this->validate);

            /* message return error */
            $this->admin_model->setAlert(array('type' => 'error', 'msg' => array('Erro no envio dos dados!')));
            redirect('/admin/' . $this->uri->segment(2) . '/edit/' . $this->input->post('id'), 'location');
        } else {
            $data = $this->input->post();
            $data['url_old'] .= (isset($data['url_old_param']) and !empty($data['url_old_param']))? '/' . $data['url_old_param'] : '';
            $data['url_old'] = str_replace(';[]', '[]', $data['url_old']);
            $data['url_redir'] = str_replace(';[]', '[]', $data['url_redir']);

            $this->admin_model->save($data);

            /* message return success */
            $this->admin_model->setAlert(array('type' => 'success', 'msg' => array('Dados salvos com sucesso!')));
            redirect('/admin/' . $this->uri->segment(2) . '/', 'location');
        }
    }

    public function delete() {
        $id = $this->input->post('id');

        /* only core_url is 1 (home page) */
        foreach ($id as $k => $v) {
            if ($v == '1') {
                unset($id[$k]);
            }
        }

        /* validate */
        if (is_array($id)) {
            $this->admin_model->delete($id);
            /* message return success */
            $this->admin_model->setAlert(array('type' => 'success', 'msg' => array('Dados excluídos com sucesso!')));
        } else {
            /* message return error */
            $this->admin_model->setAlert(array('type' => 'error', 'msg' => array('Nenhum parâmetro passado')));
        }

        redirect('/admin/' . $this->uri->segment(2) . '/', 'location');
    }

}

/* End of file admin_url.php */
/* Location: ./application/modules/admin_url/controllers/admin_url.php */