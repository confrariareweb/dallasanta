<div id="add" class="subcontent">
    <form id="form1" class="stdform" method="post" action="<?php echo site_url('admin/' . $this->uri->segment(2) . '/save'); ?>">
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
        <p>
            <label>URL</label>
            <span class="field">
                <input type="text" name="url" id="url" class="longinput <?php if ($this->error->form_error('url') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['url']) != '')
                    echo htmlspecialchars($data['url']);
                else
                    echo $this->error->set_value('url');
                ?>"  />
                       <?php echo ($this->error->form_error('url')); ?>
            </span>
        </p>
        
        <p style=" width: 33%; float: left;">
            <label>Controllers</label>
            <span class="field"  style="">
                <select name="url_old" id="url_old" style="">
                    <option><?php echo isset($data['url_old'])? $data['url_old'] : 'Nenhuma'; ?></option>
                    <?php foreach ($acos as $aco): ?>
                        <?php $url_old_exp = explode('/', $data['url_old']); ?>
                        <?php if($data['url_old'] == $aco): ?>
                            <option value="<?php echo $aco; ?>" selected="selected"><?php echo $aco; ?></option>
                        <?php else: ?>
                            <option value="<?php echo $aco; ?>"><?php echo $aco; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select> / 
                <?php echo($this->error->form_error('url_old')); ?>
            </span>
            
        </p>
        <p style=" width: 63%; float: left;">
            <!--label>Parâmetro</label-->
            <span class="">
                <input placeholder="Parametros - não obrigatório" type="text" name="url_old_param" id="url_old_param" class="longinput <?php if ($this->error->form_error('url_old_param') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['url']) != '')
                    echo htmlspecialchars($data['url_old_param']);
                else
                    echo $this->error->set_value('url_old_param');
                ?>"  />
                       <?php echo ($this->error->form_error('url_old_param')); ?>
            </span>
        </p>
        
        <p style="    clear: both;
    background: #0066ff;
    padding: 10px;
    border-radius: 10px;
        margin: 0 0 0 220px;
    width: 400px;
    text-align: left;
    color: #fff;"><span class="">Controller atual - <?php echo isset($data['url_old'])? $data['url_old'] : 'Nenhuma'; ?></span></p>
        <!--p>
            <label>URL Antiga</label>
            <span class="field">
                <input type="text" name="url_old" id="url_old" class="longinput" value="<?php
                if (htmlspecialchars($data['url_old']) != '')
                    echo htmlspecialchars($data['url_old']);
                else
                    echo $this->error->set_value('url_old');
                ?>"  />
                <small class="desc">Endereço após o domínio do site sem a barra ex: site.com.br/blog/pagina usar somente "blog/pagina"</small>
                <?php echo($this->error->form_error('url_old')); ?>
            </span>
        </p-->
        
        <p>
            <label>URL para redirecionar</label>
            <span class="field">
                <textarea cols="80" rows="5" name="url_redir" id="url_redir" class="longinput"><?php
                if (htmlspecialchars($data['url_redir']) != '')
                    echo htmlspecialchars($data['url_redir']);
                else
                    echo $this->error->set_value('url_redir');
                ?></textarea>
                <small class="desc">Endereço após o domínio do site sem a barra ex: site.com.br/blog/pagina usar somente "blog/pagina"</small>
                <?php echo($this->error->form_error('url_redir')); ?>
            </span>
        </p>

        <p>
            <label>Title</label>
            <span class="field"><input type="text" name="title" id="title" class="longinput" value="<?php
                if (htmlspecialchars($data['title']) != '')
                    echo htmlspecialchars($data['title']);
                else
                    echo $this->error->set_value('title');
                ?>"  /></span>
        </p>
		
		<p>
            <label>H1</label>
            <span class="field"><input type="text" name="h1" id="h1" class="longinput" value="<?php
                if (htmlspecialchars($data['h1']) != '')
                    echo htmlspecialchars($data['h1']);
                else
                    echo $this->error->set_value('h1');
                ?>"  /></span>
        </p>
		
		
        <p>
            <label>Keywords</label>
            <span class="field"><input type="text" name="keywords" id="keywords" class="longinput" value="<?php
                if (htmlspecialchars($data['keywords']) != '')
                    echo htmlspecialchars($data['keywords']);
                else
                    echo $this->error->set_value('keywords');
                ?>"  /></span>
        </p>
        <p>
            <label>Alt</label>
            <span class="field"><input type="text" name="alt" id="alt" class="longinput" value="<?php
                if (htmlspecialchars($data['alt']) != '')
                    echo htmlspecialchars($data['alt']);
                else
                    echo $this->error->set_value('alt');
                ?>"  /></span>
        </p>
        <p>
            <label>Descrição</label>
            <span class="field"><textarea cols="80" rows="5" name="description" id="description" class="longinput"><?php
                    if (htmlspecialchars($data['description']) != '')
                        echo htmlspecialchars($data['description']);
                    else
                        echo $this->error->set_value('description');
                    ?></textarea></span> 
        </p>
        <p>
            <label>Spam</label>
            <span class="field"><textarea cols="80" rows="5" name="spam" id="spam" class="longinput"><?php
                    if (htmlspecialchars($data['spam']) != '')
                        echo htmlspecialchars($data['spam']);
                    else
                        echo $this->error->set_value('spam');
                    ?></textarea></span> 
        </p>

        <p class="stdformbutton">
            <button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>