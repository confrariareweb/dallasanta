<div id="add" class="subcontent">
	<form id="form1" class="stdform" method="post" action="<?php echo site_url('admin/'.$this->uri->segment(2).'/save'); ?>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
        <p>
            <label>Nome</label>
            <span class="field">
                <input type="text" name="name" id="store" readonly="readonly" class="smallinput <?php if($this->error->form_error('name') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data['name']) != '') echo htmlspecialchars($data['name']); else echo $this->error->set_value('name');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Nome é obrigatório'}}}" />
                <?php  echo ($this->error->form_error('name')); ?>
            </span>
        </p>
        <p>
        	<label>E-mail</label>
            <span class="field">
                <input type="text" name="email" id="contact" readonly="readonly" class="smallinput" value="<?php if(htmlspecialchars($data['email']) != '') echo htmlspecialchars($data['email']); ?>" />
            </span>
        </p>
        <div class="clearall"></div>
        <p class="stdformbutton">
        	<button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>


