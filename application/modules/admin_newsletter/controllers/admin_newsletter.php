<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_newsletter extends MY_Controller {

	function __construct() {
		parent::__construct();

		/* load model admin */
		$this->load->model('admin/admin_model');
		$this->admin_model->directory = './upload/stores/';

		$this->admin_model->primary_key = 'email';
		$this->admin_model->table = 'newsletter';

		/* layout config for login */
		$this->layout 	= 'backend/layouts/backend';
		$this->body_cfg = 'class="withvernav"';

		$this->breadcrumbs[] = array('link'=>'javascript:void(0);','title'=>'Newsletter');
		$this->breadcrumbs[] = array('link'=>'javascript:void(0);','title'=>'Controle de Newsletter');

		/* info dislay */
		$this->data['info'] = array(
			'title' 		 => 'Newsletter',
			'description' 	 => 'Controle de Newsletter',
			'menu_active' 	 => 'dashboard',
			'submenu_active' => 'newsletter'
		);

		/* validate */
		$this->validate = array(
           array(
                 'field' => 'name',
                 'label' => 'Nome',
                 'rules' => 'required|trim'
              )
        );
	}

	public function index() {
		$query 	=
			$this->db
				->select('*',FALSE)
				->from($this->admin_model->table)
				->order_by('email ASC')
				->get();
		$rs['data'] = $query->result_array();

		$this->data['content'] = $this->load->view('list',$rs,TRUE);
		$this->load->view('structure',$this->data);
	}

	public function edit($email) {
		if(isset($email)):
			$this->db->select('*',FALSE);
			$this->db->from($this->admin_model->table);
			$query 	= $this->db->where(array('email'=>$email))->get();
			$rs['data'] = $query->first_row('array');

		endif;

		$this->data['content'] = $this->load->view('edit',$rs,TRUE);
		$this->load->view('structure',$this->data);
	}

	public function save() {
		$this->form_validation->set_rules($this->validate);

		if ($this->form_validation->run() == FALSE) {

			$this->error->set($validate);

			/* message return error */
			$this->admin_model->setAlert(array('type'=>'error','msg'=>array('Erro no envio dos dados!')));
			redirect('/admin/'.$this->uri->segment(2).'/edit/'.$this->input->post('email'), 'location');
		}
		else {
			$data = $this->input->post(NULL,TRUE);
			$email = $this->admin_model->save($data);

			/* message return success */
			$this->admin_model->setAlert(array('type'=>'success','msg'=>array('Dados salvos com sucesso!')));
			redirect('/admin/'.$this->uri->segment(2), 'location');
		}
	}

	public function delete() {
		$email = $this->input->post('email');

		/* validate */
		if(is_array($email)):
			$this->admin_model->delete($email);

			/* message return success */
			$this->admin_model->setAlert(array('type'=>'success','msg'=>array('Dados excluídos com sucesso!')));
		else:
			/* message return error */
			$this->admin_model->setAlert(array('type'=>'error','msg'=>array('Nenhum parâmetro passado')));
		endif;

		redirect('/admin/'.$this->uri->segment(2), 'location');
	}
}

/* End of file admin_usergroup.php */
/* Location: ./application/modules/admin_usergroup/controllers/admin_usergroup.php */