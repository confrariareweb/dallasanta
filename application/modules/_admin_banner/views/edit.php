<div id="add" class="subcontent">
    <form id="form1" class="stdform" method="post" action="<?=site_url('admin/'.$this->uri->segment(2).'/save'); ?>" enctype="multipart/form-data" >
        <input type="hidden" name="id" value="<?=$data->banner__id; ?>">
        <input type="hidden" name="nome" value="<?=$data->banner__nome; ?>">

        <p>
            <label>Título</label>
            <span class="field">
                <input type="text" name="titulo" id="titulo" style="width:40%" class="longinput <?php if($this->error->form_error('titulo') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->banner__titulo) != '') echo htmlspecialchars($data->banner__titulo); else echo $this->error->set_value('titulo');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Título é obrigatório'}}}" />
                <?=$this->error->form_error("titulo"); ?>
            </span>
        </p>

        <!--
        <p>
            <label>Sub-Título</label>
            <span class="field">
                <input type="text" name="subtitulo" id="subtitulo" class="longinput <?php if($this->error->form_error('subtitulo') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->banner__subtitulo) != '') echo htmlspecialchars($data->banner__subtitulo); else echo $this->error->set_value('subtitulo');  ?>" data-validate="{validate:{required:false, messages:{required:'O campo subtitulo é obrigatório'}}}" />
                <?=$this->error->form_error("subtitulo"); ?>
            </span>
        </p>
         -->

        <p>
            <label>Tipo/Categoria</label>
            <span class="field">
                <?=form_dropdown("tipo_id",$bannertipo_list,$data->bannertipo__id,""); ?>
            </span>
        </p>
        <p>
            <label>Ordem</label>
            <span class="field">
                <?=form_dropdown("ordem",$ordem_list,$data->banner__ordem,""); ?>
            </span>
        </p>
        <p>
            <label>Status</label>
            <span class="field">
                <?=form_dropdown("status",$status_list,$data->banner__status,""); ?>
            </span>
        </p>
        <p>
            <label>URL</label>
            <span class="field">
                <input type="text" name="url" id="url" style="width:30%" class="longinput <?php if($this->error->form_error('url') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data->banner__url) != '') echo htmlspecialchars($data->banner__url); else echo $this->error->set_value('url');  ?>" data-validate="{validate:{required:false, messages:{required:'O campo URL é obrigatório'}}}" />
                <?=$this->error->form_error("url"); ?>
            </span>
        </p>

        <!--
        <p>
            <label>Descricao</label>
            <span class="field">
                <textarea id="descricao" name="descricao" rows="15" style="width: 80%" class="tinymce-basic"><?php if(htmlspecialchars($data->banner__descricao) != '') echo htmlspecialchars($data->banner__descricao); ?></textarea>
            </span>
        </p>
        -->

        <p>
            <label>Imagem (1920x416)</label>
            <span class="field">
                <?php
                    echo form_upload(array('name'=>'img'));
                    echo '<br/>';
                    if (read_file($this->admin_model->directory.$data->banner__img)):
                        echo img(array('src'=>image("media/".$this->admin_model->directory.$data->banner__img,"0x150"),'class'=>'thumb_image'));
                        echo '<br/>';
                        echo form_checkbox(array('name'=>'delete_img','id'=>'delete_img','value'=>'1'));
                        echo "Deletar imagem?";
                    endif;
                ?>
            </span>
        </p>

        <p>
            <label>Imagem mobile (767x416)</label>
            <span class="field">
                <?php
                    echo form_upload(array('name'=>'img_mobile'));
                    echo '<br/>';
                    if (read_file($this->admin_model->directory.$data->banner__img_mobile)):
                        echo img(array('src'=>image("media/".$this->admin_model->directory.$data->banner__img_mobile,"0x100"),'class'=>'thumb_image'));
                        echo '<br/>';
                        echo form_checkbox(array('name'=>'delete_img','id'=>'delete_img','value'=>'1'));
                        echo "Deletar imagem?";
                    endif;
                ?>
            </span>
        </p>

        <?php
            //$this->swfu->run($data->banner__id);
        ?>

        <p class="stdformbutton">
            <button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>
