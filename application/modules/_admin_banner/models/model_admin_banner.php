<?php

class Model_Admin_Banner extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function bannerList($banner__id = null, $banner__nome = null, $bannertipo__id = null, $bannertipo__nome = null) {
        $order_by ="banner.ordem asc";
        $where_list = array("banner.status in(0,1)" => null);

        if (!empty($banner__id)) {
            $where_list["banner.id"] = $banner__id;
        }

        if (!empty($banner__nome)) {
            $where_list["banner.nome"] = $banner__nome;
        }

        if (!empty($bannertipo__id)) {
            $where_list["bannertipo.id"] = $bannertipo__id;
        }

        if (!empty($bannertipo__nome)) {
            $where_list["bannertipo.nome"] = $bannertipo__nome;
        }

        $banner_list = $this->db->select("
            banner.id id,
            banner.id banner__id,
            banner.img banner__img,
            banner.img_mobile banner__img_mobile,
            banner.tipo_id banner__tipo_id,
            banner.nome banner__nome,
            banner.titulo banner__titulo,
            banner.subtitulo banner__subtitulo,
            banner.descricao banner__descricao,
            banner.url banner__url,
            banner.ordem banner__ordem,
            banner.status banner__status,
            banner.dateupdate banner__dateupdate,
            bannertipo.id bannertipo__id,
            bannertipo.nome bannertipo__nome,
            bannertipo.titulo bannertipo__titulo,
            bannertipo.dateupdate bannertipo__dateupdate,")
        ->from("banner")
        ->join("bannertipo", "bannertipo.id = banner.tipo_id")
        ->order_by($order_by)
        ->where($where_list)
        ->get()
        ->result();

        return $banner_list;
    }
}

?>
