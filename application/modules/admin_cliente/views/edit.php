<div id="add" class="subcontent">
    <form id="form1" class="stdform" method="post" action="<?php echo site_url('admin/' . $this->uri->segment(2) . '/save'); ?>" enctype="multipart/form-data" >
        <input type="hidden" name="id_cliente" value="<?php echo $data['id_cliente']; ?>" />

        <p>
            <label>Nome</label>
            <span class="field">
                <input type="text" name="nome" id="nome" class="longinput <?php if ($this->error->form_error('nome') != '') echo 'error'; ?>" value="<?php
                if (htmlspecialchars($data['nome']) != '')
                    echo htmlspecialchars($data['nome']);
                else
                    echo $this->error->set_value('nome');
                ?>" data-validate="{validate:{required:true, messages:{required:'O campo Nome é obrigatório'}}}" />
                       <?php echo ($this->error->form_error('nome')); ?>
            </span>
        </p>

        <p>
            <label>Link para o site</label>
            <span class="field">
                <input type="text" name="url_site" id="url_site" class="<?php if ($this->error->form_error('url_site') != '') echo 'error'; ?>" style="width:30%;" value="<?php
                if (htmlspecialchars($data['url_site']) != '')
                    echo htmlspecialchars($data['url_site']);
                else
                    echo $this->error->set_value('url_site');
                ?>" data-validate="{validate:{url:true, messages:{url:'Informe uma URL válida'}}}" />
                       <?php echo ($this->error->form_error('url_site')); ?>
            </span>
        </p>

        <p>
            <label>Imagem (100x60)</label>
            <span class="field">
                <?php
                    echo form_upload(array('name'=>'img', 'class'=>'longinput'));
                    echo '<br/>';
                    if (read_file($this->admin_model->directory.$data['img'])):
                        echo img(array('src'=>image("media/".$this->admin_model->directory.$data['img'],"0x60"),'class'=>'thumb_image'));
                        echo '<br/>';
                        echo form_checkbox(array('name'=>'delete_img','id'=>'delete_img','value'=>'1'));
                        echo "Deletar imagem?";
                    endif;
                ?>
            </span>
        </p>

        <p class="stdformbutton">
            <button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>