<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_cliente extends MY_Controller {

    function __construct() {
        parent::__construct();

        /* load model admin */
        $this->load->model('admin/admin_model');

        $this->admin_model->primary_key = 'id_cliente';
        $this->admin_model->table = 'cliente';
        $this->admin_model->directory = './upload/clientes/';

        /* layout config for login */
        $this->layout = 'backend/layouts/backend';
        $this->body_cfg = 'class="withvernav"';

        $this->breadcrumbs[] = array('link' => 'javascript:void(0);', 'title' => 'Clientes');

        /* info dislay */
        $this->data['info'] = array(
            'title' => 'Clientes',
            'description' => 'Cadastro de clientes',
            'menu_active' => 'dashboard'
        );

        /* validate */
        $this->validate = array(
            array(
                'field' => 'nome',
                'label' => 'Nome',
                'rules' => 'required|trim'
            )
        );
    }

    public function index($list) {
        $login = $this->session->userdata('admin');

        $this->data['content'] = $this->load->view('list', $rs, TRUE);
        $this->load->view('structure', $this->data);

        // dynamic table
        if ($list == TRUE):
            $sIndexColumn = 'id_cliente';
            $aColumns = array('1', 'id_cliente', 'nome', 'img', 'url_site');
            $aColumns2 = array('1', 'id_cliente', 'nome');

            // sql list
            $this->db->select(' SQL_CALC_FOUND_ROWS ' . implode(", ", $aColumns), FALSE);
            $this->db->from($this->admin_model->table);


            // Total data set length
            $query = $this->db->query('SELECT COUNT(1) as total FROM ' . $this->admin_model->table)->first_row('array');
            $iTotal = $query['total'];

            // get $_GET values
            $get = $this->input->get(NULL, TRUE);
            extract($get);

            // pagination
            if (isset($get['iDisplayStart']) && $get['iDisplayLength'] != '-1')
                $this->db->limit($get['iDisplayLength'], $get['iDisplayStart']);

            // order
            if (isset($get['iSortCol_0'])) {
                for ($i = 0; $i < intval($get['iSortingCols']); $i++)
                    if ($get['bSortable_' . intval($get['iSortCol_' . $i])] == "true")
                        $this->db->order_by($aColumns2[intval($get['iSortCol_' . $i])], $get['sSortDir_' . $i]);
            } else {
                $this->db->order_by($sIndexColumn, 'DESC');
            }

            // filter
            if (isset($get['sSearch']) && $get['sSearch'] != "")
                for ($i = 0; $i < count($aColumns); $i++)
                    $this->db->or_having("{$aColumns2[$i]} LIKE ", "%{$get[sSearch]}%", FALSE);

            // filter individual
            for ($i = 0; $i < count($aColumns); $i++)
                if (isset($get['bSearchable_' . $i]) && $get['bSearchable_' . $i] == "true" && $get['sSearch_' . $i] != '')
                    $this->db->having("{$aColumns2[$i]} LIKE ", "%" . $get['sSearch_' . $i] . "%", FALSE);

            $rResult = $this->db->get()->result_array();

            // Data set length after filtering
            $query = $this->db->query('SELECT FOUND_ROWS()')->first_row('array');
            $iFilteredTotal = $query['FOUND_ROWS()'];

            // output
            $output = array("sEcho" => intval($get['sEcho']), "iTotalRecords" => $iTotal, "iTotalDisplayRecords" => $iFilteredTotal, "aaData" => array());

            foreach ($rResult as $aRow):
                $row = array();
                $row[] = '<span class="center" style="padding-left: 36px;">' . form_checkbox($data = array('name' => 'delete_id[]', 'value' => $aRow[$sIndexColumn])) . '</span>';
                for ($i = 1; $i < count($aColumns); $i++):
                    // output formatting
                    $row[] = $aRow[$aColumns2[$i]];
                endfor;
                $output['aaData'][] = $row;
            endforeach;
            die(json_encode($output));
        endif;
    }

    public function edit($id) {
        $login = $this->session->userdata('admin');

        if (isset($id)):
            $rs['data'] = $this->db->where(array('id_cliente' => $id))->get($this->admin_model->table)->first_row('array');
        endif;

        $rs['parceiros'] = $this->db->get($this->admin_model->table)->result_array();

        $this->data['content'] = $this->load->view('edit', $rs, TRUE);

        $this->load->view('structure', $this->data);
    }

    public function save() {
        $this->form_validation->set_rules($this->validate);

        if ($this->form_validation->run() == FALSE) {
            $this->error->set($this->validate);

            /* message return error */
            $this->admin_model->setAlert(array('type' => 'error', 'msg' => array('Erro no envio dos dados!')));
            redirect('/admin/' . $this->uri->segment(2) . '/edit/' . $this->input->post('id_cliente'), 'location');
        } else {
            $data = $this->input->post(NULL, TRUE);

            $id = $this->admin_model->save($data);

            /* message return success */
            $this->admin_model->setAlert(array('type' => 'success', 'msg' => array('Dados salvos com sucesso!')));
            redirect('/admin/' . $this->uri->segment(2), 'location');
        }
    }

    public function delete() {
        $id = $this->input->post('id');

        /* validate */
        if (is_array($id)):
            $this->admin_model->delete($id);

            /* message return success */
            $this->admin_model->setAlert(array('type' => 'success', 'msg' => array('Dados excluídos com sucesso!')));
        else:
            /* message return error */
            $this->admin_model->setAlert(array('type' => 'error', 'msg' => array('Nenhum parâmetro passado')));
        endif;

        redirect('/admin/' . $this->uri->segment(2), 'location');
    }

}