<div id="add" class="subcontent">
   	<form id="form1" class="stdform" method="post" action="<?php echo site_url('admin/'.$this->uri->segment(2).'/save'); ?>" enctype="multipart/form-data" >
   		<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
        <p>
        	<label>Título</label>
            <span class="field">
            	<input type="text" name="title" id="title" class="longinput <?php if($this->error->form_error('title') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data['title']) != '') echo htmlspecialchars($data['title']); else echo $this->error->set_value('title');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Título é obrigatório'}}}" />
	            <?php  echo ($this->error->form_error('title')); ?>
	        </span>
        </p>
        <p>
        	<label>Vínculo</label>
            <span class="field">
            	<input type="text" name="binding" id="binding" class="longinput <?php if($this->error->form_error('binding') != '') echo 'error'; ?>" value="<?php if(htmlspecialchars($data['binding']) != '') echo htmlspecialchars($data['binding']); else echo $this->error->set_value('binding');  ?>" data-validate="{validate:{required:true, messages:{required:'O campo Vínculo é obrigatório'}}}" />
	            <?php  echo ($this->error->form_error('binding')); ?>
	        </span>
        </p>

        <p>
            <label>Conteúdo</label>
            <!-- <textarea id="content" name="content" rows="25" style="width: 80%" class="tinymce"><?php if(htmlspecialchars($data['content']) != '') echo htmlspecialchars_decode($data['content']); else echo $this->error->set_value('content');  ?></textarea> -->
            <textarea name="content" rows="25" style="width:73.5%; height:200px;"><?php echo $data['content'] ?></textarea>
        </p>

        <p class="stdformbutton">
        	<button class="submit radius2">Enviar</button>
        </p>
    </form>
</div>