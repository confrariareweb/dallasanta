<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

/**
 * MY_LOADER
 *
 * @description Class for general loader
 * @extends MX_Loader
 * @author Eduardo Messias (eduardo.inf@gmail.com)
 */

class MY_Loader extends MX_Loader {
    function __construct() {
        parent::__construct();
    }

    /**
     * front_view
     *
     * @description Carrega uma view no frontend, utilizando o próprio load do CI
     * @params string|constant FRONTEND_VIEW O diretório onde as views do frontend estão localizadas
     * @params string|constant VIEW_DIR O diretório onde a view está localizada, caso não esteja na raíz
     * @params string $view A view a ser carregada
     * @params array $data Os dados a serem enviados para a view
     * @params bool $behavior Se TRUE, retorna o conteúdo da view como string
     * @return void
     */
    public function front_view($view, $data = NULL, $behavior = FALSE) {
        try {
            
            if (!defined('FRONTEND_VIEW'))
                throw new Exception('Informe o diretório das views do frontend no arquivo config/constants.php');
            
            if (!$view)
                throw new Exception('Informe a view a ser carregada.');
            
            $echo = $dir = NULL;
            $view = str_replace('_', '-', $view);
            
            if (defined('VIEW_DIR'))
                $dir = VIEW_DIR.'/';
            
            if ($behavior === TRUE)
                $echo = 'echo ';
            
            $str = $echo . '$this->view(FRONTEND_VIEW.$dir.$view, $data, $behavior);';
            eval($str);
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
}
