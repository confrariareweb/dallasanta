<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Layout Class
 *
 * @description Implements the type layout views in the framework.
 * @author Eduardo Messias (eduardo.inf@gmail.com)
 * @package core
 */
define('LAYOUTPATH', APPPATH . 'views/');

class MY_Output extends CI_Output {

    public $base_url;
    public $name_layout = 'frontend/layouts/default.php';

    /**
     * _display
     *
     * @descrtiption Init config layout. This method call by config/hooks.php
     * @author Eduardo Messias (eduardo.inf@gmail.com)
     * @return
     */
    public function _display($output = '') {

        if (class_exists('CI_Controller')) {
            $CI = & get_instance();
            $CI->load->helper('html');
            $CI->load->helper('SimpleHtmlDom');
            $this->CI = $CI;

            $this->base_url = $CI->config->slash_item('base_url');

            ob_start();
            $content = $output = parent::_display(); //die($output);
            $output = ob_get_contents();
            ob_get_clean();

            // escape for ajax
            if (!isset($CI->layout_skip_init)):
                if (isset($CI->layout)) {
                    switch (current(explode('/', $CI->layout))) {
                        default:
                            $_ = $this->init_frontend();
                            break;
                        case 'backend':
                            $_ = $this->init_backend();
                            break;
                    }
                } else
                    $_ = $this->init_frontend();

                // Links CSS and JS call by controller
                $_['css'] = '';
                if (isset($CI->css))
                    foreach ($CI->css as $_css)
                        $_['css'] .= link_tag($_css);

                $_['js'] = '';
                if (isset($CI->js))
                    foreach ($CI->js as $_js)
                        $_['js'] .= script_tag($_js);

                $_['body_cfg'] = (isset($CI->body_cfg)) ? $this->add_body_cfg($CI->body_cfg) : '';

                if (isset($CI->layout) && !preg_match('/(.+).php$/', $CI->layout))
                    $CI->layout .= '.php';
                else
                    $CI->layout = $this->name_layout;

                $layout = LAYOUTPATH . $CI->layout;

                if ($CI->layout !== $this->name_layout && !file_exists($layout))
                    if ($CI->layout != '.php')
                        show_error("You have specified a invalid layout: " . $CI->layout);

                if (file_exists($layout)) {
                    $layout = $CI->load->file($layout, true);

                    $this->_arReplace[] = array('key' => '{main}', 'content' => $output);

                    foreach ($_ as $k => $v)
                        $this->_arReplace[] = array('key' => '{' . $k . '}', 'content' => $v);
                    foreach ($this->_arReplace as $rs)
                        $layout = str_replace($rs['key'], $rs['content'], $layout);

                    $view = $layout;
                } else
                    $view = $output;
            else:
                $view = $output;
            endif;

            echo url_seo($view, $CI->core_url);

            // save full cache
            if ($this->cache_expiration > 0 && isset($CI) && !method_exists($CI, '_output')) {
                $this->_write_cache($view);
            }
        } else {
            echo $output;
        }
    }

    /**
     * init_frontend
     *
     * @description Config a layout from frontend
     * @author Eduardo Messias (eduardo.inf@gmail.com)
     * @return void
     */
    private function init_frontend() {

        $CI = $this->CI;

        if (isset($CI->core_config))
            extract($CI->core_config);

		if(strrpos($_SERVER['REQUEST_URI'],'-aluguel-')){
			$_temp_title = str_replace('-',' ',urldecode(current(explode('-aluguel-',end(explode('/',$_SERVER['REQUEST_URI']))))));
			$_temp_title.= ' - Dallasanta.';
		}else if(strrpos($_SERVER['REQUEST_URI'],'-venda-')){
			$_temp_title = str_replace('-',' ',urldecode(current(explode('-venda-',end(explode('/',$_SERVER['REQUEST_URI']))))));
			$_temp_title.= ' - Dallasanta.';
		}else{
			$_temp_title = null;
		}

        // SEO default tags
		$_['title'] = (isset($CI->core_url['title'])) ? $CI->core_url['title'] : ((isset($_temp_title))? $_temp_title : $meta_tag_title);
        $_['keywords'] = (isset($CI->core_url['keywords'])) ? $CI->core_url['keywords'] : ((isset($meta_tag_keywords)) ? $meta_tag_keywords : NULL);
        $_['description'] = (isset($CI->core_url['description'])) ? $CI->core_url['description'] : ((isset($meta_tag_description)) ? $meta_tag_description : NULL);
        $_['spam'] = (isset($CI->core_url['spam'])) ? $CI->core_url['spam'] : ((isset($meta_tag_spam)) ? $meta_tag_spam : NULL);
        $_['author'] = (isset($_meta_tag_author)) ? $_meta_tag_author : NULL;
        $_['copyright'] = (isset($_meta_tag_copyright)) ? $_meta_tag_author : NULL;
		
		$_['alt'] = (isset($CI->core_url['alt'])) ? $CI->core_url['alt'] : $meta_tag_alt;
		$_['h1'] = (isset($CI->core_url['h1'])) ? $CI->core_url['h1'] : $meta_tag_h1;

        // Open Graph tags
        $_['og_type'] = (isset($facebook_og_type)) ? $facebook_og_type : NULL;
        $_['og_site_name'] = (isset($facebook_og_site_name)) ? $facebook_og_site_name : NULL;
        $_['fb_admins'] = (isset($facebook_fb_admins)) ? $facebook_fb_admins : NULL;
        $_['og_image'] = (isset($facebook_og_image)) ? base_url() . 'upload/config/' . $facebook_og_image : NULL;
        $_['og_title'] = $_['title'];
        //$_['og_url'] = isset($facebook_og_url) ? $facebook_og_url : $this->base_url . $_CI->router->queryString;
        $_['og_url'] = isset($facebook_og_url) ? $facebook_og_url : $this->base_url . $CI->router->queryString;
        $_['og_description'] = isset($facebook_og_description) ? $facebook_og_description : $_['description'];

        // GET a Subviews
        $top = 'top';
        $bottom = 'bottom';
        /*if ($CI->uri->rsegments[1] == 'portal' and $CI->uri->rsegments[2] != 'login') {
            $top = 'portal_top';
            $bottom = 'portal_bottom';
        }*/
        $CI->load->helper('captcha');
        if ($post['captcha']) {

            // Verifica se o captcha corresponde (se existir)
            check_captcha('SITE_CADASTRO_CLIENTE');
            unset($post['captcha']);
        }
        $this->_arReplace[] = array('key' => '{top}', 'content' => $CI->load->view('frontend/includes/' . $top, '', true));
        $this->_arReplace[] = array('key' => '{bottom}', 'content' => $CI->load->view('frontend/includes/' . $bottom, '', true));
        $this->_arReplace[] = array('key' => '{breadcrumb}', 'content' => $CI->load->view('frontend/includes/breadcrumb', '', true));

        //$this->menu_vehicles = $this->vehicles_model->getMenu();

        return array_map('htmlspecialchars', $_);
    }

    /**
     * init_backend
     *
     * @description Config a layout from backend
     * @author Eduardo Messias (eduardo.inf@gmail.com)
     * @return void
     */
    private function init_backend() {
        $CI = $this->CI;

        /* load authentication model */
        $CI->load->model('authentication_model');
        $CI->authentication_model->initialize('1,2', 'admin');

        $this->name_layout = 'backend/layouts/backend.php';

        extract($CI->core_config);

        // SEO default tags
        $_['title'] = $CI->core_url['title'] != '' ? $CI->core_url['title'] : $meta_tag_backend_title;
        $_['keywords'] = $CI->core_url['keywords'] != '' ? $CI->core_url['keywords'] : $meta_tag_backend_keywords;
        $_['description'] = $CI->core_url['description'] != '' ? $CI->core_url['description'] : $meta_tag_backend_description;
        $_['spam'] = $CI->core_url['spam'] != '' ? $CI->core_url['spam'] : $meta_tag_backend_spam;
        $_['author'] = $_meta_tag_backend_author;
        $_['copyright'] = $_meta_tag_backend_copyright;

        if ($CI->authentication_model->check_auth()):
            // GET a Subviews for a login user on backend
            $login = $CI->session->userdata('admin');
            $query = $CI->db->select('*')->from('user')->where(array('id' => $login['user_id']))->get();
            $data['user'] = $query->first_row('array');

            $this->_arReplace[] = array('key' => '{top}', 'content' => $CI->load->view('backend/includes/top', $data, true));
            $this->_arReplace[] = array('key' => '{menu}', 'content' => $CI->load->view('backend/includes/menu', $data, true));
        endif;

        return array_map('htmlspecialchars', $_);
    }

    /**
     * add_body_cfg
     *
     * @description Create cfg for body
     * @author Eduardo Messias (eduardo.inf@gmail.com)
     * @params $links array data files js
     * @return void
     */
    private function add_body_cfg($cfg) {
        return $cfg;
    }

}
