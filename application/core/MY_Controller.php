<?php

class MY_Controller extends CI_Controller {
    function __construct() {
        date_default_timezone_set("America/Sao_Paulo");
        setlocale(LC_MONETARY,"pt_BR.UTF-8");

        parent::__construct();

        $this->coreUrl();


        $this->body_cfg = "class=''";
        $this->no_layout = false;
        $this->restricted_area = false;
        $this->breadcrumb = array();

        $this->load->model('model_banner');
        $this->load->model('model_coreconfig');
        $this->load->model('model_staticblock');
        $this->load->model('model_cliente');
        $this->load->model('model_imovel');

        $coreconfig_logo = $this->model_coreconfig->filter(
            $coreconfig__option = "general_logo",
            $coreconfig__group = null);

        $this->coreconfig_logo = $coreconfig_logo;

        $coreconfig_facebook = $this->model_coreconfig->filter(
            $coreconfig__option = "social_facebook",
            $coreconfig__group = null);

        $this->coreconfig_facebook = $coreconfig_facebook;

        $this->model_imovel->type = 'banners';
        $filtro = array(
            array(
                'term' => 'type',
                'operator' => 'equals',
                'value' => 'Banner - Home'
            )
        );
        $this->banners_conceitual = $this->model_imovel->get_imoveis($filtro);

        $filtro = array(
            array(
                'term' => 'type',
                'operator' => 'equals',
                'value' => 'Clientes'
            )
        );
        $this->logos_bottom = array_chunk($this->model_imovel->get_imoveis($filtro), 5);

        $this->model_imovel->type = 'imoveis';
        // Busca os clientes
        $this->clientes_footer = array_chunk($this->model_cliente->get_cliente(), 5);

        // Busca principal
        if ($this->uri->rsegments[2] != 'ordem') {

            if ($this->uri->rsegments[1] == 'imoveis') {
                $method = $this->uri->rsegments[2];

                if ($method == 'aluguel' || $method == 'venda') {
                    $tipo_imovel = ucfirst($method);
                } else{
                    if (isset($_GET) && !empty($_GET)) {
                        if (isset($_GET['tipo-imovel']) && !empty($_GET['tipo-imovel'])) {
                            $tipo_imovel = $_GET['tipo-imovel'];
                        }
                    }
                }
            }

            if (!isset($tipo_imovel))
                $tipo_imovel = 'Aluguel';


            $cache_dir = './application/cache';
            if (!is_dir($cache_dir)) // Verifica se a pasta cache existe
                if (!mkdir($cache_dir, 0777)) // Cria a pasta
                    die('A pasta cache deve ser criada.');


            $this->load->library('cache');
            //$imoveis = $this->cache->get(sprintf('imoveis-%s', strtolower($tipo_imovel)));

            // Verifica se existe cache e atribui à $imoveis
            if (!$imoveis = $this->cache->get('imoveis')) {
                // Se não existir, busca os imóveis
                /*$filtro = array(
                    array(
                        'term' => 'businessType',
                        'operator' => 'equals',
                        'value' => $tipo_imovel
                    )
                );*/
                $filtro = NULL;

                // Busca os imóveis
                $imoveis = $this->model_imovel->get_imoveis($filtro, NULL, 0);

                // Gera um arquivo de cache por 1 hora
                $this->cache->write($imoveis, 'imoveis', 3600);
            }

            if ($imoveis->total > 0) {
                $imoveis = $imoveis->imoveis;

                $categorias = $cidades = $bairros = array();
                foreach ($imoveis as $imovel) {
                    if (!in_array($imovel->categoria, $categorias))
                        $categorias[] = $imovel->categoria;

                    if (!in_array($imovel->endereco->city, $cidades))
                        $cidades[] = $imovel->endereco->city;

                    //if (!in_array($imovel->endereco->district, $bairros)) {
                        $bairros[] = $imovel->endereco->district;

                        $filtro_cidades = $cidades;
                        if (isset($_GET['cidades']) && is_array($_GET['cidades']))
                            $filtro_cidades = array_values(array_filter($_GET['cidades']));

                        if (in_array($imovel->endereco->city, $filtro_cidades)) {
                            if (!isset($bairros_by_cidade[$imovel->endereco->city]))
                                $bairros_by_cidade[$imovel->endereco->city]['cidade'] = $imovel->endereco->city;
                            if(!in_array($imovel->endereco->district, $bairros_by_cidade[$imovel->endereco->city]['bairros'])){
                                $bairros_by_cidade[$imovel->endereco->city]['bairros'][] = $imovel->endereco->district;
                            }
                        }
                    //}
                }
                $bairros = $bairros_by_cidade;


               

                foreach ($imoveis as $key => $i) {
                	foreach ($categorias as $key => $c) {
                 		if($i->tipo_imovel[0] == 'Aluguel' && $c == $i->categoria){	
                			$aluguel[] = $i->categoria;		
                	 	}
                        if($i->tipo_imovel[1] == 'Aluguel' && $c == $i->categoria){
                            $aluguel[] = $i->categoria;
                        }
                	}
                	 
                }
            	$result_aluguel = array_unique($aluguel);

            	foreach ($imoveis as $key => $i) {
                	foreach ($categorias as $key => $c) {
                		if($i->tipo_imovel[0] == 'Venda' && $c == $i->categoria){

                			$venda[] = $i->categoria;		
                	 	}
                        if($i->tipo_imovel[1] == 'Venda' && $c == $i->categoria){

                            $venda[] = $i->categoria;
                        }
                	}
                	 
                }
            	$result_venda = array_unique($venda);

                sort($categorias);
                sort($cidades);
                sort($bairros);

                foreach($bairros as $key => $b){
                    #novo_bairro[] = $b;

                    if($b['cidade'] == 'Porto Alegre'){
                        $porto = $b;
                        unset($bairros[$key]);
                    }

                }
                array_unshift($bairros, $porto);


               // echo '<pre>';print_r($bairros);die();

                $this->busca_principal = array(
                    //'imoveis' => $imoveis,
                    'categorias' => array_filter($categorias),
                    'categorias_venda' => array_filter($result_venda),
                    'categorias_aluguel' => array_filter($result_aluguel),
                    'cidades' => array_filter($cidades),
                    'bairros' => array_filter($bairros),
                    'checked' => (object) array()
                );


                $aluguel_checked = $venda_checked = NULL;
                $controller = $this->uri->segments[1];
                $method = $this->uri->segments[2];

                if (isset($_GET['tipo-imovel'])) {
                    if ($_GET['tipo-imovel'] == 'Aluguel'){
                        $aluguel_checked = 'checked';
                    }else{
                        $venda_checked = 'checked';
                    }
                } else {
                    if ($controller == 'imoveis') {
                        if ($method == 'venda'){
                            $venda_checked = 'checked';
                        }else{
                            $aluguel_checked = 'checked';
                        }
                    }else if($controller == 'venda'){
                        $venda_checked = 'checked';
                    }else if($controller == 'aluguel'){
                        $aluguel_checked = 'checked';
                    } else {
                        $aluguel_checked = 'checked';
                    }
                }

                $this->busca_principal['checked']->aluguel = $aluguel_checked;
                $this->busca_principal['checked']->venda = $venda_checked;
                
            }

        }

    }

    private function coreUrl() {
        // bizarro
        $CI =& get_instance();

        $rs = $CI->db->get("core_url");

        foreach($rs->result() as $r) {
            $CI->core_url["var"][$r->url_old] = base_url().$r->url;
        }

        if ($CI->router->core_url)
            foreach($CI->router->core_url as $k => $v) {
                $CI->core_url[$k] = $v;
            }
    }

    protected function debug($info) {
        header("Content-Type: application/json");
        print_r(json_encode($info));

        exit();
    }

}
