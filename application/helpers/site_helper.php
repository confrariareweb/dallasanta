<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Site helper
 *
 * @description
 * @author
 * @package helpers
*/

/**
 * tb2
 *
 * @description
 * @access public
 * @param
 * @return
 */
if ( ! function_exists('tb2')) {
	function tb2($folder, $image, $w, $h,$alt,$class,$data)
	{
		$src = base_url() . "tb2.php?img=upload/" . $folder . "/" . $image . "&x=" . $w . "&y=" . $h;

		return '<img src="' . $src . '" alt="'.$alt.'" title="'.$alt.'" class="'.$class.'" data="'.$data.'" />';
	}
}

/**
 * slug
 *
 * @description
 * @access public
 * @param
 * @return
 */

if ( ! function_exists('slug')) {
	function slug($text) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}
}

if ( ! function_exists('cutText')) {
	function cutText($str,$limit = 300)
	{
		return substr($str, 0, strrpos(substr($str, 0, $limit), ' ')) . '...';
	}
}




/* End of files ite_helper.php */
/* Location: ./applicaton/helpers/site_helper.php */
