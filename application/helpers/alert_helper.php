<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('alert')) {
	function alert($prefixo = null) {
		$CI =& get_instance();

		if ($prefixo)
			$prefixo .= '_';

		$alert = array();
		if ($CI->session->flashdata($prefixo.'error')) {
			$alert['msg'] = $CI->session->flashdata($prefixo.'error');
			$alert['class'] = 'danger';
		} else if ($CI->session->flashdata($prefixo.'success')) {
			$alert['msg'] = $CI->session->flashdata($prefixo.'success');
			$alert['class'] = 'success';
		}

		$html = null;
		if (!empty($alert)) {
	        $html = '
				<div class="active alert alert-modal">
					<button class="close" data-dismiss="alert" aria-label="Close" title="Fechar">
						<span aria-hidden="true">×</span>
					</button>
					<span class="alert-'. $alert['class'] .'">
						'. $alert['msg'] .'
					</span>
				</div>
		    ';
		}

		return $html;
	}
}