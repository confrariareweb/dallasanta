<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('captcha')) {
	function get_captcha($name) {
		$CI =& get_instance();

		$captcha = random_string('alnum', 7);
		$session_captcha = array(
			$name => $captcha
		);
		$CI->session->set_userdata('captcha', $session_captcha);

        return '
	        <div class="captcha">
	            <strong>'. $captcha .'</strong>
	            <input type="text" name="captcha['. $name .']" value="" class="input" required area-required="true" autocomplete="off">
	        </div>
	    ';
	}
}

if (!function_exists('check_captcha')) {
	function check_captcha($name) {
		$CI =& get_instance();

		if (isset($CI->session->userdata['captcha'][$name])) {
	    	$user_captcha = mysql_real_escape_string(trim($_POST['captcha'][$name]));
	        $captcha = $CI->session->userdata['captcha'][$name];

	        unset($_POST['captcha']);
	        $CI->session->unset_userdata('captcha');

	        if (strcmp($captcha, $user_captcha) !== 0) {
	        	$msg = 'O captcha está incorreto.';

	        	if ($CI->input->is_ajax_request()) {
	        		echo json_encode(array(
	        			'success' => FALSE,
	        			'msg' => $msg
	        		));
	        		exit();
	        	} else {
	        		$CI->session->set_flashdata('error', $msg);

		        	$uri = site_url('home');
		        	if (isset($_SERVER['HTTP_REFERER']))
		        		$uri = $_SERVER['HTTP_REFERER'];

		        	redirect($uri);
	            }
	        }

	    }
	}
}