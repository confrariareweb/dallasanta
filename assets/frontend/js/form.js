function message(response, form) {
    if (!response) {
        sweetAlert("Oops...", "Erro no envio da mensagem!", "error");

    } else {
        if (!response.success) {
            sweetAlert("Erro!", response.message, "error");

        } else {
            sweetAlert("Sucesso!", response.message, "success");
        }
    }

    form.trigger("reset");
}

function requestMessage(form_data,form,base_url) {
    sweetAlert("Mensagem sendo enviada, aguarde...");

    $.ajax({
        type: "POST",
        cache: false,
        url: base_url,
        processData: false,
        contentType: false,
        data: form_data,
        dataType: "json",
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);

    }).done(function(response) {
        console.log(base_url);
        console.log(form_data);
        console.log(response);

        message(response,form);
    });
}

function formValidate(form,callback) {
    var serialize_array = form.serializeArray(),
        form_data = null,
        i = null,
        field_length = 0,
        form_elements = form[0].elements,
        form_field = null,
        flag_error = false,
        flag_display_error = false;

    field_length = form_elements.length;

    if (field_length) {
        for (i in form_elements) {
            form_field = $(form_elements[i]);

            if (form_field.attr("data-required") !== undefined) {
                flag_display_error = false;

                if (form_field[0].type == "checkbox") {
                    if (!form_field.prop("checked")) {
                        flag_error = true;
                    }
                }

                if (!form_field.val()) {
                    flag_error = true;
                    flag_display_error = true;

                }

                callback(form_field,flag_display_error);
            }

            if (parseInt(i) + 1 == field_length) {
                break;
            }
        }
    }

    if (flag_error) {
        return false;
    }

    form_data = new FormData(form[0]);

    return form_data;
}
