$(document).ready(function(){
    
    $('.tapamapa').click(function() {

		$('.tapamapa').hide();
	});
						
    $('.seventh a.rank ').on('click', function(){
        $.fancybox.open({
            href: '#cadastrodeclientes',
            autoDimensions: false,
            autoSize:     false,
            width : '400',
            height: 'auto'
        });
    });
    
    
    $('#btn_aluguel').on('click', function(){
        $('#aluguel_busca').show();   
        $('#venda_busca').hide();   
    });

    $('#btn_venda').on('click', function(){
        $('#venda_busca').show();
        $('#aluguel_busca').hide();      
    });


    if($('#busca-tipo-aluguel').is(':checked')){
        $('#aluguel_busca_ot').show();
        $('#venda_busca_ot').hide();
    }
    if($('#busca-tipo-venda').is(':checked')){
        $('#venda_busca_ot').show();
        $('#aluguel_busca_ot').hide();
    }
    $('#btn_aluguel_ot').on('click', function(){
        $('#aluguel_busca_ot').show();
        $('#venda_busca_ot').hide();
    });

    $('#btn_venda_ot').on('click', function(){
        $('#venda_busca_ot').show();
        $('#aluguel_busca_ot').hide();
    });
    $(window).scroll(function() {

        var position = $(window).scrollTop();

        if(position >= 34){
            $('.top_fixo').css({'display':'block'});
        }
        if(position < 34){
            $('.top_fixo').css({'display':'none'});
        }
    });

    $('.icons-set a').click(function(event) {
        var target = this.hash;
        var target_no_hash = target.replace('#', '');
        var menu = '85';

        if (target) {
            event.preventDefault();
            if (document.getElementById(target_no_hash)) {
                $('html, body').animate({
                    scrollTop: $(target).offset().top - menu
                }, 500, function() {
                    document.location.hash = target_no_hash;
                });
            }

            return false;
        }
    });
	
	$('input[name="field[area_util]"], input[name="field[area_total]"]').keyup(function(event){
		var key = (window.event) ? event.keyCode : event.which; 
		
		if((key==8 || key==0))return true;
		
		if(key > 47 || key < 58){
			var v = $(this).val().replace(/[^0-9]/g,'');
			v = v.replace(/([0-9])$/,"$1 m²");
			$(this).val(v);
		}
		else{
			return false;
		}
	});
	
	$('input[name="field[valor_aluguel_atual]"], input[name="field[valor_pretendido]"]').keyup(function(event){
		var key = (window.event) ? event.keyCode : event.which; 
		
		if((key==8 || key==0))return true;
		
		if(key > 47 || key < 58){
			var v = $(this).val().replace(/[^0-9]/g,'');
			$(this).val(formatMoney( v, 0, "R$ ",".",","));
		}
		else{
			return false;
		}
	});
});
function set_cookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
function delete_cookie(cname) {
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

function modal_alert(data, form) {
    var msg, classe;
    var result = JSON.parse(data);

    if (result.success == 1) {
        classe = 'success';

        if (form) {
            form.reset();
            $(form).find('input-error-required').removeClass('input-error-required');
        }
    } else
        classe = 'danger';

    modal_remove();

    $('body').append(
        $('<div class="active alert alert-modal"></div>').append(
            $('<button class="close" data-dismiss="alert" aria-label="Close" title="Fechar"></button>').append(
                $('<span aria-hidden="true">&times;</span>')
            ),
            $('<span class="alert-'+ classe +'"></span>').html(result.msg)
        )
    );

    if (typeof result.redirect != 'undefined') {
        setTimeout(function(){
            document.location.href = result.redirect;
            return false;
        }, 1000);
    }
}
function modal_loading(callback) {
    $('body').append(
        $('<div class="loading-modal aguarde"></div>')
    );
    $('html, body').addClass('overflow-hidden');

    if (typeof callback != 'undefined') {
        setTimeout(function() {
            callback();
        }, 500);
    }
}
function modal_remove() {
    $('.loading-modal, .alert-modal').remove();
    $('html, body').removeClass('overflow-hidden');
}

function carousel_update() {
    var vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var dir, img;

    $('.conceitual-banners').find('.showcase').each(function() {
        dir = $(this).data('banner-dir');

        if (vw <= 767) {
            img = $(this).data('banner-mobile');
        } else {
            img = $(this).data('banner-desktop');
        }

        this.style.backgroundImage = 'url(' + img + ')';
    });
}


// To set it up as a global function:
function formatMoney(number, places, symbol, thousand, decimal) {
	number = number || 0;
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}

$(function() {

        carousel_update();

        $(window).resize(function () {
            carousel_update();
        });


    // Numerais
    if (typeof numeral != 'undefined')
        numeral.language('pt-br');

    // Máscaras
    $('.telefone').mask('(99) 9999-9999?9');

    // Forms
    $('body').on('submit', 'form', function(event) {
        event.preventDefault();
        
            var img = document.createElement("img");
            var goalId = 923539892;
            var randomNum = new Date().getMilliseconds();
            var value = 0;
            var label = "twCECIuz4mUQtLOwuAM";
            var url = encodeURI(location.href);

            var trackUrl = "http://www.googleadservices.com/pagead/conversion/"+goalId+"/?random="+randomNum+"&value="+value+"&label="+label+"&guid=ON&script=0&url="+url;
            img.src = trackUrl;
            document.body.appendChild(img);
            
        var sbm = true;
        $(this).find('input:visible, textarea, select').each(function() {
            if ($(this).attr('required')) {
                if (this.value.trim() == '') {
                    if (this.tagName == 'SELECT') {
                        $(this).parent().addClass('input-error-required');
                    } else if (this.type == 'file') {
                        $(this).parent().prev().addClass('input-error-required');
                    } else {
                        $(this).addClass('input-error-required');
                    }

                    sbm = false;
                }
            }
        });

        if (!sbm) {
            alert('Preencha os campos obrigatórios');
        } else {
            var form = this;
            var btn = $(form).find('button[type=submit]');
                btn.button('loading');

            if ((form.className.indexOf('form-ajax') > -1 || form.action.indexOf('contact') > -1) && form.enctype != 'multipart/form-data') {
                // Faz o submit via ajax
                modal_loading(function() {
                    $.post(form.action, $(form).serialize(), function(data) {
                        //alert(data);
                        modal_alert(data, form);

                        btn.button('reset');
                        $.fancybox.close();
                    });
                });
            } else {
                // Faz o submit via http
                modal_loading(function() {
                    form.submit();
                });
            }
        }
    });

    $('body').on('click', '.rank, .rank2', function(event) {
    
        var _this = this;
        var href = _this.href;

        if (href.indexOf('#modal-cadast') != 39) {

            event.preventDefault();
            if (href.indexOf('area-do-cliente/favorito') > -1) {
                modal_loading(function() {
                    $.post(href, function(data) {
                        modal_alert(data);

                        var ret = JSON.parse(data);
                        if (ret.acao == 'adicionar') {
                            $(_this).addClass('active');
                        } else{
                            $(_this).removeClass('active');
                        }
                    });
                });
            }
            return false;
        }

        
    });

    $('body').on('click', '#btn-reenviar-confirmacao', function(event) {
        event.preventDefault();

        var href = this.href;
        modal_loading(function() {
            $.post(href, function(data) {
                modal_alert(data);
            });
        });

        return false;
    });

    //delete_cookie('infos_bottom_closed');
    $('body').on('click', '#close-infos-bottom', function() {
        set_cookie('infos_bottom_closed', 'true', 7);
        $('.footer-bottom>.row').fadeOut('fast');
    });

    // Anexos
    $('.button-attachment').find('input[type=file]').change(function() {
    	$(this).parent().parent().find('input[type=text]').val(this.value);
    })

    // Captcha
    $('div.captcha').find('.input').attr({ 'maxlength': 7, 'onpaste': 'return false' })

    // Modals
    $('.fancybox').fancybox({
        tpl: {
            closeBtn : '<a title="Fechar" class="fancybox-item fancybox-close" href="javascript:;"></a>',
            next     : '<a title="Próximo" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
            prev     : '<a title="Anterior" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
        }
    });

    // Área do cliente (recuperar senha)
    if (document.getElementById('modal-recuperar-senha') != null) {
        $.fancybox.open({
            href: '#modal-recuperar-senha',
            tpl: {
                closeBtn : false
            },
            helpers: {
                overlay : {
                    closeClick : false
                }
            },
            keys: {
                close: false
            }
        });
    }

    // Input radio estilizado
    $('.radio-input').change(function(){
    	var parent = this.parentNode;

    	if (parent.nodeName == 'LABEL') {
			$(parent).parent().find('label').removeClass('button3');
			$(parent).addClass('button3');
    	}
    });

    // Busca principal
    $('#change-order-view').change(function() {
        var ordem = this.value;
        var controller = 'imoveis';

        if (document.location.href.indexOf('area-do-cliente') > -1)
            controller = 'area-do-cliente';

        modal_loading(function() {
            location.href = site_url + controller + '/ordem/' + ordem;
        });
    });

    // Busca lateral
    /*$('.filtro-bairros .todos-bairros').on('click', 'input[type=checkbox]', function() {
        if (this.checked) {
            $('.filtro-bairros .cidade').find('input').removeAttr('checked');
        }
    });
    $('.filtro-bairros .cidade').on('click', 'input[type=checkbox]', function() {
        if (this.checked) {
            $('.filtro-bairros .todos-bairros').find('input').removeAttr('checked');
        }
    });*/

    $('.filtro-bairros').on('click', '.todos-bairros-cidade', function() {
        var _this = this;
        var inputs = document.querySelectorAll('input.bairro-cidade-' + this.value);

        $(inputs).each(function() {
            if (_this.checked) {
                this.checked = true;
            } else {
                this.checked = false;
            }
        });
    });

    $('#form-optimize-search').on('click', '#fechar-selecao', function(event) {
        var _this = this;
        var bairros = $('.filtro-bairros .bairros');
        var visible = bairros.is(':visible');

        bairros.slideToggle(function(){
            if (visible) {
                $(_this).removeClass('fechar').addClass('abrir');
                $('b', _this).html('Abrir seleção');
            } else{
                $(_this).removeClass('abrir').addClass('fechar');
                $('b', _this).html('Fechar seleção');
            }
        });

        event.preventDefault();
    });

    $('.range-slider-content').each(function() {
        var slider = $(this);
        var target = $($(this).data('target'));

        var step = $(this).data('step');
        var range_min = $(this).data('min');
        var range_max = $(this).data('max');

        var prefix = $(this).data('prefix') || '';
        var sufix = $(this).data('sufix') || '';

        var max_active = $(this).data('max-active');
        var min_active = $(this).data('min-active');

        target.find('.input-min-range').val(min_active);
        target.find('.input-max-range').val(max_active);

        slider.noUiSlider({
            connect: true,
            step: step,
            range: {
                "min": range_min,
                "max": range_max
            },
            format: {
                to: function (value) {
                    return prefix + ' ' + numeral(value).format('0,0') + ' ' + sufix;
                },
                from: function (value) {
                    return value;
                }
            },
            start: [min_active, max_active]
        });

        slider.Link('lower').to(target.find('.min-range'));
        slider.Link('upper').to(target.find('.max-range'));

        slider.on({
            change: function () {
                var self = $(this).val();

                target.find('.input-min-range').val(self[0].match(/\d/g).join(''));
                target.find('.input-max-range').val(self[1].match(/\d/g).join(''));
            }
        });
    });

    $('.download-fotos-imovel').on('click', function(event) {
        event.preventDefault();

        var elem = this;

        modal_loading();

        $.post(elem.href, function(data) {
            location.replace(elem.href);

            setTimeout(function() { // Gamba porque sim
                modal_remove();
            }, 250);
        });
    });

    $('#imoveis-comparar').find('.cod-imovel-hover').hover(function() {
        $('#imovel-compara-'+ this.innerHTML).addClass('hover');
    }, function() {
        $('#imovel-compara-'+ this.innerHTML).removeClass('hover');
    });

    $('#imoveis-comparar').on('click', '.remove-imovel', function() {
        modal_loading();
    });
});


