$(document).ready(function(){

	$('html').click(function() {
	  	$('.select-with-checkbox').find('.checkbox').hide();
	});
	$('.style-select').click(function(event) {
	    event.stopPropagation();
	});

	$('select.select').each(function(){
		var title = $(this).attr('title');

		//if ($('option:selected', this).val() != '')
			title = $('option:selected', this).text();

		$(this)
		.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
		.after('<span class="select">' + title + '</span>')
		.change(function(){
			val = $('option:selected',this).text();
			$(this).next().text(val);
		})
	});

	$('.select-with-checkbox').on('click', '.select', function(event) {
		var select = $(this).parents('.select-with-checkbox');
		var c = select.find('.checkbox');

		$('.select-with-checkbox').not(select).find('.checkbox').hide();

		if (c.is(':visible')) {
			c.hide();
		} else{
			c.show();
		}

		event.preventDefault();
	});

	$('.select-with-checkbox').on('click', 'input[type=checkbox]', function() {
		var select = $(this).parents('.select-with-checkbox');
		var len = select.find('input[type=checkbox]:visible:checked').length;
		var tx;

		if (len == 0) {
			tx = 'Selecione...'
		} else{
			tx = len + ' selecionado';

			if (len > 1)
				tx += 's';
		}

		select.find('.select').html(tx);
	});

});