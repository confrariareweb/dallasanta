jQuery(document).ready(function(){

	/*
	* datatable static
	*/
	if(jQuery('#dyntable1').size()) {	
		/*
		* click button delete row
		*/
		jQuery('#dyntable1 a.btn_trash').click(function(){
			jConfirm('Deseja realmente excluir os itens selecionados?', 'Excluir', function(r) {
				if(r == true) {
					var form = jQuery('#form_delete');
					jQuery('#form_delete input').remove();
					
					jQuery('#dyntable1 input[name="delete_id[]"]:checked').each(function(){
						var value = jQuery(this).val();
						var id = form.append("<input name='id[]' type='hidden' value='"+value+"'/>");
					});
					form.submit();
				}
			});
			return false;
		});
		
		/*
		* click on row go to link edit
		*/
		jQuery('#dyntable1 tbody tr td').live('click',function(){
			var url_base = jQuery('#dyntable1 tbody').attr('data-rel');	
			
			if(jQuery(this).index() > 0 && !jQuery(this).hasClass('noclick') && !jQuery(this).find('select').size())
				window.location = url_base+"/"+jQuery(this).parents("tr").find('input').val();
		});
		
		
		dateTable = jQuery('#dyntable1').dataTable({
			"oLanguage": {"sUrl": "assets/backend/js/plugins/dataTables.pt_BR.txt"},
			"sPaginationType": "full_numbers",
			"aaSortingFixed": [[0,'asc']],
			"fnDrawCallback": function(oSettings) { },
	        "fnInitComplete": function(oSettings, json) { 
                for ( var i=0 ; i<oSettings.aoPreSearchCols.length ; i++ ){
                    if(oSettings.aoPreSearchCols[i].sSearch.length>0){
                        jQuery('.stdtable .unique_search input').eq(i).val(oSettings.aoPreSearchCols[i].sSearch);
                    }
                }
	        },
		    "bStateSave": true
		});
	}
	
	
	/*
	* datatable dynamic
	*/
	if(jQuery('#dyntable2').size()) {	
		/*
		* click button delete row
		*/
		jQuery('#dyntable2 a.btn_trash').click(function(){
			jConfirm('Deseja realmente excluir os itens selecionados?', 'Excluir', function(r) {
				if(r == true) {
					var form = jQuery('#form_delete');
					jQuery('#form_delete input').remove();
					
					jQuery('#dyntable2 input[name="delete_id[]"]:checked').each(function(){
						var value = jQuery(this).val();
						var id = form.append("<input name='id[]' type='hidden' value='"+value+"'/>");
					});
					form.submit();
				}
			});
			return false;
		});
		
		/*
		* click on row go to link edit
		*/			
		jQuery('#dyntable2 tbody tr td').live('click',function(){
			var url_base = jQuery('#dyntable2 tbody').attr('data-rel');			
			
			if(jQuery(this).index() > 0 && !jQuery(this).hasClass('noclick') && !jQuery(this).find('select').size())
				window.location = url_base+"/"+jQuery(this).parents("tr").find('input').val();
		});
		
		dateTable = jQuery('#dyntable2').dataTable({
			"oLanguage": {"sUrl": "assets/backend/js/plugins/dataTables.pt_BR.txt"},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function(oSettings) { jQuery('#dyntable2 tbody tr input[type="checkbox"]').uniform(); },
	        "fnInitComplete": function(oSettings, json) { 
                for ( var i=0 ; i<oSettings.aoPreSearchCols.length ; i++ ){
                    if(oSettings.aoPreSearchCols[i].sSearch.length>0){
                        jQuery('.stdtable .unique_search input').eq(i).val(oSettings.aoPreSearchCols[i].sSearch);
                    }
                }
	        },
		    "bStateSave": true,
		    "bProcessing": true,
	        "bServerSide": true,
	        "sAjaxSource": jQuery('#dyntable2').attr('data-source')
		});
	}

	/*
	* individual search
	*/
	jQuery('.stdtable .unique_search input').keyup( function () {
        dateTable.fnFilter( this.value, jQuery('.stdtable .unique_search input').index(this) );
    });
});

var dateTable = '';